ad_page_contract {

	Returns the Token and API Key for the current user	

	@author malte.sussdorff@cognovis.de based upon work from 
    @author frank.bergmann@project-open.com
} {
	{ user_id ""}
}

set current_user_id  [auth::require_login]
if {$user_id eq ""} {
	set user_id $current_user_id
} else {
	if {$user_id ne $current_user_id} {
		if {![ acs_user::site_wide_admin_p -user_id $current_user_id ]} {
			ad_return_error "Not authorized" "You are not authorized to see another users login details"
		}
	}
}

set auto_login [im_generate_auto_login -user_id $user_id]

set api_key [base64::encode "${user_id}:$auto_login"]

set username ""
db_0or1row user_info "
	select	username
	from	cc_users
	where	user_id = :user_id
"




