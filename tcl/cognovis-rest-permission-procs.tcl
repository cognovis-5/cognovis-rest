ad_library {
    REST Web Service Component Library Permission Functions
	@author malte.sussdorff@cognovis.de

}

namespace eval cog_rest::permission {
    ad_proc -public user {current_user_id user_id view_var read_var write_var admin_var} {
        Permissions on user. 

        The user can always see himself as well as key_accounter
    } {
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin

        im_user_permissions $current_user_id $user_id view read write admin
		
        # User should always be able to see their key_accounter
        set key_account_id [cog::company::key_account_id -user_id $current_user_id]
        if {$key_account_id eq $user_id} {
            set read 1
            set view 1
        }

        # User should always be able to see their PMs
        if {[im_user_is_pm_p $user_id]} {
            set read 1
            set view 1
        }
    }

    ad_proc -public im_project {user_id project_id view_var read_var write_var admin_var} {
        Enhance im_project_permissions to allow contact persons to see their projects
    } { 
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin
        im_project_permissions $user_id $project_id view read write admin
        if {$read eq 0} {
            set company_contact_p [db_string company_contact_id "select 1 from acs_rels r, im_projects p 
            where r.object_id_one = p.company_id and r.rel_type = 'im_company_employee_rel' and p.project_id = :project_id
            and object_id_two = :user_id limit 1" -default 0]
            if {$company_contact_p} {
                set read 1
                set view 1
            }
        }
    }

    ad_proc -public im_trans_task {user_id task_id view_var read_var write_var admin_var} {
        Enhance Trans tasks permissions to allow users with access to the project to see the trans task
    } { 
        set project_id [db_string project "select project_id from im_trans_tasks where task_id = :task_id" -default ""]
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin
        im_project_permissions $user_id $project_id view read write admin
    }

    ad_proc -public im_freelance_package {user_id freelance_package_id view_var read_var write_var admin_var} {
        Derive package permissions from project if not read
    } { 
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin

        if {![permission::permission_p -no_login -party_id $user_id -object_id $freelance_package_id -privilege "read"]} {
            set project_id [db_string project "select project_id from im_freelance_packages where freelance_package_id = :freelance_package_id" -default ""]
            cog_rest::permission::im_project $user_id $project_id view read write admin
        } else {
            set read [permission::permission_p -no_login -party_id $user_id -object_id $freelance_package_id -privilege "read"]
            set view [permission::permission_p -no_login -party_id $user_id -object_id $freelance_package_id -privilege "view"]
            set write [permission::permission_p -no_login -party_id $user_id -object_id $freelance_package_id -privilege "write"]
            set admin [permission::permission_p -no_login -party_id $user_id -object_id $freelance_package_id -privilege "admin"]                        
        }
    }

    ad_proc -public im_freelance_assignment {user_id assignment_id view_var read_var write_var admin_var} {
        Derive package permissions from freelance_package
    } { 
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin

        if {![permission::permission_p -no_login -party_id $user_id -object_id $assignment_id -privilege "read"]} {
            set freelance_package_id [db_string project "select freelance_package_id from im_freelance_assignments 
                where assignment_id = :assignment_id" -default ""]
            cog_rest::permission::im_freelance_package $user_id $freelance_package_id view read write admin
        } else {
            set read [permission::permission_p -no_login -party_id $user_id -object_id $assignment_id -privilege "read"]
            set view [permission::permission_p -no_login -party_id $user_id -object_id $assignment_id -privilege "view"]
            set write [permission::permission_p -no_login -party_id $user_id -object_id $assignment_id -privilege "write"]
            set admin [permission::permission_p -no_login -party_id $user_id -object_id $assignment_id -privilege "admin"]                        
        }
    }

    ad_proc -public content_folder {user_id folder_id view_var read_var write_var admin_var} {
        Grant permissions on content folders based of the context of the folder
    } { 
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin

        set f_view [permission::permission_p -no_login -party_id $user_id -object_id $folder_id -privilege "view"]
        set f_read [permission::permission_p -no_login -party_id $user_id -object_id $folder_id -privilege "read"]
        set f_write [permission::permission_p -no_login -party_id $user_id -object_id $folder_id -privilege "write"]
        set f_admin [permission::permission_p -no_login -party_id $user_id -object_id $folder_id -privilege "admin"]

        set project_id [intranet_fs::get_project_from_folder_id -folder_id $folder_id]

        # We might have to rewrite that if the company employees should not all have access to the folder
        # Then we might have to make companies a party as well. which would .... suck?
        cog_rest::permission::im_project $user_id $project_id p_view p_read p_write p_admin
        if {$p_read eq 0} {
            set company_contact_p [db_string company_contact_id "select 1 from acs_rels r, im_projects p 
            where r.object_id_one = p.company_id and r.rel_type = 'im_company_employee_rel' and p.project_id = :project_id
            and object_id_two = :user_id limit 1" -default 0]
            if {$company_contact_p} {
                set p_read 1
                set p_view 1
                set p_write 1
            }
        }
        set view [expr {$f_view || $p_view || $f_write || $p_write}]
        set read [expr {$f_read || $p_read || $f_write || $p_write}]
        set write [expr {$f_write || $p_write}]
        set admin [expr {$f_admin || $p_admin || [im_is_user_site_wide_or_intranet_admin $user_id]}]
    }

    ad_proc -public im_cost {user_id cost_id view_var read_var write_var admin_var} {
        Fill the "by-reference" variables read, write and admin
        with the permissions of $user_id on $cost_id.

        Cost permissions depend on the rights of the underlying company
        and on Cost Center permissions.
    } {
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin

        set user_is_freelance_p [im_user_is_freelance_p $user_id]
        set user_is_inco_customer_p [im_user_is_inco_customer_p $user_id]
        set user_is_customer_p [im_user_is_customer_p $user_id]

        # -----------------------------------------------------
        # Get Cost information
        set customer_id 0
        set provider_id 0
        set cost_center_id 0
        set cost_type_id 0
        db_0or1row get_companies "
            select c.customer_id,                
                c.provider_id,
                (select o.object_type from acs_objects o where o.object_id = c.provider_id) as provider_otype,
                c.cost_center_id,
                c.cost_type_id
            from	im_costs c
            where	c.cost_id = :cost_id
        "

        # -----------------------------------------------------
        # Cost Center permissions - check if the user has read permissions
        # for this particular cost center
        if {$cost_center_id ne ""} {
            im_cost_center_permissions $user_id $cost_center_id cc_view cc_read cc_write cc_admin
        } else {
            set cc_read 0
            set cc_write 0
        }

        set can_read [expr [im_permission $user_id view_costs] || [im_permission $user_id view_invoices]]
        set can_write [expr [im_permission $user_id add_costs] || [im_permission $user_id add_invoices]]

        # AND-connection with add/view - costs/invoices
        if {!$can_read} { set cc_read 0 }
        if {!$can_write} { set cc_write 0 }

        # Set the other two variables
        set cc_admin $cc_write
        set cc_view $cc_read

        # -----------------------------------------------------
        # Customers get the right to see _their_ invoices
        set cust_view 0
        set cust_read 0
        set cust_write 0
        set cust_admin 0
        set incust_view 0
        set incust_read 0
        set incust_write 0
        set incust_admin 0

        if {$user_is_inco_customer_p && $customer_id && $customer_id != [im_company_internal]} {
            im_company_permissions $user_id $customer_id incust_view incust_read incust_write incust_admin
        }
        if {$user_is_customer_p && $customer_id && $customer_id != [im_company_internal]} {
            im_company_permissions $user_id $customer_id cust_view cust_read cust_write cust_admin
        }

        # -----------------------------------------------------
        # Providers get the right to see _their_ invoices
        # This leads to the fact that FreelanceManagers (the guys
        # who can convert themselves into freelancers) can also
        # see the freelancer's permissions. Is this desired?
        # I guess yes, even if they don't usually have the permission
        # to see finance.
        set prov_view 0
        set prov_read 0
        set prov_write 0
        set prov_admin 0

        switch $provider_otype {
            im_company {
                if {$user_is_freelance_p && $provider_id && $provider_id != [im_company_internal]} {
                    im_company_permissions $user_id $provider_id prov_view prov_read prov_write prov_admin
                }
            }
            user {
                # This is an expense or an expense bundle, probably.
                if {$provider_id eq $user_id} {
                set prov_view 1
                set prov_read 1
                }
            }
        }

        # -----------------------------------------------------
        # Set the permission as the OR-conjunction of provider and customer
        set view [expr {$incust_view || $cust_view || $prov_view || $cc_view}]
        set read [expr {$incust_read || $cust_read || $prov_read || $cc_read}]
        set write [expr {$incust_write || $cust_write || $prov_write || $cc_write || $can_write}]
        set admin [expr {$incust_admin || $cust_admin || $prov_admin || $cc_admin || [im_is_user_site_wide_or_intranet_admin $user_id]}]

        #---------------------------------------------------------------
        # Company contact should be able to see their own invoices
        #---------------------------------------------------------------
        if {$view eq 0} {
            set view [db_string check_invoice "select 1 from im_invoices where invoice_id = :cost_id and company_contact_id = :user_id" -default 0]
        }

        if {$read eq 0} {
            set read $view
        }

        # Limit rights of all users to view & read if they dont
        # have the expressive permission to "add_costs or add_invoices".
        # if {!$can_write} {
        #    set write 0
        #    set admin 0
        # }
    }

    ad_proc -public im_user_absence {user_id absence_id view_var read_var write_var admin_var} {
        Permissions on user absences
    } { 
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin

        im_user_absence_permissions $user_id $absence_id view read write admin
    }

    ad_proc -public im_company {user_id company_id view_var read_var write_var admin_var} {
        Returns the permissions based of im_company_permissions, but grants specific ones if 
        they were manually granted on the company.

        Last but not least allow primary_contact_id and accounting_contact_id to edit company data
    } {
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin

        im_company_permissions $user_id $company_id view read write admin

        if {!$read} {
            set read [permission::permission_p -no_login -party_id $user_id -object_id $company_id -privilege "read"]
        }
        if {!$view} {
            set view [permission::permission_p -no_login -party_id $user_id -object_id $company_id -privilege "view"]
        }
        if {!$write} {
            set write [permission::permission_p -no_login -party_id $user_id -object_id $company_id -privilege "write"]
            if {!$write} {
                # If we allow users to edit themselves, allow them to edit the company if they are primary or accounting contact
                set user_can_edit_himself_p [parameter::get_from_package_key -package_key "intranet-core" -parameter UserCanEditHimselfP -default "1"]
                if {$user_can_edit_himself_p} {
                    set write [db_string primary_or_accounting_contact_p "select 1 from im_companies where company_id = :company_id and (accounting_contact_id = :user_id or primary_contact_id = :user_id)" -default 0]
                }
            }
        }
        if {!$admin} {
            set admin [permission::permission_p -no_login -party_id $user_id -object_id $company_id -privilege "admin"]                        
        }
    }

    ad_proc -public im_report {user_id report_id view_var read_var write_var admin_var} {
        Permission for reports are based of the menu_id
    } {
        upvar $view_var view
        upvar $read_var read
        upvar $write_var write
        upvar $admin_var admin
        set report_menu_id [db_string report_menu "select report_menu_id from im_reports where report_id = :report_id" -default ""]
        set read [permission::permission_p -no_login -party_id $user_id -object_id $report_menu_id -privilege "read"]
        set view [permission::permission_p -no_login -party_id $user_id -object_id $report_menu_id -privilege "view"]
        set write [permission::permission_p -no_login -party_id $user_id -object_id $report_menu_id -privilege "write"]
        set admin [permission::permission_p -no_login -party_id $user_id -object_id $report_menu_id -privilege "admin"]
        
    }
}

ad_proc -public cog_rest::permission_p {
    -object_id:required
    -user_id
    {-privilege "read"}
} {
    Checks permissions on an object based of cog_rest permission procs

    @param object_id object we want to check permission for
    @param user_id user for  whom we check permission
    @param privilege what permission are we looking for

    @return 1 if the user has permission, 0 otherwise
} {
    set object_type [acs_object_type $object_id]
    set permisison_p 0

    if {![exists_and_not_null user_id]} {
        set user_id [auth::get_user_id]
    }

    set perm_proc [cog_rest::helper::object_permission_proc -object_type $object_type]
    if {$perm_proc ne ""} {
        $perm_proc $user_id $object_id view read write admin
        set permission_p [set $privilege]    
    } else {
        set permission_p [permission::permission_p -no_login -party_id $user_id -object_id $object_id -privilege $privilege]
    }

    return $permission_p
}