ad_library {
    REST Procedures for Pricing information
    @author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
    ad_proc price {} {
        @return price_id integer Price identification
        @return price_type category "Intranet Price Type" Type of price
        @return price_status category "Intranet Price Status" Status of the price
        @return material json_object material Material for which this price is valid
        @return company object im_company Company for which this price is valid
        @return complexity_type category "Intranet Price Complexity" Complexity of the work
        @return valid_from date Since when is this price valid
        @return valid_through date How long is it valid (defaults to nothing = unlimited)
        @return currency string ISO for the currency
        @return price number Actual price per Unit
        @return min_price number Minimum price charged in case units * price is lower
        @return note string Note associated with this price
    } -

    ad_proc price_body {} {
        @param price_type_id category "Intranet Price Type" Type of price
        @param price_status_id category "Intranet Price Status" Status of the price
        @param material_id object im_material::* Material for which this price is valid. STRONGLY ENCOURAGE TO USE (will overwrite other material_ parameters)
        @param company_id object im_company::write Company for which this price is valid
        @param complexity_type_id category "Intranet Price Complexity" Complexity of the work
        @param valid_from date Since when is this price valid
        @param valid_through date How long is it valid (defaults to nothing = unlimited)
        @param currency string ISO for the currency
        @param price number Actual price per Unit
        @param min_price number Minimum price charged in case units * price is lower
        @param note string Note associated with this price
        @param material_uom_id category "Intranet UoM" Unit of Measure for the material
        @param material_source_language_id category "Intranet Translation Language" which is the source language for this material
        @param material_target_language_id category "Intranet Translation Language" target language (if any)
        @param material_subject_area_id category "Intranet Translation Subject Area" Subject area for the translation
        @param material_task_type_id category "Intranet Project Type" what type of action do we make for this material
        @param material_file_type_id category "Intranet Translation File Type" File type we work on
    } -

    ad_proc material {} {
        @return material named_id Material to return
        @return material_nr string Material Nr.
        @return material_type category "Intranet Material Type" Type of material
        @return material_status category "Intranet Material Status" Status of the material (e.g. can we use it)
        @return description string Description of the material
        @return material_uom category "Intranet UoM" Unit of Measure for the material
        @return material_billable_p boolean Is the material billable?
        @return source_language category "Intranet Translation Language" which is the source language for this material
        @return target_language category "Intranet Translation Language" target language (if any)
        @return subject_area category "Intranet Translation Subject Area" Subject area for the translation
        @return task_type category "" what type of action do we make for this material
        @return file_type category "Intranet Translation File Type" File type we work on
    } - 

    ad_proc material_body {} {
        @param material_name string Name of the material
        @param material_nr string Material Nr.
        @param material_type_id category "Intranet Material Type" Type of material
        @param material_status_id category "Intranet Material Status" Status of the material (e.g. can we use it)
        @param description string Description of the material
        @param material_uom_id category "Intranet UoM" Unit of Measure for the material
        @param material_billable_p boolean Is the material billable?
        @param source_language_id category "Intranet Translation Language" which is the source language for this material
        @param target_language_id category "Intranet Translation Language" target language (if any)
        @param subject_area_id category "Intranet Translation Subject Area" Subject area for the translation
        @param task_type_id integer what type of action do we make for this material
        @param file_type_id category "Intranet Translation File Type" File type we work on
    } - 
}

ad_proc -public cog_rest::get::price {
    -price_id
    -company_id
    -material_id
    -price_status_id
    {-valid_from ""}
    {-valid_through ""}
    -rest_user_id
} {
    Returns an array of prices for a company (or a specific price)

    @param price_id object im_price::read Identifier for the price
    @param company_id object im_company::read Company for which we want to retrieve prices
    @param material_id object im_material::read Material for which we want to retrieve prices
    @param valid_from date display only prices that are valid_from...
    @param valid_through date display only dates that are valid_through...
    @param price_status_id category "Intranet Price Status" Only search for prices of this status (ypically set to active)

    @return prices json_array price Array of prices
} {
    set prices [list]
    set where_clause_list [cog_rest::parse::where_clauses -ignore_filters_list [list valid_from valid_through]]

    if {$valid_from ne ""} {
         lappend where_clause_list "valid_from::date >= :valid_from "
    }

    if {$valid_through ne ""} {
         lappend where_clause_list "valid_through::date <= :valid_through "
    }

    if {[llength $where_clause_list] eq 0} {
        set company_id [cog::user::main_company_id -user_id $rest_user_id]
        if {$company_id eq ""} {
            cog_rest::error -http_status 400 -message "Could not limit the price information"
        }
        lappend where_clause_list "company_id = :company_id"
    }

    lappend where_clause_list "p.material_id = m.material_id"

    db_foreach price "select 
        price_id, price_type_id, price_status_id, p.material_id,
        company_id, complexity_type_id, valid_from::date, valid_through::date,
        currency, price, min_price, note,
        material_nr, material_type_id, material_status_id, description,
        material_uom_id, material_billable_p, source_language_id, target_language_id,
        subject_area_id, task_type_id, file_type_id
    from im_prices p, im_materials m
    where [join $where_clause_list " and "]" {
        set material [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::material -material_id $material_id -rest_user_id $rest_user_id]]
        lappend prices [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::price {
    {-price_type_id ""}
    {-price_status_id ""}
    {-material_id ""}
    -company_id:required
    {-complexity_type_id ""}
    {-valid_from ""}
    {-valid_through ""}
    {-currency ""}
    -price:required
    {-min_price ""}
    {-note ""}
    {-material_uom_id ""}
    {-material_source_language_id ""}
    {-material_target_language_id ""}
    {-material_subject_area_id ""}
    {-material_task_type_id ""}
    {-material_file_type_id ""}
    -rest_user_id:required
} {
    Creates a new price entry for a company. Will return existing price if price already exists.

    @param price_body request_body Price information (with material extension)

    @return price json_object price Price we just created
} {
    if {$material_id eq ""} {
        if {$material_uom_id} {
            cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_miss_mat_uom]"
        }
        set material_id [cog::material::get_material_id -material_uom_id $material_uom_id -task_type_id $material_task_type_id \
            -file_type_id $material_file_type_id -subject_area_id $material_subject_area_id \
            -source_language_id $material_source_language_id -target_language_id $material_target_language_id]
    }
    if {$material_id eq ""} {
        set material_id [cog::material::create -material_uom_id $material_uom_id -task_type_id $material_task_type_id \
            -file_type_id $material_file_type_id -subject_area_id $material_subject_area_id \
            -source_language_id $material_source_language_id -target_language_id $material_target_language_id \
            -description "Auto generated for company [im_name_from_id $company_id]."]
    }
    set price_id [cog::price::create -company_id $company_id -material_id $material_id \
        -price $price -min_price $min_price -currency $currency -user_id $rest_user_id \
        -price_type_id $price_type_id -price_status_id $price_status_id -valid_from $valid_from \
        -valid_through $valid_through -note $note  -complexity_type_id $complexity_type_id]

    set price [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::price -price_id $price_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]   
}

ad_proc -public cog_rest::put::price {
    -price_id:required
    {-price_type_id ""}
    {-price_status_id ""}
    {-material_id ""}
    -company_id:required
    {-complexity_type_id ""}
    {-valid_from ""}
    {-valid_through ""}
    {-currency ""}
    -price:required
    {-min_price ""}
    {-note ""}
    {-material_uom_id ""}
    {-material_source_language_id ""}
    {-material_target_language_id ""}
    {-material_subject_area_id ""}
    {-material_task_type_id ""}
    {-material_file_type_id ""}
    -rest_user_id:required
} {
    Creates a new price entry for a company. Will default to update if price already exists

    @param price_id object im_price::write Price which we want to update
    @param price_body request_body Price information (with material extension)

    @return price json_object price Price we just created
} {
    set price_id [cog::price::update -price_id $price_id -material_id $material_id \
        -price $price -min_price $min_price -currency $currency -price_status_id $price_status_id \
        -valid_through $valid_through -note $note  -complexity_type_id $complexity_type_id]
        
    set price [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::price -price_id $price_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]   
}

ad_proc -public cog_rest::delete::price {
    -price_id:required
    -rest_user_id:required
    {-nuke_p "0"}
} {
    Nuke or inactivate a price

    @param price_id object im_price::admin Price we want to delete
    @param nuke_p boolean define if we want to nuke instead of inactivate the price. Nuking only for admins.

    @return price json_object price Price we just deleted 
} {
    
    if {$nuke_p} {
        if {[acs_user::site_wide_admin_p -user_id $rest_user_id]} {
            cog::price::nuke -user_id $rest_user_id -price_id $price_id
            set price ""
        } else {
            cog_rest::error -http_status 401 -message "[_ cognovis-rest.err_perm_nuke_price]"
        }
    } else {
        db_dml update_old_price "update im_prices set price_status_id = [cog::price::status::inactive], valid_through = now() where price_id = :price_id"
        set price [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::price -price_id $price_id -rest_user_id $rest_user_id]]
    }
    
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::material {
    -material_type_id
    -material_id
    { -material_name_part ""}
    -source_language_id
    -target_language_id
    -subject_area_id
    -task_type_id
    -material_uom_id
    -material_status_id
    -file_type_id
    {-material_billable_p "t"}
    -rest_user_id:required
} {
    Handler for GET rest calls which returns materials for given type_id (multiple comma seperated ids possible)

    @param material_type_id category "Intranet Material Type" IDs of required material type for which to return material information 
    @param material_id object im_material::* Material_id of the material we want to retrieve
    @param material_name_part string Part of the material name we look for
    @param source_language_id category "Intranet Translation Language" Source Language for the project. Typically the language the source material is in
    @param target_language_id category "Intranet Translation Language" Language into which this task is translated to. Leaving it empty will use the projects target languages
    @param subject_area_id category "Intranet Translation Subject Area" Subject Area of the project (determines pricing among other things)
    @param task_type_id integer category of the task type we look for
    @param material_uom_id category "Intranet UoM" Units of measure of this material
    @param material_status_id category "Intranet Material Status" Status of the material
    @param file_type_id category "Intranet Translation File Type" File type we work on
    @param material_billable_p boolean Is the material billable 

    @return materials json_array material Array of material objects of the type
} {
    set materials [list]
    set where_clause_list [cog_rest::parse::where_clauses -ignore_filters_list [list material_name_part]]

    if {$material_name_part ne ""} {
        lappend where_clause_list "lower(material_name) like '%${material_name_part}%'"
    }

    if {[llength $where_clause_list] eq 0} {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_miss_filter]"
    }

    set requested_materials_sql "select * from im_materials where [join $where_clause_list " and "]"

    # Set Translation materials
    foreach trans_material_attribute [list file_type_id source_language_id target_language_id task_type_id subject_area_id] {
        if {![info exists $trans_material_attribute]} {
            set $trans_material_attribute ""
        }
    }

    db_foreach get_materials $requested_materials_sql {
        lappend materials [cog_rest::json_object]
    }
    
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::material {
    { -material_type_id ""}
    { -source_language_id ""}
    { -target_language_id ""}
    { -subject_area_id ""}
    { -task_type_id ""} 
    { -material_name ""}
    { -material_nr ""}
    -material_uom_id:required
    { -material_status_id ""}
    { -file_type_id ""}
    {-material_billable_p "t"}
    {-description ""}
    -rest_user_id:required
} {
    Creates a new material so new pricing can be added.

    @param material_body request_body Material Information

    @return material json_object material Array of material objects of the type
} {
    set permission_p [im_permission $rest_user_id "add_materials"]
    if {!$permission_p} {
        cog_rest::error -http_status 401 -message "[_ cognovis-rest.err_perm_add_materials]"
    }

    set material_id [cog::material::create -material_uom_id $material_uom_id \
        -material_name $material_name -material_nr $material_nr \
        -material_type_id $material_type_id -material_status_id $material_status_id \
        -description $description -material_billable_p $material_billable_p \
        -source_language_id $source_language_id -target_language_id $target_language_id \
        -subject_area_id $subject_area_id -task_type_id $task_type_id \
        -file_type_id $file_type_id]

    set material [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::material -material_id $material_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::price_list {
    -company_id:required
    -rest_user_id:required
} {
    Return a file with the price list of a company as CSV

    @param company_id object im_company::read Company whos pricelist we want to download
} {

    set outputheaders [ns_conn outputheaders]
    set csv_path [cog::price::export_price_list -company_id $company_id]
    set csv_filename "[file rootname $csv_path].csv"
    set xls_path [intranet_oo::convert_to -oo_file $csv_filename -convert_to "xlsx"]
    
    file delete csv_path
    
    set company_path [db_string company_path "select company_path from im_companies where company_id = :company_id"] 
    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=${company_path}.xlsx"
    ns_returnfile 200 application/xlsx $xls_path
}

ad_proc -public cog_rest::post::price_list_from_file {
    -company_id:required
    -path:required
    -filename:required
    -rest_user_id:required
} {
    Upload a file to a company using mass import

    @param company_id object im_company::write Company whos pricelist we Upload
    @param path string location of the file on the harddisk which we import the CSV from
    @param filename string Name of the file (original filename) - used to identify XLS

    @return prices json_array price Array of prices

} {
    set extension [file extension $filename]
    switch $extension {
        ".xls" - ".xlsx" {
            # Konvert the XLS to CSV for impport
            set xls_filename "[file rootname $path]$extension"
            file rename $path $xls_filename
            set csv_path [intranet_oo::convert_to -oo_file $xls_filename -convert_to "csv"]
            file delete $xls_filename
        }
        ".csv" {
            set csv_path $path
        }
        default {
            cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_invalid_file_type_upload]"
        }
    }
    set errors [cog::price::import_price_list -csv_path $csv_path -company_id $company_id -user_id $rest_user_id]
    if {$errors ne ""} {
        cog_rest::error -http_status 400 -message "$errors"
    }

    set prices [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::price -company_id $company_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]   
}