ad_library {
    REST Procedures for Relationships
    @author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
    ad_proc -public relationship {} {
        @return object_one json_object cognovis_object primary object in the relationship
        @return object_two json_object cognovis_object secondary object in the relationship
        @return rel_type string type of the relationship
        @return rel named_id relationship_id used as an identifier
    } - 

    ad_proc -public relationship_body {} {
        @param object_id_one integer object_id of the primary object in the relationship
        @param object_id_two integer object_id of the secondary object in the relationship
        @param rel_type string type of the relationship
    } - 
}

ad_proc -public cog_rest::get::relationships {
    {-object_id_one ""}
    {-object_id_two ""}
    {-rel_type ""}
    {-rel_id ""}
    -rest_user_id:required
} {
    Return a list of relationships. If no filter provided will get all the relationships of the current user

    @param object_id_one integer object_id of the primary object in the relationship
    @param object_id_two integer object_id of the secondary object in the relationship
    @param rel_type string Type of the relationship
    @param rel_id integer In case we look for a specific relationship

    @return relationships json_array relationship
} {
    set relationships [list]
    set where_clause_list [list]

    foreach var [list object_id_one object_id_two rel_id rel_type] {
        if {[set $var] ne ""} {
            lappend where_clause_list "$var = :$var"
        }
    }

    if {$where_clause_list eq ""} {
        lappend where_clause_list "(object_id_one = :rest_user_id or object_id_two = :rest_user_id)"
    }
    set relationship_sql "select object_id_one, object_id_two, rel_type, rel_id
        from acs_rels
        where [join $where_clause_list " and "]"
        
    db_foreach relationship $relationship_sql {
        set object_one [util_memoize [list cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $object_id_one] 120]
        set object_two [util_memoize [list cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $object_id_two] 120]
        lappend relationships [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::relationship {
    -object_id_one:required
    -object_id_two:required
    -rel_type:required
    -rest_user_id:required   
} {
    Adds a new relationship

    @param relationship_body request_body Infomration about the relationship we want to add.

    @return relationship json_object relationship

} {
    set rel_id [db_string rel_exists { 
        select rel_id
        from   acs_rels 
        where  rel_type = :rel_type 
        and    object_id_one = :object_id_one
        and    object_id_two = :object_id_two
    } -default ""]
    if {$rel_id eq ""} {
        set rel_id [db_string rel_id "select acs_rel__new(null,:rel_type,:object_id_one,:object_id_two,:object_id_one,:rest_user_id,'null')"]
    }
    set relationship [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::relationships -rel_id $rel_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::relationship {
    -rel_id:required
    -rest_user_id:required
} {
    Remove a relationship

    @param rel_id integer relationship we want to remove

    @return errors json_array error Array of errors found
} {
    set errors [list]
    set valid_p [db_string delete "select 1 from acs_rels where rel_id = :rel_id" -default 0]
    if {!$valid_p} {
        set parameter "Relationship"
        set object_id $rel_id
        set err_msg "You have provided an invalid rel_id"
        lappend errors [cog_rest::json_object]
    } else {
        db_1row delete "select acs_rel__delete(:rel_id)"
    }

    return [cog_rest::json_response]
}
