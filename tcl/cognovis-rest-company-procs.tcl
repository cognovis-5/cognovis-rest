ad_library {
    REST Procedures for companies. Includes all special ones
    @author malte.sussdorff@cognovis.de
    @author frank.bergmann@project-open.com
}

namespace eval cog_rest::json_object {
    ad_proc company {} {
        @return company object im_company Company information (name and id)
        @return company_path string Internal company_path for identifiying the company
        @return address_line1 string First address line
        @return address_line2 string Second address line (e.g. PO box or appartment)
        @return address_postal_code string Postal / ZIP code
        @return address_city string City the company resides in 
        @return address_country_full string Internationalized name of the country
        @return primary_contact named_id Primary contact for the company
        @return accounting_contact named_id Accounting contact for the company
        @return company_status category "Intranet Company Status" Status of company (Intranet Company Status)
        @return company_type category "Intranet Company Type" Type of company (Intranet Company Type)
        @return vat_type category "Intranet VAT Type" VAT Classification of the company
        @return vat_number string company vat number
        @return phone string phone number of the main office
        @return url string Website for the company
        @return default_referral_source category "Referral source" Where did this company come from
        @return payment_term category "Intranet Payment Term" Terms for the payment (aka how fast has the payment to be made)
        @return default_invoice_template category Template we use for new invoices for this customer
        @return default_quote_template category "Intranet Cost Template" Template we use for new quotes for this customer
        @return last_contact date When did we last contact the company
        @return logo_url string URL to the portrait of the company
        @return main_office object im_office Main office information

    } - 

    ad_proc company_body {} {
        @param company_name string Name of the company
        @param comapny_path string Internal name for the company. Useful if you have multiple companies with the same name. Needs to be unique!
        @param company_type_id category "Intranet Company Type" Type of company (Intranet Company Type)
        @param company_status_id category "Intranet Company Status" Status of company (Intranet Company Status)
        @param vat_type_id category "Intranet VAT Type" VAT Classification of the company
        @param vat_number string company vat number
        @param primary_contact_id integer Primary contact (ID) for the company
        @param accounting_contact_id integer Accounting contact (ID) for the company
        @param address_country_code string Country code of company main_office_id
        @param address_line1 string Street (address_line_1) of company main office
        @param address_line2 string Second address line (e.g. PO box or appartment)
        @param address_city string city of company main office
        @param address_postal_code string zip code / postal code of company main office
        @param phone string phone number of the main office
        @param url string Website for the company
        @param referral_source_id category "Referral source" Where did this company come from
        @param payment_term_id category "Intranet Payment Term" Terms for the payment (aka how fast has the payment to be made)
        @param default_invoice_template_id category "Intranet Cost Template" Template we use for new invoices for this customer
        @param default_quote_template_id category "Intranet Cost Template" Template we use for new quotes for this customer
        @param default_bill_template_id category "Intranet Cost Template" Template we use for new provider bills
        @param default_po_template_id category "Intranet Cost Template" Template we use for new purchase orders for this provider
    } -

    ad_proc company_contact {} {
        @return company named_id Company the contact works for
        @return contact named_id Contact we requested (with ID and name - first_names + last_name typically)
        @return key_account named_id Key Account for this contact
        @return locale string Locale for the contact to be used in communication
        @return email string E-Mail address of the contact
        @return first_names string First Name(s) of the contact. Just in case you manually want to address the person
        @return last_name string Last name of the contact
        @return salutation string Salutation (how to address the contact in communication), eg. Dear Malte or Dear Mr. Sussdorff
        @return home_phone string Home Phone of the contact 
        @return cell_phone string Cell Phone of the contact 
        @return work_phone string Work phone for the contact 
        @return position string Position in the company of the contact
        @return portrait_url string URL to the portrait of the contact
    } - 

    ad_proc company_contact_body {} {
        @param first_names string First Name(s) of the contact. Just in case you manually want to address the person
        @param last_name string Last name of the contact
        @param email string E-Mail address of the contact
        @param locale string Locale for the contact to be used in communication
        @param salutation_id category "Intranet Salutation" Salutation we are using for the user
        @param home_phone string Home Phone of the contact 
        @param cell_phone string Cell Phone of the contact 
        @param work_phone string Work phone for the contact 
        @param position string Position of the contact in the company
    } - 

    ad_proc office {} {
        @return office named_id Office we are looking at
        @return office_type category "Intranet Office Type" Type of the office, defaults to main office
        @return office_status category "Intranet Office Status" Status of the office, defaults to active
        @return address_line1 string First address line
        @return address_line2 string Second address line (e.g. PO box or appartment)
        @return address_postal_code string Postal / ZIP code
        @return address_city string City the company resides in 
        @return address_country_code string ISO for the country
        @return contact_person named_id Who is the contact person for the office
        @return note string Note about the office
        @return phone string Phone number under which we can reach this office
    } - 

    ad_proc internal_company {} {        
        @return company json_object company Object with the internal company data
        @return logo_url string url of the company logo_url
        @return favicon_url string url of favicon which is gonna be used across all apps
    } -
    
    ad_proc country {} {        
        @return iso string Iso Code of the country
        @return country_name string Country name
    } -
}



#---------------------------------------------------------------
# Companies
#---------------------------------------------------------------

ad_proc -public cog_rest::get::companies {
    { -company_id "" }
    { -primary_contact_id "" }
    { -company_type_ids ""} 
    { -company_status_ids ""}
    { -autocomplete_query "" }
    -rest_user_id:required
} {
    Handler for GET rest calls on companies.
    It returns array of companies filtered by company_type or company_status
    It is also possible to get single company by passing company_id

    @summary Endpoint return list of companies with some additional data

    @param company_id integer If provided limit to only one company_id
    @param primary_contact_id integer Primary contact for whom we like the company info(s)
    @param company_type_ids category_array "Intranet Company Type" Limit to only companies of this company_type
    @param company_status_ids category_array "Intranet Company Status" Limit to only documents of this company status
    
    @param autocomplete_query string Query string for auto completion of companies. Used in lookup of companies for projects

    @return company_info json_array company Array with company information

} {

    # Permissions
    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_company'" -default 0]]
    set rest_otype_read_all_p [im_permission $rest_user_id "view_companies_all"]

    # Always allow to see the internal company
    if {$company_id eq [im_company_internal]} {set rest_otype_read_all_p 1}

    set where_clause_list [list "im_companies.main_office_id = im_offices.office_id"]
    # if they can't read all companies, only get their own
    if {!$rest_otype_read_all_p} {
        set user_company_ids [db_list company_ids "select object_id_one from acs_rels where object_id_two =:rest_user_id and rel_type='im_company_employee_rel'"]
        lappend where_clause_list "im_companies.company_id in ([template::util::tcl_to_sql_list $user_company_ids])"
    }

    if {$company_id ne ""} {
        lappend where_clause_list "im_companies.company_id = :company_id"
    }

    if {$primary_contact_id ne ""} {
        lappend where_clause_list "im_companies.primary_contact_id = :primary_contact_id"
    }

    if {$company_type_ids ne ""} {
        lappend where_clause_list "im_companies.company_type_id in ([template::util::tcl_to_sql_list $company_type_ids])"
    }

    if {$company_status_ids ne ""} {
        lappend where_clause_list "im_companies.company_status_id in ([template::util::tcl_to_sql_list $company_status_ids])"
    }

    # ----------------------------------------------------------------------
    # If we want to minimize size of returned JSON using 'first 3 letters'
    # --------------------------------------------------------------------
    if {$autocomplete_query ne ""} {
        lappend where_clause_list "lower(company_name) like lower('$autocomplete_query%')"
    }

    # Handler for country (as we need country and not country code)
    
    template::util::list_of_lists_to_array [db_list_of_lists country_options "select iso, country_name from country_codes"] countries
    
    set sql "select im_companies.company_id, accounting_contact_id, company_name,  company_status_id, company_type_id, company_path,
        vat_type_id, vat_number, site_concept as url, default_referral_source_id, payment_term_id,
        default_invoice_template_id, default_quote_template_id, default_po_template_id, default_bill_template_id,
        address_line1, address_line2, address_postal_code, address_city, address_country_code, phone, primary_contact_id
        from im_companies, im_offices
        where [join $where_clause_list " and "]"
    
    set company_info [list]

    db_foreach objects $sql {

        if {$default_referral_source_id ne ""} {
            set default_referral_source_name [im_name_from_id $default_referral_source_id]
        }
        
        if {$address_country_code ne ""} {
            set address_country_full $countries($address_country_code)
        } else {
            set address_country_full ""
        }

        if {$primary_contact_id ne ""} {
            set primary_contact_name [im_name_from_id $primary_contact_id]
        }

        # Check permissions, even if user do not have permission we still check if he is not asking for his own company. 
        # If yes, then we can display that single company to him\
        # We check if rest_user_id is company_id member
        set rest_user_is_company_member_p [db_string rest_user_is_company_member "select 1 from acs_rels where object_id_one =:company_id and object_id_two =:rest_user_id and rel_type='im_company_employee_rel' limit 1" -default 0]
        set read_p $rest_otype_read_all_p

        if {0} {
            set last_contact [db_string contact_based_on_invoice_or_project "select max(coalesce(last_modified,creation_date))::date as last_contact from 
                acs_objects o where object_id in 
                (
                    select project_id from im_projects where company_id = :company_id
                    UNION select cost_id from im_costs where provider_id = :company_id or customer_id = :company_id
                )" -default ""]
        } else {
            set last_contact ""
        }

        set main_office_id [db_string main_office "select main_office_id from im_companies where company_id = :company_id" -default ""]
        set main_office_name [im_name_from_id $main_office_id]

        if {!$read_p && !$rest_user_is_company_member_p} { continue }
        set logo_url [cog_rest::helper::portrait_url -company_id $company_id]

        lappend company_info [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::company {
    -company_name:required
    { -company_path ""}
    { -company_type_id "" }
    { -company_status_id "" }
    { -vat_type_id ""}
    { -vat_number ""}
    { -address_country_code ""}
    { -address_line1 ""}
    { -address_line2 ""}
    { -address_city ""}
    { -address_postal_code ""}
    { -phone ""}
    { -url ""}
    { -referral_source_id ""}
    { -payment_term_id ""}
    { -default_invoice_template_id ""}
    { -default_quote_template_id ""}
    { -default_po_template_id ""}
    { -default_bill_template_id ""}
    { -primary_contact_id ""}
    { -accounting_contact_id ""}   
    -rest_user_id:required
} {
    Handler for POST calls on companies.
    Can either create new company (with user) and edit existing one

    @param company_body request_body Company Information

    @return company_info json_object company Object with the newly created company
} {

    if {$company_path eq ""} {
        regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $company_name]] "_" company_path
    }
    
    # Check if we have the path
    set company_id [db_string company_exists "select company_id from im_companies where company_path = :company_path" -default ""]

    if {$company_id eq ""} {
        if {![im_permission $rest_user_id add_companies]} {
            cog_rest::error -http_status 403 -message "[_ intranet-core.lt_Insufficient_Privileg] [_ intranet-core.lt_You_dont_have_suffici]"
        }
    
        set office_path "${company_path}_main_office"
        set office_name "$company_name [lang::message::lookup "" intranet-core.Main_Office {Main Office}]"
                
        db_transaction {

            if {$company_type_id eq ""} {
                set company_type_id [im_company_type_customer]
            }

            # First create a new main_office:
            set main_office_id [cog::office::new \
                -creation_user $rest_user_id \
                -office_path $office_path \
                -office_name $office_name \
                -address_country_code $address_country_code \
                -address_line1 $address_line1 \
                -address_line2 $address_line2 \
                -address_city $address_city \
                -address_postal_code $address_postal_code \
                -phone $phone]

            set company_id [cog::company::new \
                -company_name       $company_name \
                -company_path       $company_path \
                -company_type_id    $company_type_id \
                -company_status_id  [im_company_status_active] \
                -vat_number         $vat_number \
                -vat_type_id        $vat_type_id \
                -url                $url \
                -main_office_id     $main_office_id \
                -referral_source_id $referral_source_id \
                -payment_term_id    $payment_term_id \
                -default_invoice_template_id $default_invoice_template_id \
                -default_quote_template_id $default_quote_template_id \
                -default_po_template_id $default_po_template_id \
                -default_bill_template_id $default_bill_template_id \
                -primary_contact_id $primary_contact_id \
                -accounting_contact_id $accounting_contact_id \
                -creation_user $rest_user_id]

            
            db_dml office "update im_offices set company_id = :company_id where office_id = :main_office_id"

            cog::callback::invoke -object_type "im_office" -object_id $main_office_id -type_id [im_office_type_main] -status_id [im_office_status_active] -action after_create
            cog::callback::invoke -object_type "im_company" -object_id $company_id -type_id $company_type_id -status_id $company_status_id -action after_create
        }
    } else {
        # Check that the name is the same, otherwise return an error
        set company_name_in_db [db_string company_name "select company_name from im_companies where company_id = :company_id"]
        if {$company_name_in_db ne $company_name} {
            cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_create_company_different_paths]"
        }
    }

    set company_info [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::companies -company_id $company_id -rest_user_id $rest_user_id]]

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::company {
    -company_id:required
    { -company_name "" }
    { -company_path ""}
    { -company_type_id "" }
    { -company_status_id "" }
    { -vat_type_id ""}
    { -vat_number ""}
    { -address_country_code ""}
    { -address_line1 ""}
    { -address_line2 ""}
    { -address_city ""}
    { -address_postal_code ""}
    { -phone ""}
    { -url ""}
    { -referral_source_id ""}
    { -payment_term_id ""}   
    { -default_invoice_template_id ""}
    { -default_quote_template_id ""}
    { -default_po_template_id ""}
    { -default_bill_template_id ""} 
    { -primary_contact_id ""}
    { -accounting_contact_id ""}   
    -rest_user_id:required
} {
    Handler for PUT calls on companies.

    @param company_id object im_company::write Company we want to update the information for
    @param company_body request_body Company Information

    @return company_info json_object company Object with the newly created company
} {

    set main_office_id [db_string main_office "select main_office_id from im_companies where company_id = :company_id" -default ""]

    if {$main_office_id ne ""} {
        cog::office::update \
            -office_id $main_office_id \
            -modifying_user $rest_user_id \
            -address_country_code $address_country_code \
            -address_line1 $address_line1 \
            -address_line2 $address_line2 \
            -address_city $address_city \
            -address_postal_code $address_postal_code \
            -phone $phone
    }

    cog::company::update \
        -company_id         $company_id \
        -company_name       $company_name \
        -company_type_id    $company_type_id \
        -company_status_id  $company_status_id \
        -vat_number         $vat_number \
        -vat_type_id        $vat_type_id \
        -url                $url \
        -referral_source_id $referral_source_id \
        -payment_term_id    $payment_term_id \
        -default_invoice_template_id $default_invoice_template_id \
        -default_quote_template_id $default_quote_template_id \
        -default_po_template_id $default_po_template_id \
        -default_bill_template_id $default_bill_template_id \
        -primary_contact_id $primary_contact_id \
        -accounting_contact_id $accounting_contact_id \
        -modifying_user $rest_user_id

    set company_info [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::companies -company_id $company_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::company {
    -company_id:required
    -rest_user_id:required
} {
    Try to delete a company (as in purge from the database). Will fail if the company has any associated information though. In this case use PUT to 
    update the company status to deleted instead

    @param company_id object im_company::admin Company we want to purge
     
    @return errors json_array error Array of errors found
} {
    set errors [list]

    # Check if we can actually nuke the company
    set cost_id [db_string costs "
		select	cost_id
		from	im_costs c
		where
			(c.customer_id = :company_id or c.provider_id = :company_id)
        limit 1
	" -default ""]

    if {$cost_id ne ""} {
        set parameter "Costs"
        set object_id $cost_id
        set err_msg "Can't delete company as it has cost items"
        lappend errors [cog_rest::json_object]
    }

    set project_id [db_string projects "select project_id from im_projects where company_id = :company_id limit 1" -default ""]
    if {$project_id ne ""} {
        set parameter "Projects"
        set object_id $project_id
        set err_msg "Can't delete company as it has projects"
        lappend errors [cog_rest::json_object]
    }

    set file_id [db_string files "
		select file_id from im_fs_files
		where folder_id in (
			select	folder_id
			from	im_fs_folders
			where	object_id = :company_id
		) limit 1
	" -default ""]

    if {$file_id ne ""} {
        set parameter "Files"
        set object_id $file_id
        set err_msg "Can't delete company as it has files"
        lappend errors [cog_rest::json_object]
    } 
    
    if {$file_id eq "" && $project_id eq "" && $cost_id eq ""} {
        # Delete the offices for this company
        set companies_offices_sql "
            select	office_id
            from	im_offices o,
                acs_rels r
            where	r.object_id_one = o.office_id
                and r.object_id_one = :company_id
            UNION
            select	office_id
            from	im_offices o
            where	company_id = :company_id
        "

        db_foreach delete_offices $companies_offices_sql {
            db_dml unlink_offices "update im_companies set main_office_id = (select min(office_id) from im_offices) where main_office_id = :office_id"
            im_office_nuke -current_user_id $rest_user_id $office_id
        }

        db_dml filestorage "
		delete from im_fs_folder_status 
		where folder_id in (
			select folder_id 
			from im_fs_folders 
			where object_id = :company_id
		)
	    "

        db_dml filestorage "
            delete from im_fs_folder_perms 
            where folder_id in (
                select folder_id 
                from im_fs_folders 
                where object_id = :company_id
            )
        "

        db_dml delete_companies "
		delete from im_companies 
		where company_id = :company_id"
    }

    return [cog_rest::json_response]
}



#---------------------------------------------------------------
# Company Contacts
#---------------------------------------------------------------

ad_proc -public cog_rest::get::company_contacts {
    { -company_id ""}
    { -email ""}
    { -contact_ids ""}
    { -role_ids ""}
    { -rest_user_id 0 }
} {
    Handler for GET rest calls to get company contacts (needed in Customer Portal App)

    @param company_id object im_company::view ID of the company for which to get the contacts. Defaults to main company of logged in user
    @param email string email address of the user we want to look for
    @param contact_ids object_array user::view Company contacts to return - ignored if company_id is provided - user_id of the company contact (equals company_contact_id)
    @param role_ids category_array "Intranet Biz Object Role" Biz object role the user has (like member or key account) - only useful if you provide company_id
    @return company_contacts json_array company_contact Array of company contacts based found
} {

    set company_contacts [list]
    if {$email ne ""} {
        set party_id [party::get_by_email -email $email]
        if {$party_id >0} {
            lappend contact_ids $party_id
        }
    }
    if {$company_id ne ""} {
        # Get the company members
        set where_clause_list [list]
        lappend where_clause_list "object_id_one = :company_id"
        lappend where_clause_list "rel_type = 'im_company_employee_rel'"
        if {$role_ids ne ""} {
            lappend where_clause_list "rel_id in (select rel_id from im_biz_object_members where object_role_id in ([template::util::tcl_to_sql_list $role_ids]))"
        }
        lappend where_clause_list "acs_rels.object_id_two not in (
                -- Exclude banned or deleted users
                select  m.member_id
                from    group_member_map m,
                    membership_rels mr
                where   m.rel_id = mr.rel_id and
                    m.group_id = acs__magic_object_id('registered_users') and
                    m.container_id = m.group_id and
                    mr.member_state != 'approved'
                )"

        set contact_ids [db_list company_members "select acs_rels.object_id_two from acs_rels where [join $where_clause_list " and "]"]
    } else {
        if {$contact_ids eq ""} {
            return ""
        }
    }

    if {$contact_ids ne ""} {
        # Get the return_data
        set company_contact_sql "
            select first_names, last_name, email, position,
                home_phone, work_phone, cell_phone, user_id as contact_id
            from persons p, parties pa, users_contact uc
            where p.person_id in ([template::util::tcl_to_sql_list $contact_ids])
            and p.person_id = pa.party_id
            and p.person_id = uc.user_id
            order by first_names
        " 
        db_foreach company_contact $company_contact_sql {
            set contact_name [im_name_from_id $contact_id]
            set company_id [cog::company::users_company_id -user_id $contact_id]
            set salutation [cog_salutation -person_id $contact_id]
            set locale [lang::user::locale -user_id $contact_id]
            set key_account_id [cog::company::key_account_id -user_id $contact_id]
            set portrait_url [cog_rest::helper::portrait_url -user_id $contact_id]
            lappend company_contacts [cog_rest::json_object]
        }
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::company_contact {
    -company_id:required
    -first_names:required
    -last_name:required
    -email:required
    { -locale "" }
    { -salutation_id "" }
    { -home_phone "" }
    { -cell_phone "" }
    { -work_phone "" }
    { -position ""}
    { -password ""}
    -rest_user_id:required    
} {
    Create a contact for a company

    @param company_id object im_company::write Company for which we want to add a contact
    @param company_contact_body request_body Contact information for the contact we want to add
    @param password string Password we use by default for the user - this will make sure the contact actually can login (if empty user won't be able to login)
    
    @return company_contact json_object company_contact Contact object
} {
    
    set contact_id [db_string first_last_name_exists_p "
        select  user_id
        from    cc_users
        where   lower(trim(email)) = lower(trim(:email))
        " -default ""]

    if {$contact_id eq ""} {

        if {$password ne ""} {
            set password_confirm $password
        } else {
            set password_confirm ""
        }

        array set creation_info [auth::create_user  -username $email  -email $email  -first_names $first_names  -last_name $last_name  -password $password  -password_confirm $password_confirm ]
    
        # Extract the contact_id from the creation info
        if {[info exists creation_info(user_id)]} {
            set contact_id $creation_info(user_id)
            set role_id [im_biz_object_role_full_member]
        } else {
            cog_rest::error -http_status 400 -message [lindex $creation_info(element_messages) 0]
        }
    
        # Update creation user to allow the creator to admin the user
        db_dml update_creation_user_id "
            update acs_objects
            set creation_user = :rest_user_id
            where object_id = :contact_id
        "

        # Add the user to the "Registered Users" group, because
        # (s)he would get strange problems otherwise
        set registered_users [db_string registered_users "select object_id from acs_magic_objects where name='registered_users'"]
        set reg_users_rel_exists_p [db_string member_of_reg_users "
                select  count(*)
                from    group_member_map m, membership_rels mr
                where   m.member_id = :contact_id
                    and m.group_id = :registered_users
                    and m.rel_id = mr.rel_id
                    and m.container_id = m.group_id
                    and m.rel_type::text = 'membership_rel'::text
        "]
        if {!$reg_users_rel_exists_p} {
            relation_add -member_state "approved" "membership_rel" $registered_users $contact_id
        }

        db_1row company_info "select company_type_id from im_companies where company_id = :company_id"
        set profile [im_profile_customers]
        if {$company_type_id == [im_company_type_customer]} {set profile [im_profile_customers]}
        if {$company_type_id == [im_company_type_partner]} {set profile [im_profile_partners]}
        if {$company_type_id == [im_company_type_provider]} {set profile [im_profile_freelancers]}
        if {$company_type_id == [im_company_type_internal]} {set profile [im_profile_employees]}

        # Check whether the rest_user_id has the right to add the guy to the group:
        set managable_profiles [im_profile::profile_options_managable_for_user $rest_user_id]
        foreach profile_tuple $managable_profiles {
            set profile_name [lindex $profile_tuple 0]
            set profile_id [lindex $profile_tuple 1]
            if {$profile == $profile_id} { im_profile::add_member -profile_id $profile -user_id $contact_id }
        }

        set role_id [im_biz_object_role_full_member]
        im_biz_object_add_role $contact_id $company_id $role_id

        db_dml add_users_contact "insert into users_contact (user_id) values (:contact_id)"
        db_dml update_contact "update users_contact set cell_phone = :cell_phone, work_phone = :work_phone, home_phone = :home_phone where user_id = :contact_id"
        db_dml update_person "update persons set position = :position, salutation_id = :salutation_id where person_id = :contact_id"
        if {$locale ne ""} {
            lang::user::set_locale -user_id $contact_id $locale
        }
    } else {
        set role_id [im_biz_object_role_full_member]
        im_biz_object_add_role $contact_id $company_id $role_id      
    }

    set company_contact [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::company_contacts -contact_ids $contact_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::company_contact {
    -contact_id:required
    -first_names:required
    -last_name:required
    -email:required
    { -locale "" }
    { -salutation_id "" }
    { -home_phone "" }
    { -cell_phone "" }
    { -work_phone "" }
    { -position ""}
    -rest_user_id:required
} {
    Change a contact for a company. Note that password isn't supported

    @param contact_id object person::write Contact we want to edit
    @param company_contact_body request_body Contact information for the contact we want to add

    @return company_contact json_object company_contact Contact object
} {
    
    person::update  -person_id $contact_id  -first_names $first_names  -last_name $last_name
    party::update  -party_id $contact_id  -email $email
    acs_user::update  -user_id $contact_id -username $email
    db_dml update_contact "update users_contact set cell_phone = :cell_phone, work_phone = :work_phone, home_phone = :home_phone where user_id = :contact_id"
    db_dml update_person "update persons set position = :position, salutation_id = :salutation_id where person_id = :contact_id"


    set company_contact [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::company_contacts -contact_ids $contact_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::company_contact {
    -contact_id:required
    { -company_id ""}
    -rest_user_id:required
} {
    Nuke a company_contact instead of setting the status to deleted

    Will completely remove the contact if possible.

    @param contact_id object person::write Person we want to remove
    @param company_id object im_company::write Person we want to remove
    @return errors json_array error Array of errors found

} {
    set errors [list]
    set employee_p [im_user_is_employee_p $contact_id]
    if {$employee_p} {
        set parameter "Employee"
        set object_id $contact_id
        set err_msg "Can't delete contact as it is an employee"
        lappend errors [cog_rest::json_object]
    }

    if {!$employee_p} {
        if {$company_id eq ""} {
            if {[ permission::permission_p  -party_id $rest_user_id -object_id $contact_id -privilege "admin"]} {
                cog::user::nuke -user_id $contact_id -current_user_id $rest_user_id  
            } else {
                set parameter "Contact"
                set object_id $contact_id
                set err_msg "No permission to nuke the user"
                lappend errors [cog_rest::json_object]
            }
        } else {
            set rel_id [db_string rel "select rel_id from acs_rels where object_id_one = :company_id and object_id_two = :contact_id and rel_type = 'im_company_employee_rel'"]
            return [cog_rest::delete::relationship -rel_id $rel_id -rest_user_id $rest_user_id]
        }
    }
    return [cog_rest::json_response]
}

#---------------------------------------------------------------
# Company Offices
#---------------------------------------------------------------

ad_proc -public cog_rest::get::company_offices {
    -rest_user_id:required
    -company_id:required
    { -office_type_id "170" }
    { -office_status_id "160"}
} {
    Get offices for a company

    @param company_id object im_company::read company for which we want the offices
    @param office_type_id category "Intranet Office Type" Type of the office, defaults to main office
    @param office_status_id category "Intranet Office Status" Status of the office, defaults to active

    @return offices json_array office Array of offices
} {
    set offices [list]
    db_foreach office "select office_id, office_type_id, office_status_id,
        address_line1, address_line2, address_postal_code, address_city, address_country_code,
        contact_person_id, note, phone
        from im_offices where company_id = :company_id" {
            lappend offices [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

#---------------------------------------------------------------
# GET Endpoint for specific company scenarios
#---------------------------------------------------------------

ad_proc -public cog_rest::get::internal_company {
    -rest_user_id:required
} {
    Return data about the internal company

    @return internal_company json_object internal_company Company information for the internal company
} {
    set company_array [cog_rest::get::companies -company_id [im_company_internal] -rest_user_id $rest_user_id]
    set company [cog_rest::helper::json_array_to_object -json_array $company_array]
    
    # Logo and Favicon
    set logo_url [parameter::get_from_package_key -package_key webix-portal -parameter LogoURL]
    set favicon_url [parameter::get_from_package_key -package_key webix-portal -parameter FavIconUrl]
    
    set internal_company [cog_rest::json_object]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::countries {
} {
    Return the list of configured countries and country codes.

    @return countries json_array country Array of countries
} {
    set countries [list]
    set sql "select count(iso) as count, iso, country_name from country_codes c, im_offices o where o.address_country_code = c.iso group by iso order by count desc"
    db_foreach countries $sql {
        lappend countries [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}