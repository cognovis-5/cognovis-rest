ad_library {
    REST Web Service Component Library Helper Functions
	@author malte.sussdorff@cognovis.de

}


#---------------------------------------------------------------
# Helper functions
#---------------------------------------------------------------

ad_proc -public cog_rest::user_profile_objects {
	-user_id:required
} {
	Returns a JSON array of group_ids and names the user is a member off 

	@return profiles TCL List of profile objects
	@return_profiles group_id integer ID of the group the user is a member of
	@return_profiles group_name string Name of the group the user is a member of
} {

	set profiles [list]
	foreach group [im_profile::profile_options_of_user $user_id] {
		set group_id [lindex $group 1]
		set group_name [lindex $group 0]
		lappend profiles [cog_rest::json_object]
	}
	return $profiles
}

namespace eval cog_rest::build {
	ad_proc -public category_enum_code {
		{-language "typescript"}
		-category_type:required
	} {
		Build the enum_code for a category type

		Coding convention assumes that the category_type is transformed into CamelCase and that the categories are all upper_case.

		@param language Language for which to build the code
		@param categories Category Type for which to build the enum.
	} {
		switch $language {
			"typescript" {

				# Handle duplicates
				set category_list [list]

				# Get the categories
				db_foreach category "select category_id, category from im_categories where category_type = :category_type and enabled_p = 't'" {
					set ts_category [cog_rest::helper::to_ts_enum_name -string $category]
					if {[lsearch $category_list $ts_category] <0} {
						lappend category_list $ts_category
						lappend categories_code " $ts_category = $category_id"
					}
				}

				set enum_code "export enum [cog_rest::helper::to_CamelCase -string $category_type] {\n[join $categories_code ",\n"]\n}"
			}
		}
		return $enum_code
	}


    ad_proc -public category_enums {
        {-language "typescript"}
    } {
        Provide the code for generating category enums for development in other languages

        @param language string Language for which to generate the enum code. Typescript is the default (and currently only supported)
        @return code string Code ready for copy/paste with all the enums of the categories
    } {
        set category_enums [list]
        foreach category_type [db_list category_types "select distinct category_type from im_categories where enabled_p = 't'"] {
            lappend category_enums [cog_rest::build::category_enum_code -category_type $category_type]
        }

        set code [join $category_enums "\n\n"]
		return $code
    }


    ad_proc -public country_enums {
        {-language "typescript"}
    } {
        Provide the code for generating country enums for development in other languages

        @param language string Language for which to generate the enum code. Typescript is the default (and currently only supported)
        @return code string Code ready for copy/paste with all the enums of the countries
    } {
        set country_enums [list]
		set country_names [list]
		db_foreach category "select iso, country_name from country_codes" {
			set ts_country [cog_rest::helper::to_ts_enum_name -string "$country_name"]
			if {[lsearch $country_names $ts_country]<0} {
				lappend country_names $ts_country
				lappend country_enums " $ts_country = \"$iso\""
			}
		}
		
		set enum_code "export enum [cog_rest::helper::to_CamelCase -string "Countries"] {\n[join $country_enums ",\n"]\n}"
		return $enum_code
    }

}

namespace eval cog_rest::helper {
	ad_proc -public to_CamelCase {
		-string:required
	} {
		Returns a string all in CamelCase

		@param string String to return as CamelCase
		@return CamelCased String
	} {
		# First turn the string into snake_case if it ain't already

		regsub -all { } $string {_} string

		# Remove dash in CamelCase
		regsub -all {\-} $string {_} string

		set CamelCaseString [join [lmap el [split $string _] {string totitle $el}] ""]
		return $CamelCaseString
	}

	ad_proc -public to_ts_enum_name {
		-string:required
	} {
		Turns a String into a variable name to be used in typescript like "TRANS_PROOF" instead of "Trans + Proof" or "GERMAN_EDITING" instead of "German Editing"

		@param string String to return as an upper cased variable
		@return UPPER_CASED String
	} {

		# Replace all space with underscore
		regsub -all { } $string {_} string

		# Replace unwanted characters
		regsub -all {[^A-Za-z0-9_.]} $string {} string
		
		# Replace DOTS
		regsub -all {\.} $string {_} string

		# Replace double underscores
		regsub -all {__} $string {_} string

		# Turn a number into a String by adding a # in front
		if {[string is integer [string index $string 0]]} {
			set string "n$string"
		}

		# Remove numbers at the beginning
		# set string [string trimleft $string "1234567890_"]		
		return "[string toupper $string]"
	}

	ad_proc -public object_permission_proc {
		-object_type:required
	} {
		Returns the permission procedure

		@param object_type Object type for which we need the permission procedure
	} {
		set perm_proc ""
		
		switch $object_type {
			im_cost - im_invoice - im_trans_invoice - im_timesheet_invoice {
				set perm_proc [parameter::get -package_id [im_package_invoices_id] -parameter "InvoicePermissionProc" -default "cog_rest::permission::im_cost"]
			}
			default {
				if {[info commands "::cog_rest::permission::${object_type}"] ne ""} {
					set perm_proc cog_rest::permission::${object_type}
				} else {
					if {[info commands ${object_type}_permissions] ne ""} {
						set perm_proc "${object_type}_permissions"
					}
				}
			}
		}

		return $perm_proc
	}


	ad_proc -public object_permission {
		-object_id:required
		-user_id:required
		{-privilege "read"}
	} {
		@param object_id Object on which we check the privilege
		@param user_id User for whom we check the privilege
		@param privilege Type of privilege to show, defaults to "read"

		@return 1 if the user has permission, 0 if not.
	} {
		set object_type [acs_object_type $object_id]
		# If the object_type is empty, use default permission proc

		# Assume we don't have permission
		set permission_p 0
		set perm_proc [object_permission_proc -object_type $object_type]

		# Default proc supports privilege by default
		if {$perm_proc eq ""} {
			set permission_p [permission::permission_p -no_login -party_id $user_id -object_id $object_id -privilege $privilege]
		} else {
			# Specialized procs use the user_id object_id view_p read_p write_p admin_p format
			$perm_proc $user_id $object_id view_p read_p write_p admin_p

			switch $privilege {
				read {
					if {$read_p} { set permission_p 1 }
				}
				view {
					if {$view_p} { set permission_p 1 }
				}
				write {
					if {$write_p} { set permission_p 1 }
				}
				admin {
					if {$admin_p} { set permission_p 1 }
				}
				default {
					# We don't support any other privilege
					set permission_p 0
				}
			}
		}

		return $permission_p
	}

	ad_proc -public portrait_url {
		{ -user_id "" }
		{ -company_id "" }
	} {
		Returns the URL to the portrait of a user
	} {

		set portrait_url ""
		set item_id 0

		if {$user_id ne ""} {
			set item_id [cog::user::get_portrait_id_not_cached -user_id $user_id]
		}

		if {$company_id ne ""} {
			set item_id [db_string get_item_id {
				select max(c.item_id)
					from acs_rels a, cr_items c
					where a.object_id_two = c.item_id
					and a.object_id_one = :company_id
					and a.rel_type = 'im_company_logo_rel'
			} -default 0]
		}
		if {$item_id > 0} {
			set portrait_url "[ad_url]/cognovis/portrait-bits.tcl?item_id=$item_id"
		}
		
		return $portrait_url
	}

}

ad_proc -public cog_rest::return_json {
    -json:required
    -no_cors:boolean
} {
    Return the JSON string while handling CORS

    @param json JSON string to return
    @param no_cors Boolean, if set do not run the CORS protection
} {
    # Allow for cross origin
    if {!$no_cors_p} {
        set outputheaders [ns_conn outputheaders]
        ns_set put [ns_conn outputheaders] "Access-Control-Allow-Origin" "*"
        ns_set put [ns_conn outputheaders] "Access-Control-Allow-Methods" "GET, POST, DELETE, PUT, OPTIONS"
        ns_set put [ns_conn outputheaders] "Access-Control-Allow-Headers" "DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization"
        ns_set put [ns_conn outputheaders] "Access-Control-Expose-Headers" "Content-Length,Content-Range"
    }

	catch {ns_return 200 "application/json" $json}
}

ad_proc -public cog_rest::initialize_endpoint {
    {-endpoint_var "endpoint"}
    {-oid_var "rest_oid"}
    {-otype_var "rest_otype"}
    {-subtype_var "rest_subtype"}
} {
    Initialize the endpoint variables based on the url

    @param endpoint_var Name of the variable later on containing the endpoint
    @param oid_var Variable name containing the object_id
    @param otype_var Variable name containing the object_type
    @param subtype_var Variable name containing the subtype if one is provided

} {
    upvar $endpoint_var endpoint
    upvar $oid_var rest_oid
    upvar $otype_var rest_otype

    set url [ns_conn url]
    
	# Convert the path into object_type and rest_oid
    set url_pieces [split $url "/"]
    set rest_otype [lindex $url_pieces 2]
	set rest_oid ""

	switch [llength $url_pieces] {
		3 {
			set endpoint "$rest_otype"
		}
		4 {
			# Check if the 4th element is an integer
			# then it is the rest_oid

			if {[string is double [lindex $url_pieces 3]]} {
				set endpoint "$rest_otype"
				set rest_oid [lindex $url_pieces 3]				
			} else {
				# Apparently the 4th element is an additional type
				set endpoint "${rest_otype}::[lindex $url_pieces 3]"
			}
		}
		5 {
			if {[string is double [lindex $url_pieces 4]]} {
				set rest_oid [lindex $url_pieces 4]
				set endpoint "${rest_otype}::[lindex $url_pieces 3]"		
			} else {
				# Apparently the 4th element is an additional type
				set rest_oid ""
				set endpoint "${rest_otype}::[lindex $url_pieces 3]::[lindex $url_pieces 4]"
			}
		}
	}
}

namespace eval cog_rest::authentication {
	ad_proc -public generate_auto_login {
    	-user_id:required
	} {
    	Generates a security token for auto_login
	} {
		set user_password ""
		set user_salt ""

		set user_data_sql "
			select	u.password as user_password
			from	users u
			where	u.user_id = :user_id"
		db_0or1row get_user_data $user_data_sql

		# generate the expected auto_login variable
		set auto_login_string "$user_id$user_password"
		return [ns_sha1 $auto_login_string]
	}

	ad_proc -public valid_auto_login_p {
		{-check_user_requires_manual_login_p "1" }
		-user_id:required
		-auto_login:required
	} {
		Verifies the auto_login in auto-login variables
		@param expiry_date Expiry date in YYYY-MM-DD format
		@param user_id The users ID
		@param auto_login The security token generated by generate_auto_login.

		@author Timo Hentschel (thentschel@sussdorff-roy.com)
		@author Frank Bergmann (frank.bergmann@project-open.com)
	} {
		# Should the Unregistered Visitor to login without password?
		set enable_anonymous_login_p [parameter::get_from_package_key -package_key "intranet-core" -parameter "EnableUnregisteredUserLoginP" -default 0]

		if {$user_id == 0 && $enable_anonymous_login_p} {
			return 1
		}

		# Quick check on tokens
		set expected_auto_login [generate_auto_login -user_id $user_id]
		if {$auto_login ne $expected_auto_login } { return 0 }

		return 1
	}

	ad_proc -public header_auth {

	} {
		Returns the user_id if we found either Basic or Bearer authentication in the Headers

	    Example: Authorization: Basic cHJvam9wOi5mcmFiZXI=
		Example: Authorization: Bearer <token>

		@return user_id if successful, 0 if not.
	} {
		# --------------------------------------------------------
    	# Check for HTTP  authorization
	    set header_vars [ns_conn headers]
    	set header_auth [ns_set get $header_vars "Authorization"]
    	if {$header_auth eq ""} {
    		set header_auth [ns_set get $header_vars "authorization"]
    	}
    	
		set auth_user_id ""

		if {[regexp {^([a-zA-Z_]+)\ (.*)$} $header_auth match method header_token]} {
			# We can only deal with basic and bearer
		
			switch $method {
				Basic {
					set auth_user_id [cog_rest::authentication::basic_auth -header_token $header_token]
				}
				Bearer {
					set auth_user_id [cog_rest::authentication::bearer_auth -header_token $header_token]
				}
				default {
					# Do nothing
				}
			}
		}
		return $auth_user_id
	}

	ad_proc -public basic_auth {
		-header_token:required
	} {
		Checks for basic authentication
    	
		Example: Authorization: Basic cHJvam9wOi5mcmFiZXI=

		@param header_token Authentication token in base64 format for username/email and password.

		@return user_id if successful (nothing if not)
		@error empty string
	} {

		set basic_auth_userpass [base64::decode $header_token]
		
		set basic_auth_user_id ""

		if {[regexp {^([^\:]+)\:(.*)$} $basic_auth_userpass match basic_auth_username basic_auth_password]} {

			# Try username first
	    	set basic_auth_user_id [db_string userid "select user_id from users where lower(username) = lower(:basic_auth_username)" -default ""]

			# Try if that is an email
    		if {"" == $basic_auth_user_id} {
				set basic_auth_user_id [db_string userid "select party_id from parties where lower(email) = lower(:basic_auth_username)" -default ""]
    		}
	
	    	if {"" != $basic_auth_user_id} {
				set basic_auth_password_ok_p [ad_check_password $basic_auth_user_id $basic_auth_password]

				# Unset user_id if the password does not match
				if {!$basic_auth_password_ok_p} {
					set basic_auth_user_id ""
				}
    		}
		}
		return $basic_auth_user_id
	}

	ad_proc -public bearer_auth {
		-header_token:required
	} {
		Checks for Bearer authentication using a Base64 token for the authorization. 

		Probably needs to change to JWT eventually.... in the future....
    	
		Example: Authorization: Bearer <token>

		@param header_token Authentication token from the header
		@return user_id if successful (nothing if not)
		@error empty string
	} {

		set basic_api_key [base64::decode $header_token]
		set auth_user_id ""

		if {[regexp {^([^\:]+)\:(.*)$} $basic_api_key match api_user_id api_token]} {
	        if {$api_token ne "" && $api_user_id ne ""} {
				set valid_p [valid_auto_login_p -user_id $api_user_id -auto_login $api_token]
				if {$valid_p} {
					# It is a valid token
					set auth_user_id $api_user_id
				}
			}
		}

		return $auth_user_id		
    }

	ad_proc -public token_auth {
	} {
		Checks for token authentication with user_id and auth_token

		Probably needs to change to JWT eventually.... in the future....
    	
		Example: Authorization: Bearer <token>

		@return user_id if successful (nothing if not)
		@error empty string
	} {

		# By default assumme error
		set token_token [ns_queryget auth_token]
		if {$token_token eq ""} {
			set token_token [ns_queryget auto_login]
		}

		if {$token_token ne ""} {
			# We seem to have a request for authentication
			set token_user_id [ns_queryget auth_user_id]
			if {$token_user_id eq ""} {
				# Check old style for user_id
				set token_user_id [ns_queryget user_id]
			}
	

    		# Check if the token fits the user
    		if {"" ne $token_user_id} {
				set valid_p [valid_auto_login_p -user_id $token_user_id -auto_login $token_token]
				if {!$valid_p} {
			    	set token_user_id ""
				}
			}
		} else {
			set token_user_id ""
		}

		return $token_user_id
    }

	ad_proc -public api_key {
	} {
		Checks for token authentication with user_id and auth_token

		Probably needs to change to JWT eventually.... in the future....
    	
		Example: Authorization: Bearer <token>

		@return user_id if successful (nothing if not)
		@error empty string
	} {
		
		set query [ns_conn query]
		array set params [ns_set array [ns_parsequery $query]]
		if {![info exists params(api_key)]} {
			return ""
		}
		
		set api_key $params(api_key)
		if {[catch { set basic_api_key [base64::decode "$api_key"] }]} {
			return ""
		}
		if {[regexp {^([^\:]+)\:(.*)$} $basic_api_key match api_user_id api_token]} {
			set valid_p [valid_auto_login_p -user_id $api_user_id -auto_login $api_token]
			if {!$valid_p} {
				# Not valid, reset api_user_id
				set api_user_id ""
			}
		} else {
			set api_user_id ""
		}
		return $api_user_id
    }
}

