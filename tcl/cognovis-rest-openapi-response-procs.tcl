ad_library {
  OpenAPI functions for response objects
	@author malte.sussdorff@cognovis.de

}

namespace eval cog_rest::openapi::response {

    ad_proc -public object_name {
        -proc_name:required        
    } {
        Returns the object_name for the response
    } {
        array set doc_elements [nsv_get api_proc_doc $proc_name]
        set object_name ""
        if { [info exists doc_elements(return)] } {
            # Check that we have objects
            set return_elements [split [lindex $doc_elements(return) 0]]
            set response_object_name [lindex $return_elements 0]
            set response_object_type [lindex $return_elements 1]
            switch $response_object_type {
                "json_array" - "json_object" {
                    if {[info exists doc_elements(return_$response_object_name)]} {
                        set object_name $response_object_name
                    } else {
                        set object_name [lindex $return_elements 2]
                    }
                }
                default {
                    if {[info exists doc_elements(return_$response_object_name)]} {
                        set object_name $response_object_name
                    } 
                }
            }
        }
        return $object_name
    }

    ad_proc -public name {
        -proc_name:required
        { -operation "" }
        { -endpoint "" }
    } {
        Returns the Name of the response object / array

        @param proc_name Name of the procedure for which to get the reponse schema
        @param object_name Name of the object
    } {

        set object_name [cog_rest::openapi::response::object_name -proc_name $proc_name]

        # Check that if we have an object defintion for the object_name
        if {[nsv_exists api_proc_doc "cog_rest::json_object::$object_name"]} {
            set response_name "I[cog_rest::helper::to_CamelCase -string $object_name]"
        } else {
            set response_name "I[cog_rest::helper::to_CamelCase -string ${operation}_$endpoint]"
        }
        return $response_name
    }

    ad_proc -public schema {
        -proc_name:required
        -object_name:required
    } {
        Returns the Response schema for a procedure

        @param proc_name Name of the procedure for which to get the reponse schema
        @param object_name Name of the object
    } {

        set object_name [cog_rest::openapi::response::object_name -proc_name $proc_name]
        set return_elements [list]

        # Check that if we have an object defintion for the object_name
        if {[nsv_exists api_proc_doc "cog_rest::json_object::$object_name"]} {
            array set doc_elements [nsv_get api_proc_doc "cog_rest::json_object::$object_name"]
            foreach return_element $doc_elements(return) {
                lappend return_elements $return_element
            }
        } else {
            array set doc_elements [nsv_get api_proc_doc $proc_name]
            if { [info exists doc_elements(return_${object_name})]} {
                foreach return_element $doc_elements(return_${object_name}) {
                    lappend return_elements $return_element
                }
            }
        }

        #---------------------------------------------------------------
        # Return Element
        #---------------------------------------------------------------
        set response_object_schema "      title: '$object_name'\n      type: object\n      properties:\n"
        
        #---------------------------------------------------------------
        # Prepare the examples
        #---------------------------------------------------------------
        if {[info exists doc_elements(example_${object_name})]} {
            foreach example_element $doc_elements(example_${object_name}) {  
                # The first item is supposed to be the variable
                set example_items [split $example_element]
                set examples([lindex $example_items 0]) [lrange $example_items 1 end]
            }
        }

        #---------------------------------------------------------------
        # The single properties
        #---------------------------------------------------------------
        foreach return_element $return_elements {  
            # The first item is supposed to be the variable
            set return_items [split $return_element]
            set return_var [lindex $return_items 0]
            set return_type [lindex $return_items 1]

            append response_object_schema [cog_rest::openapi::response::object_schema -return_element $return_element -object_name $object_name]
        
            if {[info exists examples($return_var)]} {
                switch $return_type {
                    integer - number {
                        append response_object_schema "          example: $examples($return_var)\n"
                    }
                    default {
                        append response_object_schema "          example: '$examples($return_var)'\n"
                    }
                }
            }
        }

        callback cog_rest::openapi::response_schema -proc_name $proc_name -object_name $object_name

        return $response_object_schema
    }

    ad_proc -public path {
        -proc_name:required
        -schema_ref:required
    } {
        Returns a list of response paths for each of the http status which are defined

        @param proc_name Name of the procedure
    } {

        #---------------------------------------------------------------
        # Append the return values and examples
        #---------------------------------------------------------------
        array set doc_elements [nsv_get api_proc_doc $proc_name]

        # Find out if we return an array of the objects
        set return_elements [split [lindex $doc_elements(return) 0]]
        set return_type [lindex $return_elements 1]
        set object_name [lindex $return_elements 0]
        
        switch $return_type {
            json_array - json_object {
                # The type is clearly defined
                set return_description [lrange $return_elements 3 end]
            }
            default {
                # Backwards compatability to identify an object
                if {$object_name eq "data"} {
                    set return_type "json_object"
                } else {
                    set return_type "json_array"
                }
                set return_description [lrange $return_elements 1 end]
            }
        }

        append paths "      responses:
        '200':\n"

        if {$return_description eq ""} {
            set return_description "Generic Response description for $proc_name"
        } 
        
        regsub -all {'} $return_description {''} return_description

        append paths "          description: '$return_description'\n          content:
            application/json:
              schema:\n"

        switch $return_type {
            json_object {
                append paths "                \$ref: '$schema_ref'\n"
            }
            json_array {
                append paths "                type: array
                items:
                  \$ref: '$schema_ref'\n"
            }
            default {
              append paths "                type: object
                properties: 
                    success:
                      type: boolean
                    $object_name:
                      type: array
                      items:
                        \$ref: '$schema_ref'\n"

            }
        }
        return $paths
    }

    ad_proc -public generic_path {
    } {
        Returns a generic path for an unknown response object
    } {
        return "      responses:
        '200':
          description: Generic description
          content:
            application/json:
              schema:
                type: string\n"
    }

    ad_proc -public object_schema {
        -return_element:required
        -object_name:required
        {-additional_indention ""}
    } {
        Returns the schema for a single response object based on the return_element definition

        @param return_element The element (usually consisting of var_name and type + description)
        @param object_name Object 
        @param rest_user_id Current user accessing the endpoint 

        @return Schema for the response object
    } {
        
        # The first item is supposed to be the variable
        set return_var [lindex $return_element 0]
        set return_type [lindex $return_element 1]
        set response_schema_ref ""
        
        # Examples are used overall and not specifically for one type

        # Transform the return_type
        switch $return_type {
            category - category_array {
                # Get the example value
                set category_type [lindex $return_element 2]
                set category_example_id [db_string example_id "select min(category_id) from im_categories where category_type = :category_type and enabled_p = 't'" -default ""]
                if {$category_example_id ne ""} {
                    set category_example_name [im_category_from_id -translate_p 0 $category_example_id]
                    set icon_or_color "fas fa-eye"
                    set return_item_example "{\"id\": $category_example_id, \"name\": \"$category_example_name\", \"icon_or_color\": \"$icon_or_color\"}"
                    set example($return_var) $return_item_example
                }
                
                if {$return_type eq "category"} {
                    set response_schema_ref "'#/components/schemas/ICategoryObject'"          
                } else {
                    set response_schema_ref "'#/components/schemas/ICategoryArray'"
                }
                set return_item_desc [lrange $return_element 3 end]
            }
            material - material_array {
                # Get the example value
                set material_type [lindex $return_element 2]
                set material_example_id [db_string example_id "select min(material_id) from im_materials where material_type_id = (select category_id from im_categories where category = :material_type) and material_status_id = 9100" -default ""]
                if {$material_example_id ne ""} {
                    set material_example_name [im_name_from_id $material_example_id]

                    set return_item_example "{\"id\": $material_example_id, \"name\": \"$material_example_name\"}"
                    set example($return_var) $return_item_example
                }
                
                if {$return_type eq "material"} {
                    set response_schema_ref "'#/components/schemas/IMaterialObject'"          
                } else {
                    set response_schema_ref "'#/components/schemas/IMaterialArray'"
                }
                set return_item_desc [lrange $return_element 3 end]
            }
            object {
                # Use named_id for the time being.
                set response_schema_ref "'#/components/schemas/INamedId'"
                set return_item_desc [lrange $return_element 3 end]
            }
            named_id {
                set response_schema_ref "'#/components/schemas/INamedId'"
                set return_item_desc [lrange $return_element 2 end]
            }
            json_array {
                # Get the array definition
                set return_type "array\n"
                set return_array_type [lindex $return_element 2]

                if {[nsv_exists api_proc_doc "cog_rest::json_object::$return_array_type"]} {
                    set response_schema_ref "'#/components/schemas/I[cog_rest::helper::to_CamelCase -string $return_array_type]'"
                    set return_array_elements [list]
                    set return_item_desc [lrange $return_element 3 end]
                } else {
                    set return_item_desc [lrange $return_element 2 end]
                    upvar doc_elements doc_elements
                    
                    # Append the properties for each element
                    set return_array_elements $doc_elements(return_${object_name}_${return_var})
                }


                if {[llength $return_array_elements]>0} {
                    # Only append properties if we actually have elements
                    append return_type "          items:\n"
                    append return_type "            type: object\n"
                    append return_type "            properties:\n"
                    foreach return_array_element $return_array_elements {
                        append return_type [cog_rest::openapi::response::object_schema -return_element $return_array_element -object_name "${object_name}_$return_var" -additional_indention "      "]
                    }
                }
            }
            json_object {
                set return_type "object\n"
                set return_array_type [lindex $return_element 2]

                if {[nsv_exists api_proc_doc "cog_rest::json_object::$return_array_type"]} {
                    set response_schema_ref "'#/components/schemas/I[cog_rest::helper::to_CamelCase -string $return_array_type]'"
                    set return_array_elements [list]
                    set return_item_desc [lrange $return_element 3 end]
                } else {
                    set return_item_desc [lrange $return_element 2 end]
                    upvar doc_elements doc_elements
                    
                    # Append the properties for each element
                    set return_array_elements $doc_elements(return_${object_name}_${return_var})
                }
                
                if {[llength $return_array_elements]>0} {
                    # Only append properties if we actually have elements
                    append return_type "          properties:\n"
                    foreach return_array_element $return_array_elements {
                        append return_type [cog_rest::openapi::response::object_schema -return_element $return_array_element -object_name "${object_name}_$return_var" -additional_indention "      "]
                    }
                }
            }
            cr_file {
                set return_type "object\n"
                set return_item_desc [lrange $return_element 2 end]

                append return_type "          properties:\n"
                append return_type [cog_rest::openapi::response::object_schema -return_element [list id integer Revision id for the file] -object_name "${object_name}_id" -additional_indention "    "]
                append return_type [cog_rest::openapi::response::object_schema -return_element [list name string Filename (including extendsion) for the file] -object_name "${object_name}_name" -additional_indention "    "]
                append return_type [cog_rest::openapi::response::object_schema -return_element [list sname string Revision id for the file used as the unique server identifier] -object_name "${object_name}_sname" -additional_indention "    "]
                append return_type [cog_rest::openapi::response::object_schema -return_element [list status string Server (thats the value. period)] -object_name "${object_name}_status" -additional_indention "    "]
                append return_type [cog_rest::openapi::response::object_schema -return_element [list size number Filesize as a number of bytes] -object_name "${object_name}_size" -additional_indention "    "]
            }
            date {
                set return_type "string\n          format: date\n"
                if {![info exists examples($return_var)]} {
                set examples($return_var) "2018-03-20"
                }
                set return_item_desc [lrange $return_element 2 end]
            }
            date-time {
                set return_type "string\n          format: date-time\n"
                if {![info exists examples($return_var)]} {
                set examples($return_var) "2018-03-20T09:12:28Z"
                }
                set return_item_desc [lrange $return_element 2 end]
            }
            default {
                # check if this is a valid type, otherwise set to string
                if {[lsearch [list array boolean integer number object string] $return_type]<0} {
                set return_type "string\n"
                } else {
                set return_type "$return_type\n"
                }
                set return_item_desc [lrange $return_element 2 end]
            }
        }

        set response_schema "$additional_indention        $return_var:\n"

        if {$return_item_desc ne ""} {
            append response_schema "$additional_indention          description: '[cog_rest::convert_to_yaml_string -string_from $return_item_desc]'\n"
        }

        if {$response_schema_ref ne ""} {
            if {[lindex $return_element 1] eq "json_array"} {
                append response_schema "$additional_indention          type: array
$additional_indention          items:
"               
                set additional_indention "$additional_indention  "
            }
            append response_schema "$additional_indention          \$ref: $response_schema_ref \n"
        } else {
            append response_schema "$additional_indention          type: $return_type"
        }
        return $response_schema
    }


    ad_proc -public default_components {
        -response_comp_arr:required
    } {
        Initialize the main references for responses so we can reference them
    } {
        upvar $response_comp_arr response_comp

        # Category
        set response_comp(ICategoryObject) "      type: object
      title: 'ICategoryObject'
      properties:
        id:
          type: integer
          description: 'category_id of the category'
        name:
          type: string
          description: 'Translated category name'
        icon_or_color:
          type: string
          description: 'HTML for color or icon (like fas fa-icon)'\n"

        # Category Array (referencing category object)
        set response_comp(ICategoryArray) "      type: array
      title: 'ICategoryArray'
      items:
        \$ref: '#/components/schemas/ICategoryObject'\n"          

        # Material
        set response_comp(IMaterialObject) "      type: object
      title: 'IMaterialObject'
      properties:
        id:
          type: integer
          description: 'material_id of the Material'
        name:
          type: string
          description: 'Material name'\n"

        # Material Array (referencing Material object)
        set response_comp(IMaterialArray) "      type: array
      title: 'IMaterialArray'
      items:
        \$ref: '#/components/schemas/IMaterialObject'\n"       

        # Named ID
        set response_comp(INamedId) "      type: object
      title: 'INamedId'
      properties:
        id:
          type: integer
          description: 'object_id of the object we look for'
        name:
          type: string
          description: 'Name for the object_id'\n"

        # Error object
        set response_comp(IUserError404) "      type: object
      properties:
        property:
          description: 'Property (element) which caused the error'
          type: string
        message:
          description: 'Error message from the server'
          type: string\n"

        # Error object
        set response_comp(IServerError500) "      type: object
      properties:
        success:
          type: boolean
        message:
          description: 'Error message from the server'
          type: string\n"

        foreach proc_name [lsort [nsv_array names api_proc_doc "cog_rest::json_object::*"]] {
            set object_type [lindex [split $proc_name "::"] end]
            set objectType "I[cog_rest::helper::to_CamelCase -string $object_type]"
            if {![info exists response_comp($objectType)]} {
                array set doc_elements [nsv_get api_proc_doc "cog_rest::json_object::$object_type"]

                # only deal with return element definitions, as this is response schmema
                if {[exists_and_not_null doc_elements(return)]} {
                    cog_log Notice "ELements for $object_type ... $doc_elements(return)"
                    foreach return_element $doc_elements(return) {
                        lappend return_array_elements $return_element
                    }

                    if {[llength $doc_elements(return)]>0} {
                        # Only append properties if we actually have elements
                        set response_comp($objectType) "      type: object
      properties:
"
                        foreach return_element $doc_elements(return) {
                            set return_var [lindex $return_element 0]
                            append response_comp($objectType) [cog_rest::openapi::response::object_schema -return_element $return_element -object_name "${object_type}_$return_var"]
                        }
                    }
                }
                array unset doc_elements
            }
        }
    }   
}