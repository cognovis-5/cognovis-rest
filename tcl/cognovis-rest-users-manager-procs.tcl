
ad_library {
    REST Procedures for GET calls
    @author malte.sussdorff@cognovis.de
    @author frank.bergmann@project-open.com
    @author michaldrn@wp.pl
}

#---------------------------------------------------------------
# Non ACS-Object "objects"
#---------------------------------------------------------------
namespace eval cog_rest::json_object {

    ad_proc user_with_privilege {} {
        @return user named_id User who is a member of the object
        @return user_privileges json_array profile_privilege array of profile privliges  
    } -

    ad_proc user_with_roles {} {
        @return user json_object user User who is a member of the object
        @return user_roles json_array group array of user roles   
    } -

    ad_proc user_profile {} {
        @return profile named_id Object im_profile name and id of profile
        @return icon string icon of profile
        @return color string color of profile
    } -

    ad_proc profile {} {
        @return profile named_id Object im_profile name and id of profile
        @return profile_privileges json_array profile_privilege array of profile privliges
    } -

    ad_proc profile_privilege {} {
        @return privilege_id number id of privilege (fake id)
        @return privilege_name string name of the privilege
        @return permitted_p boolean is operation permitted
    } -

    ad_proc user_privilege {} {
        @return privilege_id number id of privilege (fake id)
        @return privilege_name string name of the privilege
        @return permitted_p boolean is operation permitted
    } -

    ad_proc profile_privilege_body {} {
        @param object_id integer Object that the user is a member of
        @param privilege_name string name of privilege
    } - 

    ad_proc user_profile_body {} {
        @param user_id number id of user
        @param profile_id number id of profile
    } - 

    ad_proc user_profile_body_put {} {
        @param name string name of the role (e.g. 'Project Managers'  or 'Employee')
        @param icon string icon used to decorate user profile (probably font awesome icon)
        @param color string color used to decorate user profile (e.g. '#CDCDCD' or 'grey')
    } - 
}


ad_proc -public cog_rest::get::user_manager_members {
    { -group_ids "461,463,465,467,471"}
    -rest_user_id:required
} {
    Returns an array of members of a specific group

    @param group_ids object_array im_profile IDs of the groups

    @return members json_array user User who is a member of the object
} {

    set members [list]
    set group_member_ids [db_list members "select member_id from group_member_map g, users u where 
        g.member_id = u.user_id 
        and group_id in ($group_ids)
        and g.member_id not in (
        -- Exclude banned or deleted users
        select	m.member_id
        from	group_member_map m,
            membership_rels mr
        where	m.rel_id = mr.rel_id and
            m.group_id = acs__magic_object_id('registered_users') and
            m.container_id = m.group_id and
            mr.member_state != 'approved')
        group by g.member_id"]
    foreach member_id $group_member_ids {
        lappend members [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $member_id -rest_user_id $rest_user_id]]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::users_profiles {
    -rest_user_id:required
} {
    Endpoint returnig possible user profiles

    @return users_profiles json_array user_profile user profile (e.g. Accounting Employee etc)
} {

    set users_profiles [list]
    set users_profiles_sql "select g.group_id as profile_id, g.group_name as profile_name, c.aux_html1 as color, c.aux_html2 as icon
        from groups g, im_categories c
        where g.group_id = c.aux_int1
        and c.category_type = 'Intranet User Type'
        and g.group_id > 0"
    db_foreach profiles $users_profiles_sql {
        lappend users_profiles [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::users_roles {
    -rest_user_id:required
} {
    Endpoint returning possible users and their roles

    @return users_with_roles json_array user_with_roles users together with roles
} {
    set users_with_roles [list]

    set group_ids [list 461 463 465 467 471]

    set group_member_ids [db_list members "select member_id as groups from group_member_map g, users u where 
        g.member_id = u.user_id 
        and group_id in ([template::util::tcl_to_sql_list $group_ids])
        and g.member_id not in (
        -- Exclude banned or deleted users
        select	m.member_id
        from	group_member_map m,
            membership_rels mr
        where	m.rel_id = mr.rel_id and
            m.group_id = acs__magic_object_id('registered_users') and
            m.container_id = m.group_id and
            mr.member_state != 'approved')
        group by g.member_id"]

    set perm_sql "select user_id, 
            (select string_agg(group_id::varchar, ',') as groups from group_member_map g \
            where g.member_id = u.user_id and g.group_id in ([template::util::tcl_to_sql_list $group_ids])) \
        from users u where user_id in ([template::util::tcl_to_sql_list $group_member_ids]) \
    "

    # Build map with hash key to improve performance
    db_foreach user_groups_filtered $perm_sql {
        set user_roles [list]
        set user [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $user_id -rest_user_id $rest_user_id]]
        foreach group_id [split $groups ","] {
            set group_name [im_name_from_id $group_id]
            lappend user_roles [cog_rest::json_object -object_class "group"] 
        } 
        lappend users_with_roles [cog_rest::json_object]
    }


    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::users_privileges {
    -rest_user_id:required
} {
    Endpoint returning possible users and their privliges (anyting other than 'view', 'read', 'write', 'admin')

    @return users_with_privileges json_array user_with_privilege users together with priveleges
} {
    set users_with_privileges [list]

    array set users_privileges_map {}
    

    set group_ids [list 461 463 465 467 471]
    set privs [im_core_privs ""]

    set group_member_ids [db_list members "select member_id as groups from group_member_map g, users u where 
        g.member_id = u.user_id 
        and group_id in ([template::util::tcl_to_sql_list $group_ids])
        and g.member_id not in (
        -- Exclude banned or deleted users
        select	m.member_id
        from	group_member_map m,
            membership_rels mr
        where	m.rel_id = mr.rel_id and
            m.group_id = acs__magic_object_id('registered_users') and
            m.container_id = m.group_id and
            mr.member_state != 'approved')
        group by g.member_id"]

    set perm_sql "select grantee_id, string_agg(privilege, ',') as permissions from acs_permissions p, groups g 
        where privilege in ([template::util::tcl_to_sql_list $privs])
        and p.grantee_id in ([template::util::tcl_to_sql_list $group_member_ids])
        group by p.grantee_id"


    # Build map with hash key to improve performance
    db_foreach privs_filtered $perm_sql {
        if {$permissions ne "" && $grantee_id ne ""} {
            set users_privileges_map($grantee_id) [split $permissions ","]
        }
    }

    db_foreach get_users "select user_id from users where user_id in ([template::util::tcl_to_sql_list $group_member_ids]) and user_id > 0" {
        set privelege_fake_id 1
        set user_name [im_name_from_id $user_id]
        set user_privileges [list]
        set user_roles [list]
        foreach priv $privs {
            set privilege_id $privelege_fake_id
            set privilege_name $priv
            set permitted_p 0
            if {[info exists users_privileges_map($user_id)] && [lsearch $users_privileges_map($user_id) $priv] > -1} {
                set permitted_p 1
            } 
            lappend user_privileges [cog_rest::json_object -object_class "user_privilege"] 
            incr privelege_fake_id
        }
        lappend users_with_privileges [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::profile_privileges {
    -rest_user_id:required
} {
    Endpoint returning possible users profiles

    @return user_profiles json_array profile All possible profiles with profile privileges
} {

    set user_profiles [list]
    array set profile_privileges_map {}

    set privs [im_core_privs ""]

    set perm_sql "select grantee_id, string_agg(privilege, ',') as permissions from acs_permissions p, groups g 
        where g.group_id = p.grantee_id 
        and privilege in ([template::util::tcl_to_sql_list $privs])
        group by p.grantee_id"
    
    # Build map with hash key to improve performance
    db_foreach privs_filtered $perm_sql {
        set profile_privileges_map($grantee_id) [split $permissions ","]
    }

    set profiles_sql "select distinct g.group_id as profile_id, g.group_name as profile_name from acs_objects o, groups g where g.group_id = o.object_id and o.object_type = 'im_profile'"
    
    db_foreach profile $profiles_sql {
        set privelege_fake_id 1
        set profile_privileges [list]
        foreach priv $privs {
            set privilege_id $privelege_fake_id
            set privilege_name $priv
            set permitted_p 0
            if {[info exists profile_privileges_map($profile_id)] && [lsearch $profile_privileges_map($profile_id) $priv] > -1} {
                set permitted_p 1
            } 
            lappend profile_privileges [cog_rest::json_object -object_class "profile_privilege"] 
            incr privelege_fake_id
        }
        lappend user_profiles [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::profile_privileges {
    -rest_user_id:required
    -object_id:required
    -privilege_name:required
} {
    @param profile_privilege_body request_body privlige name and object id

    @return user_profiles json_object profile All possible profiles with profile privileges
} {

    permission::grant -party_id $object_id -object_id 408 -privilege $privilege_name

    im_permission_flush
    
    set user_profiles [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::profile_privileges -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::profile_privileges {
    -object_id:required
    -privilege_name:required
} {
    @param object_id object 
    @param privilege_name string

    @return errors json_array error Array of errors found
} {
    set errors [list]
    permission::revoke -party_id $object_id -object_id 408 -privilege $privilege_name
    im_permission_flush
    
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::user_profile {
    -rest_user_id:required
    -user_id:required
    -profile_id:required
} {
    @param user_profile_body request_body user and profile ids

    @return result number 1 or 0
} {
     im_profile::add_member -profile_id $profile_id -user_id $user_id
     return 1
}


ad_proc -public cog_rest::delete::user_profile {
    -user_id:required
    -profile_id:required
} {
    @param user_id number id of the user
    @param profile_id number id of the profile

    @return result number 1 or 0
} {
     im_profile::remove_member -profile_id $profile_id -user_id $user_id
     return 1
}


ad_proc -public cog_rest::put::profile {
    -profile_id:required
    { -color ""}
    { -icon ""}
    -rest_user_id:required
} {
    @param profile_id number id of the modified profile
    @param user_profile_body_put request_body 
    @return result number 1 or 0
} {
    set update_vars [list]
    if {$color ne ""} {
        lappend update_vars "aux_html1 = :color"
    }
    if {$icon ne ""} {
        lappend update_vars "aux_html2 = :icon"
    }
    set update_sql "update im_categories set [join $update_vars ", "] where aux_int1 =:profile_id and category_type = 'Intranet User Type'"
    db_dml update_profile $update_sql
    return 1
}