ad_library {
    REST Web Service Component Library
	@author malte.sussdorff@cognovis.de

	build upon work from 
    @author frank.bergmann@project-open.com
}

# -------------------------------------------------------
# HTTP Interface
#
# Deal HTTP parameters, authentication etc.
# -------------------------------------------------------

namespace eval cog_rest {}

namespace eval cog_rest::post {
	ad_proc -private call {} {
		Handler for GET rest calls
	} {
		return [cog_rest::call -operation post]
}	
}
namespace eval cog_rest::get {
	ad_proc -private call {} {
		Handler for GET rest calls
	} {
		return [cog_rest::call -operation get]
	}
}

namespace eval cog_rest::put {
	ad_proc -private call {} {
		Handler for PUT rest calls
	} {
		return [cog_rest::call -operation put]
	}
}

namespace eval cog_rest::delete {
	ad_proc -private call {} {
		Handler for DELETE rest calls
	} {
		return [cog_rest::call -operation delete]
	}
}


ad_proc -private cog_rest::call {
    {-operation get }
    { -rest_user_id ""}
} {
    Handler for REST Calls. Takes the url parameters and urlpieces to return a valid JSON object (or an JSON error)

	THIS ONLY SUPPORTS JSON format

	@param operation Method to use for call

	@return json_string Valid json string
} {
    cog_log Debug "cog_rest::call: Starting"

    # Get the entire URL and decompose into the "rest_otype" 
    # and the "rest_oid" pieces. Splitting the URL on "/"
    # will result in "{} cognovis-rest rest_otype rest_oid":
 
	cog_rest::initialize_endpoint
	
	#---------------------------------------------------------------
	# Authentication and setting of rest_user_id
	#---------------------------------------------------------------
	# Skip authentication for the login endpoint (and pages)
    set pages [list "index" "endpoint-view" "auto-login"]
	set unauthorized_endpoints [list "auth_token" "server_up" "register_party" "register_user" "forgot_password_url"]

	if {[lsearch $unauthorized_endpoints $endpoint]<0 && [lsearch $pages $rest_otype]<0} {
		set rest_user_id [cog_rest::authenticate]
		
		# Security checks
		set alert_p 0
		set alert_p [expr {$alert_p || [im_security_alert_check_integer -location "cog_rest::call: user_id" -value $rest_user_id]}]
		if {"data-source" != $rest_otype} {
	    	set alert_p [expr {$alert_p || [im_security_alert_check_integer -location "cog_rest::call: rest_oid" -value $rest_oid]}]
	    	set alert_p [expr {$alert_p || [im_security_alert_check_alphanum -location "cog_rest::call: rest_otype" -value $rest_otype]}]
		}

		if {$rest_user_id eq 0} {
			cog_rest::error -http_status 401 -message "[_ cognovis-rest.err_access_invalid_user]"
		}

		if {$alert_p} {
		    cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_security_error]"
	    } else {

			# Signin the user to support callbacks
			ad_conn -set untrusted_user_id $rest_user_id
			ad_conn -set user_id $rest_user_id
			ad_conn -set account_status "ok"
			ad_conn -set auth_level "secure"
		}
	} else {
		set rest_user_id ""
	}

	# -------------------------------------------------------
    # Special treatment for /cognovis-rest/ pages
	# Just deliver them without authentication
    # -------------------------------------------------------

    if {"" == $rest_otype} { set rest_otype "index" }

	if {[lsearch $pages $rest_otype]>-1} {
	    set file "/packages/cognovis-rest/www/$rest_otype"

    	set result [ad_parse_template $file]
		doc_return 200 text/html $result
    	return	
	}

	#---------------------------------------------------------------
	# Find out the correct procedure to call and set parameters
	#---------------------------------------------------------------
	set proc_name [cog_rest::endpoint_proc -endpoint $endpoint -operation $operation]

	if {$proc_name eq ""} {
		cog_rest::error -http_status 404 -message "[_ cognovis-rest.err_missing_endpoint]"
	}

	#---------------------------------------------------------------
	# Check for switches of the proc
	#---------------------------------------------------------------
	array set doc_elements [nsv_get api_proc_doc $proc_name]
	if {[info exists doc_elements(switches0)]} {
		set switches $doc_elements(switches0)
	} else {
		set switches $doc_elements(switches)
	}
	array set flags $doc_elements(flags)


	# -------------------------------------------------------
	# Calls the procs to return the json based on method
	#--------------------------------------------------------

	set cmd_line "$proc_name"
	set error_line "$proc_name"

    set request_body [cog_rest::parse::get_request_body]

	foreach switch $switches {
		
		switch $switch {
			rest_user_id - rest_oid {
				set value [set $switch]
			}
			default {
				set value [cog_rest::parse::get_value -switch $switch -proc_name $proc_name -rest_user_id $rest_user_id -request_body $request_body]
			}
		}
				
		if {$value eq "{ERR}"} {
			return
		}
		if {$value eq "undefined"} {
			# Check if it is a required parameter. If yes, throw an error
			# Otherwise do nothing (as the switch does not need to be appended)
			if { [lsearch $flags($switch) "required"] >= 0 } {
				set message "[_ cognovis-rest.err_missing_switch]"
				cog_log -user_id $rest_user_id Warning $message
				cog_rest::error -http_status 400 -message $message -no_abort
				return
			} 				
			continue
		}

		set $switch $value
		if {$value eq ""} {
			append cmd_line " -$switch \"\""
			append error_line " -$switch \"\""			
		} else {
			append cmd_line " -$switch \[set $switch\]"
			append error_line " -$switch [set $switch]"
		}
	}

    switch $operation  {
		get - post - put - delete {
			# Execute the command
			if {[catch {
				set json_string [eval $cmd_line]
			} err_msg]} {
				append err_msg "\nStack Trace:\n"
				append err_msg $::errorInfo
				cog_log -user_id $rest_user_id Error "cog_rest::call: '$error_line' returned an error: $err_msg"
				cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_internal_error]" -no_abort
				return 
			} else {
				if {$operation ne "get"} {
					set fo [open "[acs_root_dir]/log/api.access.log" a]
					set now [clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"]
					if {$rest_user_id eq ""} {
						puts $fo "$now Unauthorized User $error_line"
					} else {
						puts $fo "$now [im_name_from_id $rest_user_id] $error_line"						
					}
					
					close $fo  	
				}
			}		
		}
		default {
			return [cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_unknown_http_request]"]
		}
    }

	if {$json_string ne ""} {
		# Check that json_string is valid json
		if {![util::json::validate $json_string]} {
			cog_log -user_id $rest_user_id Error "$cmd_line \n Server returned an invalid JSON. $json_string"
			cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_invalid_json]"
			return
		}
		
		cog_rest::return_json -json $json_string
	}
}

ad_proc -public cog_rest::authenticate {
} {
	Authenticate the user. Based off im_rest_authenticate

	Does not allow cookie authorization.

	@author malte.sussdorff@cognovis.de based of work from 
	@author frank.bergmann@project-open.com

	@return user_id of the authenticated user
} {
    
    # --------------------------------------------------------
    # Check for HTTP "basic" authorization
	set user_id [cog_rest::authentication::header_auth]

	# --------------------------------------------------------
    # Check for token authentication
	if {$user_id eq ""} {
		set user_id [cog_rest::authentication::token_auth]
	}

    #---------------------------------------------------------------
	# Check for api key
	# API Key is a combination of basic authorization and token
	# where the password is replaced by the token
	#---------------------------------------------------------------
	if {$user_id eq ""} {
		set user_id [cog_rest::authentication::api_key]
	}

	if {$user_id eq ""} {
   		return [cog_rest::error -http_status 401 -message "[_ cognovis-rest.err_no_auth]"] 
	}

	return $user_id
}



# ----------------------------------------------------------------------
# Error Handling
# ----------------------------------------------------------------------

ad_proc -public cog_rest::error {
    { -http_status 404 }
    { -message "" }
	{ -property ""}
	{ -rest_user_id "" }
    -no_abort:boolean
} {
    Returns a suitable REST error message
} {

	set outputheaders [ns_conn outputheaders]
	ns_set put [ns_conn outputheaders] "Access-Control-Allow-Origin" "*"
	ns_set put [ns_conn outputheaders] "Access-Control-Allow-Methods" "GET, POST, PUT, DELETE, OPTIONS"
	ns_set put [ns_conn outputheaders] "Access-Control-Allow-Headers" "DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range"
	ns_set put [ns_conn outputheaders] "Access-Control-Expose-Headers" "Content-Length,Content-Range"

	switch $http_status {
		401 {
			set result "{\"success\": false,\n\"message\": \"[cog_rest::quotejson $message]\"\n}"
		}
		404 {
			set result "{\"success\": false,\n\"message\": \"[cog_rest::quotejson $message]\"\n}"
			# cog_log -user_id $rest_user_id Error "Page not found - $message"	- hide logging until we cleaned out sencha
		}
		500 {
			set result "{\"success\": false,\n\"message\": \"[cog_rest::quotejson $message]\"\n}"
		}
		default {
    		set result "{\"success\": false,\n\"message\": \"[cog_rest::quotejson $message]\"\n}"
		}
	}

	if {[catch {ns_return $http_status "application/json" $result}]} {
	    if {$rest_user_id >0} {
		cog_log -user_id $rest_user_id Error "$result"
	    }
	}

    if {!$no_abort_p} {
		catch {ad_script_abort}
    }
    return 0
}


#---------------------------------------------------------------
# Procs to determine the endpoints
#---------------------------------------------------------------

ad_proc -public cog_rest::endpoints {
	-include_default:boolean
	{ -operation "" }
	{ -package_key "" }
} {
	Return a list of endpoints or "paths" as per OpenAPI connotation

	@param exclude_default This will exclude any object_types which can be handled using cog_rest::get_object_type
	@param operation Limit to those endpoints for which we have the operation. Defaults to any

	@return endpoints list_of_lists with each endpoint providing operation, result and proc_name
} {

	if {$operation eq ""} {
		set operations [list get post put delete]
	} else {
		set operations [list $operation]
	}

	set endpoints [list]
	
	foreach operation $operations {
		foreach proc_name [lsort [info commands "::cog_rest::${operation}::*"]] {

			# Handle custom overwrite procs
			if {![regexp "::cog_rest::${operation}::custom::(.*)" $proc_name match result]} {
				regexp "::cog_rest::${operation}::(.*)" $proc_name match result
			}

			# Exclude the object one.

			set exclude_list [list "call" "object_type"] 
			if {[lsearch $exclude_list $result]<0} {
				# And don't duplicate
				if {[lsearch $endpoints $result]<0} {

					if {$package_key ne ""} {
						# And only include those defined in a specific package
						array set doc_elements [nsv_get api_proc_doc [string trimleft $proc_name "::"]]
			        	set proc_package_key [lindex [split $doc_elements(script) "/"] 1]
						if {$proc_package_key eq $package_key} {
							lappend endpoints $result
						}
					} else {
						lappend endpoints $result
					}
				}
			}
		}

		#---------------------------------------------------------------
		# Append the standard object types
		#---------------------------------------------------------------
		if {$include_default_p} {
			set valid_rest_otypes [util_memoize [list db_list otypes "
 				select	object_type 
				from	im_rest_object_types
  			"]]
			foreach object_type $valid_rest_otypes {
				if {[lsearch $endpoints $object_type]<0} {
					lappend endpoints $object_type
				}
			}
		}
	}
	return $endpoints
}

ad_proc -public cog_rest::endpoint_proc {
	-endpoint:required
	-operation:required
} {
	Return the name of the procedure used for the endpoint

	@param endpoint name of the endpoint
	@param operation any of "GET" "POST" "DELETE" "PUT"
} {

	# Check if the operation has a subtype
	set endpoint_parts [split $endpoint "::"]
	set rest_otype [lindex $endpoint_parts 0]
	set proc_name ""
	
	#---------------------------------------------------------------
	# Find out the correct procedure to call
	#---------------------------------------------------------------
	
	set possible_proc_names [list "cog_rest::${operation}::object_type"]
	
	# Backwards compatability
	# lappend possible_proc_names "im_rest_${operation}_custom_$rest_otype"

	# Check for special implementation
	lappend possible_proc_names "cog_rest::${operation}::$rest_otype" 
	if {[llength $endpoint_parts]>1} {
		lappend possible_proc_names "cog_rest::${operation}::$endpoint"
	}

	# Check for custom(er) implementation
	lappend possible_proc_names "cog_rest::${operation}::custom::$rest_otype" 
	if {[llength $endpoint_parts]>1} {
		lappend possible_proc_names "cog_rest::${operation}::custom::$endpoint"
	}

	# Loop through all possible names. Use the one found last
	# This is the most specific one
	foreach possible_proc $possible_proc_names {
		# We always use a namespace, so info commands needs to have a different usage
		if { [llength [info commands "::${possible_proc}"]]} {
			set proc_name $possible_proc
		}
	}

	# Check the object_type is valid
	if {$proc_name eq "cog_rest::${operation}::object_type"} {		
		# Check that the object_type is valid
	    set valid_rest_otypes [util_memoize [list db_list otypes "
 				select	object_type 
				from	im_rest_object_types
  			"]]
 		if {[lsearch $valid_rest_otypes $rest_otype] < 0} {
			set proc_name ""
		} 
	}
	return $proc_name
}




#---------------------------------------------------------------
# Object Functions to create returns
#---------------------------------------------------------------



ad_proc -public cog_rest::json_object {
    {-proc_name ""}
	{-object_class ""}
} {
    Returns a formated JSON line for the documented return json "Object"
    
    Uses the documented return parameters as variables which to call. Make sure they are
    set to the values you want before calling im_rest_json_element

	
	@author Malte Sussdorff (malte.sussdorff@cognovis.de)

	@param proc_name name of the procdure which is calling us. Not necessary though
	@param object_class name of the "class" of the object. Useful if you are working with nested objects (object within an object) or multiple object Lists . so might be data_ratings

	@return Returns the formatted json string for the object based on the return_json documentation.
} {

	# Do uplevel magic to find out the procedure name
	if {$proc_name eq ""} {
		upvar proc_name_up proc_name_up
		uplevel {
			set proc_name_up [lindex [info level 0] 0]
		}
		set proc_name $proc_name_up
	}
		
    array set doc_elements [nsv_get api_proc_doc $proc_name]
	
	set return_elements [list]
	# Find if we have return elements which are actually an array
	if {$object_class ne ""} {
		if {[nsv_exists api_proc_doc "cog_rest::json_object::$object_class"]} {
			array set doc_elements [nsv_get api_proc_doc "cog_rest::json_object::$object_class"]
			foreach return_element $doc_elements(return) {
				lappend return_elements $return_element
			}
		} else {

			# Check if we have the object_class
			if { [info exists doc_elements(return_${object_class})]} {
				set return_doc "return_${object_class}"
				foreach return_element $doc_elements($return_doc) {
					set return_items [split $return_element]
					set object_name [lindex $return_items 0]
					lappend return_elements $return_element
				}
			} 
		}
	} else {
		foreach return_element $doc_elements(return) {
		    set return_items [split $return_element]
			set object_name [lindex $return_items 0]
			set return_type [lindex $return_items 1]

			switch $return_type {
				json_array - json_object {
					set object_type [lindex $return_items 2]
					if {[nsv_exists api_proc_doc "cog_rest::json_object::$object_type"]} {
						array set doc_elements [nsv_get api_proc_doc "cog_rest::json_object::$object_type"]
						foreach return_element $doc_elements(return) {
							lappend return_elements $return_element
						}
					} else {
						if { [info exists doc_elements(return_${object_name})]} {
							foreach return_element $doc_elements(return_${object_name}) {
								lappend return_elements $return_element
							}
						}
					}
				}
				default {
					# Check if we have submodules for any of them, making them an array with more detailed info
					if { [info exists doc_elements(return_${object_name})]} {
						foreach return_element $doc_elements(return_${object_name}) {
							lappend return_elements $return_element
						}
					}
				}
			}
		}
	}

	callback cog_rest::object_return_element -proc_name $proc_name -object_class $object_class
	
	set jsonObj [new_CkJsonObject]

	foreach return_element $return_elements {
		# The first item is supposed to be the variable
		set return_items [split $return_element]
		set v [lindex $return_items 0]
		set type [lindex $return_items 1]

		# Check if the var is set
		switch $type {
			category - named_id - object - cr_file {
				upvar "${v}_id" a
				upvar rest_user_id rest_user_id
			}
			category_array - object_array - cr_file_array {
				upvar "${v}_ids" a
				upvar rest_user_id rest_user_id
			}
			default {
				upvar $v a
			}
		}
		if {[info exists a]} {
			regsub -all {\n} $a {} a
			regsub -all {\r} $a {} a
			if {$a eq ""} {
				switch $type {
					string {
						CkJsonObject_AddStringAt $jsonObj -1 $v ""
					}
					default {
						CkJsonObject_AddNullAt $jsonObj -1 $v
					}
				}				
				continue
			} 

			switch $type {
				integer {
					CkJsonObject_AddIntAt $jsonObj -1 $v $a
				}

				number {
					# We might need to do a conversion integer vs. numeric....
					CkJsonObject_AddNumberAt $jsonObj -1 $v $a
				}
				boolean {
					if {[string is true $a]} {
						CkJsonObject_AddBoolAt $jsonObj -1 $v 1
					} else {
						CkJsonObject_AddBoolAt $jsonObj -1 $v 0						
					}
				}
				string {
					CkJsonObject_AddStringAt $jsonObj -1 $v $a
				}
				category {
					# we return an object for the category
					upvar rest_user_id rest_user_id	
					
					# We only deal with a single category, so only return the first value
					set category_id [lindex $a 0]

					set catObj [new_CkJsonObject]
					CkJsonObject_AddIntAt $catObj -1 "id" $category_id
					CkJsonObject_AddStringAt $catObj -1 "name" [im_category_from_id -current_user_id $rest_user_id $category_id]
					CkJsonObject_AddStringAt $catObj -1 "icon_or_color" [im_category_icon_or_color -category_id $category_id]
					CkJsonObject_AddObjectCopyAt $jsonObj -1 $v $catObj
					delete_CkJsonObject $catObj
				}
				category_array {
					# we return an object for the category
					upvar rest_user_id rest_user_id	

					CkJsonObject_AddArrayAt $jsonObj -1 $v
					set catArr [CkJsonObject_ArrayAt $jsonObj [expr [CkJsonObject_get_Size $jsonObj] - 1]]
					
					foreach category_id $a {
						CkJsonArray_AddObjectAt $catArr -1
						set catObj [CkJsonArray_ObjectAt $catArr [expr [CkJsonArray_get_Size $catArr] - 1]]
	
						CkJsonObject_AddIntAt $catObj -1 "id" $category_id
						CkJsonObject_AddStringAt $catObj -1 "name" [im_category_from_id -current_user_id $rest_user_id $category_id]
						CkJsonObject_AddStringAt $catObj -1 "icon_or_color" [im_category_icon_or_color -category_id $category_id]
						
						delete_CkJsonObject $catObj
					}
					delete_CkJsonArray $catArr
				}
				object - named_id {
					# Check for the name
					upvar ${v}_name object_id_name
					if {![info exists object_id_name]} {
						set value [im_name_from_id $a]
					} else {
						set value [set object_id_name]
						unset object_id_name
					}
					set objObj [new_CkJsonObject]
					CkJsonObject_AddIntAt $objObj -1 "id" $a
					CkJsonObject_AddStringAt $objObj -1 "name" $value
					CkJsonObject_AddObjectCopyAt $jsonObj -1 $v $objObj
					delete_CkJsonObject $objObj
				}
				object_array {		
					CkJsonObject_AddArrayAt $jsonObj -1 $v
					set objArr [CkJsonObject_ArrayAt $jsonObj [expr [CkJsonObject_get_Size $jsonObj] - 1]]
					foreach object_id $a { 
						CkJsonArray_AddObjectAt $objArr -1
						set objObj [CkJsonArray_ObjectAt $objArr [expr [CkJsonArray_get_Size $objArr] - 1]]
						CkJsonObject_AddIntAt $objObj -1 "id" $object_id
						CkJsonObject_AddStringAt $objObj -1 "name" [im_name_from_id $object_id]
						delete_CkJsonObject $objObj
					}
					delete_CkJsonArray $objArr
				}
				json_array {
					# Build the array if it is properly define
					# The calling environment should have a list named so it behaves just like return_array.
					upvar $v return_objects_list
					regsub -all {\\n} $return_objects_list {} return_objects_list
					CkJsonObject_AddArrayAt $jsonObj -1 $v
					set objArr [CkJsonObject_ArrayAt $jsonObj [expr [CkJsonObject_get_Size $jsonObj] - 1]]
					foreach return_object $return_objects_list {
						set objObj [new_CkJsonObject]
						CkJsonObject_Load $objObj $return_object
						CkJsonArray_AddObjectCopyAt $objArr -1 $objObj
						delete_CkJsonObject $objObj
					}
					delete_CkJsonArray $objArr
				}
				json_object {
					# Don't quote
					set Obj [new_CkJsonObject]
					CkJsonObject_Load $Obj $a
					CkJsonObject_AddObjectCopyAt $jsonObj -1 $v $Obj
					delete_CkJsonObject $Obj
				}
				cr_file {
					# Get the cr_file info. 
					set item_id [content::revision::item_id -revision_id $a]
					if {$item_id eq ""} {
						set revision_id $a
					} else {
						set revision_id [content::item::get_best_revision -item_id $item_id]
					}

			        db_1row revision_info "select title as name, content_length as size, content as cr_path  from cr_revisions where revision_id = :revision_id"

					set crObj [new_CkJsonObject]
					CkJsonObject_AddIntAt $crObj -1 "id" $revision_id
					CkJsonObject_AddStringAt $crObj -1 "name" $name
					CkJsonObject_AddStringAt $crObj -1 "sname" $cr_path
					CkJsonObject_AddStringAt $crObj -1 "status" "server"
					CkJsonObject_AddStringAt $crObj -1 "sizetext" "[expr $size/1024] Kb"
					CkJsonObject_AddObjectCopyAt $jsonObj -1 $v $crObj
					delete_CkJsonObject $crObj
				}
				cr_file_array {
					CkJsonObject_AddArrayAt $jsonObj -1 $v
					set crArr [CkJsonObject_ArrayAt $jsonObj [expr [CkJsonObject_get_Size $jsonObj] - 1]]
					foreach id $a {
						# Get the cr_file info. 
						set item_id [content::revision::item_id -revision_id $id]
						if {$item_id eq ""} {
							set revision_id $id
						} else {
							set revision_id [content::item::get_best_revision -item_id $item_id]
						}

						db_1row revision_info "select title as name, content_length as size  from cr_revisions where revision_id = :revision_id"
						
						CkJsonArray_AddObjectAt $crArr -1
						set crObj [CkJsonArray_ObjectAt $crArr [expr [CkJsonArray_get_Size $crArr] - 1]]
						CkJsonObject_AddIntAt $crObj -1 "id" $revision_id
						CkJsonObject_AddStringAt $crObj -1 "name" $name
						CkJsonObject_AddStringAt $crObj -1 "sname" $revision_id
						CkJsonObject_AddStringAt $crObj -1 "status" "server"
						CkJsonObject_AddNumberAt $crObj -1 "size" $size
						delete_CkJsonObject $crObj
					}
				}
				named_id_deprecated {
					# Check for the name
					upvar ${v}_name named_id_name
					if {![info exists named_id_name]} {
						set value [im_name_from_id $a]
					} else {
						set value [set named_id_name]
						unset named_id_name
					}

					set namedObj [new_CkJsonObject]
					CkJsonObject_AddIntAt $namedObj -1 "id" $a
					CkJsonObject_AddStringAt $namedObj -1 "name" $value
					CkJsonObject_AddObjectCopyAt $jsonObj -1 $v $namedObj
					delete_CkJsonObject $namedObj
				}
				default {
					# Quote by default 
					CkJsonObject_AddStringAt $jsonObj -1 $v $a
				}
			}
			
		} else {
			cog_log Error "cog_rest::json_object for $proc_name can't reference $v"
		}
    }
	
	return 	[CkJsonObject_emit $jsonObj]
}

ad_proc -public cog_rest::json_object_new {
    {-proc_name ""}
	{-object_class ""}
} {
    Returns a formated JSON line for the documented return json "Object"
    
    Uses the documented return parameters as variables which to call. Make sure they are
    set to the values you want before calling im_rest_json_element

	
	@author Malte Sussdorff (malte.sussdorff@cognovis.de)

	@param proc_name name of the procdure which is calling us. Not necessary though
	@param object_class name of the "class" of the object. Useful if you are working with nested objects (object within an object) or multiple object Lists . so might be data_ratings

	@return Returns the formatted json string for the object based on the return_json documentation.
} {
	upvar rest_user_id rest_user_id	
	if {![info exists rest_user_id]} {
		set rest_user_id [auth::get_user_id]
	}
	
	# Do uplevel magic to find out the procedure name
	if {$proc_name eq ""} {
		upvar proc_name_up proc_name_up
		uplevel {
			set proc_name_up [lindex [info level 0] 0]
		}
		set proc_name $proc_name_up
	}
		
    array set doc_elements [nsv_get api_proc_doc $proc_name]
	
	set return_elements [list]
	# Find if we have return elements which are actually an array
	if {$object_class ne ""} {
		if {[nsv_exists api_proc_doc "cog_rest::json_object::$object_class"]} {
			array set doc_elements [nsv_get api_proc_doc "cog_rest::json_object::$object_class"]
			foreach return_element $doc_elements(return) {
				lappend return_elements $return_element
			}
		} else {

			# Check if we have the object_class
			if { [info exists doc_elements(return_${object_class})]} {
				set return_doc "return_${object_class}"
				foreach return_element $doc_elements($return_doc) {
					set return_items [split $return_element]
					set object_name [lindex $return_items 0]
					lappend return_elements $return_element
				}
			} 
		}
	} else {
		foreach return_element $doc_elements(return) {
		    set return_items [split $return_element]
			set object_name [lindex $return_items 0]
			set return_type [lindex $return_items 1]

			switch $return_type {
				json_array - json_object {
					set object_type [lindex $return_items 2]
					if {[nsv_exists api_proc_doc "cog_rest::json_object::$object_type"]} {
						array set doc_elements [nsv_get api_proc_doc "cog_rest::json_object::$object_type"]
						foreach return_element $doc_elements(return) {
							lappend return_elements $return_element
						}
					} else {
						if { [info exists doc_elements(return_${object_name})]} {
							foreach return_element $doc_elements(return_${object_name}) {
								lappend return_elements $return_element
							}
						}
					}
				}
				default {
					# Check if we have submodules for any of them, making them an array with more detailed info
					if { [info exists doc_elements(return_${object_name})]} {
						foreach return_element $doc_elements(return_${object_name}) {
							lappend return_elements $return_element
						}
					}
				}
			}
		}
	}

	callback cog_rest::object_return_element -proc_name $proc_name -object_class $object_class
	
	set elements [list]
	foreach return_element $return_elements {
		# The first item is supposed to be the variable
		set return_items [split $return_element]
		set v [lindex $return_items 0]
		set type [lindex $return_items 1]

		# Check if the var is set
		switch $type {
			category - named_id - object - cr_file {
				upvar "${v}_id" a
				upvar rest_user_id rest_user_id
			}
			category_array - object_array - cr_file_array {
				upvar "${v}_ids" a
				upvar rest_user_id rest_user_id
			}
			default {
				upvar $v a
			}
		}
		if {[info exists a]} {
			switch $type {
				object - named_id {
					# Support for already set variables
					upvar ${v}_name object_id_name
					if {[info exists object_id_name]} {
						if {$object_id_name eq ""} {
							set a "null"
						} else {
							set a "{\"id\": $a, \"name\": \"[set object_id_name]\"}"
						}
						set type object
						unset object_id_name
					} elseif {![string is integer -strict $a] } {                    
						set type object
					} else {
						set type named_id
					}
				}
			}
			set element [list $v $type $a]
			lappend elements $element	
		} else {
			cog_log Error "cog_rest::json_object for $proc_name can't reference $v"
		}
    }
	
	return 	[cog::json::object -elements $elements -user_id $rest_user_id]
}


ad_proc -public cog_rest::json_objects {
	-sql:required
	-rest_user_id:required
	{ -valid_vars "" }
	{ -data_type_arr "" }
	{ -permission_proc "" }
	{ -rest_oid ""}
	{ -rest_otype ""}
	{ -additional_var_procs "" }
	{ -limit "" }
} {
	Returns a list of json_objects

	@param sql SQL Query to use for loading the data
	@param valid_vars List of valid vars to return
	@param permission_proc Procedure to check permissions with against each single line
	@param format format in which to return the json objects. might be html or json
	@param rest_oid Rest Object ID requested. Indicates we should only return a single object....
	@param rest_user_id UserID for this rest call.
	@param additional_var_procs List with potentially multiple variable_name & procedure to get it list. It is a list of var proc.
	@param limit Amount of objects to return at max
} {


	# Store the result especially in case of HTML
	set result ""

	set json_list [list]
	set denied_objects [list]
	
	# Save keep the originally supplied rest_oid
	set org_rest_oid $rest_oid

	if {$data_type_arr ne ""} {
		# Get the datatypes
		upvar $data_type_arr data_types
	}
	
	# Find the valid vars if they are not provided
	if {$valid_vars eq ""} {
		# Do uplevel magic to find out the procedure name
		upvar proc_name_up proc_name_up
		uplevel {
			set proc_name_up [lindex [info level 0] 0]
		}
		set proc_name $proc_name_up
	    array set doc_elements [nsv_get api_proc_doc $proc_name]

		# Find if we have return elements which are actually an array
		if { [info exists doc_elements(return)] } {
			foreach return_element $doc_elements(return) {
				set return_items [split $return_element]
				set object_name [lindex $return_items 0]
				if { [info exists doc_elements(return_${object_name})]} {
					foreach return_element $doc_elements(return_${object_name}) {
						# The first item is supposed to be the variable
						set return_items [split $return_element]
						set v [lindex $return_items 0]
						lappend valid_vars $v
						set data_types($v) [lindex $return_items 1]
					}
				}
			}
		}
	}

    db_foreach objects $sql {
        # Skip objects with empty object name
        if {"" == $object_name} { 
            cog_log Error "cog_rest::get_object_type: Skipping object #$rest_oid because object_name is empty."
            continue
        }

		# Limit to the number of objects
		if {$limit ne "" && [llength $json_list]>$limit} {
			continue
		}

		# Permission_proc thing probably needs more debugging and testing.
		if {$permission_proc ne ""} {
          	cog_log Debug "cog_rest::get_object_type: Checking for individual permissions: $permission_proc $rest_user_id $rest_oid"
           	eval "$permission_proc $rest_user_id $rest_oid view_p read_p write_p admin_p"

			# Apparently no read access... so continue
        	if {!$read_p} { 
				# Append the object_id to denied objects
				lappend denied_objects $rest_oid
				continue 
			}
		}

		# Set additional variables based on the proc
		if {$additional_var_procs ne ""} {
			foreach additional_var_proc $additional_var_procs {
				set variable_name [lindex $additional_var_proc 0]
				set procedure_to_exec [lindex $additional_var_proc 1]
				set $variable_name [eval $procedure_to_exec]
			}
		}

        set json_object_list [list]
        set json_attributes ""
        foreach v $valid_vars {
            set value [set $v]

            # Correctly return NULL
            if {$value eq ""} {
                append json_attributes ", \"$v\": null"
            } else {

                # Check for the datatype of the var.
                switch $data_types($v) {
                    int4 - int2 - float - number - float8 - numeric {
                        append json_attributes ", \"$v\": $value"
                    }
                    bpchar - boolean - bool {
                        if {[string is boolean $value]} {
                            if {$value} {
                                append json_attributes ", \"$v\": true"
                            } else {
                                append json_attributes ", \"$v\": false"
                            }
                        } else {
                            # Unsure what this is
                            if {[string is double $value]} {
                                # number
                                append json_attributes ", \"$v\": $value"
                            } else {
								append json_attributes ", \"$v\": \"[cog_rest::quotejson $value]\""
                            }  
                        }
                    }
                    default {
                        # Treat everything as a string by default
                        append json_attributes ", \"$v\": \"[cog_rest::quotejson $value]\""
                    }
                }
			}
		}
		lappend json_list "{\"id\": $rest_oid, \"object_name\": \"[cog_rest::quotejson $object_name]\"$json_attributes}"
    }

	if {$permission_proc ne "" && [llength $denied_objects]>0 && [llength $json_list] eq 0} {
		if {$org_rest_oid ne ""} {
			set message "[_ cognovis-rest.err_denied_access_object_id]"
		} else {
			set message "[_ cognovis-rest.err_denies_access_object_type]"
		}
		return [cog_rest::error -http_status 401 -message $message -no_abort]
	} else {
		return $json_list
	}
}

ad_proc -public cog_rest::json_object_from_list {
    -objects
} {
    Returns a formated JSON line for the documented return json "Object"
    
    Uses the passed objects to build json object
    
	@author Malte Sussdorff (malte.sussdorff@cognovis.de)

    @param objects list of objects which we want to turn into json
} {
    set final_json ""
    if {[llength objects]>0} {
	    set value_json_strings [list]
	    foreach single_object $objects {
	    	set value_json_strings ""
            array set single_obj_arr $single_object
            foreach single_obj_key [array names single_obj_arr] {
            	set obj_name $single_obj_key
            	set value $single_obj_arr($single_obj_key)
	            regsub -all {\n} $obj_name {\n} obj_name
	            regsub -all {\r} $obj_name {} $obj_name   
	            lappend value_json_strings "\"$obj_name\": \"[cog_rest::quotejson $value]\""  	
            }
            lappend final_json "\{[join $value_json_strings ", "]\}"
	    }    
	    return "[join $final_json ", "]"
    } else {
	    return ""
    }
}

ad_proc -public cog_rest::object_type_columns { 
    {-deref_p 0}
    {-include_acs_objects_p 1}
    -rest_otype:required
	{-data_type_arr ""}
	{-default_arr ""}
	{-required_arr ""}
} {
    Returns a list of all columns for a given object type.
	Sets the data type array with the datatype for correct translation in to json

	@param deref_p Should we return the deref columns?
	@param rest_otype Objecttype for which to return the columns
	@param data_type_arr name of the array into which we write the datatype for each column
	@param default_arr name of the array holding the default_value for the column
	@param required_arr name of the array which is set to 1, if the column is required (not nullable)

	@author malte.sussdorff@cognovis.de
} {
    set super_types [im_object_super_types -object_type $rest_otype]
    if {!$include_acs_objects_p} {
		# Exclude base tables if not necessary
		set super_types [lsearch -inline -all -not -exact $super_types acs_object]
		set super_types [lsearch -inline -all -not -exact $super_types im_biz_object]
    }

    # Get a list of dereferencing functions
    if {$deref_p} {
		array set dynfield_hash [im_rest_deref_plpgsql_functions -rest_otype $rest_otype]
    }

    # ---------------------------------------------------------------
    # Construct a SQL that pulls out all tables for an object type,
    # plus all table columns via user_tab_colums.
    set columns_sql "
	select distinct
		lower(isc.column_name) as column_name,
		lower(isc.table_name) as table_name,
		lower(isc.data_type) as data_type,
		lower(isc.column_default) as column_default,
		lower(isc.is_nullable) as is_nullable
	from
		information_schema.columns isc
	where
		(-- check the main tables for all object types
		lower(isc.table_name) in (
			select	lower(table_name)
			from	acs_object_types
			where	object_type in ('[join $super_types "', '"]')
		) OR
		-- check the extension tables for all object types
		lower(isc.table_name) in (
			select	lower(table_name)
			from	acs_object_type_tables
			where	object_type in ('[join $super_types "', '"]')
		)) and
		-- avoid returning 'format' because format=json is part of every query
		lower(isc.column_name) not in ('format', 'rule_engine_old_value')
    "

    set columns [list]
	if {$data_type_arr ne ""} {
		upvar $data_type_arr type_arr
	}
	if {$default_arr ne ""} {
		upvar $default_arr def_arr
	}
	if {$required_arr ne ""} {
		upvar $required_arr req_arr
	}

    db_foreach columns $columns_sql {
		lappend columns $column_name
		# Handle the datatypes
		if {$data_type_arr ne ""} {
			# Append the data type to the array
			set type_arr($column_name) $data_type
		}
		if {$default_arr ne ""} {
			set def_arr($column_name) $column_default
		}
		if {$required_arr ne ""} {
			if {[string tolower $is_nullable] eq "no"} {
				set req_arr($column_name) 1
			} else {
				set req_arr($column_name) 0
			}
		}

		# Add potential deref functions
		set key "$table_name-$column_name"
		if {[info exists dynfield_hash($key)]} {
		    lappend columns ${column_name}_deref
			if {$data_type_arr ne ""} {
				set type_arr(${column_name}_deref) varchar
			}
		}
    }

	# get the basic info from object type
	if {[db_0or1row object_info "select status_category_type, type_category_type,status_column, type_column from acs_object_types where object_type = :rest_otype"]	&&$data_type_arr ne ""} {
		#overwrite to category if we know it
		if {$type_column ne ""} {
			set type_arr($type_column) "category"
		}
	}

    return $columns
}


ad_proc -public cog_rest::return_array {
	{-success_p 1}
	-pure_array:boolean
} {
    Returns a standard JSON based on the @return parameter of the calling procedure.
	It takes the @return and wraps the list into an array

	DO NOT USE THIS if you don't want to return an array

    Wrapper with the minimum amount of information

    success: true / false
    total: Total_number of objects

	@param success_p do we return a success (was the call successful)
	@param pure_array Will only return the array, nothing in addition.
} {
    # Get the calling procedure

    upvar proc_name_up proc_name_up
	uplevel {
		set proc_name_up [lindex [info level 0] 0]
	}
	set proc_name $proc_name_up
    
    array set doc_elements [nsv_get api_proc_doc $proc_name]

    if { [info exists doc_elements(return)] } {
        set return_array_name [lindex [lindex $doc_elements(return) 0] 0]
    } else {
        set return_array_name "data"
    } 

    upvar $return_array_name return_objects_list

	if {$success_p} {
		set success true
	} else {
		set success false
	}

	if {[info exists return_objects_list]} {
		if {$pure_array_p} {
			set result "\[\n[join $return_objects_list ,]\n\]"
		} else {
    		set result "{\"success\": $success, \"total\":[llength $return_objects_list], \n\"$return_array_name\": \[\n[join $return_objects_list ,]\n\]\n}"
		}
	    return $result
	} else {
		cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_missing_array_set]"
	}

}


ad_proc -public cog_rest::json_response {
} {
    Returns a standard JSON based on the @return parameter of the calling procedure.
	It takes the @return and wraps the list into an array

} {
    # Get the calling procedure

    upvar proc_name_up proc_name_up
	uplevel {
		set proc_name_up [lindex [info level 0] 0]
	}
	set proc_name $proc_name_up
    
    array set doc_elements [nsv_get api_proc_doc $proc_name]
	set return_item [lindex $doc_elements(return) 0]
	set return_name [lindex $return_item 0]
	set return_type [lindex $return_item 1]
	set return_object_type [lindex $return_item 2]

    upvar $return_name return_objects_list

	if {[info exists return_objects_list]} {
		switch $return_type {
			json_array {
				set result "\[\n[join $return_objects_list ",\n"]\n\]"
			}
			json_object {
				if {[llength $return_objects_list] > 1} {
					set result "[lindex $return_objects_list 0]"
				} else {
					set result $return_objects_list
				}
			}
			default {
				cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_proc_miss_return_type]"
			}
		}
	    return $result
	} else {
		cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_proc_miss_return_array]"
	}

}

#---------------------------------------------------------------
# Formatting functions
#---------------------------------------------------------------


ad_proc -public cog_rest::convert_to_yaml_string {
	-string_from:required
} {
	Returns a cleaned version of the string for inclusion in YAML code 

	@param string String to be converted
} {
    set string_to [util_convert_line_breaks_to_html $string_from]
    regsub -all {\n} $string_to {} string_to
    regsub -all {'} $string_to {''} string_to
	regsub -all {\{} $string_to {} string_to
	regsub -all {\}} $string_to {} string_to
	regsub -all {\[} $string_to {} string_to
	regsub -all {\]} $string_to {} string_to
	return [cog_rest::quotejson $string_to]
}

ad_proc -public cog_rest::quotejson { 
	str 
} {
    Quote a JSON string. In particular this means escaping
    single and double quotes, as well as new lines, tabs etc.

	@author malte.sussdorff@cognovis.de
    @author Frank Bergmann
} {

    set chars_to_be_escaped_list [list \
				      "\"" "\\\"" \\ \\\\ \b \\b \f \\f \n \\n \r \\r \t \\t \
				      \x00 \\u0000 \x01 \\u0001 \x02 \\u0002 \x03 \\u0003 \
				      \x04 \\u0004 \x05 \\u0005 \x06 \\u0006 \x07 \\u0007 \
				      \x0b \\u000b \x0e \\u000e \x0f \\u000f \x10 \\u0010 \
				      \x11 \\u0011 \x12 \\u0012 \x13 \\u0013 \x14 \\u0014 \
				      \x15 \\u0015 \x16 \\u0016 \x17 \\u0017 \x18 \\u0018 \
				      \x19 \\u0019 \x1a \\u001a \x1b \\u001b \x1c \\u001c \
				      \x1d \\u001d \x1e \\u001e \x1f \\u001f \x7f \\u007f \
				      \x80 \\u0080 \x81 \\u0081 \x82 \\u0082 \x83 \\u0083 \
				      \x84 \\u0084 \x85 \\u0085 \x86 \\u0086 \x87 \\u0087 \
				      \x88 \\u0088 \x89 \\u0089 \x8a \\u008a \x8b \\u008b \
				      \x8c \\u008c \x8d \\u008d \x8e \\u008e \x8f \\u008f \
				      \x90 \\u0090 \x91 \\u0091 \x92 \\u0092 \x93 \\u0093 \
				      \x94 \\u0094 \x95 \\u0095 \x96 \\u0096 \x97 \\u0097 \
				      \x98 \\u0098 \x99 \\u0099 \x9a \\u009a \x9b \\u009b \
				      \x9c \\u009c \x9d \\u009d \x9e \\u009e \x9f \\u009f \
				     ]

    set str [string map $chars_to_be_escaped_list $str]

	return $str
}

ad_proc -public cog_rest::sort_and_paginate {
	{-pagination ""}
} {
	Generate a SQL clause for PostgreSQL to support pagination and sorting. 

	@param pagination comma separated list of pagination information
} {
	set return_sql ""
	set pagination_list [split $pagination ","]

	array set pagination_array $pagination_list
    
	# Only append ordering if property is provided
	if {[info exists pagination_array(property)]} {
		set property $pagination_array(property)

		if {[info exists pagination_array(direction)]} {
			set direction [string toupper $pagination_array(direction)]
		} else {
			# Always go for latest first, just in case
			set direction "DESC"
		}
		#Perform security checks on the sorters
		if {![regexp {} $property match]} { 
			return ""
		}
		if {[lsearch {DESC ASC} $direction] < 0} { 
			return ""
		}

		append return_sql "order by $property $direction\n"
	}

	# Append pagination "LIMIT $limit OFFSET $start" to the sql.
	if {[info exists pagination_array(limit)]} {
		set limit $pagination_array(limit)
		if {$limit ne ""} {
			append return_sql "LIMIT $limit\n"
		}
	}
	if {[info exists pagination_array(start)]} {
		set start $pagination_array(start)
		if {$start ne ""} {
			append return_sql "OFFSET $start\n"
		}
	}
	return $return_sql
}


ad_proc -public cog_rest::wiki_url {
	-object_type 
} {
	Returns the URL to the wiki for the object_type
	in Case we have one
} {
	# ]project-open[ wikis
	array set wiki_hash {
			object_type_im_indicator 1
			object_type_acs_attribute 1
			object_type_acs_object 1
			object_type_acs_object_type 1
			object_type_acs_permission 1
			object_type_acs_privilege 1
			object_type_acs_rel 1
			object_type_apm_package 1
			object_type_cal_item 1
			object_type_calendar 1
			object_type_group 1
			object_type_im_biz_object 1
			object_type_im_category 1
			object_type_im_company 1
			object_type_im_component_plugin 1
			object_type_im_conf_item 1
			object_type_im_cost 1
			object_type_im_cost_center 1
			object_type_im_dynfield_attribute 1
			object_type_im_employee 1
			object_type_im_expense 1
			object_type_im_expense_bundle 1
			object_type_im_forum_topic 1
			object_type_im_forum_topic_name 1
			object_type_im_fs_file 1
			object_type_im_hour 1
			object_type_im_indicator 1
			object_type_im_invoice 1
			object_type_im_invoice_item 1
			object_type_im_material 1
			object_type_im_menu 1
			object_type_im_office 1
			object_type_im_payment 1
			object_type_im_profile 1
			object_type_im_project 1
			object_type_im_report 1
			object_type_im_ticket 1
			object_type_im_ticket_ticket_rel 1
			object_type_im_timesheet_invoice 1
			object_type_im_timesheet_price 1
			object_type_im_timesheet_task 1
			object_type_im_user_absence 1
			object_type_im_view 1
			object_type_object 1
			object_type_party 1
			object_type_person 1
			object_type_user 1
		}
	
	set wiki_key "object_type_$object_type"
	regsub -all {_} $object_type {-} object_type_dashes
	if {[info exists wiki_hash($wiki_key)]} {
		set object_wiki_url "http://www.project-open.com/en/object-type-$object_type_dashes"
	} else {
		set object_wiki_url ""
	}
}
