ad_library {
    REST Procedures for Notes and other note like services
    @author malte.sussdorff@cognovis.de
}


namespace eval cog_rest::json_object {
    ad_proc note {} {
        @return creator json_object user Information about the person who added the note
        @return note json_object cognovis_object Note object information
        @return note_content string HTML of the note
        @return creation_date date-time Time when the note was created
    } - 

    ad_proc note_body {} {
        @param note string HTML of the note
        @param note_type_id category "Intranet Notes Type" What kind of note are we creating. Will default to default
        @param note_status_id category "Intranet Notes Status" What status is the note going to be in. Typically active. Set to deleted instead of delete endpoint (unless you want to purge)
    } - 

}

ad_proc -public cog_rest::get::note {
    { -object_id "" }
    { -note_id "" }
    -rest_user_id:required
    {-include_deleted_p 0}
} {
    Return the notes for an object

    @param note_id object im_note::read note which we want to retrieve
    @param object_id object *::read object for which we want to retrieve the notes
    @param include_deleted_p boolean Should we included deleted notes?

    @return notes json_array note Notes for the object as an array with user information
} {
    set notes [list]
    set where_clause_list [list "n.note_id = o.object_id"]
    if {$note_id ne ""} {
        lappend where_clause_list "n.note_id = :note_id"
    }

    if {$object_id ne ""} {
        lappend where_clause_list "n.object_id = :object_id"
    }

    db_foreach notes "select n.note_id, note as note_content,
            to_char(o.creation_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as creation_date,
            o.creation_user
        from im_notes n, acs_objects o
        where [join $where_clause_list " and "]
    " {
        set creator [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $creation_user -rest_user_id $rest_user_id]]
        catch {set note_content [template::util::richtext::get_property html_value $note_content]}
        regsub -all {\n} $note_content {<br />} note_content
        set note [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $note_id]
        lappend notes [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::note {
    -rest_user_id:required
    -note:required
    -object_id:required
    { -note_type_id "" }
    { -note_status_id "" }
} {
    Create a new note
    
    @param object_id object *::read Object we want to attach the note to
    @param note_body request_body The note information we want to provide


    @return single_note json_object note Notes for the object as an array with user information
} {
    set note_id [cog::note::add \
        -user_id $rest_user_id \
        -note $note \
        -object_id $object_id \
        -note_type_id $note_type_id \
        -note_status_id $note_status_id]

    # Handle PM's permissions
    permission::grant -party_id [im_profile_project_managers] -object_id $note_id -privilege "read"
    permission::grant -party_id [im_profile_project_managers] -object_id $note_id -privilege "write"
    permission::grant -party_id [im_profile_project_managers] -object_id $note_id -privilege "admin"
    # Handle Employees
    permission::grant -party_id [im_profile_employees] -object_id $note_id -privilege "read"
    permission::grant -party_id [im_profile_employees] -object_id $note_id -privilege "write"
    permission::grant -party_id [im_profile_employees] -object_id $note_id -privilege "admin"

    set single_note [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::note -note_id $note_id -rest_user_id $rest_user_id]]
    
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::note {
    -rest_user_id:required
    -note_id:required
    { -note "" }
    { -note_type_id "" }
    { -note_status_id "" }
} {
    @param note_id object im_note::write Note we want to update
    @param note_body request_body The note information we want to provide

    @return single_note json_object note Notes for the object as an array with user information
} {
    
    set update_list [list]
    if {$note ne ""} {
        lappend update_list "note = :note"
    }

    if {$note_status_id ne ""} {
        lappend update_list "note_status_id = :note_status_id"
    }

    if {$note_type_id ne ""} {
        lappend update_list "note_type_id = :note_type_id"
    }

    if {[llength $update_list]>0} {
        db_dml update_note "update im_notes set [join $update_list " , "] where note_id = :note_id"
    }
    set single_note [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::note -note_id $note_id -rest_user_id $rest_user_id]]
    
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::note {
    -rest_user_id:required
    -note_id:required
} {
    Purge a note from the database 

    @param note_id object im_note::admin Note we want to purge from the database

    @return errors json_array error Array of errors found
} {
    set errors [list]

    im_note_nuke -current_user_id $rest_user_id $note_id
    return [cog_rest::json_response]
}

