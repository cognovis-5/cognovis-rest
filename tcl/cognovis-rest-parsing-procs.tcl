ad_library {
    REST Web Service Component Library Parsing Functions
	@author malte.sussdorff@cognovis.de

}

#---------------------------------------------------------------
# Parsing procedures
#---------------------------------------------------------------

namespace eval cog_rest::parse {

    ad_proc -public get_param_value {
        -switch:required
        -proc_name:required
        { -request_body "" }
    } {
        Get the parameter value unparsed
    } {
        # First try with naviserver natively
        set values [ns_querygetall $switch "undefined"]
        if {$values eq "undefined"} {

            # Try query variables
            set query [ns_conn query]
            set query_values [list]
            if {$query ne ""} {
                foreach {key value} [ns_set array [ns_parsequery $query]] {
                    if {$key eq $switch} {
                        if {$value ne ""} {
                            lappend query_values $value
                        }
                    }
                }
            }
            if {[llength $query_values]>0} {
                set values $query_values
            }
        }

        if {$values eq "undefined"} {
            
            set form [ns_conn form]
            set form_values [list]
            if {$form ne ""} {
                foreach {key value} [ns_set array $form] {
                    if {$key eq $switch} {
                        lappend form_values $value
                    }
                }
            }
            if {[llength $form_values]>0} {
                set values $form_values
            }
        }

        if {$values eq "undefined" && $request_body ne ""} {
            # Try with requestBody
            set values [cog_rest::parse::values_from_json_object -key $switch -json_object $request_body]
        }
        return $values
    }
    
    ad_proc -public get_request_body {
    } {
        Returns the json_object from the body if it exists
    } {
        set json_object ""
        set contenttype  [ns_set iget [ns_conn headers] Content-Type]
        switch $contenttype {		
            application/json {
                set content [ns_getcontent -as_file false -binary false]
                array set passed_json [util::json::parse $content]
                set json_object $passed_json(_object_)
            }
        }

        return $json_object     
    }

    ad_proc -public get_value {
        -switch:required
        -proc_name:required
        -rest_user_id:required
        { -request_body "" }
    } {
        Returns the value(s) provided in QUERY or POST parsed based on the procedure

    } {
        set values [cog_rest::parse::get_param_value -switch $switch -proc_name $proc_name -request_body $request_body]
        set switch_type [cog_rest::parse::switch_type -switch $switch -proc_name $proc_name]

        if {$values eq "undefined"} {return "undefined"}
        
        # Clean out null for non string

        if {$switch_type ne "string"} {
            if {$values eq "null"} {
                set values ""
            }
        }

        switch $switch_type {
            category_array - object_array - integer_array {
                # Remove nulls and invalid types
                set unique_values [list]
                regsub -all {,} $values { } values
                foreach value $values {
                    if {$value ne "" && $value ne "null"} {
                        lappend unique_values [cog_rest::parse::validate_value -value $value -type $switch_type]
                    }
                }
                set values [lsort -unique $unique_values]		
            }
        }

        # Additional parsing
      	switch $switch_type {
            category_array {

		        set category_type [cog_rest::parse::switch_sub_type -switch $switch -proc_name $proc_name]
                set values [cog_rest::parse::category -values $values -category_type $category_type]
            }
            category {
		        set category_type [cog_rest::parse::switch_sub_type -switch $switch -proc_name $proc_name]
                set all_category_ids [cog_rest::parse::category -values $values -category_type $category_type]
                if {[lsearch $all_category_ids $values]<0} {
                    set values [lindex $all_category_ids 0]
                }
            }
            object - object_array {
                set object_type [cog_rest::parse::switch_sub_type -switch $switch -proc_name $proc_name]
                set values [cog_rest::parse::object -values $values -object_type $object_type -rest_user_id $rest_user_id]
            } 
            number {
                regsub -all {,} $values {.} values 
            }
            string {
                # Check if we get the string as a list
                if {[llength $values] eq 1} {
                    set values [lindex $values 0]
                }
            }
            default {
                set values [encoding convertfrom $values]
            }
        }

        return $values
    }

    ad_proc -public values_from_json_object {
        -key:required
        -json_object:required
    } {
        Return the value for the key in a JSON object provided as part of application/json
        
        @param key to look for
        @param json_object Object definition in which we look for the value
    } {
        array set hash_array $json_object
        if {[info exists hash_array($key)]} {
            set vals $hash_array($key)
        } else {
            set vals "undefined"
        }
        
        if {[llength $vals]>1} {
            # Check if we might have an array
            if {[lindex $vals 0] eq "_array_"} {
                set vals [lindex $vals 1]
            }
        }
        return $vals
    }

    ad_proc -public validate_value {
        -type:required
        -value:required
    } {
        Checks if the value is of the required type. If yes, return the value formatted

        @param type Should be any of integer, boolean, number, string - assumes string if it's not in the list
        @param value Value to check
    } {

        set parsed_value ""
        switch $type {
            boolean {
                # Check if entry is a boolean
                if {[string is boolean $value]} {
                    if {$value} {
                        set parsed_value "1"
                    } else {
                        set parsed_value "0"
                    }
                } 
            }
            number {
                if {[string is double $value]} {
                    set parsed_value $value
                }
            }
            integer - pagination {
                if {[string is integer $value]} {
                    set parsed_value $value
                }
            }
            category - category_array - object - object_array {
           		# Compatiblity with explode = false setting.
	        	set value [split $value ","]
                set parsed_value [list]
                foreach value $value {
                    if {[string is integer $value]} {
                        lappend parsed_value $value
                    }
                }
            }
            date - date-time {
                # Check for valid date format and return timestamp
                set error_p [catch {set timestamp [db_string test "select '${value}'::timestamp from dual"]}]
                if {!$error_p} {
                    set parsed_value $timestamp
                }
            }
            default {
                set parsed_value "$value"
            }
        }
        return $parsed_value
    }

   ad_proc -public param_doc {
        -switch:required
        -proc_name:required
    } {
        Returns the Parameter documentation for the switch

        @param switch The switch/key we look for
        @param proc_name Name of the procedure
    } {

        # Now check for the doc parameters
        array set doc_elements [nsv_get api_proc_doc $proc_name]

        # Set a default switch type, just in case none is defined
        set switch_type ""
        if {[info exists doc_elements(param)]} {

            # Find out if we have a request_body as a parameter
            set request_body_param ""
            foreach param_docs $doc_elements(param) {
                set param_type [lindex $param_docs 1]
                if {$param_type eq "request_body"} {
                    set request_body_param [lindex $param_docs 0]
                }
                if {$switch ne [lindex $param_docs 0]} {
                    # not the switch we are looking for
                    continue
                }
                
                # Found it :-)
                set switch_type [lindex $param_docs 1]
                break
            }

            if {$request_body_param ne "" && $switch_type eq ""} {
        		if {[nsv_exists api_proc_doc "cog_rest::json_object::$request_body_param"]} {
                    array set doc_elements [nsv_get api_proc_doc "cog_rest::json_object::$request_body_param"]
        			foreach param_docs $doc_elements(param) {
                        if {$switch ne [lindex $param_docs 0]} {
                            # not the switch we are looking for
                            continue
                        }

                        # Found it :-)
                        set switch_type [lindex $param_docs 1]
                        break
                    }
                } else {
                    foreach param_docs $doc_elements(param_$request_body_param) {
                        
                        if {$switch ne [lindex $param_docs 0]} {
                            # not the switch we are looking for
                            continue
                        }

                        # Found it :-)
                        set switch_type [lindex $param_docs 1]
                        break
                    }
                }
            }
        }
        return $param_docs
    }

    ad_proc -public switch_type {
        -switch:required
        -proc_name:required
    } {
        Returns the type of switch 

        @param switch The switch/key we look for
        @param proc_name Name of the procedure
    } {

        set param_doc [cog_rest::parse::param_doc -switch $switch -proc_name $proc_name]
        set switch_type [lindex $param_doc 1]

        if {$switch_type eq ""} {
            set switch_type "string"
        }

        return $switch_type
    }

   ad_proc -public switch_sub_type {
        -switch:required
        -proc_name:required
    } {
        Returns the object /category type of switch 

        @param switch The switch/key we look for
        @param proc_name Name of the procedure
    } {
        set param_doc [cog_rest::parse::param_doc -switch $switch -proc_name $proc_name]
        set switch_type [lindex $param_doc 1]
        set switch_sub_type [lindex $param_doc 2]

        switch $switch_type {
            category - category_array {
                # Check if this is a valid category_type
                set valid_sub_type_p [db_string cat_type_check "select 1 from im_categories where category_type = :switch_sub_type limit 1" -default 0]
            }
            object - object_array {
                # Parse out the object_type from the privilege
                if {[regexp "(.*)::(.*)" $switch_sub_type match matched_object_type privilege]} {
                    set object_type $matched_object_type
                } else {
                    set object_type $switch_sub_type
                }

                set valid_sub_type_p [db_string cat_type_check "select 1 from acs_object_types where object_type = :object_type limit 1" -default 0]
            }
            default {
                set valid_sub_type_p 0
            }
        }
        
        if {$valid_sub_type_p} {
            return $switch_sub_type
        } else {
            return ""
        }
    }

    ad_proc -public category {
        -values:required
        -category_type:required
    } {
        Parse a category (or array of categories)
    } {

		set category_ids [list]

		foreach category_id $values {
            if {$category_id ne ""} { 
                set orig_category_id $category_id
                if {$category_type ne "*" && $category_type ne ""} {
                    set category_id [db_string valid_category "select category_id from im_categories where category_id = :category_id and category_type = :category_type and enabled_p = 't' " -default ""]
                } else {
                    set category_id [db_string valid_category "select category_id from im_categories where category_id = :category_id and enabled_p = 't' " -default ""]
                }
                if {$category_id eq ""} {
                    # enabled check
                    set orig_cat_name [im_name_from_id $orig_category_id]
                    set disabled_p [db_string valid_category "select 1 from im_categories where category_id = :orig_category_id and enabled_p = 'f' " -default "0"]
                    if {$disabled_p} {
                        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_cat_not_enabled]"
                    } else {
                        set actual_category_type [db_string category "select category_type from im_categories where category_id = :orig_category_id" -default "Not a category"]
                        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_cat_wrong_type]"
                    }
                } else {
                    # Use sub_categories by default
                    set category_ids [concat $category_ids [im_sub_categories $category_id]]
                }
            }
		}
		return "[lsort -unique $category_ids]"
    }

    ad_proc -public object {
        -values:required
        -object_type:required
        -rest_user_id:required
    } {
        Parse object value.

        Allow for "*" for the object_type (checking for any object) and "*" for privilege, skipping the permission check

        @param values Object_ids for which we parse the value. might be a list of values
        @param object_type Type for which we check permissions etc.
        @param rest_user_id Currently accessing user
    } {
        # Have the privilege set to default to read if it isn't provided
        if {![regexp "(.*)::(.*)" $object_type match object_type privilege]} {
            set privilege "read"
        }
        
        set object_ids [list]

        foreach object_id $values {

            if {![string is integer $object_id] || ![acs_object::object_p -id $object_id]} {
                cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_id_invalid_object]"
                return "{ERR}"
            }

            if {$object_type ne "*" && $object_type ne ""} {
                # Check if the object is of type
                set types_of_object_id [im_object_super_types -object_type [acs_object_type $object_id]]
                if {[lsearch $types_of_object_id $object_type]<0} {
                    cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_id_not_type]"
                    return "{ERR}"
                }
            }

            if {$privilege ne "*"} {
                # Get the rest_user_id from the calling procedure
                set permission_p [cog_rest::helper::object_permission -object_id $object_id -user_id $rest_user_id -privilege $privilege]
            } else {
                set permission_p 1
            }    

            # If we don't have permission return an error
            if {!$permission_p} {
                upvar proc_name proc_name
                set object_name [im_name_from_id $object_id] 
                set rest_user_name [im_name_from_id $rest_user_id]
                set message "[_ cognovis-rest.err_perm_user_priv_proc]"
                set fo [open "[acs_root_dir]/log/api.access.log" a]
                set now [clock format [clock seconds] -format "%Y-%m-%d %H:%M:%S"]
                puts $fo "$now $message"
                cog_rest::error -http_status 401 -message $message
                return "{ERR}"
            } else {
                lappend object_ids $object_id
            }
        }
        if {[llength $object_ids]>0} {
            return "[lsort -unique $object_ids]"
        } else {
            return ""
        }
    }

	ad_proc -public where_clauses {
		{ -proc_name ""}
		-include_sub_categories:boolean
		{ -ignore_filters_list ""}
	} {
		Returns a list of where clauses for the filters currently set in the calling procedure

		@param proc_name Procedure for which to generate the where clauses
		@param include_sub_categories Should we include the subcategories
		@param ignore_filters_list List of filter names which we ignore
	} {
		# Do uplevel magic to find out the procedure name
		if {$proc_name eq ""} {
			upvar proc_name_up proc_name_up
			uplevel {
				set proc_name_up [lindex [info level 0] 0]
			}
			set proc_name $proc_name_up
		}

	    array set doc_elements [nsv_get api_proc_doc $proc_name]

		set where_clauses [list]

		# Loop through all parameters to identify the set filters
		foreach param_docs $doc_elements(param) {
			set switch [lindex $param_docs 0]
			if {[lsearch $ignore_filters_list $switch] >-1} {
				continue
			}
			upvar $switch key
			if {[info exists key]} {
				set switch_type [lindex $param_docs 1]
				
				# Now we have the switch type, append the value(s)
				switch $switch_type {
					category - category_array {
						# Categories allow multiple values to be passed in
						# along with sub-categories
						if {$include_sub_categories_p} {
							set category_ids [im_sub_categories $key]
						} else {
							set category_ids $key
						}

						if {$switch_type eq "category_array" && [lindex [split $switch "_"] end] eq "ids"} {
							set category_name "[join [lrange [split $switch "_"] 0 end-1] "_"]_id"
						} else {
							set category_name $switch
						}

						if {[llength $category_ids] < 2} {
							if {$category_ids eq ""} {
								lappend where_clauses "$category_name is null"
							} else {
								lappend where_clauses "$category_name = :$switch"
							}
						} else {
							lappend where_clauses "$category_name in ([template::util::tcl_to_sql_list $category_ids])"
						}
					}
					pagination_object - json_array - json_object {
						# Ignore
					}
					default {
						# By default we only support a single value and there is it clear
						# what we need
						if {$key ne ""} {
							lappend where_clauses "$switch = :$switch"
						}
					}
				}
			}
		}

		return $where_clauses
	}

	ad_proc -public set_clauses {
		{ -proc_name ""}
		{ -ignore_params_list ""}
	} {
		Returns a list of set statements which are applicable for the procedure based on the values provided

		@param proc_name Procedure for which to generate the where clauses
		@param ignore_params_list List of params we ignore

        @return List of set statements
    } {
    	# Do uplevel magic to find out the procedure name
		if {$proc_name eq ""} {
			upvar proc_name_up proc_name_up
			uplevel {
				set proc_name_up [lindex [info level 0] 0]
			}
			set proc_name $proc_name_up
		}

	    array set doc_elements [nsv_get api_proc_doc $proc_name]

		# Loop through all parameters to identify the set filters

        set params_list [list]
		foreach param_docs $doc_elements(param) {
			set switch [lindex $param_docs 0]
            set param_type [lindex $param_docs 1]
            if {$param_type eq "request_body"} {
                set request_body_param [lindex $param_docs 0]
                if {[nsv_exists api_proc_doc "cog_rest::json_object::$request_body_param"]} {
                    array set doc_elements [nsv_get api_proc_doc "cog_rest::json_object::$request_body_param"]
                    foreach param_docs $doc_elements(param) {
                        lappend params_list [lindex $param_docs 0]
                    }
                } else {
                    foreach param_docs $doc_elements(param_$request_body_param) {
                        lappend params_list [lindex $param_docs 0]
                    }
                }
            } else {
                lappend params_list $switch
            }
        }

        set set_clauses [list]
        foreach param $params_list {
			if {[lsearch $ignore_params_list $param] >-1} {
				continue
			}

			upvar $param key
			if {[info exists key]} {
                if {$key eq ""} {
                    lappend set_clauses "$param = null"
                } else {
                    lappend set_clauses "$param = :$param"
                }
            }
        }

		return $set_clauses
    }

}