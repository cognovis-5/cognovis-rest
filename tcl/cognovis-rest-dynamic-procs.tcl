ad_library {
    Procedures for the dynfields, portlets, dynviews and other dynamic stuff
	
    @author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
    ad_proc dynamic_attribute {} {
        @return attribute named_id Attribute we should display/edit - attribute.name reflects the most likely name used in the endpoints. For skills it is the column in the table where we have a match to (e.g. source language skill matches to source_language_id as this is denormalized).
        @return display_name string Name to use for displaying the attribute 
        @return i18n_key string Key with the i18n for this attribute
        @return attribute_type string What type of attribute do we have (acs_attributes / im_skills)
        @return dynamic_subtypes json_array dynamic_subtype Information about the subtype dependent data
        @return widget named_id Widget object
        @return widget_type string Type of widget to use - frontend needs to figure out what to do with it. Special cases are skills and categories
        @return category_type string If filled, have the frontend get the data from CognovisCategory.getCategory - provided both for skills as well as categories
        @return widget_data json_array id_value Array of id + name objects. Might contain additional information. Not used for categories or skills
        @return sort_order integer Y position of dynfield (order)
    } -

    ad_proc dynamic_subtype {} {
        @return help_text string Help Text we want to display for the attribute in case someone does not know how to fill the filled
        @return section_heading string Section we want to display the attribute - Useful if we want to group the fields in the frontend.
        @return default_value string Value to use as default - might actually be an integer but we get it as string from the DB
        @return subtype named_id Subtype for which this attribute 
        @return display_mode string Any of edit/display/none
        @return required_p boolean Is this a required attribute (for this subtype)
        @return help_url string URL to link to if someone clicks on the helptext icon
    } -

    ad_proc dynamic_object_body {} {
        @param object_id integer Object we want to retrieve. Will check read permission
        @param id_values json_array id_value Array of attribute_name (id) and value (of the attribute) objects.
    } - 

    ad_proc component_plugin {} {
        @return plugin named_id Plugin we display - Permission is on the plugin_id and the plugin_name can be used to match the portlet with the frontend class
        @return sort_order integer In which order should this plugin be displayed
        @return location string Where should we display the portlet - default options are left, right, top, buttom - but anything is possible if you know what to do with it.
        @return title string Internationalized title to display for the portlet
        @return title_i18n_key string Internationalization key for the title if it can be found / deducted from title_tcl
    } -

    ad_proc dynview_table {} {
        @return view_id integer id of the im_view
        @return view_name string name of the im_view
        @return view_label string label for the dynview
        @return json_config string json config for the whole im_view
        @return dynview_columns json_array dynview_column Array of dynview rows together with fields
        @return sort_order number order of the im_view
    } - 

    ad_proc dynview_column {} {
        @return column_id integer id of column
        @return column_name string name of the column (usually translation key)
        @return column_render_tcl string TCL executed to add some fancy render
        @return variable_name string name of the variable
        @return visible_for string for whom is that column visible
        @return ajax_configuration string ajax config (usually related to display)
        @return sort_order number order of the column
    } - 

    ad_proc dynview_row {} {
        @return object_id number id of the object
        @return object_fields json_array dynview_field Array of id+values for the object. ID is the attribute_name and value is the corresponding value.
    } -

    ad_proc dynview_field {} {
        @return field_name string Identifier for that field. Usually same as 'variable_name'
        @return field_id string Actual name which belongs to the id
        @return field_value string Actual name which belongs to the id
        @return field_display_value string Internationalized value to display

    } -

    ad_proc view {} {
        @return view_id integer id of the dynview (im_views table)
        @return view_name string name of the dynview
        #@return view_type_id category "Intranet DynView Type" type of the dynview
        @return view_status_id string placeholder for future view_status (line commmented above)
        @return view_sql string sql query which is used to later get data with that view
        @return visible_for string tcl proc which checks against permissions
        @return view_label string dynview title (visible to end user)
        @return json_configuration string json config of table (usefeul especially for webix js framework)
        @return sort_order integer order of dynview (e.g. on a subpage)

    } - 

    ad_proc view_body {} {
        @param view_name string name of the dynview
        #@param view_type_id category "Intranet DynView Type" type of the dynview
        @param view_status_id string placeholder for future view_status (line commmented above)
        @param view_sql string sql query which is used to later get data with that view
        @param visible_for string tcl proc which checks against permissions
        @param view_label string dynview title (visible to end user)
        @param json_configuration string json config of table (usefeul especially for webix js framework)
        @param sort_order integer order of dynview (e.g. on a subpage)
    } - 

    ad_proc view_column {} {
        @return column_id integer The identifier for the column.
        @return view_id integer The identifier for the view.
        @return group_id integer The identifier for the group.
        @return column_name string Name of the column.
        @return column_render_tcl string TCL rendering for the column.
        @return extra_select string Extra select information for the column.
        @return extra_from string Extra from information for the column.
        @return extra_where string Extra where condition for the column.
        @return sort_order integer Sort order for the column.
        @return order_by_clause string Order by clause for the column.
        @return visible_for string Visibility settings for the column.
        @return json_configuration string JSON configuration for the column.
        @return variable_name string Variable name associated with the column.
        @return datatype string Data type of the column.

    } -

    ad_proc view_column_body {} {
        @param view_id integer The identifier for the view.
        @param column_name string Name of the column in the dynview
        @param variable_name string Name of the variable that corresponds to the column
        @param visible_for string TCL proc which checks against permissions
        @param json_configuration string JSON configuration for the column (useful especially for Webix JS framework)
        @param sort_order integer Order of columns in the dynview (e.g., when displayed in a table)
    } -

    ad_proc dynfield_widget {} {
        @return widget_id integer unique identifier for the widget
        @return widget_name string unique Name of the widget
        @return pretty_name string readable name of the widget
        @return pretty_plural string readable name of the widget (plural)
        @return storage_type category "Intranet DynField Storage Type" type of the storage for widget
        @return acs_datatype string  acs_datatype for the widget
        @return widget string description or content of the widget
        @return sql_datatype string SQL datatype used by the widget
        @return parameters string parameters for the widget
        @return deref_plpgsql_function string function for the widget ('im_name_from_id' as used as default one )
    } -

    ad_proc widget_body {} {
        @param widget_name string ame of the widget.
        @param pretty_name string name of the widget.
        @param pretty_plural string plural form of the widget's name
        #@param storage_type_id integer Identifier for the storage type associated with the widget.
        @param acs_datatype string datatype as recognized by the ACS system.
        @param widget string details and configuration of the widget itself.
        @param sql_datatype string sql datatype associated with the widget.
        @param parameters string any parameters or configurations associated with the widget.
        @param deref_plpgsql_function string function used for dereferencing
    } -

    ad_proc dynfield_layout_page {} {
        @return page_url string URL of the page
        @return object_type string type of object the layout is associated with
        @return layout_type string type of the layout
        @return default_p boolean is page default?
    } -

    ad_proc dynfield_layout_page_body {} {
        @param page_url string URL of the page for the layout.
        @param object_type string Type of object the layout is associated with.
        @param layout_type string Type of the layout.
    } -


    ad_proc dynfield_layout_attribute {} {
        @return attribute named_id Attribute name and id
        @return attribute_id integer id from the acs_attributes table
        @return pretty_name string Pretty name of the attribute
        @return pos_x integer X position of the layout
        @return pos_y integer Y position of the layout
        @return size_x integer Width of the layout element
        @return size_y integer Height of the layout element
        @return label_style string Style of the label
        @return div_class string Class for the div element
    } -

    ad_proc dynfield_layout_page_attributes_body {} {
        @param page_url string URL of the page for the layout.
        @param attribute_id integer id of the attribute. It's mandatory for the PUT request.
        @param object_type string Type of object the layout attribute is associated with.
        @param pos_x integer X position of the attribute on the layout.
        @param pos_y integer Y position of the attribute on the layout.
        @param size_x integer Width of the attribute on the layout.
        @param size_y integer Height of the attribute on the layout.
        @param label_style string Style for the label of the attribute.
        @param div_class string CSS class for the attribute div.
        @param add_all_attributes_p boolean flag which allows to decide if we want to add all attributes.
    } -


    # ad_proc dynfield_layout {} {
    #     @return attribute_id integer unique identifier for the attribute
    #     @return page_url string URL of the page
    #     @return object_type string type of object the layout is associated with
    #     @return pos_x integer position on the x-axis
    #     @return pos_y integer position on the y-axis
    #     @return size_x integer size along the x-axis
    #     @return size_y integer size along the y-axis
    #     @return label_style string style of the label ('plain', 'no_label', or 'table')
    #     @return div_class string class of the div
    #     @return sort_key integer sort order key for the layout
    # } -

    ad_proc dynfield_attr {} {
        @return dynfield_id integer id of the dynfield (im_dynfield_attributes)
        @return dynfield_name string name of the dynfield (im_dynfield_attributes)
        @return acs_attribute_id integer id from acs_attributes
        @return object_type string type of the object that field is related to
        @return widget_name string name of the widget used for the dynamic field
        @return also_hard_coded_p boolean flag indicating if the attribute is also hard-coded (true or false)
        @return required_p boolean flag indicating if attribute is mandatory (true or false)
        @return table_name string name of the database table associated with the attribute
        @return pretty_name string user-friendly name of the attribute
        @return pretty_plural string plural form of the pretty name
        @return pos_x integer position on the x-axis
        @return pos_y integer position on the y-axis
        @return help_text string help text in English
        @return help_url string help url
        @return default_value string default value of the dynfield attribute
        @return secion_heading string section heading of the dynfield attribute
    } -

    ad_proc dynfield_attr_body {} {
        @param attribute_name string Name of the attribute.
        @param object_type string Type of object the attribute is associated with.
        @param widget_name string Name of the widget associated with the attribute.
        @param pretty_name string Human-readable name of the attribute.
        @param table_name string Database table where the attribute is stored.
        @param also_hard_coded boolean Indicates if the attribute is hard-coded or dynamic.
        @param required boolean Indicates if the attribute is required or optional.
        @param pretty_plural string Plural form of the attribute's name.
        @param include_in_search boolean Specifies if the attribute is included in search operations.
        @param label_style string Style or class for the attribute's label.
        @param pos_y integer Specifies the vertical position of the attribute in UI.
        @param help_text string Help text associated with the attribute for UI hints.
        @param help_url string URL to provide additional help or documentation for the attribute.
        @param default_value string Default value of the attribute if not provided.
        @param section_heading string Heading or section where the attribute is grouped in UI.
    } -


    ad_proc dynfield_permission {} {
        @return dynfield_id integer id of the dynfield (im_dynfield_attributes)
        @return dynfield_name string name of the dynfield (im_dynfield_attributes)
        @return object_type string type of object the attribute is associated with
        @return group_id integer id of the group
        @return privilege string name of the privilege
    } -

    ad_proc dynfield_type_map {} {
        @return dynfield_id integer id of the dynfield (im_dynfield_attributes)
        @return dynfield_name string name of the dynfield (im_dynfield_attributes)
        @return object_type string type of object the attribute is associated with
        @return mapped_dynfield_types json_array mapped_dynfield_type mapped types for that dynfield
    } -

    ad_proc mapped_dynfield_type {} {
        @return display_mode string display mode. 'edit' / 'display'
        @return mapped_object_type_id integer id of the object_type
        @return mapped_object_type_name string name of the obeject type
    } -
    

    ad_proc dynfield_type_map_body {} {
        @param attribute_id integer id of the dynamic field type we want to map
        @param action string type of the action. One of the following: 'none', 'display', 'edit'.
        @param object_type_id integer object type of attribute / display mode
    } -

}


ad_proc -public cog_rest::get::dynview_columns {
    -view_id:required
    -rest_user_id:required
} {
    Returns configuration for DynView 

    @param view_id integer id of dynview which we want to build

    @return dynviews json_array dynview_table array of dynviews (it's almost always single object)

} {

    set dynviews [list]

    set dynview_sql "select view_id, view_name, view_label, sencha_configuration as json_config, sort_order from im_views where view_id = :view_id"

    set column_sql "select column_id, column_name, column_render_tcl, variable_name, visible_for, ajax_configuration,
    extra_select, extra_from, extra_where, sort_order
    from im_view_columns
    where view_id = :view_id and group_id is null
    order by sort_order"

    db_foreach dynview $dynview_sql {
        set view_label [_ $view_label]
        set dynview_columns [list]
        db_foreach column $column_sql {
            set visible_for_p [eval $visible_for]
            if {$visible_for_p eq ""} {
                set visible_for_p 1
            }
            if {$visible_for_p} {
                set column_name [_ $column_name]
                lappend dynview_columns [cog_rest::json_object -object_class dynview_column]
            }
        }
        lappend dynviews [cog_rest::json_object]
    }

    return [cog_rest::json_response]

}

ad_proc -public cog_rest::get::dynview_rows {
    -view_id:required
    -parameter_object_json
    -rest_user_id:required
} {
    Returns configuration for DynView 

    @param view_id integer id of dynview which we want to build
    @param parameter_object_json string JSON object with additional key/values that might be needed by the widget to return widget_data
    

    @return dynview_rows json_array dynview_row Array of dynview rows together with fields

    
} {
    set dynview_columns [list]
    set dynview_rows [list]


    # First we check permission for whole view_id (we later also check permissions for each column)
    set visible_for_p [eval [db_string get_visible_for "select visible_for from im_views where view_id = :view_id" -default 1]]
    if {$visible_for_p eq ""} {
        set visible_for_p 1
    }
    if {$visible_for_p || [im_is_user_site_wide_or_intranet_admin $rest_user_id]} {

        if {[info exists parameter_object_json]} {
            # Initialize the variable of the parameter_object_json
            set parameter_json [new_CkJsonObject]
            set status [CkJsonObject_Load $parameter_json $parameter_object_json]
            set numMembers [CkJsonObject_get_Size $parameter_json]
            for {set i 0} {$i <= [expr $numMembers - 1]} {incr i} {
                set name [CkJsonObject_nameAt $parameter_json $i]
                set value [CkJsonObject_stringAt $parameter_json $i]
                set $name $value
            }
            delete_CkJsonObject $parameter_json
        }

        set column_sql "select column_id, column_name, column_render_tcl, variable_name, visible_for, ajax_configuration, sort_order
            from im_view_columns
            where view_id = :view_id and group_id is null
            order by sort_order"

        # Create dictonary to later avoid nested 'db_foreach'
        db_foreach column $column_sql {
            set visible_for_p [eval $visible_for]
            if {$visible_for_p eq ""} {
                set visible_for_p 1
            }
            if {$visible_for_p || [im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
                set columns($column_id) [dict create \
                    column_name $column_name \
                    column_render_tcl $column_render_tcl \
                    variable_name $variable_name \
                    visible_for $visible_for \
                    ajax_configuration $ajax_configuration
                ]
            }
        }

        set row_nr 0

        set view_sql [db_string get_view_sql "select view_sql from im_views where view_id = :view_id" -default ""]
        if {$view_sql ne ""} {
            db_foreach row $view_sql {
                incr row_nr
                set object_fields [list]  
                set column_defined_p 0
                foreach column_id [array names columns] {
                    if {[info exists columns($column_id)]} {
                        set column_info $columns($column_id)
                        set variable_name [dict get $column_info variable_name]
                        set ajax_configuration [dict get $column_info ajax_configuration]
                        set field_name $variable_name
                        set field_id [set $variable_name]
                        set field_value $field_id
                        set field_display_value $field_value
                        set column_type [cog::json::get_value_from_json -json_object $ajax_configuration -value_key "columnType"]
                        switch $column_type {
                            pmColumn {
                                set field_value [im_name_from_id $field_id] 
                                set field_display_value [db_string get_screen_name "select screen_name from cc_users where user_id = :field_id" -default ""]
                            }
                            textColumn - objectColumn - selectColumn - iconColumn - categorySelectColumn {
                                if {[string is integer -strict $field_id]} {
                                    set field_display_value [im_name_from_id $field_id]
                                }                    
                            }
                            categoryMultiSelectColumn {
                                set cat_list [split $field_id ","]
                                foreach item $cat_list {
                                    set index [lsearch -exact $cat_list $item]
                                    set new_value [im_name_from_id $item]
                                    set cat_list [lreplace $cat_list $index $index $new_value]
                                }
                                set field_display_value [join $cat_list ","]
                            }
                        }
                        set column_defined_p 1
                        lappend object_fields [cog_rest::json_object -object_class dynview_field]   
                    } 
                }
                if {$column_defined_p} {
                    lappend dynview_rows [cog_rest::json_object]
                }
            }
        }

    }

    return [cog_rest::json_response]    

}



ad_proc -public cog_rest::get::dynamic_subtypes {
    -attribute_id
    { -object_subtype_ids "" }
    -rest_user_id:required
} {
    Return the dynamic subtypes for an attribute to know how it is configured

    @param attribute_id integer dynfield attribute we want to get subtype permission info
    @return subtypes json_array dynamic_subtype Array of subtypes and if we show the attribute for them
} {
    set subtypes [list]
    if {[info exists attribute_id]} {
        set read_p [permission::permission_p -no_login -party_id $rest_user_id -object_id $attribute_id -privilege "read"]
        set write_p [permission::permission_p -no_login -party_id $rest_user_id -object_id $attribute_id -privilege "write"]
        set where_clause_list [list "da.attribute_id = :attribute_id"]
        lappend where_clause_list "da.attribute_id = tam.attribute_id"
        lappend where_clause_list "c.category_id = tam.object_type_id"
        if {$object_subtype_ids ne ""} {
            lappend where_clause_list "tam.object_type_id in ([template::util::tcl_to_sql_list $object_subtype_ids])"
        }
        if {$read_p || $write_p} {
            db_foreach subtype "select object_type_id as subtype_id, category as subtype_name,display_mode,section_heading,help_text,help_url,default_value,required_p
                from im_dynfield_type_attribute_map tam, im_dynfield_attributes da, im_categories c
                where [join $where_clause_list " and "]" {
                    if {$display_mode eq "edit" && !$write_p} {
                        set display_mode "display"
                    }
                    lappend subtypes [cog_rest::json_object]
                }
        }
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::dynamic_attributes {
    -object_type:required
    -object_subtype_id
    -object_id
    { -page_url "default" }
    -rest_user_id:required
    -parameter_object_json
} {
    Returns the widget information for a given object_type.

    @param object_type string Object Type we look for (e.g. persons)
    @param object_subtype_id integer Category ID for the subtype we want to get information for
    @param object_id integer Object we look at. Necessary for persons or anything with multiple subtypes. Only use if you can't pass a single subtype_id
    @param page_url string What is the URL where we display the form. should be in the form of /#!/...... - determines sort_order and which attributes to actually display (irrespective of subtype configuration)
    @param parameter_object_json string JSON object with additional key/values that might be needed by the widget to return widget_data

    @return attributes json_array dynamic_attribute Array of attributes to display / edit
} {
    if {[lsearch [cog::dynfield::valid_object_types] $object_type]<0} {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_invalid_object_type]"
    }

    set page_url_exists [db_string exists "select 1 from im_dynfield_layout where page_url = :page_url limit 1" -default 0]
    if {!$page_url_exists} {
        set page_url "default"
    }


    set default_max_age [parameter::get_from_package_key -package_key intranet-dynfield -parameter GenericSQLWidgetMemoizeMaxAgeDefault -default 600]

    if {[info exists parameter_object_json]} {
        # Initialize the variable of the parameter_object_json
        set parameter_json [new_CkJsonObject]
        set status [CkJsonObject_Load $parameter_json $parameter_object_json]
        set numMembers [CkJsonObject_get_Size $parameter_json]
        for {set i 0} {$i <= [expr $numMembers - 1]} {incr i} {
            set name [CkJsonObject_nameAt $parameter_json $i]
            set value [CkJsonObject_stringAt $parameter_json $i]
            set $name $value
        }
        delete_CkJsonObject $parameter_json
    }

    #---------------------------------------------------------------
    # Get subtypes either directly or from object_id
    #---------------------------------------------------------------

    set object_subtype_ids [list]
    if {[info exists object_subtype_id]} {
        lappend object_subtype_ids [list $object_subtype_id]
    }

    if {[info exists object_id]} {
        set object_type [acs_object_type $object_id]

        switch $object_type {
            person - user - party {
                set object_subtype_ids [db_list members "select c.category_id from group_member_map g, im_categories c where 
                    g.member_id = :object_id 
                    and g.group_id = c.aux_int1
                    and c.category_type = 'Intranet User Type'
                    and g.member_id not in (
                    -- Exclude banned or deleted users
                    select	m.member_id
                    from	group_member_map m,
                        membership_rels mr
                    where	m.rel_id = mr.rel_id and
                        m.group_id = acs__magic_object_id('registered_users') and
                        m.container_id = m.group_id and
                        mr.member_state != 'approved')"]
            }
            default {
                db_1row object_type_info "select table_name, id_column, type_column from acs_object_types where object_type = :object_type"
                set object_subtype_id ""
                if {$table_name ne "" && $id_column ne "" && $type_column ne ""} {
                    set object_subtype_id [db_string subtype "select $type_column from $table_name where $id_column = :object_id" -default ""]
                }
                if {$object_subtype_id ne ""} {
                    lappend object_subtype_ids $object_subtype_id
                }
            }
        }
    }
    switch $object_type {
        person - user - party {
            set object_type "person"
        }
    }

        # First get the dynfield attributes
    set attributes [list]

    set attributes_sql "
        select
            dl.pos_y as sort_order,
            a.attribute_id,
            aa.attribute_id as dynfield_attribute_id,
            a.attribute_name,
            a.pretty_name,
            case when a.min_n_values = 0 then 'f' else 't' end as required_p, 
            a.default_value,
            aw.widget_id, 
            aw.widget_name,
            aw.widget,
            aw.parameters
        from
            im_dynfield_attributes aa,
            im_dynfield_layout dl,
            im_dynfield_widgets aw,
            acs_attributes a 
        where 
            a.object_type = :object_type
            and aa.attribute_id = dl.attribute_id
            and a.attribute_id = aa.acs_attribute_id
            and aa.widget_name = aw.widget_name
            and dl.page_url = :page_url
            and aa.attribute_id in (select distinct attribute_id from im_dynfield_type_attribute_map)"


    db_foreach attribute $attributes_sql {
        set dynamic_subtypes [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::dynamic_subtypes -object_subtype_ids $object_subtype_ids -attribute_id $dynfield_attribute_id -rest_user_id $rest_user_id]]
        if {$dynamic_subtypes ne ""} {
            set attribute_type "acs_attribute"
            set widget_type $widget
            foreach parameter_list $parameters {

                set custom_pos [lsearch $parameter_list "custom"]
                if {$custom_pos >= 0} {
                    array set custom_parameters [lindex $parameter_list $custom_pos+1]
                }

                set options_pos [lsearch $parameter_list "options"]
                if {$options_pos >= 0} {
                    set option_parameters [lindex $parameter_list $options_pos+1]
                }
            }

            set widget_data [list]
            if {[exists_and_not_null custom_parameters(include_empty_p)]} {
                set include_empty_p $custom_parameters(include_empty_p)
            
                if {$include_empty_p} {
                    set id ""
                    set value ""
                    if {[info exists custom_parameters(include_empty_name)]} {
                        set value $custom_parameters(include_empty_name)
                    } 
                    lappend widget_data [cog_rest::json_object -object_class "id_value"]
                }
            }

            switch $widget {
                select {
                    set widget_type "combo"
                    set category_type ""
                    foreach option $option_parameters {
                        set id [lindex $option 0]
                        set value [lindex $option 1]
                        set display_value ""
                        lappend widget_data [cog_rest::json_object -object_class "id_value"]
                    }
                }
                generic_sql {
                    set widget_type "combo"
                    set category_type ""
                    eval "set sql \"$custom_parameters(sql)\""
                    foreach id_value [db_list_of_lists generic_sql $sql] {
                        set id [lindex $id_value 0]
                        set value [lindex $id_value 1]
                        set display_value ""
                        lappend widget_data [cog_rest::json_object -object_class "id_value"]
                    }
                }
                generic_tcl {
                    set widget_type "combo"
                    set category_type ""
                    if {[exists_and_not_null custom_parameters(tcl)]} {
                        set tcl_code $custom_parameters(tcl)
                        set key_value_list [list]
                        if {[exists_and_not_null custom_parameters(global_var)]} {
                            set global_var_name $custom_parameters(global_var)
                            if {[info exists ::$global_var_name]} {
                                set $global_var_name [set ::$global_var_name]
                            } 
                            if {![info exists $global_var_name]} {
                                set $global_var_name ""
                            }
                        }

                        if {[exists_and_not_null custom_parameters(memoize_p)]} {
                            if {[exists_and_not_null custom_parameters(memoize_max_age)]} {
                                set memoize_max_age $custom_parameters(memoize_max_age)
                            } else {
                                set memoize_max_age $default_max_age
                            }
                            if {[catch {
                                set key_value_list [util_memoize [list eval $tcl_code] $memoize_max_age]
                            } errmsg]} {
                                cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_generic_tcl_statement]"
                            }
                        } else {
                            if {[catch {
                                set key_value_list [eval $tcl_code]
                            } errmsg]} {
                                cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_generic_tcl_stat2]"
                            }
                        }

                        # Switch_p is useful if the procedure/sql returns value,id instead of id,value
                        if {[exists_and_not_null custom_parameters(switch_p)]} {
                            set switch_p $custom_parameters(switch_p)
                        } else {
                            set switch_p 0
                        }

                        foreach id_value $key_value_list {
                            if {$switch_p} {
                                set id [lindex $id_value 1]
                                set value [lindex $id_value 0]
                            } else {
                                set id [lindex $id_value 0]
                                set value [lindex $id_value 1]
                            }	
                            set display_value ""
                            lappend widget_data [cog_rest::json_object -object_class "id_value"]
                        }
                    }
                }
                im_category_tree {                        
                    if {[exists_and_not_null custom_parameters(category_type)]} {
                        set category_type $custom_parameters(category_type)
                    } else {
                        cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_widget_miss_cat_type]"
                    }
                    set widget_type "combo"
                }
                richtext {
                    set widget_type "richtext"
                    set category_type ""
                }
                im_cost_center_tree {
                    set widget_type "combo"
                    set category_type ""

                    if {[exists_and_not_null custom_parameters(department_only_p)]} {
                        set department_only_p $custom_parameters(department_only_p)
                    } else {
                        set department_only_p 0
                    }
                    foreach id_value [im_cost_center_options -department_only_p $department_only_p ] {
                        set id [lindex $id_value 0]
                        set value [lindex $id_value 1]
                        lappend widget_data [cog_rest::json_object -object_class "id_value"]
                    }
                }
                date {
                    set widget_type "datepicker"
                    set category_type ""
                    set widget_data ""
                }
                jq_datetimepicker {
                    set widget_type "datetimepicker"
                    set category_type ""
                    set widget_data ""
                }
                default {
                    set widget_type "text"
                    set category_type ""
                    set widget_data ""
                }
            }
            
            # Localization - use the cognovis-rest L10n space for translation.
            set pretty_key [lang::util::suggest_key $pretty_name]
            if { ![string match "*#*" $pretty_name] } {
                set i18n_key "cognovis-rest.$pretty_key"
            } else {
                regsub -all {#} $pretty_name {} i18n_key
            }
            
            if {![lang::message::message_exists_p en_US $i18n_key]} {
                set pretty_name [lang::message::lookup "" intranet-core.$pretty_key $pretty_name]
                if {$pretty_name ne ""} {
                    lang::message::register en_US cognovis-rest $pretty_key $pretty_name
                }
            }
            set display_name [lang::message::lookup "" $i18n_key $pretty_name]
            lappend attributes [cog_rest::json_object]
        }
    }

    set type_category_type [db_string type_category "select type_category_type from acs_object_types where object_type = :object_type" -default ""]
    if {$type_category_type ne ""} {
        set skill_configured [db_string skill "select  1
            from    im_freelance_skill_type_map, im_categories
            where
                category_type = :type_category_type
                and category_id = object_type_id limit 1" -default 0]
        
        if {$skill_configured} {
            db_foreach skill "select category_id as skill_type_id, aux_string1 as category_type, aux_string2 as attribute_name from im_categories where category_type = 'Intranet Skill Type'" {
                set dynamic_subtypes [list]

                db_foreach subtype "select object_type_id as subtype_id, category as subtype_name,display_mode,help_text
                    from im_freelance_skill_type_map stm, im_categories c
                    where stm.skill_type_id = :skill_type_id
                    and c.category_type = :type_category_type
                    and c.enabled_p = 't'
                    and c.category_id = stm.object_type_id" {
                        set section_heading "Skills"
                        set help_url ""
                        lappend dynamic_subtypes [cog_rest::json_object -object_class "dynamic_subtype"]
                    }
                if {[llength $dynamic_subtypes]>0} {
                    set attribute_id $skill_type_id
                    set display_name [im_category_from_id -current_user_id $rest_user_id $skill_type_id]
                    set category_name [util_memoize [list db_string cat "select im_category_from_id($skill_type_id)" -default {}]]
                    set i18n_key "cognovis-rest.[lang::util::suggest_key $category_name]"
                    set attribute_type "im_skills"
                    set widget_type "combo"
                    set widget ""
                    set widget_data ""

                    lappend attributes [cog_rest::json_object]
                }
            }
        }
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::dynamic_object {
    -object_id:required
    {-page_url "default"}
    -rest_user_id:required
} {
    Returns an array of key/value pairs for an object

    @param object_id integer Object we want to retrieve. Will check read permission
    @param page_url string PageURL to limit the values we want to return for the object

    @return key_value_list json_array id_value Array of id+values for the object. ID is the attribute_name and value is the corresponding value.
} {

    if {![acs_object::object_p -id $object_id]} {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_miss_object_id]"
    }

    set locale [lang::user::locale -user_id $rest_user_id]

    set rest_user_name [im_name_from_id $rest_user_id]
    if {![cog_rest::permission_p -object_id $object_id -user_id $rest_user_id -privilege "read"]} {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_user_perm_object]"
    }
    
    set object_type [acs_object_type $object_id]
    set object_types [im_object_super_types -object_type $object_type]
    set type_category_type [db_string type_category "select type_category_type from acs_object_types where object_type = :object_type" -default ""]

    set page_url_exists [db_string exists "select 1 from im_dynfield_layout where page_url = :page_url limit 1" -default 0]
    if {!$page_url_exists} {
        set page_url "default"
    }

    set attributes_sql "
        select
            a.attribute_id,
            aa.attribute_id as dynfield_attribute_id,
            a.table_name,
            a.attribute_name,
            a.datatype as attribute_datatype,
            dw.deref_plpgsql_function
        from
            im_dynfield_attributes aa,
            im_dynfield_widgets dw,
            im_dynfield_layout dl,
            acs_attributes a 
        where 
            a.object_type in ([template::util::tcl_to_sql_list $object_types])
            and aa.attribute_id = dl.attribute_id
            and a.attribute_id = aa.acs_attribute_id
            and aa.widget_name = dw.widget_name
            and dl.page_url = :page_url
            and aa.attribute_id in (select distinct attribute_id from im_dynfield_type_attribute_map)"

    set select_clause_list [list]
    set attribute_list [list]
    set tables_list [list]


    db_foreach tables_sql "
	select	table_name,
		id_column
	from	(
		select	table_name,
			id_column,
			1 as sort_order
		from	acs_object_types
		where	object_type in ([template::util::tcl_to_sql_list $object_types])
		UNION
		select	table_name,
			id_column,
			2 as sort_order
		from	acs_object_type_tables
		where	object_type in ([template::util::tcl_to_sql_list $object_types])
		) t
	order by t.sort_order
    " {
        if {[lsearch $tables_list $table_name]<0} {
            lappend tables_list $table_name
            if {[llength $tables_list] == 1} {
                set froms "$table_name"
                set primary_id_column ${table_name}.$id_column
            } else {
                append froms "\n\t LEFT OUTER JOIN $table_name on ($primary_id_column = ${table_name}.$id_column)"
            }
        }
    } 
    
     
    if {![info exists froms]} {
        cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_valid_attr_for_obj]"
    }

    db_foreach attributes $attributes_sql {
        set read_p [permission::permission_p -no_login -party_id $rest_user_id -object_id $dynfield_attribute_id -privilege "read"]
        if {!$read_p} { continue }
        set datatype($attribute_name) $attribute_datatype
        lappend attribute_list $attribute_name
        lappend select_clause_list "${table_name}.$attribute_name"
        set deref_function($attribute_name) $deref_plpgsql_function
    }

    if {$select_clause_list ne ""} {
        set object_sql "select [join $select_clause_list ",\n"] from $froms where $primary_id_column = $object_id"
        db_1row get_object $object_sql -column_array object_info
    } else {
        set object_info($primary_id_column) $object_id
    }

    set key_value_list [list]

    foreach attribute $attribute_list {
        set id $attribute
        if {[info exists object_info($attribute)]} {
            set value $object_info($attribute)
        } else {
            set value ""
        }

        set display_value $value
        if {$value ne ""} {
            if {[string is integer $value]} {
                set deref_plpgsql_function $deref_function($attribute)
                switch $deref_plpgsql_function {
                    im_name_from_id {
                        set display_value [im_name_from_id $value]                        
                    }
                    im_integer_from_id {
                        if {[catch { set display_value [lc_parse_number $value $locale 1]}]} {
                            set display_value $value
                            cog_log Warning "$attribute should be integer, is $value"
                        }
                    }
                    im_cost_center_name_from_id {
                        set display_value [im_cost_center_name $value]
                    }
                    im_numeric_from_id {
                        lc_numeric $value "" $locale
                    }
                    im_percent_from_number {
                        set display_value [im_percent_from_number $value]
                    }
                    im_category_from_id {
                        set display_value [im_category_from_id -locale $locale $value]
                    }
                }
            }
        }
        lappend key_value_list [cog_rest::json_object]
    }

    cog::callback::invoke cog_rest::dynamic::get_object -object_id $object_id -object_types $object_types 

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::dynamic_object {
    -object_id:required
    -id_values:required
    {-page_url "default"}
    {-replace_skills_p 1}
    -rest_user_id:required
} {
    Returns an array of key/value pairs for an object


    @param page_url string PageURL to limit the values we want to return for the object
    @param dynamic_object_body request_body Object we want to update
    @param replace_skills_p boolean should we replace skills ?

    @return key_value_list json_array id_value Array of id+values for the object. ID is the attribute_name and value is the corresponding value.
} {

    if {![acs_object::object_p -id $object_id]} {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_missing_object_id]"
    }

    if {![cog_rest::permission_p -object_id $object_id -user_id $rest_user_id -privilege "write"]} {
        set rest_user_name [im_name_from_id $rest_user_id]
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_user_perm_put_obj]"
    }
    
    foreach id_value $id_values {
        array set passed_json $id_value
        array set id_value_arr $passed_json(_object_)
        set values($id_value_arr(id)) $id_value_arr(value)
    }

    set object_type [acs_object_type $object_id]
    set object_types [im_object_super_types -object_type $object_type]
    set type_category_type [db_string type_category "select type_category_type from acs_object_types where object_type = :object_type" -default ""]

    set attributes_sql "
        select
            a.attribute_id,
            aa.attribute_id as dynfield_attribute_id,
            a.table_name,
            a.attribute_name
        from
            im_dynfield_attributes aa,
            im_dynfield_layout dl,
            acs_attributes a 
        where 
            a.object_type in ([template::util::tcl_to_sql_list $object_types])
            and aa.attribute_id = dl.attribute_id
            and a.attribute_id = aa.acs_attribute_id
            and dl.page_url = :page_url
            and aa.attribute_id in (select distinct attribute_id from im_dynfield_type_attribute_map)"


    db_foreach attributes $attributes_sql {
        set write_p [permission::permission_p -no_login -party_id $rest_user_id -object_id $dynfield_attribute_id -privilege "write"]
        if {!$write_p} { continue }
        if {[info exists values($attribute_name)]} {
            # Get the ID Column for the update in the table
            if {![info exists id_columns($table_name)]} {
                set id_column [db_string get_id_column "select id_column from	(
                    select id_column,
                        1 as sort_order
                    from	acs_object_types
                    where	object_type in ([template::util::tcl_to_sql_list $object_types])
                    and table_name = :table_name
                    UNION
                    select	id_column,
                        2 as sort_order
                    from	acs_object_type_tables
                    where	object_type in ([template::util::tcl_to_sql_list $object_types])
                    and table_name = :table_name
                    ) t
                order by t.sort_order limit 1"]
                set id_columns($table_name) $id_column
            } else {
                set id_column $id_columns($table_name)
            }

            set $attribute_name $values($attribute_name)

            if {[set $attribute_name] eq "null"} {set $attribute_name ""}
            db_dml update_value "update $table_name set $attribute_name = :$attribute_name where $id_column = :object_id"
        }
    }

    set type_category_type [db_string type_category "select type_category_type from acs_object_types where object_type = :object_type" -default ""]
    if {$type_category_type ne ""} {
        set skill_configured [db_string skill "select  1
            from    im_freelance_skill_type_map, im_categories
            where
                category_type = :type_category_type
                and category_id = object_type_id limit 1" -default 0]
        if {$skill_configured} {
            db_foreach skill "select category_id as skill_type_id, aux_string1 as category_type, aux_string2 as attribute_name from im_categories where category_type = 'Intranet Skill Type'" {
                if {[info exists values($attribute_name)]} {
                    set skill_id $values($attribute_name)
                    if {$replace_skills_p} {
                        set delete_skills_sql "delete from im_object_freelance_skill_map where object_id =:object_id and skill_type_id =:skill_type_id"
                        db_dml delete_target_languages $delete_skills_sql
                    }
                    if {$skill_id ne ""} {
                        if {[string first "," $skill_id] > -1} {
                            set skills_to_be_inserted [split $skill_id ","]
                        } else {
                            set skills_to_be_inserted [list $skill_id]
                        }
                        im_freelance_add_required_skills -object_id $object_id -skill_type_id $skill_type_id -skill_ids $skills_to_be_inserted
                    }
                } 
            }
        }
    }

    cog::callback::invoke cog_rest::dynamic::put_object -object_id $object_id -object_types $object_types 

    cog::callback::invoke -object_id $object_id -action "after_update"
    set key_value_list [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::dynamic_object -object_id $object_id -rest_user_id $rest_user_id]]

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::component_plugins {
    -page_url:required
    -rest_user_id:required
} {
    Returns an array of portlets to display for an object at a given URL - they are component_plugins in the database, hence the 
    component_plugin datatype

    @param page_url string URL in which we want to display the portlets

    @return portlets json_array component_plugin Array of portlets to display.
} {


    set portlets [list]
    db_foreach component_plugin "select plugin_id, plugin_name,
        sort_order, location, title_tcl
        from im_component_plugins
        where page_url = :page_url
        and enabled_p = 't' " {

        if {![cog_rest::permission_p -object_id $plugin_id -user_id $rest_user_id -privilege "read"]} {
            continue
        }

        set pretty_key [lang::util::suggest_key $plugin_name]        
        set title_i18n_key "cognovis-rest.$pretty_key"
        
        if {![lang::message::message_exists_p en_US $title_i18n_key]} {
            if {$title_tcl ne ""} {
                if { ![string match "*#*" $title_tcl] } {
		            if {[catch $title_tcl title]} {
                        cog_log Error "Could not generate title for plugin $plugin_id - $title_tcl :: $title"
                    } else {
                        lang::message::register en_US cognovis-rest $pretty_key $title
                    }
                } else {
                    regsub -all {#} $title_tcl {} title_i18n_key
                }
            } else {
                lang::message::register en_US cognovis-rest $pretty_key $plugin_name
            }
		}

        set title [lang::message::lookup "" $title_i18n_key $plugin_name]
        lappend portlets [cog_rest::json_object]

    }
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::get::views {
    { -view_ids ""}
    { -view_type_id ""}
    -rest_user_id:required
} {
    Returns configuration for DynView 

    @param view_ids string ids of dynviews which we want to get. We get all in case its empty.

    @return views json_array view array of dynviews (im_views)

} {
    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set views [list]
    set where_clause_list [list]
    
    if {$view_ids ne ""} {
        set view_ids [split $view_ids ","]
        lappend where_clause_list "and view_id in([template::util::tcl_to_sql_list $view_ids])"
    }

    set sql "select v.*, v.sencha_configuration as json_configuration from im_views v where 1=1 [join $where_clause_list " and "]"
    db_foreach dynview $sql {
        lappend views [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::view {
    -rest_user_id:required
    -view_name:required
    -sort_order:required
    -view_type_id:required
    { -json_configuration "" }
    { -view_sql "" }
    { -visible_for "" }
    { -view_label "" }
} {
    @param view_body request_body The view information we want to provide

    @return single_view json_object Views for the object as an array with user information
} {
    
    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    # Mandatory fields are already set, directly include them in the list
    set inserts [list "view_id, view_name" "sort_order" "view_type_id"]
    set values [list "nextval('im_views_seq'), :view_name" ":sort_order" ":view_type_id"]

    if {$json_configuration ne ""} {
        lappend inserts "sencha_configuration"
        lappend values ":json_configuration"
    }

    if {$view_sql ne ""} {
        lappend inserts "view_sql"
        lappend values ":view_sql"
    }

    if {$visible_for ne ""} {
        lappend inserts "visible_for"
        lappend values ":visible_for"
    }

    if {$view_label ne ""} {
        lappend inserts "view_label"
        lappend values ":view_label"
    }

    db_dml create_im_view "insert into im_views ([join $inserts ", "]) values ([join $values ", "])"

    set view_id [db_string last_inserted_view_id "select lastval()" -default ""]
    if {$view_id ne ""} {
        set single_view [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::views -view_ids $view_id -rest_user_id $rest_user_id]]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::put::view {
    -rest_user_id:required
    -view_id:required
    { -view_name "" }
    { -json_configuration "" }
    { -view_type_id "" }
    { -view_sql "" }
    { -visible_for "" }
    { -sort_order "" }
    { -view_label "" }
} {
    @param view_id integer id of the view 
    @param view_body request_body The view information we want to provide

    @return view json_object Views for the object as an array with user information
} {
    
    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set update_list [list]
    if {$view_name ne ""} {
        lappend update_list "view_name = :view_name"
    }

    if {$json_configuration ne ""} {
        lappend update_list "sencha_configuration = :json_configuration"
    }

    if {$view_type_id ne ""} {
        lappend update_list "view_type_id = :view_type_id"
    }

    if {$view_sql ne ""} {
        lappend update_list "view_sql = :view_sql"
    }

    if {$visible_for ne ""} {
        lappend update_list "visible_for = :visible_for"
    }

    if {$sort_order ne ""} {
        lappend update_list "sort_order = :sort_order"
    }

    if {$view_label ne ""} {
        lappend update_list "view_label = :view_label"
    }

    if {[llength $update_list] > 0} {
        db_dml update_view "update im_views set [join $update_list " , "] where view_id = :view_id"
    }

    set view [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::views -view_ids "$view_id" -rest_user_id $rest_user_id]]
    
    return [cog_rest::json_response]
}



ad_proc -public cog_rest::delete::view {
    -rest_user_id:required
    -view_id:required
} {
    Purge a view and all its related columns from the database 

    @param view_id integer View we want to purge from the database

   @return errors json_array error Array of errors found
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set errors [list]

    # Try deleting columns related to the view
    if {[catch {
        db_dml delete_columns "delete from im_view_columns where view_id = :view_id"
    } errorMsg]} {
        lappend errors "Error while deleting columns: $errorMsg"
    }
    
    # Try deleting the view itself
    if {[catch {
        db_dml delete_view "delete from im_views where view_id = :view_id"
    } errorMsg]} {
        lappend errors "Error while deleting view: $errorMsg"
    }
 
    return [cog_rest::return_array]
    
}


ad_proc -public cog_rest::get::view_columns {
    -view_id:required
    -rest_user_id:required
} {
    Returns configuration for DynView columns 

    @param view_id integer id of dynview for which we want to get the columns.

    @return view_columns json_array view_column array of dynview columns (im_view_columns)

} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set view_columns [list] 
    # Set the where clause to filter the columns by view_id
    if {$view_id ne ""} {
        set where_clause_list [list]
        lappend where_clause_list "and view_id = :view_id"
    } else {
        return [cog_rest::json_response -code error -message "View ID is required."]
    }

    # Build SQL query to fetch columns from im_view_columns
    set sql "
        select
            column_id,
            view_id,
            group_id,
            column_name,
            column_render_tcl,
            extra_select,
            extra_from,
            extra_where,
            sort_order,
            order_by_clause,
            visible_for,
            ajax_configuration as json_configuration,
            variable_name,
            datatype
        from im_view_columns
        where 1=1 [join $where_clause_list " and "]
    "
    
    # Iterate through the results and append to view_columns list
    db_foreach view_column $sql {
        lappend view_columns [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::view_column {
    -view_id:required
    -rest_user_id:required
    -column_name:required
    -variable_name:required
    { -visible_for "" }
    { -json_configuration "" }
    { -sort_order "" }
} {
    @param view_column_body request_body The column information we want to provide

    @return single_view_column json_object Details of the column added
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }
    
    # Mandatory fields are already set, directly include them in the list
    set inserts [list "view_id" "column_id, column_name" "variable_name"]
    set values [list $view_id "nextval('im_view_columns_seq'), :column_name" ":variable_name"]

    if {$json_configuration ne ""} {
        lappend inserts "ajax_configuration"
        lappend values ":json_configuration"
    }

    if {$visible_for ne ""} {
        lappend inserts "visible_for"
        lappend values ":visible_for"
    }

    if {$sort_order ne ""} {
        lappend inserts "sort_order"
        lappend values ":sort_order"
    }

    db_dml create_im_view_column "insert into im_view_columns ([join $inserts ", "]) values ([join $values ", "])"

    set column_id [db_string last_inserted_column_id "select lastval()" -default ""]

    if {$column_id ne ""} {
        set single_view_column [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::view_columns -view_id $view_id -rest_user_id $rest_user_id]]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::put::view_column {
    -rest_user_id:required
    -column_id:required
    { -column_name "" }
    { -variable_name "" }
    { -visible_for "" }
    { -json_configuration "" }
    { -sort_order "" }
} {
    @param column_id integer id of the column
    @param view_column_body request_body The column information we want to provide

    @return column json_object Columns for the object as an array with user information
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }
    
    set update_list [list]
    set view_id [db_string get_view_id "select view_id from im_view_columns where column_id = :column_id" -default ""]

    if {$view_id ne ""} {
    
        if {$column_name ne ""} {
            lappend update_list "column_name = :column_name"
        }

        if {$variable_name ne ""} {
            lappend update_list "variable_name = :variable_name"
        }

        if {$visible_for ne ""} {
            lappend update_list "visible_for = :visible_for"
        }

        if {$json_configuration ne ""} {
            lappend update_list "ajax_configuration = :json_configuration"
        }

        if {$sort_order ne ""} {
            lappend update_list "sort_order = :sort_order"
        }

        if {[llength $update_list] > 0} {
            db_dml update_column "update im_view_columns set [join $update_list " , "] where column_id = :column_id"
        }

        set column [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynview_columns -view_id $view_id -rest_user_id $rest_user_id]]
    
    }
    
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::delete::view_column {
    -rest_user_id:required
    -column_id:required
} {
    Purge a single column from the database 

    @param column_id integer Column we want to purge from the database

    @return errors json_array error Array of errors found
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set errors [list]

    # Try deleting the specified column by its ID
    if {[catch {
        db_dml delete_column "delete from im_view_columns where column_id = :column_id"
    } errorMsg]} {
        lappend errors "Error while deleting column: $errorMsg"
    }

    return [cog_rest::return_array]
}


ad_proc -public cog_rest::get::dynfield_widgets {
    -rest_user_id:required
    { -widget_name "" }
    { -widget_id "" }
} {

    @param widget_id integer widget_id in case we want to query by id
    @param widget_name string widget_name in case we want to query by name

    @return dynfield_widgets json_array dynfield_widget Array of dynamic field widgets
} {
    
    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set dynfield_widgets [list]
    set where_clause_list [list "storage_type_id is not null"]

    if {$widget_id ne ""} {
        lappend where_clause_list "widget_id = :widget_id"
    }
    if {$widget_name ne ""} {
        # If we use 'widget_name' to query database, we should reset 'wheres' as we can't use both
        set where_clause_list [list]
        lappend where_clause_list "widget_name = :widget_name"
    }

    set where ""
    if {[llength $where_clause_list] > 0} {
        set where "where 1=1 and [join $where_clause_list " and "]"
    }

    set sql "select * from im_dynfield_widgets $where"
    db_foreach dynview $sql {
        set storage_type_name [im_category_from_id $storage_type_id]
        lappend dynfield_widgets [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::dynfield_widget {
    -rest_user_id:required
    -widget_name:required
    -pretty_name:required
    -pretty_plural:required
    -storage_type_id:required
    -acs_datatype:required
    -widget:required
    -sql_datatype:required
    { -parameters "" }
    { -deref_plpgsql_function "" }
} {
    @param widget_body request_body The widget information we want to provide

    @return single_widget json_object Details of the widget added
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set single_widget ""
    set widget_id [db_string create_widget "SELECT im_dynfield_widget__new (
		null,			-- widget_id
		'im_dynfield_widget',   -- object_type
		now(),			-- creation_date
		null,			-- creation_user
		null,			-- creation_ip
		null,			-- context_id
		:widget_name,		-- widget_name
		:pretty_name,		-- pretty_name
		:pretty_plural,		-- pretty_plural
		:storage_type_id,	-- storage_type_id
		:acs_datatype,		-- acs_datatype
		:widget,		-- widget
		:sql_datatype,		-- sql_datatype
		:parameters		-- parameters
	);" -default ""]


    if {$widget_id ne ""} {
        set single_widget [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynfield_widgets -widget_id $widget_id -rest_user_id $rest_user_id]]
    }

    return [cog_rest::json_response]
}



ad_proc -public cog_rest::put::dynfield_widget {
    -rest_user_id:required
    -widget_id:required
    { -widget_name "" }
    { -pretty_name "" }
    { -pretty_plural "" }
    { -storage_type_id "" }
    { -acs_datatype "" }
    { -widget "" }
    { -sql_datatype "" }
    { -parameters "" }
    { -deref_plpgsql_function "" }
} {
    @param widget_id integer id of the widget
    @param widget_body request_body The widget information we want to provide

    @return widget json_object Widget details updated
} {
    
    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }


    set update_list [list]

    if {$widget_name ne ""} {
        lappend update_list "widget_name = :widget_name"
    }

    if {$pretty_name ne ""} {
        lappend update_list "pretty_name = :pretty_name"
    }

    if {$pretty_plural ne ""} {
        lappend update_list "pretty_plural = :pretty_plural"
    }

    if {$storage_type_id ne ""} {
        lappend update_list "storage_type_id = :storage_type_id"
    }

    if {$acs_datatype ne ""} {
        lappend update_list "acs_datatype = :acs_datatype"
    }

    if {$widget ne ""} {
        lappend update_list "widget = :widget"
    }

    if {$sql_datatype ne ""} {
        lappend update_list "sql_datatype = :sql_datatype"
    }

    if {$parameters ne ""} {
        lappend update_list "parameters = :parameters"
    }

    if {$deref_plpgsql_function ne ""} {
        lappend update_list "deref_plpgsql_function = :deref_plpgsql_function"
    }

    if {[llength $update_list] > 0} {
        db_dml update_widget "update im_dynfield_widgets set [join $update_list " , "] where widget_id = :widget_id"
    }
    
    set widget [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynfield_widgets -widget_id $widget_id -rest_user_id $rest_user_id]]
    
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::delete::dynfield_widget {
    -rest_user_id:required
    -widget_id:required
} {
    Purge a single widget from the database 

    @param widget_id integer Widget we want to purge from the database

    @return errors json_array Array of errors found
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set errors [list]

    # Try deleting the specified widget by its ID
    if {[catch {
        db_dml delete_widget "delete from im_dynfield_widgets where widget_id = :widget_id"
    } errorMsg]} {
        lappend errors "Error while deleting widget: $errorMsg"
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::get::dynfield_layout_pages {
    -rest_user_id:required
    { -object_type "" }
    { -page_url "" }
} {
    Retrieve layouts for dynamic fields based on either attribute_id or page_url

    @param object_type string type of object to which that layout page is associated with
    @param page_url string page_url of the page

    @return dynfield_layout_pages json_array dynfield_layout_page Array containing layouts for dynamic fields
} {
    
    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set dynfield_layout_pages [list]
    set where_clause_list [list]

    if {$page_url ne ""} {
        lappend where_clause_list "page_url = :page_url"
    }

    if {$object_type ne ""} {
        lappend where_clause_list "object_type = :object_type"
    }
 
    set where ""
    if {[llength $where_clause_list] > 0} {
        set where "where [join $where_clause_list " and "]"
    }

    set sql "select 
		page_url, 
        layout_type,
        object_type,
		default_p
	from 
		im_dynfield_layout_pages
	$where
    order by object_type
    "

    db_foreach layout $sql {
        lappend dynfield_layout_pages [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::dynfield_layout_pages {
    -rest_user_id:required
    -page_url:required
    -object_type:required
    -layout_type:required
} {
    Create a new dynamic field layout page entry in the database based on the provided parameters.

    @param dynfield_layout_page_body request_body The layout page information we want to provide

    @return single_layout_page json_object Details of the layout page just added to the database
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }
    
    # Directly use the mandatory fields for insertion
    set inserts [list "page_url" "object_type" "layout_type"]
    set values [list ":page_url" ":object_type" ":layout_type"]

    db_dml create_dynfield_layout_page "insert into im_dynfield_layout_pages ([join $inserts ", "]) values ([join $values ", "])"
    set single_layout_page [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynfield_layout_pages -page_url $page_url -object_type $object_type -rest_user_id $rest_user_id]]

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::put::dynfield_layout_pages {
    -rest_user_id:required
    -page_url:required
    -object_type:required
    { -layout_type "" }
} {
    Update a dynamic field layout page entry in the database based on the provided parameters.

    @param dynfield_layout_page_body request_body The layout page information we want to provide

    @return updated_layout_page json_object Details of the updated layout page
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }
    
    db_dml update_layout_page "update im_dynfield_layout_pages set layout_type = :layout_type where page_url = :page_url and object_type = :object_type"
    set updated_layout_page [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynfield_layout_pages -page_url $page_url -object_type $object_type -rest_user_id $rest_user_id]]

    return [cog_rest::json_response]
}



ad_proc -public cog_rest::delete::dynfield_layouts {
    -rest_user_id:required
    -page_url:required
} {
    Purge a single layout and associated attributes from the database based on its page_url

    @param page_url string Page URL of the layout we want to purge from the database

    @return errors json_array Array of errors found
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set errors [list]

    if {[catch {
        db_transaction {
            db_dml delete_page_attributes {
                delete from im_dynfield_layout
                where page_url = :page_url
            }
            db_dml delete_page_layout {
                delete from im_dynfield_layout_pages
                where page_url = :page_url
            }
        }
    } errorMsg]} {
        lappend errors "Error while deleting layout and its attributes: $errorMsg"
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::get::dynfield_layout_page_attributes {
    -rest_user_id:required
    { -attribute_id "" }
    { -page_url "default" }
    { -object_type "" }
} {
    Retrieve layout attributes based on the page_url and object_type.

    @param attribute_id integer id of the attribute. It's optional.
    @param page_url string (optional) Filters results based on page_url. Defaults to 'default'.
    @param object_type string (optional) Filters results based on object_type. Defaults to 'person'.

    @return dynfield_layout_attributes json_array dynfield_layout_attribute The array containing layout attributes for dynamic fields
} {


    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set dynfield_layout_attributes [list]
    set where_clause_list [list]

    if {$attribute_id ne ""} {
        lappend where_clause_list "da.attribute_id = :attribute_id"
    }

    set where ""
    if {[llength $where_clause_list] > 0} {
        set where "and [join $where_clause_list " and "]"
    }


    # Construct the SQL query
    set sql "
        select distinct
            aa.*,
            da.*,
            dl.*
        from
            im_dynfield_layout_pages dlp,
            im_dynfield_layout dl,
            im_dynfield_attributes da,
            acs_attributes aa
        where
            dlp.page_url = dl.page_url and
            dl.attribute_id = da.attribute_id and
            da.acs_attribute_id = aa.attribute_id and
            aa.object_type = :object_type and
            dl.page_url = :page_url
            $where
    "

    # Execute the SQL query
    db_foreach layout_attr $sql {
        lappend dynfield_layout_attributes [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}



ad_proc -public cog_rest::post::dynfield_layout_page_attributes {
    -rest_user_id:required
    -page_url:required
    { -attribute_id ""}
    { -object_type ""}
    { -pos_x "" }
    { -pos_y "" }
    { -size_x "" }
    { -size_y "" }
    { -label_style "" }
    { -div_class "" }
    { -sort_key "" }
    { -add_all_attributes_p "0"}
} {
    Create a new dynamic field layout page attribute entry in the database based on the provided parameters.

    @param dynfield_layout_page_attributes_body request_body The layout page attributes information we want to provide

    @return single_layout_page_attribute json_object Details of the layout page attribute just added to the database
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    if {$add_all_attributes_p} {
        set pos_y 0
        set label_style "plain"
        set sql "
            select distinct
            da.*
            from 
                im_dynfield_attributes da,
                acs_attributes aa
            where
                da.acs_attribute_id = aa.attribute_id and
                aa.object_type = :object_type
            "
        db_foreach all_attrib $sql {
            set exists_p [db_string exists "
            select	count(*) 
            from	im_dynfield_layout 
            where	attribute_id = :attribute_id and page_url = :page_url
            "]
            if {!$exists_p} {
                db_dml insert "
                insert into im_dynfield_layout (
                    attribute_id, page_url, pos_y, label_style
                ) values (
                    :attribute_id, :page_url, :pos_y, :label_style
                )
                "
            }
        }
    } else {
        db_dml insert_attribute {
            insert into im_dynfield_layout
            (attribute_id, page_url, pos_x, pos_y, size_x, size_y, label_style, div_class, sort_key)
                values
                (:attribute_id, :page_url, :pos_x, :pos_y, :size_x, :size_y, :label_style, :div_class, :sort_key)
        }
        set object_type [db_string get_object_type "select object_type from im_dynfield_attributes da, acs_attributes a where a.attribute_id = da.acs_attribute_id and da.attribute_id = :attribute_id" -default ""]
    }

    if {$object_type ne ""} {
        set single_layout_page_attribute [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynfield_layout_page_attributes -attribute_id $attribute_id -page_url $page_url -object_type $object_type -rest_user_id $rest_user_id]]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::put::dynfield_layout_page_attributes {
    -rest_user_id:required
    -attribute_id:required
    -page_url:required
    { -pos_x "" }
    { -pos_y "" }
    { -size_x "" }
    { -size_y "" }
    { -label_style "" }
    { -div_class "" }
    { -sort_key "" }
} {
    Update the dynamic field layout page attribute in the database based on the provided parameters.

    @param dynfield_layout_page_attributes_body request_body The layout page attribute information we want to update

    @return single_layout_page_attribute json_object Details of the layout page attribute just updated in the database
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set where_clause_list [list]

    if {$pos_x ne ""} {
        lappend where_clause_list "pos_x = :pos_x"
    }
    if {$pos_y ne ""} {
        lappend where_clause_list "pos_y = :pos_y"
    }
    if {$size_x ne ""} {
        lappend where_clause_list "size_x = :size_x"
    }
    if {$size_y ne ""} {
        lappend where_clause_list "size_y = :size_y"
    }
    if {$label_style ne ""} {
        lappend where_clause_list "label_style = :label_style"
    }
    if {$div_class ne ""} {
        lappend where_clause_list "div_class = :div_class"
    }
    if {$sort_key ne ""} {
        lappend where_clause_list "sort_key = :sort_key"
    }

    set update_sql "
        update im_dynfield_layout set 
        [join $where_clause_list ", "]
        where attribute_id = :attribute_id
        and page_url = :page_url
    "
    
    db_dml update_attribute $update_sql
    
    set object_type [db_string get_object_type "select object_type from im_dynfield_attributes da, acs_attributes a where a.attribute_id = da.acs_attribute_id and da.attribute_id = :attribute_id" -default ""]
    if {$object_type ne ""} {
        set single_layout_page_attribute [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynfield_layout_page_attributes -attribute_id $attribute_id -page_url $page_url -object_type $object_type -rest_user_id $rest_user_id]]
    }
   
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::delete::dynfield_layout_page_attributes {
    -rest_user_id:required
    -page_url:required
    -attribute_id:required
} {
    Purge a single attribute associated with a layout page from the database based on its page_url and attribute_id

    @param page_url string Page URL of the layout the attribute is associated with
    @param attribute_id integer Attribute ID we want to purge from the database

    @return errors json_array Array of errors found
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set errors [list]

    # Try deleting the specified attribute associated with the given layout page
    if {[catch {
        db_dml remove_attribute {
            delete from im_dynfield_layout 
            where 
                page_url = :page_url and 
                attribute_id = :attribute_id
        }
    } errorMsg]} {
        lappend errors "Error while deleting the attribute associated with the layout page: $errorMsg"
    }

    return [cog_rest::json_response]
}



# ad_proc -public cog_rest::post::dynfield_layouts { }
# ad_proc -public cog_rest::put::dynfield_layouts {}




ad_proc -public cog_rest::get::dynfield_attributes {
    -rest_user_id:required
    { -attribute_id "" }
    { -object_type ""}
    { -page_url "" }
} {
    Retrieve attributes for dynamic fields based on either attribute_id or page_url

    @param attribute_id integer (optional) Filters results based on attribute_id
    @param object_type string (optional) type of the object

    @return dynfield_attributes json_array dynfield_attr Array containing attributes for dynamic fields
} {
    
    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set dynfield_attributes [list]
    set where_clause_list [list]

    set secion_heading ""
    set default_value ""
    set help_url ""
    set help_text ""

    if {$attribute_id ne ""} {
        lappend where_clause_list "fa.attribute_id = :attribute_id"
        db_0or1row get_help_text "select section_heading, default_value, help_url, help_text from im_dynfield_type_attribute_map where attribute_id = :attribute_id limit 1"
    }

    if {$object_type ne ""} {
        lappend where_clause_list "a.object_type = :object_type"
    }

    set where_clause ""
    if {[llength $where_clause_list] > 0} {
        set where_clause "and [join $where_clause_list " and "]"
    }

    set sql "
        select
        a.attribute_name as dynfield_name,
        fa.attribute_id as dynfield_id,
        a.object_type,
        a.pretty_name,
        a.pretty_plural,
        a.attribute_id as acs_attribute_id,
        a.attribute_name,
        a.table_name,
        case when a.min_n_values = 0 then 'f' else 't' end as required_p,
            fa.widget_name,
        fa.include_in_search_p,
        fa.also_hard_coded_p,
        dl.pos_x, dl.pos_y,
        dl.size_x, dl.size_y,
        dl.label_style, dl.div_class
        from 	
        acs_attributes a,
            im_dynfield_attributes fa
        LEFT OUTER JOIN
            (select * from im_dynfield_layout where page_url = 'default') dl
            ON (fa.attribute_id = dl.attribute_id)
        where
             fa.acs_attribute_id = a.attribute_id
            $where_clause
    "

    db_foreach attribute $sql {
        lappend dynfield_attributes [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::dynfield_attribute {
    -rest_user_id:required
    -attribute_name:required
    -object_type:required
    -widget_name:required
    -pretty_name:required
    -table_name:required
    { -also_hard_coded "0"}
    { -required "0"}
    { -pretty_plural "" }
    { -include_in_search "0" }
    { -label_style "plain" }
    { -pos_y "" }
    { -help_text "" }
    { -help_url "" }
    { -default_value "" }
    { -section_heading "" }
} {
    @param dynfield_attr_body request_body The attribute information we want to provide

    @return dynfield_attribute json_object Details of the attribute added
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set dynfield_attribute ""
    set modify_sql_p "t"

    set datatype [db_string datatype "select acs_datatype from im_dynfield_widgets where widget_name = :widget_name" -default ""]
    set include_in_search $required


    set attribute_id [im_dynfield::attribute::add \
        -object_type $object_type \
        -widget_name $widget_name \
        -attribute_name $attribute_name \
        -pretty_name $pretty_name \
        -pretty_plural $pretty_plural \
        -table_name $table_name \
        -required_p $required \
        -modify_sql_p $modify_sql_p \
        -include_in_search_p $include_in_search \
        -also_hard_coded_p $also_hard_coded \
        -datatype $datatype \
        -label_style $label_style \
        -pos_y $pos_y \
        -help_text $help_text \
        -help_url $help_url \
        -default_value $default_value \
        -section_heading $section_heading \
    ]

    if {$attribute_id ne ""} {
        set dynfield_attribute [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynfield_attributes -attribute_id $attribute_id -rest_user_id $rest_user_id]]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::put::dynfield_attribute {
    -rest_user_id:required
    -attribute_id:required
    { -attribute_name "" }
    { -widget_name "" }
    { -pretty_name "" }
    { -pretty_plural "" }
    { -also_hard_coded_p "" }
    { -required_p "" }
    { -pos_y "" }
    { -help_text "" }
    { -help_url "" }
    { -default_value "" }
    { -section_heading "" }
} {
    @param attribute_id integer ID of the attribute.
    @param dynfield_attr_body request_body The attribute information we want to update.

    @return attribute json_object Updated details of the attribute.
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set update_acs_list [list]
    set update_dynfield_list [list]
    set update_dynfield_map_list [list]
    set update_dynfield_layout_list [list]

    if {$attribute_name ne ""} {
        lappend update_acs_list "attribute_name = :attribute_name"
    }

    if {$widget_name ne ""} {
        lappend update_dynfield_list "widget_name = :widget_name"
    }

    if {$pretty_name ne ""} {
        lappend update_acs_list "pretty_name = :pretty_name"
    }

    if {$pretty_plural ne ""} {
        lappend update_acs_list "pretty_plural = :pretty_plural"
    }

    if {$also_hard_coded_p ne ""} {
        lappend update_dynfield_list "also_hard_coded_p = :also_hard_coded_p"
    }

    if {$pos_y ne ""} {
       lappend update_dynfield_layout_list "pos_y = :pos_y"
    }
    
    if {$required_p ne ""} {
        lappend update_dynfield_map_list "required_p = :required_p"
    }

    if {$help_text ne ""} {
        lappend update_dynfield_map_list "help_text = :help_text"
    }

    if {$help_url ne ""} {
        lappend update_dynfield_map_list "help_url = :help_url"
    }

    if {$default_value ne ""} {
        lappend update_dynfield_map_list "default_value = :default_value"
    }

    if {$section_heading ne ""} {
        lappend update_dynfield_map_list  "section_heading = :section_heading"
    }

     set object_type [db_string get_object_type "select object_type from im_dynfield_attributes da, acs_attributes a where a.attribute_id = da.acs_attribute_id and da.attribute_id = :attribute_id" -default ""]

    db_transaction {
        if {[llength $update_acs_list] > 0} {
            db_dml update_acs_attributes "
                update acs_attributes set
                    [join $update_acs_list " , "]
                where attribute_id = (
                    select acs_attribute_id 
                    from im_dynfield_attributes 
                    where attribute_id = :attribute_id
                )
            "
        }

        if {[llength $update_dynfield_list] > 0} {
            db_dml update_dynfield_attributes "
                update im_dynfield_attributes set
                    [join $update_dynfield_list " , "]
                where attribute_id = :attribute_id
            "
        }

        if {[llength $update_dynfield_layout_list] > 0} {
            # Make sure there is a layout entry for this DynField
            set layout_exists_p [db_string layout_exists "select count(*) from im_dynfield_layout where attribute_id = :attribute_id and page_url = 'default'"]
            if {!$layout_exists_p && 0 != $attribute_id} {
                db_dml insert_layout "
                insert into im_dynfield_layout (
                    attribute_id, page_url, pos_y
                ) values (
                    :attribute_id, 'default', :pos_y
                )
                "
            }


            db_dml update_layout "
                update im_dynfield_layout set
                    [join $update_dynfield_layout_list ", "]
                where
                    attribute_id = :attribute_id
                    and page_url = 'default'
            "
        }

        set category_ids [db_list category_types "
                select category_id 
                from im_categories, acs_object_types
                where object_type = :object_type
                and category_type = type_category_type"]
        foreach category_id $category_ids {
            set attribute_object_type_exists_p [db_string attribute_exists_object_type "select 1 from im_dynfield_type_attribute_map where attribute_id = :attribute_id and object_type_id = :category_id limit 1" -default 0]
            if {!$attribute_object_type_exists_p} {
                db_dml insert "insert into im_dynfield_type_attribute_map (
                        attribute_id, object_type_id, display_mode, help_text, help_url, default_value, section_heading, required_p
                    ) values (
                        :attribute_id, :category_id, 'edit', :help_text, :help_url, :default_value, :section_heading, :required_p
                    )"
            } else {
                db_dml update_atrribute_type_map "update im_dynfield_type_attribute_map set
                    [join $update_dynfield_map_list " ,"]
                    where attribute_id = :attribute_id
                    and object_type_id = :category_id"
            }
        }
        
    }

    set attribute [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynfield_attributes -attribute_id $attribute_id -rest_user_id $rest_user_id]]
    
    return [cog_rest::json_response]
}



ad_proc -public cog_rest::delete::dynfield_attribute {
    -rest_user_id:required
    -attribute_id:required
} {
    Purge an attribute from the system using the defined procedure.

    @param attribute_id integer Attribute we want to purge from the system.

    @return errors json_array Array of errors encountered during deletion.
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set errors [list]

    # Try deleting the attribute using the defined procedure
    if {[catch {
        attribute::delete_xt $attribute_id
    } errorMsg]} {
        lappend errors "Error while deleting attribute: $errorMsg"
    }

    return [cog_rest::return_array]
}


ad_proc -public cog_rest::get::dynfield_permissions {
    { -object_type "" }
    { -attribute_id "" }
} {

    @param object_type string Optional object type filter
    @param attribute_id integer Optional attribute id filter

    @return dynfield_permissions json_array dynfield_permission Permission details for dynamic fields
} {
    

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set dynfield_permissions [list]

    set object_type_where ""

    if {$object_type ne ""} {
        set object_type_where "and aa.object_type = :object_type"
    }
    if {$attribute_id ne ""} {
        set object_type_where "and fa.attribute_id = :attribute_id"
    }

    set group_ids [list 461 463 465 467 471]

    set attributes_permissions_sql "
        select da.attribute_id as dynfield_id, a.attribute_name as dynfield_name, p.grantee_id as group_id, p.privilege, a.object_type
        from acs_permissions p, im_dynfield_attributes da, acs_attributes a
        where a.attribute_id = da.acs_attribute_id
        and p.grantee_id in ([template::util::tcl_to_sql_list $group_ids])
        and p.object_id = da.attribute_id
        $object_type_where
    "

    # Execute SQL and append results to the list
    db_foreach attribute_perm $attributes_permissions_sql {
        lappend dynfield_permissions [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::get::dynfield_types_map {
    -object_type:required
    -rest_user_id:required
    { -attribute_id "" }
} {

    @param object_type string Optional object type filter
    @param attribute_id integer Optional attribute id filter

    @return dynfield_types_maps json_array dynfield_type_map Permission details for dynamic fields
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set dynfield_types_maps [list]
    set category_type [im_dynfield::type_category_for_object_type -object_type $object_type]

    set types_sql "select category_id as type_id, category as type_name from im_categories where category_type = :category_type"

    set attribute_where ""
    if {$attribute_id ne ""} {
        set attribute_where "and m.attribute_id = :attribute_id"
    }

    set sql "select m.attribute_id as dynfield_id, aa.attribute_name as dynfield_name,
	        m.object_type_id,
	        m.display_mode as mode,
            c.category as object_type_name
	from
	        im_dynfield_type_attribute_map m,
	        im_dynfield_attributes a,
	        acs_attributes aa,
            im_categories c
	where
	        m.attribute_id = a.attribute_id
	        and a.acs_attribute_id = aa.attribute_id
            and c.category_id = m.object_type_id
	        and aa.object_type = :object_type
            $attribute_where
	order by
		aa.pretty_name"


    db_foreach get_map $sql {
        set key "$dynfield_id.$object_type_id"
        set mapped_attribute_types($key) $mode
    }


    set dynfields_sql "select distinct m.attribute_id as dynfield_id, aa.attribute_name as dynfield_name 
        from im_dynfield_attributes a, acs_attributes aa, im_dynfield_type_attribute_map m
        where
        m.attribute_id = a.attribute_id
        and a.acs_attribute_id = aa.attribute_id
        and aa.object_type = :object_type
        $attribute_where
    "

    db_foreach get_map $dynfields_sql {
        set mapped_dynfield_types [list]
        db_foreach type $types_sql {
            set key "$dynfield_id.$type_id"
            set mapped_object_type_id $type_id
            set mapped_object_type_name $type_name
            if {[info exists mapped_attribute_types($key)]} {
                set display_mode $mapped_attribute_types($key)
            } else {
                set display_mode "none"
            }
            lappend mapped_dynfield_types [cog_rest::json_object -object_class mapped_dynfield_type]
        }
        lappend dynfield_types_maps [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::post::dynfield_type_map {
    -rest_user_id:required
    -object_type_id:required
    -attribute_id:required
    -action:required
} {
    Operate on the dynfield_type_attribute_map based on provided parameters.

    @param dynfield_type_map_body request_body post data we want to insert/edit

    @return attribute json_object Updated details of the attribute type map
} {

    if {![im_is_user_site_wide_or_intranet_admin $rest_user_id]} {
        cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_perm_change_other_users]"
        return [cog_rest::json_error "Permission denied."]
    }

    set currently_exist_p [db_string check_existance "select 1 from im_dynfield_type_attribute_map where attribute_id = :attribute_id and object_type_id = :object_type_id" -default 0]

    if {$action eq "none"} {
        if {$currently_exist_p} {
            db_dml delete_dynfield_type_attribute_map "
            delete from im_dynfield_type_attribute_map
            where
                attribute_id = :attribute_id
                and object_type_id = :object_type_id
            "
        }
    } else {
        if {$currently_exist_p} {
		    db_dml update_dynfield_type_attribute_map "
                update im_dynfield_type_attribute_map
                set display_mode = :action
                where
                    attribute_id = :attribute_id
                    and object_type_id = :object_type_id
		    "
        } else {
            # Didn't exist before - insert
            db_dml insert_dynfield_type_attribute_map "
                insert into im_dynfield_type_attribute_map (
                    attribute_id, object_type_id, display_mode
                ) values (
                    :attribute_id, :object_type_id, :action
                )
            "
        }
    }
    
    set object_type [db_string get_object_type "select object_type from im_dynfield_attributes da, acs_attributes a where da.acs_attribute_id = a.attribute_id and da.attribute_id = :attribute_id" -default ""]
    set attribute [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::dynfield_types_map -attribute_id $attribute_id -rest_user_id $rest_user_id -object_type $object_type]]
    
    return [cog_rest::json_response]

}



