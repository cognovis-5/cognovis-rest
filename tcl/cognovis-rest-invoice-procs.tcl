ad_library {
    REST Procedures for GET calls related to Invoices and Costs
    @author malte.sussdorff@cognovis.de
    @author frank.bergmann@project-open.com
    @author michaldrn@wp.pl
}

namespace eval cog_rest::json_object {
    ad_proc invoice_item {} {
        @return item object im_invoice_item Line item of the invoice
        @return invoice object im_invoice Invoice in which we find the invoice item
        @return item_units number of units of the line item
        @return item_uom category "Intranet UoM" unit of measure id of the line item
        @return price_per_unit number price per unit of the line item
        @return currency string currency ISO of the line item (e.g. EUR or USD)
        @return symbol string currency symbol to display in the line item
        @return sort_order integer sequence order of the line item
        @return item_material named_id material of the line item
        @return source_language category "Intranet Translation Language" Source Language of the Material
        @return target_language category "Intranet Translation Language" Target Language of the Material
        @return task_type category "Intranet Project Type" Type of task associated with the Material
        @return subject_area category "Intranet Translation Subject Area" Subject Area of the associated material for the line item's material
        @return description string Description for the line item
        @return context named_id context in which the line item was created
        @return item_already_invoiced_p boolean was item already invoiced?
    } - 

    ad_proc invoice_item_body {} {
        @param item_name string line item name displayed on invoice
        @param item_uom_id category_id "Intranet UoM" line item unit of measure id
        @param item_units number line item number of units
        @param price_per_unit number line item price per unit
        @param sort_order integer position displayed in invoice
        @param source_language_id category "Intranet Translation Language" Source Language of the Material
        @param target_language_id category "Intranet Translation Language" Target Language of the Material
        @param material_nr string Material Nr. of the material we are trying to create the invoice_item for
        @param item_material_id integer Material ID of the material we are trying to create the invoice_item for
        @param task_id integer TaskId for/from which we create this invoice_item
        @param context_id integer Assignment or other context for which create this item. defaults back to task_id
        @param description string description of line item
    } - 

    ad_proc invoice_item_body_put {} {
        @param item_name string line item name displayed on invoice
        @param item_uom_id category_id "Intranet UoM" line item unit of measure id
        @param item_units number line item number of units
        @param price_per_unit number line item price per unit
        @param sort_order integer position displayed in invoice
        @param description string description of line item
        @param source_language_id category "Intranet Translation Language" Source Language of the Material
        @param target_language_id category "Intranet Translation Language" Target Language of the Material
        @param material_nr string Material Nr. of the material we are trying to create the invoice_item for
        @param item_material_id integer Material ID of the material we are trying to create the invoice_item for
        @param context_id integer Assignment or other context for this item

    } - 

    ad_proc invoice {} {
        @return invoice object im_invoice Invoice item
        @return project named_id Project in which the invoice resides
        @return company named_id Company which is the recipient / provider for this invoice (aka the 'other' party)
        @return company_contact named_id Contact for the invoice
        @return provider named_id Company who is the providing contact for the invoice. Should be an internal company.
        @return first_names string First name of the Contact
        @return last_name string Last name of the Contact
        @return email string E-mail address of the contact
        @return phone string Phone number under which the user can be reached. cell_phone > work_phone > private_phone
        @return invoice_office named_id Office which is to recieve the invoice
        @return address_line1 string Usually the street address or c/o
        @return address_line2 string Additional address line if needed
        @return address_city string City in which the office resides
        @return address_state string State of the office
        @return address_postal_code string Postal code for the invoice
        @return address_country string Actual country name (internationalized)
        @return creation_user named_id Who created the invoice
        @return creation_date date When was the invoice created
        @return cost_center named_id Name of the cost center for the invoice
        @return cost_status category "Intranet Cost Status" Status of the invoice
        @return cost_type category "Intranet Cost Type" Type of the invoice
        @return template named_id Template used for printing the invoice
        @return payment_method category "Intranet Invoice Payment Method" Method of payment
        @return payment_term category "Intranet Payment Term" Terms for the payment (aka how fast has the payment to be made)
        @return currency_name string Name of the currency
        @return currency_symbol string Symbol for the currency to be used
        @return vat_type category "Intranet VAT Type" Type of VAT, used for calculation
        @return delivery_date date When were the services provided (especially if the invoice is issued later)
        @return effective_date date The actual invoice date (to be printed on the invoice pdf)
        @return linked_invoice named_id linked invoice name and id. Invoice in case of quote, Provider Bill in case of PO
        @return linked_objects json_array cognovis_object Linked objects for this invoice
        @return linked_objects_p boolean Do we have linked objects
        @return amount number Amount for the invoice before VAT
        @return vat number value (percentage) of VAT
        @return vat_amount number VAT for the invoice
        @return grand_total number Grand total for the invoice
    } -

    ad_proc invoice_body {} {

        @param company_id object im_company::read Company which is the recipient / provider for this invoice (aka the 'other' party)
        @param company_contact_id integer user Contact for the invoice
        @param project_id object im_project::read Project in which we want to create the invoice
        @param provider_id object im_company::read Company which is the originator for the invoice. Should be an internal company
        @param provider_contact_id integer user Contact for the invoice
        @param cost_center_id integer im_cost_center Cost center for the invoice
        @param cost_status_id category "Intranet Cost Status" Status of the invoice
        @param cost_type_id category "Intranet Cost Type" Type of the invoice
        @param template_id category "Intranet Cost Template" Template used for printing the invoice
        @param payment_method_id category "Intranet Invoice Payment Method" Method of payment
        @param payment_term_id category "Intranet Payment Term" Terms for the payment (aka how fast has the payment to be made)
        @param currency_iso string ISO code of the currency
        @param vat_type_id category "Intranet VAT Type" Type of VAT, used for calculation
        @param delivery_date date When were the services provided (especially if the invoice is issued later)
        @param effective_date date The actual invoice date (to be printed on the invoice pdf)
    } - 

    ad_proc invoice_body_put {} {

        @param company_id integer im_company Company which is the recipient / provider for this invoice (aka the 'other' party)
        @param company_contact_id integer user Contact for the invoice
        @param cost_center_id integer im_cost_center Cost center for the invoice
        @param cost_status_id category "Intranet Cost Status" Status of the invoice
        @param cost_type_id category "Intranet Cost Type" Type of the invoice
        @param template_id category "Intranet Cost Template" Template used for printing the invoice
        @param payment_method_id category "Intranet Invoice Payment Method" Method of payment
        @param payment_term_id category "Intranet Payment Term" Terms for the payment (aka how fast has the payment to be made)
        @param currency_iso string ISO code of the currency
        @param delivery_date date When were the services provided (especially if the invoice is issued later)
        @param effective_date date The actual invoice date (to be printed on the invoice pdf)
    } - 

    ad_proc invoice_timesheet_tasks_put {} {
        @param hour_log_ids integer_array Hours we want to add to the invoice
        @param only_unassigned boolean Only add hours which are not yet assigned to an invoice
    } - 

    ad_proc invoice_items_sort_order_put {} {
        @param invoice_items_sort_order json_array invoice_item array of integers appearing in new order
    } - 


}

#---------------------------------------------------------------
# Invoices
#---------------------------------------------------------------

ad_proc -public cog_rest::get::invoice {
    { -invoice_ids "" }
    { -project_id "" }
    { -cost_type_ids "" }
    { -cost_status_ids "" }    
    { -pagination "" }
    { -company_id "" }
    -rest_user_id
} {
    Return invoice data for the selected invoices (or all per filter)
    
    Note invoice is a placeholder for every kind of invoice (invoice, credit_memo, provider bill, quote, cancellation invoice ........)

    @param company_id object im_company::read Company which is the recipient / provider for this invoice (aka the 'other' party)
    @param invoice_ids object_array im_invoice::view invoice_ids which we want to view the invoice for
    @param project_id object im_project::view id for which we need quotes
    @param cost_type_ids category_array "Intranet Cost Type" Limit to only documents of this cost_type
    @param cost_status_ids category_array "Intranet Cost Status" Limit to only documents of this cost status
    @param pagination pagination_object Sort by property cost_name or effective_date is sensible

    @return invoices json_array invoice Array of the invoice information

} {
    # Prepare for the return parameters
    set invoices [list]
    set where_clause_list [list]
    
    if {$invoice_ids ne ""} {
        lappend where_clause_list "i.invoice_id in ([template::util::tcl_to_sql_list $invoice_ids])"
    }

    if {$cost_type_ids eq ""} {
        set cost_type_ids [concat [im_sub_categories [im_cost_type_customer_doc]] [im_sub_categories [im_cost_type_provider_doc]]]
    } else {
        set cost_type_ids [cog_sub_categories $cost_type_ids]
    }
    lappend where_clause_list  "c.cost_type_id in ([template::util::tcl_to_sql_list $cost_type_ids])"

    if {$cost_status_ids eq ""} {
        # Do not include financial documents with status 3818 and 3812 with subcategories
        set not_in_cost_status_id [im_sub_categories [im_cost_status_deleted]]
        lappend not_in_cost_status_id [im_cost_status_rejected]
        if {$invoice_ids eq ""} {
            lappend where_clause_list "c.cost_status_id not in ([template::util::tcl_to_sql_list $not_in_cost_status_id]) "
        }
    } else {
        lappend where_clause_list "cost_status_id in ([template::util::tcl_to_sql_list [cog_sub_categories $cost_status_ids]])"
    }

    # If project_id is not given, we show documents associated with rest_user_id
    set view_p 0

    if {$project_id eq ""} {

        set read_all_p [im_permission $rest_user_id "view_finance"]
        
        if {!$read_all_p} {
            # If user is a freelancer we want to limit invoice search to his assignments
            if {[im_user_is_freelance_p $rest_user_id]} {
                set freelancer_company_id [cog::user::main_company_id -user_id $rest_user_id]
                lappend where_clause_list "c.provider_id =$freelancer_company_id"
                set view_p 1
            } 
            if {[im_user_is_customer_p $rest_user_id]} {
                set customer_company_id [cog::user::main_company_id -user_id $rest_user_id]
                lappend where_clause_list "c.customer_id =:customer_company_id"
                set view_p 1
            } 
            if {!$view_p} {
                # See all invoices the user has access to

                if {$company_id ne ""} {
                    lappend where_clause_list "(customer_id = :company_id or provider_id = :company_id)"
                }

                set sql "
                select project_id
                from acs_rels r, im_projects p
                where   object_id_one = project_id and 
                    (   object_id_two = :user_id
                    OR  object_id_two in (
                            select   group_id
                            from     group_distinct_member_map
                            where    member_id = :user_id
                        )
                    )
                "

                lappend where_clause_list "c.project_id in ($sql)"
                set view_p 1
            }
        } else {
            set view_p 1
        }
    } else {
        lappend where_clause_list "c.cost_id in (
                        select distinct cost_id
                        from im_costs
                        where project_id in (
                    select  children.project_id
                    from    im_projects parent,
                        im_projects children
                    where   children.tree_sortkey 
                            between parent.tree_sortkey 
                            and tree_right(parent.tree_sortkey)
                        and parent.project_id = :project_id)
                )"

        set read_all_p [im_permission $rest_user_id "view_finance"]
        if {$read_all_p} {
            set view_p 1
        } 
        
        if {!$view_p} {
            # Try the biz object members
            if {[im_biz_object_member_p $rest_user_id $project_id]} {
                set view_p 1
            }
        }
        
        if {!$view_p} {
            # check for assignments

            if {[im_user_is_freelance_p $rest_user_id]} {
                # Freelancers can view if we limit the query further
                set freelancer_company_id [cog::user::main_company_id -user_id $rest_user_id]
                lappend where_clause_list "c.provider_id =$freelancer_company_id and c.project_id = $project_id"
                set view_p 1
            } 

            # Check if the customer is a member of the company of the project
            if {[im_user_is_customer_p $rest_user_id]} {
                set company_id [db_string company "select company_id from im_projects where project_id = :project_id"]
                if {[im_biz_object_member_p $rest_user_id $company_id]} {
                    set view_p 1
                }
            }
        } 
    }

    set invoice_sql "select i.invoice_id, i.company_contact_id, i.invoice_office_id,i.payment_method_id, 
        to_char(c.delivery_date, 'YYYY-MM-DD') as delivery_date, (c.vat / 100) as vat,
        p.first_names, p.last_name,
        pa.email,
        c.cost_name as invoice_name, c.cost_type_id, c.cost_status_id, c.customer_id, c.provider_id, c.effective_date, c.project_id, c.cost_center_id, c.payment_term_id, c.template_id, c.vat_type_id, 
        coalesce(round(c.amount * c.vat / 100 * 100) / 100,0) as vat_amount,
        coalesce(round(c.amount * c.tax / 100 * 100) / 100,0) as tax_amount,
        coalesce(c.amount,0) as amount,
        coalesce(uc.work_phone,uc.cell_phone,uc.home_phone) as phone,
        to_char(ao.creation_date, 'YYYY-MM-DD') as creation_date,
        to_char(c.effective_date, 'YYYY-MM-DD') as effective_date,
        ao.creation_user as creation_user_id,
        cc.currency_name, cc.symbol as currency_symbol
        from im_invoices i 
            left outer join users_contact uc on i.company_contact_id = uc.user_id,
            im_costs c, 
            persons p, 
            parties pa, 
            acs_objects ao, 
            currency_codes cc
        where c.cost_id = i.invoice_id
            and i.company_contact_id = p.person_id
            and i.company_contact_id = pa.party_id
            and i.invoice_id = ao.object_id
            and c.currency = cc.iso
            and [join $where_clause_list " and "]"


    if {$project_id eq ""} {
        # Add a limit if we are not looking at a project
        set pagination "limit,50,start,0,property,c.effective_date,direction,DESC"
    }

    if {$pagination ne ""} {
        append invoice_sql "\n [cog_rest::sort_and_paginate -pagination $pagination]"
    }

    if {$view_p} {

        db_foreach invoice $invoice_sql {

            set grand_total [expr $amount + $vat_amount + $tax_amount]
            # Note to future me - We might have to calculate the grand_total as we do in view.tcl later
            # Also double check we update the vat_amount if there is a vat amount during invoice creation

            if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
                set company_id $customer_id
                set company_name [im_name_from_id $company_id]
            } else {
                set orig_customer_id $customer_id
                set company_id $provider_id
                set provider_id $orig_customer_id
                set company_name [im_name_from_id $company_id]
                if {[string match "Freelance*" $company_name]} {
                    set company_name [lrange [split $company_name " "] 1 end]
                }
            }
            
            set provider_name [im_name_from_id $provider_id]

            if {$invoice_office_id eq ""} {
                set invoice_office_id [db_string company "select main_office_id from im_companies where company_id = :company_id"]
            }

            if {![db_0or1row office_info "select address_line1, address_line2, address_city,  address_state,  address_postal_code, address_country_code, country_name as address_country
                from im_offices o, country_codes cc
                where office_id = :invoice_office_id
                and cc.iso = o.address_country_code"]} {
                set address_line1 ""
                set address_line2 ""
                set address_city ""
                set address_postal_code ""
                set address_country_code ""
                set country_name ""
                set address_state ""
                set address_country ""
            }

            if {$vat_type_id eq ""} {
                # Get the vat type from the company
                set vat_type_id [db_string vat_info "select vat_type_id from im_companies where company_id = :company_id" -default ""]
            }

            # Figure out what is type of cost of linked invoice
            set linked_invoice_id ""
            set linked_invoice_name ""
            
            switch $cost_type_id {
                3700 {
                    set linked_document_cost_type_id [im_cost_type_quote]
                }
                3702 - 3725 - 3740 {
                    # In case of quote
                    set linked_document_cost_type_id [im_cost_type_invoice]
                }  
                3704 {
                    set linked_document_cost_type_id [im_cost_type_po]
                }                  
                3706 - 3735 - 3741 {
                    # In case of PO
                    set linked_document_cost_type_id [im_cost_type_bill]
                }
                default {
                    # Set to null
                    set linked_document_cost_type_id ""
                }
            }
            if {$linked_document_cost_type_id ne ""} {
                set linked_invoices_list [im_invoices::linked_invoices -invoice_id $invoice_id -cost_type_id $linked_document_cost_type_id]
                if {[llength $linked_invoices_list] > 0} {
                    set linked_invoice_id [lindex $linked_invoices_list [expr [llength $linked_invoices_list] -1]]
                    set linked_invoice_name [db_string get_cost_name "select cost_name from im_costs where cost_id = :linked_invoice_id" -default ""]
                }
            }

            set linked_objects [list]
            set linked_object_ids [im_invoices::linked_invoices -invoice_id $invoice_id]
            callback cog_rest::invoice::linked_objects -invoice_id $invoice_id -cost_type_id $cost_type_id
            if {[llength $linked_object_ids] >0} {
                set linked_objects_p 1
            } else {
                set linked_objects_p 0
            }
            
            foreach object_id $linked_object_ids {
                lappend linked_objects [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $object_id]
            }

            lappend invoices [cog_rest::json_object]
        }
        return [cog_rest::json_response]
    } else {
        return [cog_rest::error -http_status 403 -message "[_ cognovis-rest.err_perm_fin_docs]"]
    }
}

ad_proc -public cog_rest::post::invoice {
    { -cost_status_id ""}
    -cost_type_id:required
    { -template_id ""}
    { -payment_term_id "" }
    { -payment_method_id "" }
    { -delivery_date "" }
    { -effective_date "" }
    { -currency_iso "" }
    { -cost_center_id "" }
    { -project_id ""}
    { -provider_id "" }
    { -company_id "" }
    { -company_contact_id ""} 
    { -provider_contact_id ""}
    -rest_user_id:required
} {
    Update an existing invoice

    @param invoice_body request_body Invoice we would like to update

    @return invoice json_object invoice Object of the invoice information
} {
    if {$company_id eq ""} {
        if {$company_contact_id ne ""} {
            set company_id [cog::user::main_company_id -user_id $company_contact_id]
        } else {
            set company_id [im_company_internal]
            set company_contact_id [db_string accounting "select coalesce(accounting_contact_id, primary_contact_id) from im_companies where company_id = :company_id" -default ""]
        }
    }
    
    
    if {$provider_id eq ""} {
        if {$provider_contact_id eq ""} {
            set provider_id [im_company_internal]
        } else {
            set provider_id [cog::user::main_company_id -user_id $provider_contact_id]
        }
    }

    if {$provider_contact_id eq ""} {
        set provider_contact_id [im_invoices_default_company_contact $provider_id]
    }

    if {$company_id ne "" && $company_id ne [im_company_internal]} {
	    db_1row company_type "select company_type_id,default_payment_method_id as default_payment_method_id,payment_term_id as default_payment_term_id, vat_type_id, default_tax, default_vat from im_companies where company_id = :company_id"
    } else {
	    db_1row company_type "select company_type_id,default_payment_method_id as default_payment_method_id,payment_term_id as default_payment_term_id, vat_type_id, default_tax, default_vat from im_companies where company_id = :provider_id"
    }

    set invoice_id [cog::invoice::create -customer_id $company_id -provider_id $provider_id -provider_contact_id $provider_contact_id -customer_contact_id $company_contact_id -invoice_type_id $cost_type_id \
        -project_id $project_id -cost_center_id $cost_center_id -delivery_date $delivery_date -currency $currency_iso \
        -payment_method_id $payment_method_id -payment_term_id $payment_term_id -template_id $template_id]

    set invoice [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::invoice -invoice_id $invoice_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::invoice {
    -invoice_id:required
    -cost_status_id
    -cost_type_id
    -template_id
    -payment_term_id
    -payment_method_id
    -vat_type_id
    { -delivery_date "" }
    -effective_date
    -currency_iso
    { -cost_center_id "" }
    -company_id
    -company_contact_id
    -rest_user_id:required
} {
    Update an existing invoice

    @param invoice_id object im_invoice::write Invoice which we want to update
    @param invoice_body_put request_body Invoice we would like to update

    @return invoice json_object invoice Object of the invoice information
} {

    # We will probably need to double check if queries executed in here are not creating any business perspective problems

    # Prevent from accidently setting something to deleted
    db_1row old_invoice "select * from im_invoices i, im_costs c where c.cost_id = i.invoice_id and i.invoice_id = :invoice_id" -column_array old_invoice

    if {![info exists cost_status_id]} {
        set cost_status_id $old_invoice(cost_status_id)
    }

    if {![info exists cost_type_id]} {
        set cost_type_id $old_invoice(cost_type_id)
    }

    if {$cost_status_id eq [im_cost_status_deleted]} {
        switch $old_invoice(cost_status_id) {
            3802 {
                # Allow to set to deleted
                set cost_status_id [im_cost_status_deleted]
            }
            3815 - 3816 {
                # Allow to be set to rejected
                set cost_status_id [im_cost_status_rejected]
            }
            default {
                # Do nothing
                set cost_status_id $old_invoice(cost_status_id)
            }
        }
    }

    set update_list ""
    set update_list [list]
    if {[info exists currency_iso]} {
        set currency $currency_iso
    }

    if {[info exists company_id]} {
        set customer_id $company_id
    }

    foreach mandatory_var [list cost_status_id cost_type_id template_id payment_term_id effective_date currency customer_id vat_type_id] {
        if {[exists_and_not_null $mandatory_var]} {
            lappend update_list "$mandatory_var = :$mandatory_var"
        }
    }
    foreach optional_var [list delivery_date cost_center_id] {
        if {[set $optional_var] ne ""} {
            lappend update_list "$optional_var = :$optional_var"
        }
    }

    if {[llength $update_list]>0} {
        set im_cost_update_sql "
            UPDATE im_costs
            SET [join $update_list " , "]
            WHERE cost_id = :invoice_id
        "
        # We might use different 'error catch' method in here
        if {[catch {
            db_dml update_im_cost $im_cost_update_sql
        } errmsg]} {
            cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "$errmsg"
        }        
    }

    if {[exists_and_not_null company_id]} {
        set invoice_office_id [db_string office_id "select main_office_id from im_companies where company_id = :company_id"]
    }

    set invoice_list [list]
    foreach invoice_var [list payment_method_id company_contact_id invoice_office_id] {
        if {[exists_and_not_null $invoice_var]} {
            lappend invoice_list "$invoice_var = :$invoice_var"
        }
    } 

    if {[llength $invoice_list]>0} {
        set invoice_update_sql "
            UPDATE im_invoices
            SET [join $invoice_list " , "]
            WHERE invoice_id = :invoice_id
        "

        # We might use different 'error catch' method in here
        if {[catch {
            db_dml update_im_cost $invoice_update_sql
        } errmsg]} {
            cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "$errmsg"
        }
    }

    cog::callback::invoke -object_type "im_invoice" -status_id $cost_status_id -type_id $cost_type_id -object_id $invoice_id -action after_update

    set invoice [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::invoice -invoice_id $invoice_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::invoice_reply {
    -rest_user_id:required
    -invoice_id:required
    -cost_status_id:required
    { -note "" }
} {
    Allow the company_contact_id to reply to a financial invoice document (typically a quote)

    @param invoice_id object im_invoice::read Invoice the contact is looking at
    @param cost_status_id category "Intranet Cost Status" Status of the invoice
    @param note string Additional information provided by the contact

    @return invoice json_object invoice Object of the invoice information

} {
    db_1row invoice_info "select company_contact_id, project_id from im_invoices i, im_costs c 
        where i.invoice_id = c.cost_id
        and i.invoice_id = :invoice_id"

    set allow_edit_p [cog_rest::helper::object_permission -object_id $invoice_id -user_id $rest_user_id -privilege "write"]
    if {$rest_user_id eq $company_contact_id} {
        set allow_edit_p 1
    }

    set invoice_name [im_name_from_id $invoice_id]
    set cost_status [im_name_from_id $cost_status_id]

    if {$allow_edit_p} {
        if {[catch {set invoice_json [cog_rest::put::invoice -invoice_id $invoice_id -cost_status_id $cost_status_id -rest_user_id $rest_user_id]} error_message]} {
            cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_invoice_put_status]"
        } else {
            return $invoice_json
        }
    } else {
        cog_rest::error -http_status 403 -message "[_ cognovis-rest.err_perm_put_invoice_status]"
    }
}


#---------------------------------------------------------------
# Invoice Line Items
#---------------------------------------------------------------

ad_proc -public cog_rest::get::invoice_item {
    -rest_user_id:required
    -invoice_id:required
    { -item_id ""}
    { -only_not_invoiced_p 0}
} {

    Provide the line items from ]project-open[ for a specific invoice

    @param invoice_id object im_invoice::read Invoice for which to return the line items
    @param item_id integer ID of the item we want to retrieve within the invoice. Will result in an array of length 1
    @param only_not_invoiced_p boolean should we filter out items which were already invoiced?

    @return invoice_items json_array invoice_item Array of line items for given invoice_id
} {

    set invoice_items [list]
    set where_clause_list [list "c.iso = ii.currency" ]

    lappend where_clause_list "ii.invoice_id = :invoice_id"

    if {$item_id ne ""} {
        lappend where_clause_list "ii.item_id = :item_id"
    }
    set invoice_invoice_items_sql "select ii.item_id, ii.item_name, ii.item_units, ii.item_uom_id, ii.price_per_unit,
            ii.sort_order, ii.item_material_id, ii.description, ii.context_id,
            ii.currency,c.symbol,
            m.source_language_id,m.target_language_id,m.task_type_id,m.subject_area_id
        from currency_codes c, im_invoice_items ii left outer join im_materials m on m.material_id = ii.item_material_id
        where 
            [join $where_clause_list " and "]
        order by ii.sort_order asc
    "

    db_foreach invoice_item $invoice_invoice_items_sql {
        set item_already_invoiced_p [cog::invoice_item::was_already_invoiced -item_id $item_id]
        if {$only_not_invoiced_p} {
            if {!$item_already_invoiced_p} {
                lappend invoice_items [cog_rest::json_object]
            }
        } else {
            lappend invoice_items [cog_rest::json_object]
        }
    }

    return [cog_rest::json_response]

}

ad_proc -public cog_rest::post::invoice_item {
    -invoice_id:required
    -item_name:required
    { -item_uom_id ""}
    { -item_units "" }
    { -price_per_unit ""}
    { -sort_order ""}
    { -source_language_id "" }
    { -target_language_id "" }
    { -material_nr "" }
    { -item_material_id "" }
    { -task_id "" }
    { -context_id "" }
    -rest_user_id:required
} {
    Create completely new invoice_item

    @param invoice_id object im_invoice::read invoice id in which we generate the line item
    @param invoice_item_body request_body Information for a new line item.

    @return invoice_item json_object invoice_item Array of line items for given invoice_id

} {
    set allow_edit_p [cog_rest::helper::object_permission -object_id $invoice_id -user_id $rest_user_id -privilege "write"]
    if {!$allow_edit_p} {
        cog_rest::error -http_status 401 -message "[_ cognovis-rest.err_perm_add_line_item]"
    }

    if {$context_id eq ""} {
        set context_id $task_id
    }

    if {$item_uom_id eq ""} {
        set item_uom_id [im_uom_unit]
    }

    if {$item_material_id eq ""} {
        if {$material_nr eq ""} {

            if {$task_id ne ""} {
                set task_type_id [db_string task_type_from_trans "select task_type_id from im_trans_tasks where task_id = :task_id" -default "" ]
                if {$task_type_id eq ""} {
                    set task_type_id [db_string task_type_from_trans "select task_type_id from im_projects where task_id = :task_id" -default "" ]                
                }
            } else {
                set task_type_id ""
            }

            set item_material_id [cog::material::get_material_id -task_type_id $task_type_id -source_language_id $source_language_id -target_language_id $target_language_id -material_uom_id $item_uom_id]
            
        } else {
            set item_material_id [db_string material "select material_id from im_materials where material_nr = :material_nr" -default ""]
        }
    }

    # Setting currency of invoice
    if {$item_material_id eq ""} {
        set item_material_id [cog::material::create -material_uom_id $item_uom_id -task_type_id $task_type_id \
            -source_language_id $source_language_id -target_language_id $target_language_id \
            -description "Auto generated for invoice [im_name_from_id $invoice_id]."]
    }

    if {$item_material_id ne ""} {

        set newly_created_invoice_item_id [cog::invoice_item::create -invoice_id $invoice_id \
            -item_name $item_name -item_material_id $item_material_id \
            -item_units $item_units -price_per_unit $price_per_unit \
            -task_id $task_id -context_id $context_id  -sort_order $sort_order \
            -user_id $rest_user_id]

        cog::callback::invoke -object_type "im_invoice" -object_id $invoice_id -action after_update

        # Get newly created line item back
        set invoice_item [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::invoice_item -invoice_id $invoice_id -item_id $newly_created_invoice_item_id -rest_user_id $rest_user_id]]

        set project_id [db_string get_project_id "select project_id from im_costs where cost_id = :invoice_id" -default ""]
        if {$project_id ne ""} {
            cog::project::update_cost_cache -project_id $project_id
        }

        return [cog_rest::json_response]

    } else {
        cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "[_ cognovis-rest.err_no_material]"
    }
}

ad_proc -public cog_rest::put::invoice_item_sort_order {
    -invoice_id:required
    -invoice_items_sort_order:required
    -rest_user_id:required
} {
    Update all line items order for given invoice_id
    Idea is that we only send array of integers and order of ids is new order for whole document

    @param invoice_id number id of invoice for which we change invoice_item orders
    @param invoice_items_sort_order_put request_body put body holding invoice_items_sort_order (array of integers)

    @return invoice_items json_array invoice_item Array of line items for given invoice_id
} {
    set invoice_items [list]

    set new_sort_order 1
    foreach invoice_item_id $invoice_items_sort_order {
        db_dml update_sort_order "update im_invoice_items set sort_order = :new_sort_order where item_id = :invoice_item_id"
        incr new_sort_order
    }

    set invoice_items [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::invoice_item -invoice_id $invoice_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::invoice_item {
    -item_id:required
    { -item_name ""}
    { -description ""}
    { -item_units "" }
    { -price_per_unit ""}
    { -sort_order ""}
    { -material_nr ""}
    { -item_material_id ""}
    { -context_id ""}
    { -source_language_id ""}
    { -target_language_id ""}
    { -item_uom_id ""}
    -rest_user_id:required
} {
    Update existing line item

    @param item_id integer id of line item
    @param invoice_item_body_put request_body single line item which we overwrite

    @return invoice_item json_object invoice_item line item which was updated

} {

    set invoice_items [list]
    set allow_edit_p 0

    # We need to check if user has invoice::writre permission and if its status is Created
    # First we need to get invoice_id from invoice_item
    set invoice_id [db_string get_invoice_id "select invoice_id from im_invoice_items where item_id = :item_id" -default ""]
    if {$invoice_id eq ""} {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_miss_invoice_for_item]"
    }
    
    db_1row invoice_info "select project_id from im_costs where cost_id = :invoice_id"

    set allow_edit_p [cog_rest::helper::object_permission -object_id $invoice_id -user_id $rest_user_id -privilege "write"]
    
    if {$allow_edit_p} {
        # We update sort_order only in case it was passed to endpoint
        set update_sql_list [list]
        foreach var [list item_units context_id price_per_unit sort_order item_name] {
            if {[set $var] ne ""} {
                lappend update_sql_list "$var = :$var"
            }
        }

        # We update 'description' even if value is empty string
        lappend update_sql_list "description = :description"

        if {$material_nr ne "" && $item_material_id eq ""} {
            set item_material_id [db_string material "select material_id from im_materials where material_nr = :material_nr" -default ""]
        }

        if {$item_uom_id ne ""} {
            lappend update_sql_list "item_uom_id = :item_uom_id"
        }

        if {$item_material_id eq "" && $source_language_id ne "" && $target_language_id ne "" && $item_uom_id ne ""}  {
            set task_type_id [db_string task_type "select project_type_id from im_projects p where p.project_id = :project_id"]
            set item_material_id [cog::material::get_material_id -task_type_id $task_type_id -source_language_id $source_language_id -target_language_id $target_language_id -material_uom_id $item_uom_id]
        }

        if {$item_material_id ne ""} {
            lappend update_sql_list "item_material_id = :item_material_id"
        }
        
        if {$sort_order ne ""} {
            set original_sort_order [db_string so "select sort_order from im_invoice_items where item_id = :item_id"]
        }

        if {[llength $update_sql_list]>0} {
            set update_sql "
                UPDATE im_invoice_items SET
                    [join $update_sql_list " , "]
                WHERE
                    item_id = :item_id
            "

            db_dml update $update_sql
            # Update the invoice value
            cog::callback::invoke -object_type "im_invoice" -object_id $invoice_id -action after_update
        }

        # If sort_order was updated, we also need to update other invoice_items
        if {$sort_order ne ""} {
            if {$original_sort_order < $sort_order} {
                # we need to exclude item_id, because it was already updated
                db_dml update_other_items "update im_invoice_items set sort_order = (sort_order - 1) where invoice_id = :invoice_id and sort_order > :original_sort_order and sort_order <= :sort_order and item_id <> :item_id"
            } 
            if {$original_sort_order > $sort_order} {
                # we need to exclude item_id, because it was already updated
                db_dml update_other_items "update im_invoice_items set sort_order = (sort_order + 1) where invoice_id = :invoice_id and sort_order >= :sort_order and sort_order < :original_sort_order and item_id <> :item_id"
            }
        }

        set invoice_item [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::invoice_item -invoice_id $invoice_id -item_id $item_id -rest_user_id $rest_user_id]]
        return [cog_rest::json_response]

    } else {
        cog_rest::error -http_status 401 -message "[_ cognovis-rest.err_perm_edit_line_item]"
    }
}

namespace eval cog_rest::get {

    ad_proc -public invoice_document {
        -invoice_id:required
        { -type "pdf" }
        -rest_user_id:required
        { -preview_p 0 }
    } {
        Return a file for the invoice back to the browser directly

        @param invoice_id object im_invoice::read Invoice for which we want to generate the document
        @param preview_p boolean If we are not generating a preview we store the file in the content repository
        @param type string Filetype we want to generate
    } {

    	set outputheaders [ns_conn outputheaders]

        set invoice_nr [db_string invoice_nr "select invoice_nr from im_invoices where invoice_id = :invoice_id" -default ""]
        if {$invoice_nr eq ""} {
            cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_miss_invoice_id]"
        }
        
        switch $type {
            fodt {
                if {[catch {set odt_file [cog::invoice::oo_document -invoice_id $invoice_id -user_id $rest_user_id]} err_msg]} {
                    cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "[_ cognovis-rest.err_gen_oo_doc]"
                    return
                }
        	    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=${invoice_nr}.fodt"
	            ns_returnfile 200 application/fodt $odt_file
            }
            pdf {

                if {$preview_p} {
                    # Just return the PDF
                    if {[catch {set odt_file [cog::invoice::oo_document -invoice_id $invoice_id -user_id $rest_user_id]} err_msg]} {
                        cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "[_ cognovis-rest.err_gen_oo_doc]"
                        return
                    }

                    if {[catch {set odt_pdf [intranet_oo::docker_convert_to -oo_file $odt_file -outfile "${invoice_nr}.pdf"]} err_msg]} {
                        cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "[_ cognovis-rest.err_gen_pdf]"
                        return
                    }
                    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=${invoice_nr}.pdf"
	                ns_returnfile 200 "application/odt" $odt_pdf
                } else {
                    db_1row invoice_info "select invoice_nr,last_modified from im_invoices,acs_objects where invoice_id = :invoice_id and invoice_id = object_id"

                	set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $invoice_id]	
                    set invoice_item_id ""
                    if {"" == $invoice_item_id} {
                        if {[catch {set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id -preview_p $preview_p -user_id $rest_user_id]} err_msg]} {
                            cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "[_ cognovis-rest.err_gen_oo_doc]"
                            return                           
                        }
                    } else {
                        set invoice_revision_id [content::item::get_latest_revision -item_id $invoice_item_id]

                        # Check if we need to create a new revision
                        if {[db_string date_check "select 1 from acs_objects where object_id = :invoice_revision_id and last_modified < :last_modified" -default 0]} {
                            # The modification of the invoice was after the revision modification
                            # Therefore we need to update this.
                            if {[catch {set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id -preview_p $preview_p -user_id $rest_user_id]} err_msg]} {
                                cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "[_ cognovis-rest.err_gen_oo_doc]"
                                return                           
                            }
                        } else {
                            # Check if the current revision is the live revision
                            set live_revision_id [content::item::get_live_revision -item_id $invoice_item_id]            
                            if {$live_revision_id ne $invoice_revision_id} {
                                
                                # Callbacks to exchange the PDF
                                callback im_openoffice_invoice_pdf_before_create -invoice_id $invoice_id -preview_p $preview_p -invoice_nr $invoice_nr -invoice_pdf_revision_id $live_revision_id
                                
                                # Set the live revision, as we don't have to create it.
                                content::item::set_live_revision -revision_id $invoice_revision_id
                                callback im_openoffice_invoice_pdf_after_create -invoice_id $invoice_id -preview_p $preview_p -invoice_nr $invoice_nr -invoice_pdf_revision_id $invoice_revision_id
                            }
                        }
	                }
                    if {[exists_and_not_null invoice_revision_id]} {
                        # If we are not previewing set to outstanding if it is downloaded
                        set cost_status_id [db_string cost_status "select cost_status_id from im_costs where cost_id = :invoice_id" -default ""]
                        if {$cost_status_id eq [im_cost_status_created]} {
                            cog_rest::put::invoice -invoice_id $invoice_id -cost_status_id [im_cost_status_outstanding] -rest_user_id $rest_user_id
                        }
                    }
                }

                if {[exists_and_not_null invoice_revision_id]} {
                    ns_set cput $outputheaders "Content-Disposition" "attachment; filename=${invoice_nr}.pdf"
                    ns_returnfile 200 application/pdf [content::revision::get_cr_file_path -revision_id $invoice_revision_id]
                } else {
                    return ""
                }
            }
        }

        return ""
    }

    ad_proc -public invoice_mail {
        -invoice_id:required
        -rest_user_id:required
    } {
        Get the data necessary to send the invoice via mail to the client

        @param invoice_id object im_invoice::read Invoice for which we want to get the mail parties

        @return mail json_object mail Mail object to fill out  the template
    } {

        if {![db_0or1row invoice_info "select invoice_nr,last_modified,
            im_name_from_user_id(:rest_user_id) as internal_contact_name from im_invoices,acs_objects where invoice_id = :invoice_id and invoice_id = object_id"]} {
                cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_miss_invoice_id]"
            }

        set render_template_id [db_string template "select template_id from im_costs where cost_id = :invoice_id" -default ""]
        set invoice_item_id [content::item::get_id_by_name -name "${invoice_nr}.pdf" -parent_id $invoice_id]

        if {"" == $invoice_item_id} {
            if {[catch {set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id -user_id $rest_user_id]} err_msg]} {
                cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "[_ cognovis-rest.err_gen_oo_doc]"
                return                           
            }
        } else {
            set invoice_revision_id [content::item::get_best_revision -item_id $invoice_item_id]

            # Check if we need to create a new revision
            if {[db_string date_check "select 1 from acs_objects where object_id = :invoice_revision_id and last_modified < :last_modified" -default 0]} {
                if {[catch {set invoice_revision_id [intranet_openoffice::invoice_pdf -invoice_id $invoice_id -user_id $rest_user_id]} err_msg]} {
                    cog_rest::error -rest_user_id $rest_user_id -http_status 500 -message "[_ cognovis-rest.err_gen_latest_inv_rev]"
                    return                           
                }
            }
        }

        set cr_files [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::cr_file -file_id [content::revision::item_id -revision_id $invoice_revision_id] -rest_user_id $rest_user_id]]

        set recipient_id [db_string company_contact_id "select company_contact_id from im_invoices where invoice_id = :invoice_id" -default $rest_user_id]
        set recipient_name [im_name_from_id $recipient_id]
        
        #---------------------------------------------------------------
        # Variables for the template
        #---------------------------------------------------------------

        # Project details
        if {![db_0or1row related_projects_sql "
                select distinct
                r.object_id_one as project_id,
                p.project_name,
                project_lead_id,
                im_name_from_id(project_lead_id) as project_manager,
                p.project_nr,
                p.parent_id,
                p.description,
                p.processing_time,
                trim(both p.company_project_nr) as customer_project_nr
            from
                    acs_rels r,
                im_projects p
            where
                r.object_id_one = p.project_id
                and r.object_id_two = :invoice_id
                order by project_id desc
                limit 1
        "]} {
            set project_name ""
            set project_manager [im_name_from_user_id $rest_user_id]
            set project_lead_id $rest_user_id
            set customer_project_nr ""
            set project_nr ""
            set processing_time ""
        }

    

        db_1row user_info "select first_names, last_name from persons where person_id = :recipient_id"
        set salutation_pretty [im_invoice_salutation -person_id $recipient_id]

        # Get the type information so we can get the strings
        set invoice_type_id [db_string type "select cost_type_id from im_costs where cost_id = :invoice_id"]

        set recipient_locale [lang::user::locale -user_id $recipient_id]

        # Append sender
        set person_id $project_lead_id
        set email [party::email -party_id $person_id]
        set type "from"
        set sender [cog_rest::json_object -object_class mail_person]

        # Append recipient(s)
        set recipients [list]
        set person_id $recipient_id
        set email [party::email -party_id $person_id]
        set type "to"
        lappend recipients [cog_rest::json_object -object_class mail_person]

        callback intranet-invoices::mail_before_send -invoice_id $invoice_id -type_id $invoice_type_id

        if {$render_template_id ne ""} {
            db_1row template_subject_body "select aux_html1 as body, aux_string1 as subject from im_categories where category_id = :render_template_id"
            set subject [lang::message::format $subject "" 1]
            set body [lang::message::format $body "" 1]
        } else {
            set subject [lang::util::localize "#intranet-invoices.invoice_email_subject_${invoice_type_id}#" $recipient_locale]
            set body [lang::util::localize "#intranet-invoices.invoice_email_body_${invoice_type_id}#" $recipient_locale]
        }

        set context_id $invoice_id
        set sent_date ""        
        set message_id ""
        set log_id ""

        return [cog_rest::json_object]
    }

    ad_proc -public invoice_mail_sender {
        { -project_id ""}
        { -invoice_id ""}
        -rest_user_id:required
    } {
        Returns a list of potential sender for the invoice_id

        @param invoice_id object im_invoice::read Invoice for which we want to get the senders.
        @param project_id object im_project::read Project for which we want to get the senders.

        @return senders json_array mail_person List of senders
        
    } {

        if {$project_id eq "" && $invoice_id eq ""} {
            cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message "[_ cognovis-rest.err_miss_invoice_project]"
            return
        }

        if {$invoice_id ne ""} {
            # By default send from the primary_accounting_contact of the internal company, otherwise the current_user, but offer the project_lead
            # but only from the accounting contact if it is an invoice or bill
            db_1row cost_info "select cost_type_id, provider_id, customer_id, company_contact_id, project_id from im_costs c, im_invoices i where c.cost_id = :invoice_id and c.cost_id = i.invoice_id"
            set invoice_or_bill_p [im_cost_type_is_invoice_or_bill_p $cost_type_id]
            if {$invoice_or_bill_p} {
                if {[im_category_is_a $cost_type_id [im_cost_type_customer_doc]]} {
                    # A Customer document
                    set accounting_contact_id [db_string accounting_contact "select accounting_contact_id from im_companies where company_id = :provider_id" -default $rest_user_id]
                } else {
                    # A provider document
                    set accounting_contact_id [db_string accounting_contact "select accounting_contact_id from im_companies where company_id = :customer_id" -default $rest_user_id]
                }
                set from_ids [list $accounting_contact_id]
            } else {
                set from_ids [list $rest_user_id]
            }
        } else {
            set accounting_contact_id [db_string accounting_contact "select accounting_contact_id from im_companies where company_id = [im_company_internal]" -default $rest_user_id]
            set from_ids [list $accounting_contact_id]
        }

        lappend from_ids $rest_user_id

        set pm_ids [cog_project_managers -project_id $project_id]
        set from_ids [lsort -unique [concat $from_ids $pm_ids]]

        set senders [list]
        set type "from"

        foreach id $from_ids {
            if {$id ne ""} {
                set person_id $id
                set email [party::email -party_id $person_id]
                lappend senders [cog_rest::json_object]
            }
        }

        return [cog_rest::json_response]
    }

    ad_proc -public invoice_mail_recipient {
        -invoice_id:required
        -rest_user_id:required
    } {
        Returns a list of potential recipients for the invoice_id

        @param invoice_id object im_invoice::read Invoice for which we want to get the senders.

        @return recipients json_array mail_person List of Recipients
    } {
        # By default send from the primary_accounting_contact of the internal company, otherwise the current_user, but offer the project_lead
        # but only from the accounting contact if it is an invoice or bill
        db_1row cost_info "select cost_type_id, provider_id, customer_id, company_contact_id, project_id from im_costs c, im_invoices i where c.cost_id = :invoice_id and c.cost_id = i.invoice_id"

        set to_ids [list]
        if {$company_contact_id ne ""} {
            lappend to_ids $company_contact_id
        }

        set project_company_contact_id [db_string project_contact "select company_contact_id from im_projects where project_id = :project_id" -default ""]
        if {$project_company_contact_id ne ""} {
            lappend to_ids $project_company_contact_id
        }

        if {[im_category_is_a $cost_type_id [im_cost_type_customer_doc]]} {
            # A Customer document
            set accounting_contact_id [db_string accounting_contact "select accounting_contact_id from im_companies where company_id = :customer_id" -default ""]
            set customer_or_provider_id $customer_id
        } else {
            # A provider document
            set accounting_contact_id [db_string accounting_contact "select accounting_contact_id from im_companies where company_id = :provider_id" -default ""]
            set customer_or_provider_id $provider_id
        }
    
        if {$accounting_contact_id ne ""} {
            lappend to_ids $accounting_contact_id
        }

        # Get also all other employees of customer / provider company
        set employee_ids [db_list company_members "select acs_rels.object_id_two from acs_rels where object_id_one = :customer_or_provider_id and rel_type = 'im_company_employee_rel' and acs_rels.object_id_two not in (
                -- Exclude banned or deleted users
                select  m.member_id
                from    group_member_map m,
                    membership_rels mr
                where   m.rel_id = mr.rel_id and
                    m.group_id = acs__magic_object_id('registered_users') and
                    m.container_id = m.group_id and
                    mr.member_state != 'approved'
                )"]

        lappend to_ids {*}$employee_ids

        set to_ids [lsort -unique $to_ids]
        set recipients [list]
        
        set type "to"

        foreach id $to_ids {
            if {$id ne ""} {
                set person_id $id
                set email [party::email -party_id $person_id]
                lappend recipients [cog_rest::json_object]
            }
        }

        return [cog_rest::json_response]
    } 

    ad_proc -public cost_centers {
        -rest_user_id 
        { -cost_center_type_id "" }
        { -cost_center_status_id "" }
        { -parent_id "" }
    } {
        Return a list of cost centers the user can see for selection

        @param cost_center_type_id category "Intranet Cost Center Type" What kind of cost centers are we looking for
        @param cost_center_status_id category "Intranet Cost Center Status" What kind of cost centers are we looking for        
        @param parent_id object im_cost_center::read Parend Cost Center for which we are looking for siblings

        @return cost_centers json_array cost_center Array of Cost Centers
    } {
        set cost_centers [list]

        set cost_center_sql "select * from im_cost_centers"
        set where_clause_list [list]

        if {$cost_center_type_id ne ""} {
            lappend where_clause_list "cost_center_type_id = :cost_center_type_id"
        }

        if {$cost_center_status_id ne ""} {
            lappend where_clause_list "cost_center_status_id = :cost_center_status_id"
        }

        if {$parent_id ne ""} {
            # We need to not only get the immediate children but any objects below that, so go recursively 
            # Take a look for recursive implementation later
            # https://stackoverflow.com/questions/54907495/postgresql-recursive-parent-child-query#54909513
            
            lappend where_clause_list "parent_id = :parent_id"
        }

        if {[llength $where_clause_list]>0} {
            append cost_center_sql " where [join $where_clause_list " and "]"
        }

        db_foreach cost_centers $cost_center_sql {
            lappend cost_centers [cog_rest::json_object]
        }
        return [cog_rest::json_response]
    }
}

namespace eval cog_rest::put {

    ad_proc -public import_timesheet_tasks {
        -invoice_id:required
        { -only_unassigned "t" }
        -hour_log_ids
        -rest_user_id:required
    } {
        Import the timesheet tasks into the invoice. 
        
        For a quote it will use the billable hours of the timesheet tasks plus 
        the planned budget hours from the project.

        For an invoice it will use the actual hours logged.

        @param invoice_id object im_invoice::write Invoice which we want to update
        @param invoice_timesheet_tasks_put request_body Hours information

        @return invoice_items json_array invoice_item Array of line items for given invoice_id
    } {

        db_1row invoice_info "select project_id, cost_type_id from im_costs where cost_id = :invoice_id"
        set project_ids [im_project_subproject_ids -project_id $project_id]
        set project_ids [concat $project_ids [im_project_subproject_ids -project_id $project_id -type "task"]]
        
        if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
            set sql ""
            set invoice_p [expr  [im_category_is_a $cost_type_id [im_cost_type_invoice]] ||  [im_category_is_a $cost_type_id [im_cost_type_interco_invoice]]]
            if {$invoice_p} {
                # Get the logged hours on all the timesheet tasks in the project and find line items for them.
                set where_clause_list [list]
                lappend where_clause_list "h.project_id in ([template::util::tcl_to_sql_list $project_ids])"
                lappend where_clause_list "h.project_id = p.project_id"
                if {$only_unassigned} {
                    lappend where_clause_list "(h.invoice_id is null or h.invoice_id = :invoice_id)"
                }
                if {[exists_and_not_null hour_log_ids]} {
                    lappend where_clause_list "h.hour_id in ([template::util::tcl_to_sql_list $hour_log_ids])"
                }
                
                set sql "select sum(h.hours) as total_hours, h.project_id, 
                p.project_name, ii.item_id, tt.material_id
                from im_projects p, im_hours h
                left join im_invoice_items ii on ii.task_id = h.project_id
                    and ii.invoice_id = :invoice_id 
                left join im_timesheet_tasks tt on ii.task_id = tt.task_id
                where [join $where_clause_list " and "]
                group by h.project_id,p.project_name,ii.item_id, tt.material_id"
            }

            set quote_p [expr  [im_category_is_a $cost_type_id [im_cost_type_quote]] ||  [im_category_is_a $cost_type_id [im_cost_type_interco_quote]]]
            if {$quote_p} {
                set sql "select sum(tt.billable_units) as total_hours, p.project_id,
                p.project_name, ii.item_id, tt.material_id
                from im_projects p, im_timesheet_tasks tt
                left join im_invoice_items ii on ii.task_id = tt.task_id
                    and ii.invoice_id = :invoice_id 
                where p.project_id in ([template::util::tcl_to_sql_list $project_ids])
                and tt.task_id = p.project_id
                and (tt.invoice_id is null or tt.invoice_id = :invoice_id)
                group by p.project_id,p.project_name,ii.item_id, tt.material_id" 
            }

            db_foreach task $sql {
                if {$material_id eq ""} {
                    set material_id [im_material_default_material_id]
                }
                if {$total_hours > 0} {
                    if {$item_id eq ""} {
                        cog_rest::post::invoice_item -item_name $project_name -item_units $total_hours -task_id $project_id \
                            -item_uom_id [im_uom_hour] -invoice_id $invoice_id -rest_user_id $rest_user_id -item_material_id $material_id
                    } else {
                        cog_rest::put::invoice_item -item_id $item_id -item_units $total_hours -rest_user_id $rest_user_id
                    }
                }
            }
            set invoice_items [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::invoice_item -invoice_id $invoice_id -rest_user_id $rest_user_id]]
            return [cog_rest::json_response]
        } else {
            cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_invoice_id_type]"
        }
    }
}

namespace eval cog_rest::post {

    ad_proc -public send_financial_documents {
        {-from_party_id ""}
        -cost_type_ids:required
        -project_id:required
        {-cost_status_id ""}
        {-new_cost_status_id ""}
        {-cc_addr ""}
        -rest_user_id:required
    } {
        Sends all created provider bills to the providers of the project. set them to a new status

        @param from_party_id object person Who is the sender of the E-mails. Defaults to the project_lead_id
        @param project_id object im_project::read Project which we send the messages for
        @param new_cost_status_id category "Intranet Cost Status" New cost status which we set the bill to. Defaults to outstanding
        @param cost_type_ids category_array "Intranet Cost Type" Which cost type should we send the financial_documents for
        @param cost_status_id category "Intranet Cost Status" Status of the documents we want to send. Defaults to created
        @param cc_addr string Whom should we send the bills in CC to

        @return invoices json_array invoice Array of the invoice information
    } {

        if {$new_cost_status_id eq ""} {
            set new_cost_status_id [im_cost_status_outstanding]
        }

        if {$cost_status_id eq ""} {
            set cost_status_id [im_cost_status_created]
        }

        if {$from_party_id eq ""} {
            set from_party_id [db_string project_lead "select project_lead_id from im_projects where project_id = :project_id" -default [auth::get_user_id]]
        }

        #  find the bills and mails them
		set cost_ids [db_list quote "select cost_id from im_costs c
			where cost_type_id in ([template::util::tcl_to_sql_list $cost_type_ids]) 
			and cost_status_id = :cost_status_id
			and project_id = :project_id"]

        set invoices [list]

        set err_msg_list [list]
		foreach cost_id $cost_ids {
			set from_addr [party::email -party_id $from_party_id]

			if {[cog::invoice::send_email -from_addr $from_addr -cc_addr $cc_addr -invoice_id $cost_id] eq ""} {
                lappend err_msg_list "Could not send document for [im_name_from_id $cost_id] using $from_addr"
            } else {
                lappend invoices [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::invoice -invoice_id $cost_id -rest_user_id $rest_user_id]]
            }
    	}
        if {[llength $err_msg_list]>0} {
            cog_rest::error -http_status 400 -message "[join $err_msg_list "<br/>"]"
        }
        return [cog_rest::json_response]
    }
    
    ad_proc -public invoice_copy {
        -source_invoice_ids:required
        -target_cost_type_id:required
        { -filed_p "0"}
        { -no_items_p "0"}
        -rest_user_id:required
    } {
        Generate an invoice out of a quote

        @param source_invoice_ids object_array im_invoice::read Invoices which we want to turn into a single new one of a specific type
        @param target_cost_type_id category "Intranet Cost Type" Type of the new generated invoice
        @param filed_p boolean Should we file the source invoice_ids after we created the new one?
        @param no_items_p boolean Should we incluce line items?

        @return invoice json_object invoice Information of the copied invoice

    } {
        set errors [list]
        if {$no_items_p} {
            set invoice_id [cog::invoice::copy_new -source_invoice_ids $source_invoice_ids -target_cost_type_id $target_cost_type_id -error_arr errors -no_items]
        } else {
            set invoice_id [cog::invoice::copy_new -source_invoice_ids $source_invoice_ids -target_cost_type_id $target_cost_type_id -error_arr errors]
        }
         
        if {$filed_p} {
            foreach source_invoice_id $source_invoice_ids {
                cog_rest::put::invoice -invoice_id $source_invoice_id -rest_user_id $rest_user_id -cost_status_id [im_cost_status_filed]
            }
        }
        if {[llength $errors]>0} {
            cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message [join $errors "<br/>"]
        } else {
            set invoice [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::invoice -invoice_id $invoice_id -rest_user_id $rest_user_id]]
            return [cog_rest::json_response]
        }
    }

    ad_proc -public invoice_cancellation {
        -invoice_id:required
        -rest_user_id:required
    } {
        Cancel an existing invoice and create a cancellation invoice for it.

        @param invoice_id object im_invoice::write Invoice which we want to cancel. Needs write as we update the status.
        
        @return invoice json_object invoice Information of the copied invoice
    } {
        db_1row invoice_info "select cost_type_id from im_costs where cost_id = :invoice_id"

        if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
            set cancel_cost_type_id [im_cost_type_cancellation_invoice]
        } else {
            set cancel_cost_type_id [im_cost_type_cancellation_bill]
        }

        set errors [list]
        set cancel_invoice_id [cog::invoice::copy_new -source_invoice_ids $invoice_id -target_cost_type_id $cancel_cost_type_id -error_arr errors] 
        if {[llength $errors]>0} {
            cog_rest::error -rest_user_id $rest_user_id -http_status 400 -message [join $errors "<br/>"]
        } else {
            cog_rest::put::invoice -invoice_id $invoice_id -rest_user_id $rest_user_id -cost_status_id [im_cost_status_cancelled]

            # Link the any other cost items from the original invoice
            foreach related_invoice_id [db_list rels "select object_id_one from acs_rels 
                where object_id_two = :invoice_id and rel_type = 'im_invoice_invoice_rel'"] {
                set rel_id [cog::rel::add -creation_user $rest_user_id -rel_type "im_invoice_invoice_rel" -object_id_one $related_invoice_id -object_id_two $cancel_invoice_id]
            }
            set invoice [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::invoice -invoice_id $cancel_invoice_id -rest_user_id $rest_user_id]]
            return [cog_rest::json_response]
        }
    }
}

namespace eval cog_rest::delete {

    # @Malte: this will probably need different fix. Line item is saved to acs_objects 
    # as different type almost each time quote is generated. Sometimes its 'im_invoice_invoice_rel'
    # sometimes it is 'relationship' or 'content_item', so for now using ::write is not possible
    # That is why for now here I'm using plain Integer instead of object_array
    # This is sadly limiting us to delete only single line item at once, but we are doing that in frontend anyway

    ad_proc -public invoice_item {
        -invoice_item_id:required
        -rest_user_id:required
    } {
        Delete the provided line items if it is possible to remove them

        Will only remove all or none. If there are no errors, then the removal was successful.

        @param invoice_item_id integer invoice_items_id id of line item which try to remove

        @return errors json_array error Array of errors found
    } {
        set errors [list]

        set created_status_ids [im_sub_categories [im_cost_status_created]]

        set invoice_id [db_string invoice_id_from_invoice_item "select invoice_id from im_invoice_items where item_id =:invoice_item_id" -default ""]
        if {$invoice_id ne ""} {
            # Make sure that user has invoice::write permission and invoice status is: created
            set privilege "write"
            set permission_p [cog_rest::helper::object_permission -object_id $invoice_id -user_id $rest_user_id -privilege $privilege]
            if {$permission_p} {
                db_dml delete_invoice_item "delete from im_invoice_items where item_id =:invoice_item_id"
            } else {
                set parameter ""
                set err_msg "Invoice: $invoice_id - User $rest_user_id does not have $privilege permission"
                lappend errors [cog_rest::json_object]
                cog_rest::error -http_status 403 -message $err_msg 
            }
        }

        cog::callback::invoke -object_type "im_invoice" -object_id $invoice_id -action after_update

        return [cog_rest::return_array]
    }

    ad_proc -public invoice {
        {-invoices ""}
        -rest_user_id
    } {

        Allow multiple invoices to be set to deleted at once. Should be deprecated and you should use PUT with a new cost_status_id
        
        @param invoices object_array im_invoice::admin invoice_ids which we want to delete
        @return errors json_array error Array of errors found
    } {
        # Get the invoices from the parameters
        set invoice_ids $invoices
        set errors [list]
        # Set the invoice to status deleted if not PDF document is linked to it.
        foreach invoice_id $invoice_ids {
            set object_id $invoice_id
            # Check the current status. Depending on status and type combination we might need to do things differently
            if {[db_0or1row invoice_info "select cost_status_id, cost_type_id from im_costs where cost_id = :invoice_id"]} {
                # Apparently the invoice was found.
                if {[im_cost_type_is_invoice_or_bill_p $cost_type_id]} {
                    set permission_p [cog_rest::helper::object_permission -object_id $invoice_id -user_id $rest_user_id -privilege "admin"]
                    if {!$permission_p} {
                        set parameter "Permission"
                        set err_msg "You need special privileges to delete an invoice or bill"
                        lappend errors [cog_rest::json_object]              
                    } else {
                        switch $cost_status_id {
                            3802 {
                                # Allow to set to deleted
                                db_dml set_to_deleted "update im_costs set cost_status_id = [im_cost_status_deleted] where cost_id = :invoice_id"
                                callback im_invoice_after_update -object_id $invoice_id -status_id [im_cost_status_deleted] -type_id $cost_type_id                    
                            }
                            3815 - 3816 {
                                # Allow to be set to rejected
                                db_dml set_to_deleted "update im_costs set cost_status_id = [im_cost_status_rejected] where cost_id = :invoice_id"
                                callback im_invoice_after_update -object_id $invoice_id -status_id [im_cost_status_rejected] -type_id $cost_type_id                    
                            }
                            default {
                                # Do nothing
                                set parameter "Permission"
                                set err_msg "We do not allow to delete financial documents in status [im_category_from_id $cost_status_id]. <br />
                                    If you are sure what you are doing, set the status to [im_category_from_id 3802] or [im_category_from_id 3815] first."
                                lappend errors [cog_rest::json_object]  
                            }
                        }
                    }
                } else {
                    db_dml set_to_deleted "update im_costs set cost_status_id = [im_cost_status_deleted] where cost_id = :invoice_id"
                    callback im_invoice_after_update -object_id $invoice_id -status_id [im_cost_status_deleted] -type_id $cost_type_id                                        
                }

                # Remove relationship
                set rel_ids [db_list relations "
			        select	rel_id
	                from    acs_rels
	                where   rel_type = 'im_invoice_invoice_rel'
                    and ( object_id_one = :invoice_id
	                      or object_id_two = :invoice_id)"]
                foreach rel_id $rel_ids {
                    db_1row delete "select acs_rel__delete(:rel_id)"
                }
            }
        }

        return [cog_rest::return_array]
    }
}
