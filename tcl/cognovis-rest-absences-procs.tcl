ad_library {
    REST Procedures for Timesheet and Timesheets tasks
    @author malte.sussdorff@cognovis.de
}


namespace eval cog_rest::json_object {
    ad_proc user_absence {} {
        @return absence object im_user_absence User absence
        @return owner json_object user Information about the person who was / will be absent
        @return group object group Group we for which the absence was created
        @return start_date date-time When is the absence starting
        @return end_date date-time when is absence ending
        @return absence_type category "Intranet Absence Type" what kind of absence do we have
        @return absence_status category "Intranet Absence Status" what is current status of absence
        @return description string description (reason) for the absence
        @return duration number automaticaly calculated duration
    } -

    ad_proc user_absence_body {} {
        @param absence_name string Name of the absence
        @param owner_id object person::read Information about the person who was / will be absent
        @param group_id integer Group for which we want to add the absence
        @param start_date date When is the absence starting
        @param end_date date when is absence ending
        @param absence_type_id category "Intranet Absence Type" what kind of absence do we have
        @param absence_status_id category "Intranet Absence Status" what is current status of absence
        @param description string description (reason) for the absence
        @param contact_info string how can the user be reached
    } -

    ad_proc day_off {} {
        @return day date Date which is off
        @return color string Hex Code for the color of the absence
        @return absence_type category "Intranet Absence Type" what kind of absence do we have
    } -
} 


ad_proc -public cog_rest::get::user_absence {
    { -absence_id "" }
    { -owner_id "" }
    { -group_id "" }
    { -only_future "0"}
    -rest_user_id:required
} {
    Information about user absence.

    @param absence_id object im_user_absence::read Absence we would like to get information for
    @param owner_id object person::read User we want to get absences for
    @param group_id object im_profile group_id which we are looking for
    @param only_future boolean only search for absences which end_date is "in future"

    @return user_absences json_array user_absence Array of Absences

} {

    set user_absences [list]
    set where_clause_list [list]
    if {$absence_id ne ""} {
        lappend where_clause_list "ua.absence_id = :absence_id"
    }

    if {$only_future} {
        lappend where_clause_list "ua.end_date::date >= now()::date "
    }

    if {$owner_id eq "" && $absence_id eq ""} {
        set owner_id $rest_user_id
    }

    if {$group_id ne "" && $owner_id ne ""} {
        lappend where_clause_list "(ua.group_id = :group_id or ua.owner_id = :owner_id)"
    } else {
        if {$group_id ne ""} {
            lappend where_clause_list "ua.group_id = :group_id"
        }
        if {$owner_id ne ""} {
            lappend where_clause_list "ua.owner_id = :owner_id"
        } 
    }

    db_foreach user_absences "
        SELECT ua.absence_id, ua.owner_id, ua.description, to_char(ua.start_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as start_date,
            to_char(ua.end_date, 'YYYY-MM-DD\"T\"HH24:MI:SS.US\"Z\"') as end_date, ua.absence_type_id, ua.absence_status_id,ua.group_id
        FROM im_user_absences ua
        WHERE
            [join $where_clause_list " and "]  
    " {
        set group_name [group::title -group_id $group_id]
        if {$owner_id ne ""} {
            set owner [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $owner_id -rest_user_id $rest_user_id]]
        } else {
            set owner ""
        }
        set duration [im_absence_calculate_absence_days -start_date "[join [template::util::date get_property linear_date_no_time $start_date] "-"]" -end_date "[join [template::util::date get_property linear_date_no_time $end_date] "-"]" -owner_id $owner_id -ignore_absence_ids $absence_id -absence_id $absence_id]

        lappend user_absences [cog_rest::json_object]
    }

    return [cog_rest::json_response]

}

ad_proc -public cog_rest::post::user_absence {
    -rest_user_id:required
    -absence_name:required
    { -owner_id ""}
    { -group_id ""}
    { -absence_type_id ""}
    { -absence_status_id ""}
    -start_date:required
    -end_date:required
    { -description ""}
    { -contact_info ""}
} {

    Create a new vacation
    
    @param user_absence_body request_body Information of the user absence

    @return user_absence json_object user_absence Currently created absence after running through all workflows

} {
    set add_absences_for_group_p [im_permission $rest_user_id "add_absences_for_group"]
    set write [im_permission $rest_user_id "add_absences"]

    if {!$write} {
        cog_rest::error -http_status 403 "[_ cognovis-rest.err_perm_add_absence]"
    }

    if {$group_id ne "" && !$add_absences_for_group_p} {
        cog_rest::error -http_status 403 "[_ cognovis-rest.err_perm_add_group_absence]"
    }

    if {$group_id eq "" && $owner_id eq ""} {
        set owner_id $rest_user_id
    }

    set absence_id [cog::absence::new -current_user_id $rest_user_id \
        -absence_name $absence_name \
        -owner_id $owner_id \
        -group_id $group_id \
        -absence_type_id $absence_type_id \
        -absence_status_id $absence_status_id \
        -start_date $start_date \
        -end_date $end_date \
        -description $description \
        -contact_info $contact_info
    ]

    set user_absence [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user_absence -absence_id $absence_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::delete::user_absence {
    -absence_id:required
    -rest_user_id:required
} {
    Remove an absence if possible

    @param absence_id object im_user_absence::write Absence we want to be able to delete

    @return errors json_array error Array of errors found
} {
    set errors [list]
    set valid_p [db_0or1row hours "select owner_id, group_id, absence_status_id from im_user_absences where absence_id = :absence_id and absence_status_id != [im_user_absence_status_deleted] "]
    if {!$valid_p} {
        set parameter "Absence"
        set object_id $absence_id
        set err_msg "You have provided an invalid absence_id"
        lappend errors [cog_rest::json_object]
    } else {
    
        set perm_table [im_absence_new_page_wf_perm_table]
        set perm_set [cog::workflow::object_permissions  -object_id $absence_id  -perm_table $perm_table -user_id $rest_user_id]
        if {[lsearch $perm_set "d"] <0} {
            set parameter "Delete Permission"
            set object_id $absence_id
            set err_msg "[_ cognovis-rest.err_perm_del_absence]"
            lappend errors [cog_rest::json_object]
        }
    }
    
    if {[llength $errors] eq 0} {
        set delete_successful_p [cog::absence::delete -absence_id $absence_id -current_user_id $rest_user_id]

        if {!$delete_successful_p} {
            set parameter "Absence"
            set object_id $absence_id
            set err_msg "[_ cognovis-rest.err_absence_del]"
            lappend errors [cog_rest::json_object]
        }
    }    
    # To be implemented
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::days_off {
    -start_date:required
    -end_date:required
    { -owner_id "" }
    -rest_user_id:required
} {
    Returns the list of days a user is off due to public holidays or weekends. Useful in displaying datepickers.

    @param start_date date Which is the start date we look at
    @param end_date date Which is the last day to consider for days off

    @return days_off json_array day_off Array of days off
} {
    set days_off [list]

    set weekend_days [im_absence_week_days -start_date $start_date -end_date $end_date] 
    foreach day $weekend_days {
        set absence_type_id 5009 ; # Weekend
        set color [im_category_string2 -category_id $absence_type_id]
        lappend days_off [cog_rest::json_object]
    }

    if {$owner_id eq ""} { set owner_id $rest_user_id}
    set group_ids [im_biz_object_memberships -member_id $owner_id]

    db_foreach group_absences "
        select absence_type_id, aux_string2 as color,
            to_char(start_date,'YYYY-MM-DD') as absence_start_date, to_char(end_date,'YYYY-MM-DD') as absence_end_date
        from    im_user_absences a, im_categories c
        where   a.absence_type_id = c.category_id
        and a.start_date::date <= :end_date 
        and a.end_date::date >= :start_date
        and a.group_id in ([template::util::tcl_to_sql_list $group_ids])
    " {
        foreach group_day_off [im_absence_week_days -week_day_list [list 1 2 3 4 5] -start_date $absence_start_date -end_date $absence_end_date] {
            lappend days_off [cog_rest::json_object]
        }
    }

    return [cog_rest::json_response]
}