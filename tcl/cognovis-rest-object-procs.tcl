ad_library {
    REST Procedures for standard object types
    @author malte.sussdorff@cognovis.de
    @author frank.bergmann@project-open.com
}

namespace eval cog_rest::json_object {
    ad_proc cognovis_object {} {
        @return id integer Identifier for the object
        @return name string Name of the object
        @return object_type string ACS Object type for the object
        @return type named_id Category Type of the object
        @return status named_id Category Status of the object
        @return type_icon string Class definition for the icon based of the (sub-)type of the object - taken from the aux_html2 of the type category_id
        @return status_color string Color code based of the status of the object - taken from the aux_html2 of the status category_id
        @return privileges string granted privileges for currently logged user. Todo: we might change that to JSON array
    } - 

    ad_proc id_value {} {
        @return id string Identifier for the id/value pair
        @return value string Actual name which belongs to the id
        @return display_value string Internationalized value to display

    } -

    ad_proc object_type {} {
        @return object_type string object type. Should be snake cased.
        @return pretty_name string Pretty name for the object type. Might contain spaces
        @return table_name string Primary table for the object type. This is where the data relating to the object is stored
        @return id_column string What is the primary attribute_name (or id_column) which can be joined against acs_objects.object_id (corresponds to the object_id)
        @return status_column string Which column in the table contains the status(_id) of the object 
        @return type_column string Which column (attribute_name) contains the subtype for this object type
        @return type_category_type string What is the category type to use for the type_column values (e.g. 'Intranet Project Type' for 'im_project')
        @return status_category_type string What is the category type to use for the status values.
    } - 
}

ad_proc -public cog_rest::get::cognovis_object {
    -rest_user_id:required
    -object_id:required 
} {
    Provides a cognovis object with additional information

    @param object_id integer object we want to look attributes

    @return object json_object cognovis_object Object data
} {
    set object_p [acs_object::object_p -id $object_id]
    if {!$object_p} {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_id_not_object]"
    }

    set id $object_id
    set name [ad_html_to_text -no_format [im_name_from_id $object_id]]
    db_1row type_info "select o.object_type, ot.type_column, ot.status_column, ot.table_name, ot.id_column
        from acs_objects o, acs_object_types ot
        where o.object_id = :object_id
        and o.object_type = ot.object_type"
    
    set object_id [db_string object_id "select $id_column from $table_name where $id_column = :object_id" -default ""]
    if {$object_id ne ""} {
        switch $object_type {
            im_invoice - im_timesheet_invoice - im_trans_invoice {
                db_1row object_info "select cost_status_id as status_id, cost_type_id as type_id from im_costs where cost_id = :object_id"
                set type_icon [im_category_icon_or_color -category_id $type_id]
                set status_color [im_category_icon_or_color -category_id $status_id]
            }
            default {
                if {$type_column ne "" && $status_column ne ""} {
                    db_1row object_info "select $status_column as status_id, $type_column as type_id from $table_name where $id_column = :object_id"
                    set type_icon [im_category_icon_or_color -category_id $type_id]
                    set status_color [im_category_icon_or_color -category_id $status_id]
                } else {
                    set type_icon ""
                    set status_color ""
                    set status_id ""
                    set type_id ""
                }
            }
        }

        # Get object permissions for that user
        set privileges [list]
        foreach privilege [list view read write admin] {
            set permission_p [im_object_permission -object_id $object_id -user_id $rest_user_id -privilege $privilege]
            if {$permission_p} {
                lappend privileges $privilege
            }
        }
        set privileges [join $privileges " "]
        cog::callback::invoke cog_rest::object_custom_permissions -object_id $object_id -user_id $rest_user_id

        set object [cog_rest::json_object]
    } else {
        set object ""
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::object_types {
    -rest_user_id:required
    -object_type:required
} {
    Returns an object type details 
    
    @param object_type Filter for a specific object type (to just return the values for that)
    @return object_type json_array object_type Data describing the object_type
} {
    if {![db_0or1row object_type "select pretty_name, table_name, id_column, status_column, type_column, status_category_type, type_category_type from acs_object_types where object_type = :object_type"]} {
        cog_rest::error -http_status 400 -message [_ "cognovis-rest.err_unknown_object_type"]
    } 
    set object_type [list [cog_rest::json_object]]
    return [cog_rest::json_response]
}


ad_proc -public cog_rest::get::linked_objects {
    -rest_user_id:required
    -object_id:required 
} {
    Returns an array of linked objects 

    @param object_id integer Object for which we want to get the linked objects
    @return linked_objects json_array cognovis_object Linked objects for this invoice
} {
    set object_type [db_string object_type "select object_type from acs_objects where object_id = :object_id" -default ""]
    if {$object_type eq ""} {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_type_from_id]"
    }

    set linked_objects [list]

    switch $object_type {
        im_invoice - im_timesheet_invoice - im_trans_invoice {
            set invoice_id $object_id
            set linked_object_ids [im_invoices::linked_invoices -invoice_id $invoice_id]
            set cost_type_id [db_string cost_type "select cost_type_id from im_costs where cost_id = :invoice_id"]
            callback cog_rest::invoice::linked_objects -invoice_id $invoice_id -cost_type_id $cost_type_id            
        }
        default {
            set linked_object_ids [db_list linked "select object_id_one from acs_rels where object_id_two = :object_id union select object_id_two from acs_rels where object_id_one = :object_id"]
        }
    }

    foreach object_id $linked_object_ids {
        lappend linked_objects [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $object_id]
    }
    

    return [cog_rest::json_response]
}

#---------------------------------------------------------------
# Disable the usage of object_type as it isn't working for the time being
#---------------------------------------------------------------
if {0} {

    ad_proc -private cog_rest::post::object_type_do_not_use_example_only {
        -rest_user_id:required
        -rest_otype:required
        { -rest_oid "" }
        { -query_hash_pairs {} }
        { -debug 0 }
    } {
        Handler for POST rest calls to an object type - create a new object.
    } {

        set possible_columns [cog_rest::object_type_columns -rest_otype "$rest_otype" -required_arr required_arr]

        if {$rest_oid eq ""} {
            # We need to create the project

            switch rest_otype {
                im_project {
                    set project_id [im_project::new -project_name $project_name -project_nr $project_nr \
                        -company_id $company_id -parent_id $parent_id \
                        -project_type_id $project_type_id -project_status_id $project_status_id]
                }
                default {
                    if {[nsv_exists api_proc_doc "::xo::db::sql::$rest_otype proc new"]} {
                        array set doc_elements [nsv_get api_proc_doc "::xo::db::sql::$rest_otype proc new"]
                        set switches $doc_elements(switches)
                    }
                    # Loop through the switches. If the switch is a required one (as per possible columns) make it mandatory
                    # Otherwise "just" provide it but leave it empty


                    # Update all additionaly possible columns once we recieved the object_id

                }
            }
        
        } else {
            # update

            foreach column $possible_columns {
                if {[info exists $column]} {
                    if {$required_arr($column)} {
                        # Check that the required var is provided
                        if {[set $column] ne ""} {
                            lappend set_vars "$column = :$column"
                        } 
                    } else {
                        lappend set_vars "$column = :$column"
                    }
                }
            }

            if {[llength $set_vars]>0} {
                db_1row object_column_and_id "select table_name, id_column from acs_object_types where object_type = :rest_otype"

                # We need to check that the column is not from an extension table.
                # Ignore for now, but if you run into an error, you know why :-).

                db_dml update_project "update $table_name set [join $set_vars " , "] where $id_column = :project_id"
            } else {
                # nothing to set, nothing to do.
            }

            cog::callback::invoke -action after_update -object_id $project_id
        }

        return "{ \"project_id\": $project_id}"
    }

    ad_proc -public cog_rest::get::object_type {
        -rest_otype:required
        -columns:required
        { -deref_p 0 }
        { -rest_user_id 0 }
        { -rest_oid "" }
        { -query_hash_pairs {} }
        { -pagination ""}
    } {
        Handler for GET rest calls on a whole object type -
        mapped to queries on the specified object type

        @author malte.sussdorff@cognovis.de
        @author frank.bergmann@project-open.com

        @param rest_otype string Object type to get.
        @param query_hash_pairs string List of key value attributes passed through the request, which are not already in the switches

        @param columns json_array List of columns to return
        @param_columns column string Database column to return

        @param rest_oid integer id of object in case single one is needed

        @param deref_p boolean Should we dereference certain columns?

        @param pagination pagination_object Appends pagination information to a SQL statement depending on URL parameters

        @return data Array with the objects of the object_type requested
    } {
        cog_log -user_id "" Debug "::cog_rest::get::object_type: rest_user_id=$rest_user_id, rest_otype=$rest_otype, rest_oid=$rest_oid, query_hash=$query_hash_pairs"

        array set query_hash $query_hash_pairs
        set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = '$rest_otype'" -default 0]]

        im_security_alert_check_integer -location "::cog_rest::get::object: deref_p" -value $deref_p

        set base_url "[im_rest_system_url]/cognovis-rest"

        # -------------------------------------------------------
        # Get some more information about the current object type
        set otype_info [util_memoize [list db_list_of_lists rest_otype_info "select table_name, id_column from acs_object_types where object_type = '$rest_otype'"]]

        set table_name [lindex $otype_info 0 0]
        set id_column [lindex $otype_info 0 1]
        if {"" == $table_name} {
            return [cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_invalid_dyn_conf]"]
        }
        # Deal with ugly situation that usre_id is defined multiple times for object_type=user
        if {"users" == $table_name} { set id_column "person_id" }
        
        if {$rest_oid eq ""} {
            # Set the rest_oid to the id_column in 
            # case the ID column was provided as a filter
            if {[info exists query_hash($id_column)]} {
                set rest_oid $query_hash($id_column)
            }
        } 


        # -------------------------------------------------------
        # Check if there is a where clause specified in the URL
        # and validate the clause.
        set where_clause_list [list]
        set where_clause_unchecked_list [list]

        # -------------------------------------------------------
        # Check if there are "valid_vars" specified in the HTTP header
        # and add these vars to the SQL clause
        set valid_vars [cog_rest::object_type_columns -deref_p $deref_p -rest_otype $rest_otype -data_type_arr data_types]

        foreach v $valid_vars {
            if {[info exists query_hash($v)]} {

                set value $query_hash($v)

                # Check for special values
                switch $value {
                    "NULL" {
                        lappend where_clause_list "$v is null"
                    }
                    "NOT NULL" {
                        lappend where_clause_list "$v is not null"
                    }
                    default {
                        switch $data_types($v) {
                            int4 - int2 - float - number - float8 - numeric {
                                lappend where_clause_list "$v = $value"
                            } 
                            bpchar - boolean - bool {
                                if {$value} {
                                    lappend where_clause_list "$v = 't'"
                                } else {
                                    lappend where_clause_list "$v = 'f'"
                                }
                            }
                            default {
                                lappend where_clause_list "$v = '$value'"
                            }
                        }
                    }
                }
            }
        }

        # -------------------------------------------------------
        # Check if there was a rest_oid provided as part of the URL
        # for example /im_project/8799. In this case add the oid to
        # the query.
        # rest_oid was already security checked to be an integer.
        if {"" != $rest_oid && 0 != $rest_oid} {
            lappend where_clause_list "$id_column=$rest_oid"
        }

        # -------------------------------------------------------
        # Transform the database table to deal with exceptions
        #
        switch $rest_otype {
            user - person - party {
                set table_name "(
                select	*
                from	users u, parties pa, persons pe
                where	u.user_id = pa.party_id and u.user_id = pe.person_id and
                    u.user_id in (
                        SELECT  o.object_id
                        FROM    acs_objects o,
                                group_member_map m,
                                membership_rels mr
                        WHERE   m.member_id = o.object_id AND
                                m.group_id = acs__magic_object_id('registered_users'::character varying) AND
                                m.rel_id = mr.rel_id AND
                                m.container_id = m.group_id AND
                                m.rel_type::text = 'membership_rel'::text AND
                                mr.member_state = 'approved'
                    )
                )"
            }
            file_storage_object {
                # file storage object needs additional security
                lappend where_clause_unchecked_list "'t' = acs_permission__permission_p(rest_oid, $rest_user_id, 'read')"
            }
            im_ticket {
                # Testing per-ticket permissions
                set read_sql [im_ticket_permission_read_sql -user_id $rest_user_id]
                lappend where_clause_unchecked_list "rest_oid in ($read_sql)"
            }
        }

        # Check that the where_clause elements are valid SQL statements
        foreach where_clause $where_clause_list {
            set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
            if {!$valid_sql_where} {
                return [cog_rest::error -http_status 403 -message "[_ cognovis-rest.err_invalid_sql]"]
            }
        }

        # Build the complete where clause
        set where_clause_list [concat $where_clause_list $where_clause_unchecked_list]
        if {[llength $where_clause_list]>0} {
            set where_sql "where [join $where_clause_list " and\n\t\t"]"
        } else {
            set where_sql ""
        }
        
        # -------------------------------------------------------
        # Select SQL: Pull out objects where the acs_objects.object_type 
        # is correct AND the object exists in the object type's primary table.
        # This way we avoid "dangling objects" in acs_objects and sub-types.
        set sql [im_rest_object_type_select_sql -deref_p $deref_p -rest_otype $rest_otype -no_where_clause_p 1]
        append sql "
        where	o.object_type in ('[join [im_rest_object_type_subtypes -rest_otype $rest_otype] "','"]') and
            o.object_id in (
                select  t.$id_column
                from    $table_name t
            )\
        "

        # Add $where_clause to the outside of the SQL in order to
        # avoid ambiguities of duplicate columns like "rel_id"
        set sql "
        select	*
        from	($sql
            ) t
        $where_sql
        "
        
        # -------------------------------------------------------
        # Permissions
        # -------------------------------------------------------

        # Check for generic permissions to read all objects of this type
        set rest_otype_read_all_p [im_object_permission -object_id $rest_otype_id -user_id $rest_user_id -privilege "read"]

        if {!$rest_otype_read_all_p} {
            # There are "view_..._all" permissions allowing a user to see all objects:
            switch $rest_otype {
                bt_bug		{ }
                im_company		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_companies_all"] }
                im_cost		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_finance"] }
                im_conf_item	{ set rest_otype_read_all_p [im_permission $rest_user_id "view_conf_items_all"] }
                im_invoices		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_finance"] }
                im_project		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_projects_all"] }
                im_user_absence	{ set rest_otype_read_all_p [im_permission $rest_user_id "view_absences_all"] }
                im_office		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_offices_all"] }
                im_profile		{ set rest_otype_read_all_p 1 }
                im_ticket		{ set rest_otype_read_all_p [im_permission $rest_user_id "view_tickets_all"] }
                im_timesheet_task	{ set rest_otype_read_all_p [im_permission $rest_user_id "view_timesheet_tasks_all"] }
                im_timesheet_invoices { set rest_otype_read_all_p [im_permission $rest_user_id "view_finance"] }
                im_trans_invoices	{ set rest_otype_read_all_p [im_permission $rest_user_id "view_finance"] }
                im_translation_task	{ }
                user		{ }
                default { 
                    # No read permissions? 
                    # Well, no object type except the ones above has a custom procedure,
                    # so we can deny access here:
                    return [cog_rest::error -http_status 403 -message "[_ cognovis-rest.err_perm_user_objects_type]"]
                }
            }
        }

        set result ""
        set user_id $rest_user_id

        # Check permission procedure
        set permission_proc ""
        if {!$rest_otype_read_all_p} {
            # Check if there is a special proc for the object_tpye
            if { [llength [info commands ${rest_otype}_permissions]]} {
                set permission_proc "${rest_otype}_permissions"
            }
        }

        # Only return those vars which are valid and which were requested
        if {$columns ne ""} {
            set valid_vars_orig $valid_vars
            set valid_vars [list]
            foreach valid_var $valid_vars_orig {
                if {[lsearch $columns $valid_var]>-1} {
                    lappend valid_vars $valid_var
                }
            }
            
        }

        if {$pagination ne ""} {
            append sql [cog_rest::sort_and_paginate -pagination $pagination]
        }
        set data [cog_rest::json_objects -sql $sql -valid_vars $valid_vars -rest_user_id $rest_user_id -data_type_arr data_types -permission_proc $permission_proc -rest_oid $rest_oid -rest_otype $rest_otype]
        
        return [cog_rest::return_array]
    }
}


ad_proc -public cog_rest::delete::object {
    -object_id:required
    -rest_user_id
} {
    @param object_id integer Object Id to delete
} {
    set object_type [acs_object_type $object_id]

    set permission_p [im_object_permission -object_id $object_id -user_id $rest_user_id -privilege write]

    if {$permission_p} {
        if {[catch {
            db_exec_plsql delete_note {
                select ${object_type}__delete($object_id);
            }
        } errmsg]} {
            cog_rest::error -http_status 500 -message "$errmsg"
        }
    }
}
