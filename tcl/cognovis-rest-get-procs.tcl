ad_library {
    REST Procedures for GET calls
    @author malte.sussdorff@cognovis.de
    @author frank.bergmann@project-open.com
}

#---------------------------------------------------------------
# Non ACS-Object "objects"
#---------------------------------------------------------------

namespace eval cog_rest::json_object {
    ad_proc category {} {
        @return category_id integer ID of the category
        @return category string Name of the category
        @return category_translated string translated string in the locale of the current rest_user_id
        @return category_description string Description for the category
        @return category_type string Type of the category, especially important for so called object_type category types
        @return enabled_p boolean Is the category enabled and ready to use
        @return aux_int1 number aux_int1
        @return aux_int2 number aux_int2
        @return parent_only_p boolean parent_only_p
        @return aux_string1 string aux_string1
        @return aux_string2 string aux_string2
        @return aux_html1 html aux_html1
        @return aux_html2 html aux_html2
        @return sort_order number Order in which to display the category
    } -

    ad_proc parameter {} {
        @return package_id integer package_id of returned package
        @return package_key string package_key of returned package 
        @return parameter_id integer parameter_id of returned package parameter
        @return parameter_name string parameter_name of returned package parameter
        @return value string value of package parameter
        @return default_value string default_value of package parameter - Returns a json string even if the default value might be something else...
    } -

    ad_proc cost_center {} {
        @return cost_center object im_cost_center::read Cost center id and name
        @return parent object im_cost_center::read Cost center parent if it exists
        @return cost_center_status category "Intranet Cost Center Status" Status of the cost center
        @return cost_center_type category "Intranet Cost Center Type" What kind of cost center are we looking at
        @return department_p boolean Is this a department?
        @return manager object user Person managing the cost center, typically for the department
        @return description string Description of the cost center
    } -

    ad_proc error {} {
        @return parameter string Parameter which caused the error
        @return object_id integer ID of the object which caused the error
        @return err_msg string Message of the error
    } -

    ad_proc translation {} {
        @return message_key string Key of the message used in PO
        @return message string Translated string of the message key
        @return key_of_package string Package key of fetched message
        @return locale string Locale of the translation
    } - 

    ad_proc group {} {
        @return group_id integer ID of the group the user is a member of
        @return group_name string Name of the group the user is a member of
    } - 

    ad_proc menu_item {} {

        @return menu_id integer ID of the menu
        @return parent_menu object named_id parent menu
        @return name string Translated name for the menu
        @return sort_order integer Position of the menu item in the menu list
        @return url string Routing information
        @return icon string Icon to use, can be 'fa fa-stop' or 'mdimdi-stop' 
        @return label string Label for the menu, used in lookups of parent_menu_id. Like a menu_nr. Not making any sense, should be swapped with name, but thats how ]project-open[ thinks

    } - 

    ad_proc user {} {
        @return user json_object cognovis_object Username and ID of the user
        @return screen_name string Screenname used as an Alias
        @return locale string Locale to use in communication
        @return seconds_since_last_request integer How many seconds since this user was last seen on the site (not with this object)
        @return portrait_url string URL to the portrait of the user
        @return company json_object cognovis_object Main company for the user
    } -

    ad_proc biz_object_member {} {
        @return rel named_id Relation information
        @return object named_id Object that the user is a member of
        @return member json_object user User who is a member of the object
        @return role category "Intranet Biz Object Role" Role the user has in relation to the object
        @return percentage number How much of their time are they in this role
    } - 

    ad_proc biz_object_member_body {} {
        @param object_id integer Object that the user is a member of
        @param member_id object user::* User who is a member of the object
        @param role_id category "Intranet Biz Object Role" Role the user has in relation to the object
        @param percentage number How much of their time are they in this role
    } - 

    ad_proc privilege {} {
        @return permission_user named_id User we has the permission
        @return object named_id Object that user is looking at
        @return view boolean Can the user view the object (e.g. in lists)
        @return read boolean Can the user read the full object information
        @return write boolean Can the user create and / or update the object
        @return admin boolean Can the user admin / delete the object
    } - 

    ad_proc named_object {} {
        @return id integer ID of the object we return
        @return name string Actual name of the object usually from im_name_from_id
    } - 

}

namespace eval cog_rest::get {

    ad_proc -public im_category {
        -rest_user_id:required
        { -category_id "" }
        { -category_type "" }
        { -enabled_p "1" }
        { -parent_category_id ""}
        { -pagination "property,sort_order,direction,ASC" }
    } {
        Handler for GET rest calls on categories

        @param category_type string Type of category we are returning
        @param category_id integer Category id to return a single category
        @param enabled_p boolean Should we only return enabled categories
        @param parent_category_id integer Parent Category for which we want to retrieve the children

        @param pagination pagination_object Limit the array to only a certain number of entries and sort

        @return categories json_array category Array of categories in the system
    } {
        set base_url "[im_rest_system_url]/intranet-rest"        
        
        set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_category'" -default 0]]
        set rest_otype_read_all_p [im_object_permission -object_id $rest_otype_id -user_id $rest_user_id -privilege "read"]
        set rest_otype_name [im_name_from_id $rest_otype_id]
        if {!$rest_otype_read_all_p} {
            return [cog_rest::error -http_status 404 -message "[_ cognovis-rest.err_perm_read_type]"]
        }

        # Get locate for translation
        set locale [lang::user::locale -user_id $rest_user_id]
        
        # -------------------------------------------------------
        # Check if there is a where clause specified in the URL and validate the clause.
        set where_clause ""

        # -------------------------------------------------------
        # Check if there are "valid_vars" specified in the HTTP header
        # and add these vars to the SQL clause
        set where_clause_list [list]

        foreach v [list category_type category_id] {
            if {[set $v] ne ""} {
                lappend where_clause_list "$v=:$v"
            }
        }

        if {$parent_category_id ne ""} {
            set subcategories [im_sub_categories $parent_category_id]
            lappend where_clause_list "c.category_id in ([template::util::tcl_to_sql_list $subcategories])"
        }

        if {$enabled_p} {
            lappend where_clause_list "(c.enabled_p is null OR c.enabled_p = 't')"
        } else {
            lappend where_clause_list "(c.enabled_p is null OR c.enabled_p = 'f')"
        }

        # Sorting
        if {$pagination ne ""} {
            set sort_sql [cog_rest::sort_and_paginate -pagination $pagination]
        }

        # Select SQL: Pull out categories.
        set sql "
        select  c.category_id,
            c.category as object_name, visible_tcl,
            im_category_path_to_category(c.category_id) as tree_sortkey,
            c.*
        from im_categories c
        where [join $where_clause_list " and "]
        $sort_sql
        "
        
        set categories [list]
        db_foreach category $sql {
            set is_visible_p 1
            if {$visible_tcl ne ""} {
                set is_visible_p [expr $visible_tcl]
            }
            if {$is_visible_p || [acs_user::site_wide_admin_p]} {
                set category_translated [im_category_from_id -current_user_id $rest_user_id $category_id]
                lappend categories [cog_rest::json_object]
            }
        }

        return [cog_rest::json_response]
    }

    
    ad_proc -public i18n {
        -package_key:required
        { -message_key "" }
        { -locale ""}
        { -rest_user_id 0 }
    } {

        Returns translations json array of translations containing messsage_key, message

        @summary Endpoint returning i18n translations for given package_keys in the locale

        @param package_key string Package key to get. Can also be comma seperated set of package_names
        @param message_key string Message to get
        
        @param locale string locale (e.g. \"de-DE\"), if nothing is provided, we use currently logged user locale
        @param rest_user_id integer user_id in case no locale is set

        @return translations json_array translation Array of translation objects 
    } {
        
        # Checking for @param:locale param
        if {$locale eq ""} {
            # Getting currently logged user locale
            set locale [lang::user::locale -user_id $rest_user_id]
        } 

        if {$locale eq "all"} {
            set locale_sql ""
        } else {
            set locale_sql "AND locale =:locale" 
        }

        set translations [list]
        set package_keys [split $package_key ","]

        set message_sql "select distinct message, message_key, package_key as key_of_package, locale from lang_messages where package_key in ([template::util::tcl_to_sql_list $package_keys]) $locale_sql"

        if {$message_key ne ""} {
            append message_sql " and message_key = :message_key"
        }

        db_foreach message_keys $message_sql {

            if {[string first "%" $message] != -1} {
                # We have variables in here... ignore
                continue
            }

            set message ""
            # Make sure messages are in the cache
            lang::message::cache

            set key "${key_of_package}.$message_key"
            set message [cog::message::lookup -key $key -locale $locale]

            if {$message ne ""} {
                regsub -all {[^[:print:]]} $message {} message 
                regsub -all {[^[:print:]]} $message_key {} message_key              	  
                lappend translations [cog_rest::json_object]
            }
        }
        return [cog_rest::json_response]
    }

    ad_proc -public user_groups {
	    { -rest_user_id 0 }
        { -user_id "" }
    } {
    	Returns a JSON array of group_ids and names the user is a member off 

        @param user_id object user::read User for whom we want to get the groups.
	    @return profiles json_array group array of groups the user is a member of.
    } {
        if {$user_id eq ""} {
            set user_id $rest_user_id
        }
        set profiles [cog_rest::user_profile_objects -user_id $user_id]
        return [cog_rest::json_response]
    }

    ad_proc -public object_permission {
        -object_id:required
        -rest_user_id:required
        { -permission_user_id "" }
    } {

        Endpoint which allows to check what kind of permissions user have for give object_id

        @param object_id integer id of object for which we need to check permissions types
        @param permission_user_id integer user_id for which to check the permission

        @return permission json_object privilege Array of privileges
    } {

        set allow_permission_check_p 0
        if {$permission_user_id eq ""} {
            set permission_user_id $rest_user_id
        } 

        # If permission_user_id and rest_user_id are different it means someone else checks permission for other user
        # That is why we need to check if currently logged user is an admin first
        if {$permission_user_id eq $rest_user_id} {
            set allow_permission_check_p 1
        } else {
            set admin_p [im_is_user_site_wide_or_intranet_admin $rest_user_id]
            if {$admin_p} {
                set allow_permission_check_p 1
            }
        }

        if {$allow_permission_check_p} {
            set object_type [acs_object_type $object_id]
    		set perm_proc [cog_rest::helper::object_permission_proc -object_type $object_type]
            if {$perm_proc ne ""} {
                ds_comment "$perm_proc"
			    $perm_proc $permission_user_id $object_id view read write admin
            } else {
			    set read [permission::permission_p -no_login -party_id $permission_user_id -object_id $object_id -privilege "read"]
                set view [permission::permission_p -no_login -party_id $permission_user_id -object_id $object_id -privilege "view"]
                set write [permission::permission_p -no_login -party_id $permission_user_id -object_id $object_id -privilege "write"]
                set admin [permission::permission_p -no_login -party_id $permission_user_id -object_id $object_id -privilege "admin"]
            }
            set permission [cog_rest::json_object] 
            return [cog_rest::json_response]
        } else {
            return [cog_rest::error -http_status 403 -message "[_ cognovis-rest.err_perm_other_users]"]
        }       
    }


    ad_proc -public menu_items {
        -rest_user_id:required
        { -parent_menu_id "" }
        { -parent_menu_label "" }
        { -package_name "" }
        { -object_id "" }

    } {

        Return the data structure for the menu for the specific user viewing the parent.

        @param parent_menu_id integer menu_id of the parent menu
        @param parent_menu_label string Label for which we lookup the parent_menu_id
        @param package_name string Package Name for which we lookup the parent_menu_id
        @param object_id integer Object_id for which we want to show the menu

        @return menu_items json_array menu_item Array of menu items
    } {
        set user_id $rest_user_id
        set locale [lang::user::locale -user_id $user_id]
        set admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
        set menu_items [list]

        if {$parent_menu_id eq ""} {
            set sql "select menu_id from im_menus where label = :parent_menu_label"
            if {$package_name ne ""} {
                append sql " and package_name = :package_name"
            }
            set parent_menu_id [db_string parent_menu $sql -default ""]
            if {$parent_menu_id eq ""} {
                cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_miss_parent_menu_label]"
            }
        }

        # First loop so we can collect menu ids
        set menus_ids [list $parent_menu_id]
        set menu_id_select_sql "
        select  menu_id from  im_menus m
        where   parent_menu_id = :parent_menu_id and
            (enabled_p is null or enabled_p = 't') and
            im_object_permission_p(m.menu_id, :user_id, 'read') = 't'
        order by sort_order
        "

        db_foreach menu_select $menu_id_select_sql {
            lappend menus_ids $menu_id
        }

        # Now that we have menu_ids we execute main query
        set menu_select_sql "
        select  menu_id, parent_menu_id, url, package_name, name, label, sort_order, visible_tcl, menu_gif_small
        from    im_menus m
        where   parent_menu_id in ([template::util::tcl_to_sql_list $menus_ids]) and
            (enabled_p is null or enabled_p = 't') and
            im_object_permission_p(m.menu_id, :user_id, 'read') = 't'
        order by sort_order
        "
    
        db_foreach menu_select $menu_select_sql {
            if {"" != $visible_tcl} {
                set visible 0
                set errmsg ""
                if [catch { set visible [expr $visible_tcl] } errmsg] {
                # Return err?            ad_return_complaint 1 "<pre>$visible_tcl\n$errmsg</pre>"
                }
                if {!$visible} { continue }
            }
            regsub -all {[^0-9a-zA-Z]} $name {_} name_key
            set name [lang::message::lookup "" $package_name.$name_key $name]
            set icon $menu_gif_small
            set parent_menu_name [db_string get_parent_menu_name "select label from im_menus where menu_id=:parent_menu_id limit 1" -default ""]
            lappend menu_items [cog_rest::json_object]
        }

        return [cog_rest::json_response]
    }

    ad_proc -public user {
        {-user_ids ""}
        -profile_id
        -rest_user_id:required
        
    } {
        Get information about users

        @param user_ids object_array person::read IDs of the users we want to look at
        @param profile_id object im_profile::* Profile we look for

        @return users json_array user Array with information of the users
    } {
        set users [list]
        if {[info exists profile_id]} {
             set user_ids [list]
            foreach user_info [im_profile::user_options -profile_ids $profile_id] {
                lappend user_ids [lindex $user_info 1]
            }
        }
        foreach user_id $user_ids {
            set screen_name [db_string screen "select screen_name from users where user_id = :user_id" -default ""]
            set locale [lang::user::locale -user_id $user_id]
            set seconds_since_last_request [whos_online::seconds_since_last_request $user_id]
            set portrait_url [cog_rest::helper::portrait_url -user_id $user_id]
            set user [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $user_id]
            set main_company_id [cog::user::main_company_id -user_id $user_id]
            if {$main_company_id ne ""} {
                set company [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $main_company_id]
            } else {
                set company ""
            }
            lappend users [cog_rest::json_object]
        }

        return [cog_rest::json_response]
    }

    ad_proc -public group_members {
        -group_id:required
        -rest_user_id:required
    } {
        Returns an array of members of a specific group

        @param group_id object im_profile group_id which we are looking for

        @return members json_array user User who is a member of the object
    } {
        set members [list]
        set group_member_ids [db_list members "select member_id from group_member_map g, users u where 
            g.member_id = u.user_id 
            and group_id = :group_id 
            and g.member_id not in (
			-- Exclude banned or deleted users
			select	m.member_id
			from	group_member_map m,
				membership_rels mr
			where	m.rel_id = mr.rel_id and
				m.group_id = acs__magic_object_id('registered_users') and
				m.container_id = m.group_id and
				mr.member_state != 'approved')"]
        foreach member_id $group_member_ids {
            lappend members [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $member_id -rest_user_id $rest_user_id]]
        }
        return [cog_rest::json_response]
    }

    ad_proc -public api_key {
        -user_id:required
        -rest_user_id:required
    } {
        Returns the api_key

        @param user_id object user::read User for whom we want to get the api_key. Defaults back to current user for non admins

        @return login json_object Login info
        @return_login api_key string API Key for the currently logged in user
    } {
        set admin_p [im_is_user_site_wide_or_intranet_admin $rest_user_id]
        if {!$admin_p} {
            set user_id $rest_user_id
        }

        set auto_login [im_generate_auto_login -user_id $user_id]

        set api_key [base64::encode "${user_id}:$auto_login"]
        return [cog_rest::json_object]
    }
}


#---------------------------------------------------------------
# Biz Object members
#---------------------------------------------------------------

ad_proc -public cog_rest::get::biz_object_members {
    { -object_id "" }
    { -member_id "" }
    -rest_user_id:required

} {
    Returns an array of members for a business object (like project or company)

    @param object_id object *::read Object for which we look at the members
    @param member_id object person::read Member for whom we are looking for memberships

    @return members json_array biz_object_member Array of Members for this object
} {
    set members [list]
    set where_clause_list [list]
    if {$object_id ne "" } {
        lappend where_clause_list "object_id_one = :object_id"
    }

    if {$member_id ne "" } {
        lappend where_clause_list "object_id_two = :member_id"
    }

    if {[llength $where_clause_list] eq 0} {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_miss_member_object]"
    }

    set member_sql "
        select
        rels.rel_id,
        rels.object_id_two as member_id, 
        rels.object_id_one as object_id,
        bo_rels.percentage,
        bo_rels.object_role_id as role_id
    from
        acs_rels rels
        LEFT OUTER JOIN im_biz_object_members bo_rels ON (rels.rel_id = bo_rels.rel_id)
    where
        [join $where_clause_list " and "] 
        and rels.object_id_two in (select user_id from users)
        and rels.object_id_two not in (
            -- Exclude banned or deleted users
            select	m.member_id
            from	group_member_map m,
                membership_rels mr
            where	m.rel_id = mr.rel_id and
                m.group_id = acs__magic_object_id('registered_users') and
                m.container_id = m.group_id and
                mr.member_state != 'approved'
        )"

    db_foreach members $member_sql {
        set member [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $member_id -rest_user_id $rest_user_id]]
        lappend members [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::biz_object_member {
    -object_id:required
    -member_id:required
    { -role_id ""} 
    { -percentage ""}
    -rest_user_id:required
} {
    Add a member to an biz object

    @param biz_object_member_body request_body Member and object info

    @return members json_array biz_object_member Array of Members for this object after the user was added
} {
    if {$role_id eq ""} {
        set role_id [im_biz_object_role_full_member]
    }

    im_biz_object_add_role -percentage $percentage -current_user_id $rest_user_id $member_id $object_id $role_id

    return [cog_rest::get::biz_object_members -object_id $object_id -rest_user_id $rest_user_id]
}

ad_proc -public cog_rest::delete::biz_object_member {
    -object_id:required
    -member_id:required
    -rest_user_id:required
} {
    Remove a member from the object
    @param object_id integer object we want to remove the user from
    @param member_id object user::*  member we want to remove

    @return members json_array biz_object_member Array of Members for this object after the user was removed
} {

    db_1row remove_member "select im_biz_object_member__delete(:object_id,:member_id) from dual"
    return [cog_rest::get::biz_object_members -object_id $object_id -rest_user_id $rest_user_id]
}



ad_proc -public cog_rest::get::valid_object_types {
    -rest_user_id:required
} {
    Retrieve a list of valid object types that have associated dynamic attributes.

    @return object_types json_array object_type Array of valid object types
} {

    set object_types [list]
    
    set object_types_sql "select distinct * from acs_attributes aa, acs_object_types a, im_dynfield_attributes da where aa.object_type = a.object_type and da.acs_attribute_id = aa.attribute_id"

    db_foreach object_type $object_types_sql {
        lappend object_types [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}