ad_library {

    Callbacks for cognovis-rest module
    
    @author malte.sussdorff@cognovis.de

}

ad_proc -public -callback cog_rest::openapi::response_schema {
    -proc_name
    -object_name
} {
    Callback to allow injecting custom response values

    Typically used to append to the response_object_schema using cog_rest::openapi::response::object_schema 

    @param proc_name Name of the procedure
    @param object_name Name of the object of the procedure
} - 

ad_proc -public -callback cog_rest::object_return_element {
    -proc_name
    -object_class
} {
    Callback to allow injecting custom response values in cog_rest::json_object

    Typically used to append to the return_elements definition or to lappend to the value_json_strings
    
    @param proc_name Name of the procedure
    @param object_class Name of the object of the procedure

    @see cog_rest::json_object
} - 


ad_proc -public -callback cog_rest::cr_file_after_upload {
    -file_item_id
    -file_revision_id
    -context_id
    -context_type
    -user_id
} {
    Callback after a file was uploaded to the content repository

    @param file_item_id ItemId of the file we upload
    @param file_revision_id Revision of the file we uploaded
    @param context_id Context which was Provided
    @param context_type Object Type of the context
    @param user_id User who uploaded the file
} -

ad_proc -public -callback cog_rest::cr_folder_after_upload {
    -file_item_ids
    -folder_id
    -description
    -user_id
} {
    Callback after files where uploaded to a folder

    @param file_item_ids List of ItemIds of the files we upload
    @param folder_id Folder which files where uploaded to 
    @param description String which was provided during the upload
    @param user_id User who uploaded the file
} -

namespace eval cog_rest::invoice {}

ad_proc -public -callback cog_rest::invoice::linked_objects {
    -invoice_id
    -cost_type_id
} {
    Callback to allow injecting additional linked objects based of invoice type
} - 

ad_proc -public -callback cog_rest::register_party_after_create {
    -email
    -party_id
} {
    Callback to allow code execution after we created a new account_status

    Initially used to send an E-Mail to the created party for further login with token
} -

namespace eval cog_rest::dynamic {}

ad_proc -public -callback cog_rest::dynamic::values {
    -object_id
    -object_type
    -array_name
} {
    Callback to allow handling values (in an array) before they are updated in the dynamic proc.

    You can remove a value in an array if you want to prevent updates of this value. You can could also execute custom code for a value
} -

ad_proc -public -callback cog_rest::dynamic::values -impl user_password {
    -object_id
    -object_type
    -array_name
} {
    Update the users password if found
} {
    upvar $array_name values
    if {[lsearch [list user person party] $object_type]>-1 && [info exists $values(password)]} {
        cog_log Notice "Password found, trying to update"
    }
}

ad_proc -public -callback cog_rest::dynamic::get_object {
    -object_id
    -object_types
} {
    Callback to allow handling additional key/values when getting an object

} -

ad_proc -public -callback cog_rest::dynamic::put_object {
    -object_id
    -object_types
} {
    Callback to allow handling of skills updating

} -


ad_proc -public -callback cog_rest::object_custom_permissions {
    -object_id
    -user_id
} {
    Callback to allow handling additional key/values when getting an object

} -