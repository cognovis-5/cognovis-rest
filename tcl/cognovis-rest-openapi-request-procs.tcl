ad_library {
  OpenAPI functions for request objects & parameters
	@author malte.sussdorff@cognovis.de

}

namespace eval cog_rest::openapi::request_body {

    ad_proc -public schema {
        -proc_name:required
        -object_name:required
        -schema_arr:required
        -operation:required
    } {
        Returns the Request schema for a procedure for a specific object

        @param proc_name Name of the procedure for which to get the reponse schema
        @param object_name Name of the object
        @param schema_arr Array holding the specific schemas which we already have from paramaters
    } {

        upvar $schema_arr parameters_schema

        array set doc_elements [nsv_get api_proc_doc $proc_name]
        foreach param $doc_elements(param) {
            if {[lindex $param 0] eq $object_name} {
                set request_body_description [cog_rest::quotejson [lrange $param 2 end]]
            }
        }

        #---------------------------------------------------------------
        # Request Element
        #---------------------------------------------------------------
        switch $operation {
            put {
                set content_type "application/json"
            }
            post {
                # set content_type "multipart/form-data"
                set content_type "application/json"
            }
        }

        set request_body_name "I[cog_rest::helper::to_CamelCase -string "$object_name"]"
        set request_object_schema "        description: $request_body_description
        required: true
        content: 
          $content_type:
            schema:
              \$ref: '#/components/schemas/${request_body_name}'\n"

        # The object schema holds the reusable schema for the object e.g. if we have put & post
        if {![info exists parameters_schema($request_body_name)]} {
            set object_schema "    $request_body_name:
      type: object
      properties:\n"
            
            #---------------------------------------------------------------
            # The single properties
            #---------------------------------------------------------------
            set request_elements [list]
            set required_elements [list]
            array set flags $doc_elements(flags)

            if {[nsv_exists api_proc_doc "cog_rest::json_object::$object_name"]} {
                array set doc_elements [nsv_get api_proc_doc "cog_rest::json_object::$object_name"]
                foreach request_element $doc_elements(param) {
                    lappend request_elements $request_element
                }
            } else {
                foreach request_element $doc_elements(param_${object_name}) {
                    lappend request_elements $request_element
                }  
            }

            foreach request_element $request_elements {  
                # The first item is supposed to be the variable
                set request_items [split $request_element]
                set request_var [lindex $request_items 0]
                set request_type [lindex $request_items 1]

                append object_schema [cog_rest::openapi::request_body::object_schema -request_element $request_element -proc_name $proc_name -schema_arr "parameters_schema"]
                if {[info exists flags($request_var)]} {
                    # Search for required in the flags
                    if {[lsearch $flags($request_var) "required"]>-1} {
                        lappend required_elements $request_var
                    }
                }
            }

            #---------------------------------------------------------------
            # Add required flags
            #---------------------------------------------------------------

            if {[llength $required_elements]>0} {
                append object_schema "      required:\n"
                foreach required_element $required_elements {
                    append object_schema "        - $required_element\n"
                }
            }
            
            set parameters_schema($request_body_name) $object_schema
        }
        return $request_object_schema
    }

    ad_proc -public object_schema {
        -request_element:required
        -proc_name:required
        -schema_arr:required
    } {
        Returns the schema for a single response object based on the request_element definition

        @param request_element The element (usually consisting of var_name and type + description)
        @param object_name Object 

        @return Schema for the response object
    } {

        upvar $schema_arr schemas_comp
        # The first item is supposed to be the variable
        set request_var [lindex $request_element 0]
        set request_type [lindex $request_element 1]

        array set doc_elements [nsv_get api_proc_doc $proc_name]
        array set default_values $doc_elements(default_values)
        if {[info exists default_values($request_var)]} {
            set default $default_values($request_var)
        } else {
            set default ""
        }

        set request_schema_ref "[cog_rest::openapi::parameter::schema_ref -param_list $request_element -proc_name $proc_name -default $default]"

        # Transform the request_type
        switch $request_type {
            integer - string - boolean - number - date - date-time - binary - uuid {
                # The first parameter apparently is the type 
                set request_item_desc [cog_rest::convert_to_yaml_string -string_from "[lrange $request_element 2 end]"]
                # ignore basic types
                if {[lsearch [list String Number] $request_schema_ref] <0} {
                    if {![info exists schemas_comp($request_schema_ref)]} {
                        set schemas_comp($request_schema_ref) [cog_rest::openapi::schema::schema_yml -type $request_type -default $default -schema_ref $request_schema_ref]
                    }
                }
            } 
            integer_array {
                set request_schema_ref "IntegerArray"
                set request_item_desc [cog_rest::convert_to_yaml_string -string_from "[lrange $request_element 2 end]"]
            }
            object - object_array {
                set request_item_desc [cog_rest::convert_to_yaml_string -string_from "[lrange $request_element 3 end]"]
                set schemas_comp($request_schema_ref) "    $request_schema_ref:\n[cog_rest::openapi::schema::$request_type -default $default]"
            }
            json_object - json_array {
                set request_item_desc [cog_rest::convert_to_yaml_string -string_from "[lrange $request_element 3 end]"]
                set schemas_comp($request_schema_ref) "    $request_schema_ref:\n[cog_rest::openapi::schema::$request_type -default $default -type [lindex $request_element 2]]"
            }
            category - category_array {
                set category_type [lindex $request_element 2]

                set request_item_desc [cog_rest::convert_to_yaml_string -string_from "[lrange $request_element 3 end]"]
                set schemas_comp($request_schema_ref) "    $request_schema_ref:\n[cog_rest::openapi::schema::$request_type -category_type $category_type -default $default]"
                
                set valid_category_type_p [db_string valid_type "select 1 from im_categories where category_type = :category_type limit 1" -default 0]
                if {$valid_category_type_p} {

                    # Set the category_type schemas
                    if {![info exists schemas_comp($category_type)]} {
                        set enum_yml [cog_rest::openapi::schema::category_enum -category_type $category_type]
                        if {$enum_yml ne ""} {
                            set schemas_comp($category_type) "    [cog_rest::helper::to_CamelCase -string $category_type]:\n$enum_yml"
                        } else {
                            set schemas_comp($category_type) "    [cog_rest::helper::to_CamelCase -string $category_type]:\n      type: integer\n"
                        }
                    }
                }
            }
            material - material_array {
                set material_type [lindex $request_element 2]

                set request_item_desc [cog_rest::convert_to_yaml_string -string_from "[lrange $request_element 3 end]"]
                set schemas_comp($request_schema_ref) "    $request_schema_ref:\n[cog_rest::openapi::schema::$request_type -material_type $material_type -default $default]"
                
                set valid_material_type_p [db_string valid_type "select 1 from im_materials m, im_categories c where material_type_id = category_id and category = :material_type limit 1" -default 0]
                if {$valid_material_type_p} {

                    # Set the material_type schemas
                    if {![info exists schemas_comp($material_type)]} {
                        set enum_yml [cog_rest::openapi::schema::material_enum -material_type $material_type]
                        if {$enum_yml ne ""} {
                            set schemas_comp($material_type) "    [cog_rest::helper::to_CamelCase -string $material_type]:\n$enum_yml"
                        } else {
                            set schemas_comp($material_type) "    [cog_rest::helper::to_CamelCase -string $material_type]:\n      type: integer\n"
                        }
                    }
                }
            }
            default {
                set request_item_desc [lrange $request_element 2 end]
            }
        }

        set request_schema "        $request_var:\n"

        if {$request_item_desc ne ""} {
            append request_schema "          description: '[cog_rest::convert_to_yaml_string -string_from $request_item_desc]'\n"
        }

        
        # Avoid building the schema_ref for number and string
        
        switch $request_schema_ref {
            String - Number {
                append request_schema "          type: [string tolower $request_schema_ref]\n"
            }
            NumberFloat {
                append request_schema "          type: number\n"
                append request_schema "          format: float\n"                
            }
            StringDateTime {
                append request_schema "          type: string\n"
                append request_schema "          format: date-time\n"           
            }            
            IntegerArray {
                append request_schema "          type: array\n          items:\n            type: integer\n"
            }
            default {
                append request_schema "          \$ref: '#/components/schemas/$request_schema_ref'\n"
            }
        }
        return $request_schema
    }
}


namespace eval cog_rest::openapi::parameter {

    ad_proc -public schema_ref {
        -param_list:required
        -proc_name:required
        { -default "" }
    } {
        Return the schema_ref for a parameter

        @param param_list List of elements for the parameter. First one being name, followed by type
        @param proc_name Name of the procedure being used for the endpoint
        @param default Value used as default
        
        @return schema_ref Reference to the schema
    } {

        set type [lindex $param_list 1]

        switch $type {

            object - object_array {
                set object_type [lindex $param_list 2]

                # Strip the privilege of the object_type
                regexp "(.*)::(.*)" $object_type match object_type privilege
                
                set valid_object_type_p [db_string valid_type "select 1 from acs_object_types where object_type = :object_type" -default 0]

                if {$valid_object_type_p} {
                    if {$type eq "object"} {
                        set schema_ref_name "${object_type}_id"
                    } else {
                        set schema_ref_name "${object_type}_ids"
                    }
                } else {
                    if {$type eq "object"} {
                        set schema_ref_name "Id"
                    } else {
                        set schema_ref_name "Ids"
                    }
                }

                if {$default ne ""} {
                    set schema_ref ${schema_ref_name}_$default
                } else {
                    set schema_ref $schema_ref_name
                }
            }
            category - category_array {
                set category_type [lindex $param_list 2]

                set valid_category_type_p [db_string valid_type "select 1 from im_categories where category_type = :category_type limit 1" -default 0]

                if {$valid_category_type_p} {
                    if {$type eq "category"} {
                        set schema_ref_name "${category_type}_id"
                    } else {
                        set schema_ref_name "${category_type}_ids"
                    }
                } else {
                    if {$type eq "category"} {
                        set schema_ref_name "Id"
                    } else {
                        set schema_ref_name "Ids"                        
                    }
                }
                if {$default ne ""} {
                    set schema_ref "${schema_ref_name}_$default"
                } else {
                    set schema_ref $schema_ref_name
                }
            }
            material - material_array {
                set material_type [lindex $param_list 2]

                set valid_material_type_p [db_string valid_type "select 1 from im_materials m, im_categories c where material_type_id = category_id and category = :material_type limit 1" -default 0]

                if {$valid_material_type_p} {
                    if {$type eq "material"} {
                        set schema_ref_name "${material_type}_id"
                    } else {
                        set schema_ref_name "${material_type}_ids"
                    }
                } else {
                    if {$type eq "material"} {
                        set schema_ref_name "Id"
                    } else {
                        set schema_ref_name "Ids"                        
                    }
                }
                if {$default ne ""} {
                    set schema_ref "${schema_ref_name}_$default"
                } else {
                    set schema_ref $schema_ref_name
                }
            }
            json_object {
                set schema_ref [lindex $param_list 2]
            }
            json_array {
                set schema_ref [lindex $param_list 2]
            }
            pagination_object {
                set schema_ref "Pagination"
            }
            default {
                set type_list [cog_rest::openapi::convert_to_openapi_type -type $type]
                
                set type [lindex $type_list 0]
                set format [lindex $type_list 1]

                # Set the schema ref as CamelCase
                set schema_ref "${type}"
                if {$format ne ""} {
                    append schema_ref "_${format}"
                }
                if {$default ne ""} {
                    append schema_ref "_$default"
                }
            }
        }

        set schema_ref [cog_rest::helper::to_CamelCase -string $schema_ref]
        return $schema_ref
    }


    ad_proc -public single_path {
        -name:required
        -schema_ref:required
        -type:required
        { -description "" } 
        { -required_p 0 }
    } {
        Returns the yml code for a single parameter

        @param name Name of the parameter
        @param schema_ref Name of the schema to reference
        @param type Type of parameter, used for style & explode definition
        @param description Description of the parameter
        @param required_p is this a required parameter?
    } {
        # initialize parameter component array
        set parameter_yml "        - in: query\n"
        append parameter_yml "          name: $name\n"
        if {$description ne ""} {
            append parameter_yml "          description: '$description'\n"
        }
        if {$required_p} {
            append parameter_yml "          required: true\n"
        }
        
        switch $type {
            object_array - category - category_array - material - material_array - json_object - pagination_object {
                # Don't explode, so provide category_ids=2,45,567 instead of category_ids=2&category_ids=45&category_ids=567
                append parameter_yml "          explode: false\n"
            }
        }
        # Avoid building the schema_ref for number and string
        switch $schema_ref {

            String - Number {
                append parameter_yml "          schema:\n            type: [string tolower $schema_ref]\n"
            }
            default {
                append parameter_yml "          schema:\n            \$ref: '#/components/schemas/$schema_ref'\n"
            }
        }

        return $parameter_yml
    }
    
    ad_proc -public path {
        -proc_name:required
        { -schema_arr "schemas_comp"}
    } {
        Build the path object for parameters

        @param proc_name procedure which is being called for that endpoint
        @param schema_arr Array which holds the schemas to be build 

        @return parameters_yml YAML for the parameters section in the path.
    } {
        upvar $schema_arr schemas_comp

        array set doc_elements [nsv_get api_proc_doc $proc_name]
        array set default_values $doc_elements(default_values)

        # Handle the flags to determine boolean / required
        array set flags $doc_elements(flags)

        set parameters_yml ""

        foreach param $doc_elements(param) {

            set name [lindex $param 0]
            
            if {[info exists default_values($name)]} {
                set default $default_values($name)
            } else {
                set default ""
            }

            # Find out if this is a required paramater
            set required_p 0
            if {[info exists flags($name)]} {
                # Search for required in the flags
                if {[lsearch $flags($name) "required"]>-1} {
                    set required_p 1
                }
            }
            
            set schema_ref [cog_rest::openapi::parameter::schema_ref -param_list $param -default $default -proc_name $proc_name]

            set type [lindex $param 1]

            switch $type {
                integer - string - boolean - number - date - date-time - binary - uuid {
                    # The first parameter apparently is the type 
                    set description [cog_rest::convert_to_yaml_string -string_from "[lrange $param 2 end]"]
                    
                    # ignore basic types
                    if {[lsearch [list String Number] $schema_ref] <0} {
                        if {![info exists schemas_comp($schema_ref)]} {
                            set schemas_comp($schema_ref) [cog_rest::openapi::schema::schema_yml -type $type -default $default -schema_ref $schema_ref]
                        }
                    }
                } 
                object - object_array {
                    set description [cog_rest::convert_to_yaml_string -string_from "[lrange $param 3 end]"]
                    switch $schema_ref {
                        id - ids {
                            # Do nothing
                        }
                        default {
                            set schemas_comp($schema_ref) "    $schema_ref:\n[cog_rest::openapi::schema::$type -default $default]"
                        }
                    }
                }
                category - category_array {
                    set category_type [lindex $param 2]

                    set description [cog_rest::convert_to_yaml_string -string_from "[lrange $param 3 end]"]
                    
                    set valid_category_type_p [db_string valid_type "select 1 from im_categories where category_type = :category_type limit 1" -default 0]

                    if {$valid_category_type_p} {
                        set schemas_comp($schema_ref) "    $schema_ref:\n[cog_rest::openapi::schema::$type -category_type $category_type -default $default]"
                        
                        # Set the category_type schemas
                        if {![info exists schemas_comp($category_type)]} {
                            set enum_yml [cog_rest::openapi::schema::category_enum -category_type $category_type]
                            if {$enum_yml ne ""} {
                                set schemas_comp($category_type) "    [cog_rest::helper::to_CamelCase -string $category_type]:\n$enum_yml"
                            } else {
                                set schemas_comp($category_type) "    [cog_rest::helper::to_CamelCase -string $category_type]:\n      type: integer\n"
                            }
                        }
                    }
                }
                material - material_array {
                    set material_type [lindex $param 2]

                    set description [cog_rest::convert_to_yaml_string -string_from "[lrange $param 3 end]"]
                    
                    set valid_material_type_p [db_string valid_type "select 1 from im_materials m, im_categories c where material_type_id = category_id and category = :material_type limit 1" -default 0]

                    if {$valid_material_type_p} {
                        set schemas_comp($schema_ref) "    $schema_ref:\n[cog_rest::openapi::schema::$type -material_type $material_type -default $default]"
                        
                        # Set the material_type schemas
                        if {![info exists schemas_comp($material_type)]} {
                            set enum_yml [cog_rest::openapi::schema::material_enum -material_type $material_type]
                            if {$enum_yml ne ""} {
                                set schemas_comp($material_type) "    [cog_rest::helper::to_CamelCase -string $material_type]:\n$enum_yml"
                            } else {
                                set schemas_comp($material_type) "    [cog_rest::helper::to_CamelCase -string $material_type]:\n      type: integer\n"
                            }
                        }
                    }
                }
                json_array {
                    set description [cog_rest::convert_to_yaml_string -string_from "[lrange $param 2 end]"]
                    set schema_ref [cog_rest::helper::to_CamelCase -string [lindex [split "${proc_name}_$name" "::"] end]]
                    set schemas_comp($schema_ref) "[cog_rest::openapi::schema::array_schema_yml -proc_name $proc_name -schema_ref $schema_ref -name $name]"
                }
                json_object {
                    set description [cog_rest::convert_to_yaml_string -string_from "[lrange $param 2 end]"]                    
                    set schema_ref [cog_rest::helper::to_CamelCase -string [lindex [split "${proc_name}_$name" "::"] end]]        
                    set schemas_comp($schema_ref) "    $schema_ref:\n[cog_rest::openapi::schema::json_object -proc_name $proc_name -name $name]"
                }
                pagination_object {
                    set description [cog_rest::convert_to_yaml_string -string_from "[lrange $param 2 end]"]                    
                    set schema_ref "Pagination"
                    if {![info exists schemas_comp($schema_ref)]} {
                        set schemas_comp($schema_ref) "    $schema_ref:\n[cog_rest::openapi::schema::pagination_object]"
                    }
                }
                request_body {
                    # do nothing.
                    continue
                }
                default {
                    # We default to string if undocumented
                    set description [cog_rest::convert_to_yaml_string -string_from "[lrange $param 1 end]"]
                    set type "string"
                    
                    # We might have a String with a default
                    if {[lsearch [list String Number] $schema_ref] <0} {
                        if {![info exists schemas_comp($schema_ref)]} {
                            set schemas_comp($schema_ref) [cog_rest::openapi::schema::schema_yml -type $type -default $default -schema_ref $schema_ref]
                        }
                    }
                }
            }

            append parameters_yml [cog_rest::openapi::parameter::single_path -name $name -schema_ref $schema_ref -description $description -required_p $required_p -type $type]
        }

        if {$parameters_yml ne ""} {
            return "      parameters:\n$parameters_yml"
        } else {
            return ""
        }
    }
}