ad_library {
    APM callback procedures.

    @creation-date 2021-06-21
    @author malte.sussdorff@cognovis.de
}

namespace eval cognovis-rest::apm {}

ad_proc -public cognovis-rest::apm::after_upgrade {
    {-from_version_name:required}
    {-to_version_name:required}
} {
    After upgrade callback.
} {
    apm_upgrade_logic \
        -from_version_name $from_version_name \
        -to_version_name $to_version_name \
        -spec {
            5.0.3.0.8 5.0.3.0.9 {
                # Grant read on categories for customers and freelancers
                set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_category'" -default 0]]
                permission::grant -party_id [im_profile_freelancers] -object_id $rest_otype_id -privilege "read"
                permission::grant -party_id [im_profile_customers] -object_id $rest_otype_id -privilege "read"

                set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_project'" -default 0]]
                permission::grant -party_id [im_profile_freelancers] -object_id $rest_otype_id -privilege "read"
                permission::grant -party_id [im_profile_customers] -object_id $rest_otype_id -privilege "read"
                permission::grant -party_id [im_profile_employees] -object_id $rest_otype_id -privilege "write"

                cognovis-rest::apm::import_user_portraits
            }
            5.0.3.0.9 5.0.3.1.0 {
                cognovis-rest::apm::project_permissions
            }
        }
}

ad_proc -public cognovis-rest::apm::after_install {} {
    Setup stuff from upgrades upon install
} {
    # Grant read on categories for customers and freelancers
    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_category'" -default 0]]
    permission::grant -party_id [im_profile_freelancers] -object_id $rest_otype_id -privilege "read"
    permission::grant -party_id [im_profile_customers] -object_id $rest_otype_id -privilege "read"

    set rest_otype_id [util_memoize [list db_string otype_id "select object_type_id from im_rest_object_types where object_type = 'im_project'" -default 0]]
    permission::grant -party_id [im_profile_freelancers] -object_id $rest_otype_id -privilege "read"
    permission::grant -party_id [im_profile_customers] -object_id $rest_otype_id -privilege "read"
    permission::grant -party_id [im_profile_employees] -object_id $rest_otype_id -privilege "write" 
    
    cognovis-rest::apm::import_user_portraits
    cognovis-rest::apm::project_permissions
}

ad_proc -public cognovis-rest::apm::import_user_portraits {
} {
    Import the user portraits into the content repository
} {
    set user_ids [db_list users {select distinct user_id from users u where 
            u.user_id not in (
			-- Exclude banned or deleted users
			select	m.member_id
			from	group_member_map m,
				membership_rels mr
			where	m.rel_id = mr.rel_id and
				m.group_id = acs__magic_object_id('registered_users') and
				m.container_id = m.group_id and
				mr.member_state != 'approved')}]
    
    foreach user_id $user_ids {
         # Get the current user id to not show the current user's portrait
        set base_path "/var/lib/aolserver/projop/filestorage/users/$user_id"
        set base_paths [split $base_path "/"]
        set base_paths_len [llength $base_paths]

        if { [catch {
            file mkdir $base_path
            im_exec chmod ug+w $base_path
            set file_list [im_exec find "$base_path/" -maxdepth 1 -type f]
        } err_msg] } {
            # Probably some permission errors - return empty string
            set file_list ""
        }

        set files [lsort [split $file_list "\n"]]
        set portrait_path ""
        foreach file $files {
            set file_paths [split $file "/"]
            set file_paths_len [llength $file_paths]
            set rel_path_list [lrange $file_paths $base_paths_len $file_paths_len]
            set rel_path [join $rel_path_list "/"]
            if {[regexp "^portrait\....\$" $rel_path match]} {  
                set portrait_path "${base_path}/$rel_path"               
            }
        }

        if {$portrait_path ne ""} {
            set file_revision_id [cog::file::import_fs_file -context_id $user_id -user_id $user_id -file_path $portrait_path]
            set file_item_id [content::revision::item_id -revision_id $file_revision_id]
            cog_rest::post::relationship -object_id_one $user_id -object_id_two $file_item_id -rel_type "user_portrait_rel" -rest_user_id $user_id
            ns_log Notice "Imported $portrait_path into $file_item_id"
        }
    }
}

ad_proc -public cognovis-rest::apm::project_permissions {} {
    Sets the project permissions for employees and project managers
} {
    set project_ids [db_list projects "select project_id from im_projects where project_type_id not in (100,101)"]
    foreach project_id $project_ids {
        permission::grant -party_id [im_profile_employees] -object_id $project_id -privilege "read"
        permission::grant -party_id [im_profile_project_managers] -object_id $project_id -privilege "write"
    }
}
