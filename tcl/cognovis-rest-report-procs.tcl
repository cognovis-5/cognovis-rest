ad_library {
	@author malte.sussdorff@cognovis.de
}
namespace eval cog_rest::json_object {
    ad_proc report {} {
        @return report json_object cognovis_object Report object information
        @return parent_menu named_id Parent Header for the report
        @return report_description string Description of the report
        @return report_columns json_array report_column Definition for the columns of the report
        @return report_rows json_array report_row Rows with the data of the table
    } -

    ad_proc report_column {} {
        @return column_name string Column Mame used to display in the header
        @return variable_name string Name of the variable used for the column value in the SQL statement. Identifier to match the rows.
        @return sort_order number Sort order of the column
        @return column_description string Description of the column
        @return column_status category "Intranet Report Column Status" Status of the column
        @return column_type category "Intranet Report Column Type" Type of data to be displayed in the column. Defines what to expect in the value of the cells of the row
        @return column_position string Position of the column. Useful for Pivot Grids
        @return filters_p boolean filters turned on or turned off ? (Pivot Grid)
    } -

    ad_proc report_row {} {
        @return row_id integer Identifier for the row - Primary key if we know it
        @return cells json_array id_value Array of id+values for the object. ID is the variable_name and value is the corresponding value.
    } - 

}

ad_proc -public cog_rest::get::reports {
    -rest_user_id:required
    {-report_parent_id ""}
    {-report_status_id "15000"}
    {-report_type_ids "15120"}
} {
    Returns a list of reports a user has access to without the data
    @param report_type_ids category_array "Intranet Report Type" Type of reports
    @param report_status_id category "Intranet Report Status" Status of reports
    @param report_parent_id integer report parent id

    @return reports json_array report Array of reports without the columns or row information
} {
    set report_columns ""
    set report_rows ""

    set reports [list]

     # Get only Active reports (15000 is category of 'Active' in Intranet Report Status)
    set where_clause_list [list "r.report_menu_id = m.menu_id" "m.parent_menu_id = p.menu_id"]

    if {[exists_and_not_null report_status_id]} {
        lappend where_clause_list "r.report_status_id = :report_status_id"
    }

    if {[exists_and_not_null report_type_id]} {
        lappend where_clause_list "r.report_type_id in ([template::util::tcl_to_sql_list $report_type_ids])"
    }

    if {[exists_and_not_null report_parent_id]} {
        lappend where_clause_list "m.parent_menu_id = :report_parent_id"
    }

    db_foreach report "select r.report_id, r.report_name, r.report_menu_id, m.parent_menu_id, p.name as parent_menu_name, r.report_description 
        from im_reports r, im_menus m, im_menus p
        where [join $where_clause_list " and "]" {
        set name_key [string map {" " "_" "(" "" ")" ""} $report_name]
        regsub -all {[^0-9a-zA-Z]} $name_key {_} name_key
        set report_name [lang::message::lookup "" "intranet-reporting.$name_key" $report_name]

        if {[cog_rest::permission_p -object_id $report_menu_id -user_id $rest_user_id -privilege "read"]} {
            set report [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $report_id]
            lappend reports [cog_rest::json_object]
        }    
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::report {
    {-report_id ""}
    {-report_name ""} 
    -rest_user_id:required
} {
    @param report_id object im_report Report for which we want to retrieve the data
    @param report_name string we can also query by report_name instea of id

    @return cog_report json_object report Report with data
} {

    if {$report_id eq "" && $report_name eq ""} {
        cog_rest::error -http_status 400 -message "report_id or report_name must be provided"
    }

    set where_clause_list [list]

    if {$report_name ne ""} {
        lappend where_clause_list "report_name = :report_name"
    } else {
         lappend where_clause_list "report_id = :report_id"
    }
    
    set report_exists_p [db_0or1row report_info "select r.report_id, r.report_name, r.report_menu_id, m.parent_menu_id, r.report_description, r.report_code, r.report_sql 
        from im_reports r, im_menus m where [join $where_clause_list " and "] and m.menu_id = r.report_menu_id"]
    if {!$report_exists_p} {
        cog_rest::error -http_status 400 -message "Invalid report_id"
    }

    set report_columns [list]
    set report_rows [list]

    # Check if we have columns
    set columns_exists_p [db_string columns_exists "select 1 from im_report_columns where report_id = :report_id limit 1" -default 0]

    if {!$columns_exists_p} {
        set sort_order 0
        db_with_handle db {
            if {[catch {set selection [db_exec select $db report_$report_id "$report_sql limit 1"]} err_msg]} {
                cog_log Error "Misconfigured report $report_id: $err_msg"
                cog_rest::error -http_status 500 -message "Report $report_name is misconfigured. Please fix it at [export_vars -base "[ad_url]/intranet-reporting/new" -url {report_id}]. $err_msg"
            }

            # Append the columns based of the selection key
            set variable_names [ns_set keys $selection]
            foreach variable_name $variable_names {
                set column_type_id [cog::report::column::type_from_variable -variable_name $variable_name]
                set column_status_id 15050
                set column_name [cog_rest::helper::to_CamelCase -string $variable_name]
                set column_description "Auto generated on the fly"
                set column_types($variable_name) $column_type_id
                set column_position ""
                set filters_p 0
                lappend report_columns [cog_rest::json_object -object_class report_column]
                incr sort_order
            }
        } 
        
    } else {
        set variable_names [list]
        db_foreach report_column "select variable_name, column_name, column_status_id,
        column_type_id, sort_order, column_description, position as column_position, filters_p
        from im_report_columns where report_id = :report_id order by sort_order" {
            lappend variable_names $variable_name
            set column_types($variable_name) $column_type_id
            lappend report_columns [cog_rest::json_object -object_class report_column]
        }
    }

    set row_id 0
    db_foreach report $report_sql {
        set cells [list]
        foreach variable_name $variable_names {
            set id $variable_name
            set value [set $variable_name]
            set column_type_id $column_types($id)
            switch $column_type_id {
                15160 - 15161 - 15162 - 15163 {
                    # Array
                    set values [split [lindex $value 0] ","]
                    set display_values [list]
                    foreach val $values {
                        lappend display_values [cog::report::column::display_value -value $val -column_type_id $column_type_id -user_id $rest_user_id]
                    }
                    set value "\[[join $values ","]\]"
                    set display_value "\[[join $display_values ","]\]"
                }
                default {
                    set display_value [cog::report::column::display_value -value $value -column_type_id $column_type_id -user_id $rest_user_id]
                }
            }
            lappend cells [cog_rest::json_object -object_class id_value]
        }
        lappend report_rows [cog_rest::json_object -object_class report_row]
        incr row_id
    }
    
    set report [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $report_id]

    set cog_report [cog_rest::json_object]
    return [cog_rest::json_response]
}