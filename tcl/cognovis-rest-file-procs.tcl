ad_library {
	@author malte.sussdorff@cognovis.de
}

 
namespace eval cog_rest::json_object {
    ad_proc cr_file {} {
        @return file_id integer item_id of the cr_file
        @return id integer RevisionId of the live revision we could find
        @return revision_id integer RevisionId of the live revision we could find
        @return size number Filesize of the file in bytes.
        @return sizetext string Size as a text.
        @return name string name of the file
        @return mime_type string type of the content revision
        @return type string Extension of the mime_type
        @return parent named_id Parent object for the file
        @return publish_date string Date when the revision was uploaded
        @return description string Description of file
        @return sname string RevisionID for webix
        @return status string Always server
    } -

    ad_proc cr_file_body {} {
        @param parent_id integer Object_id in which we upload the file (context) - might be a file_item_id, then we upload a new revision
        @param description string Description / comment for the file
        @param upload binary Actual file to upload
    } -

    ad_proc cr_folder {} {
        @return folder object content_item Folder name of the folder
        @return date integer UNIX Time When was the file last updated
        @return size number Filesize of the file in bytes.
        @return parent object content_item Folder name of the parent
        # @return sub_folders json_object cr_folder Folders contained in this folder
    } -
}


ad_proc -public cog_rest::get::cr_file {
    {-file_id ""}
    {-parent_id ""}
    {-file_item_name ""}
    -rest_user_id:required
} {
    Returns a list of files within a folder or a project

    @param parent_id integer object_id for which we are looking for files. might also be a folder
    @param file_id object cr_item ItemID of the file we look for
    @param file_item_name string name of the file we look for in a parent

    @return files json_array cr_file Array of files and folders within the folder.

} {
    set files [list]
    set where_clauses [list]
    lappend where_clauses "cr_revisions.revision_id = cr_items.live_revision"
    lappend where_clauses "cr_revisions.item_id = cr_items.item_id"
    lappend where_clauses "cr_revisions.revision_id = acs_objects.object_id"
    set status "server"

    if {$parent_id ne ""} {
        set folder_id [cog::file::get_folder_id -context_id $parent_id]
        lappend where_clauses "cr_items.parent_id = $folder_id"
    }
    
    if {$file_id ne ""} {
        lappend where_clauses "cr_items.item_id = :file_id"
    } else {
        if {$file_item_name ne ""} {
            lappend where_clauses "cr_items.name = :file_item_name"
        }
    }
    lappend where_clauses "cr_mime_types.mime_type = cr_revisions.mime_type"
    
    db_foreach file "
        SELECT cr_revisions.item_id AS file_id, cr_revisions.publish_date,
            cr_revisions.revision_id, cr_revisions.mime_type, cr_revisions.description,
            cr_revisions.content_length AS size, cr_items.name,
            acs_objects.last_modified, cr_items.parent_id, im_name_from_id(cr_items.parent_id) as parent_name,
            cr_mime_types.file_extension as type
        FROM cr_revisions, cr_items, acs_objects, cr_mime_types
        WHERE [join $where_clauses " and "]" {
            set sname $revision_id
            set id $revision_id
            if {$size > 1048576} {
                set sizetext "[expr $size/1024/1024] Mb"
            } elseif {$size > 1024} {
                set sizetext "[expr $size/1024] Kb"
            } else {
                set sizetext "$size B"
            }
            
        lappend files [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::cr_file {
    -parent_id
    -file_id:required
    { -revision_id "" }
    { -file_item_name ""}
    { -live_revision "" }
    -description
    -rest_user_id:required 
} {
    Updates information of a file. Can rename the file, update the parent or change the description

    @param parent_id integer Object_id in which we locate the file (context)
    @param file_id object cr_items::write FileId which we want to update.
    @param revision_id object cr_revisions::* Revision we want to ammend of the file
    @param file_item_name string Name of the file, in case we want to rename
    @param live_revision object cr_revisions::* New live revision for this file. If live_revision is 0 then unpublish the file
    @param description string Description / comment for the file
    
    @return file json_object cr_file File definition

} {
    # Check if the context is an object or a cr_item or nothing.
    if {[info exists parent_id]} {
        if {![acs_object::object_p -id $parent_id]} {
            cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_context_invalid]"
        } else {
            if {$file_item_name eq ""} {
                set file_item_name [db_string name "select name from cr_items where item_id = :file_id"]
            } else {
                set item_name_exists_p [db_string name "select 1 from cr_items where name = :file_item_name and parent_id = :parent_id limit 1" -default 0]
                if {$item_name_exists_p} {
                    cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_file_already_exists]"
                } else {
                    db_dml update_filename "update cr_items set parent_id = :parent_id, name = :file_item_name where item_id = :file_id"
                }
            }
        }
    }

    if {$revision_id eq ""} {
        set revision_id [content::item::get_best_revision -item_id $file_id]
    }

    if {[info exists description]} {
        set creation_user_id [db_string creation_user "select creation_user from acs_objects where object_id = :revision_id"]
        if {$creation_user_id eq $rest_user_id} {
            db_dml update_description "update cr_revisions set description = :description where revision_id = :revision_id"
        } else {
            cog_rest::error -http_status 403 -message "[_ cognovis-rest.err_perm_desc_up]"
            return
        }
    }

    if {$live_revision ne ""} {
        # check if the live revision is a valid one
        set valid_revision_p [db_string revision "select from cr_revisions where revision_id = :live_revision and item_id = :file_id" -default 0]
        if {$valid_revision_p} {
            db_dml update_live_revision "update cr_items set live_revision = :live_revision where item_id = :file_id"
        }
    }

    if {$file_item_name ne ""} {
        # check if we have the name already, then don't rename
        set item_name_exists_p [db_string name "select 1 from cr_items where name = :file_item_name 
            and item_id != :file_id
            and parent_id = (select parent_id from cr_items where item_id = :file_id) limit 1" -default 0]
        if {$item_name_exists_p} {
            cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_file_already_exists]"
            return
        } else {
            db_dml update_filename "update cr_items set name = :file_item_name where item_id = :file_id"
        }
    }

    db_dml update_object "update acs_objects set last_modified=now(), modifying_user = :rest_user_id where object_id = :revision_id"
    db_dml update_object "update acs_objects set last_modified=now(), modifying_user = :rest_user_id where object_id = :file_id"

    set file [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::cr_file -file_id $file_id -rest_user_id $rest_user_id]]
    
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::cr_file {
    -file_id:required
    -rest_user_id:required 
} {
    Unpublishes a file (effectively "deleting" it)

    @param file_id object cr_items::write FileId which we want to update.
    
    @return file json_object cr_file File definition in case the unpublishing was not successful

} {
    set errors [list]
    db_dml update_live_revision "update cr_items set live_revision = null where item_id = :file_id"
    set file  [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::cr_file -file_id $file_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::download_cr_file {
    { -file_id "" }
    { -revision_id "" }
    -rest_user_id:required
} {
    Serves a file from the content repository back to the user

    @param file_id integer File which we want to read
    @param revision_id integer If provided, download the specific revision for the file 

    @return The file if found

} {

    if { $revision_id eq "" && $file_id eq "" } {
        cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_file_or_revision_id]"
    }

    if { $revision_id eq "" } {
        set revision_id [content::item::get_live_revision -item_id $file_id]
    }

    if { $file_id eq "" } {
        set file_id [content::revision::item_id -revision_id $revision_id]
    }

    set allowed_p [cog_rest::helper::object_permission -user_id $rest_user_id -object_id $revision_id -privilege read]
    
    if {!$allowed_p} {
        cog_rest::error -http_status 403 -message "[_ cognovis-rest.err_perm_file]"
        return
    }

    db_1row revision_info "select mime_type, content_length, title, name from cr_revisions crr, cr_items cri where cri.item_id = crr.item_id and revision_id = :revision_id"
    set file [content::revision::get_cr_file_path -revision_id $revision_id]
    
    if {[file readable $file]} {
        if {[apm_package_installed_p views]} {
            views::record_view -object_id $file_id -viewer_id $rest_user_id
        }

        set outputheaders [ns_conn outputheaders]
        ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"$name\""
        ns_returnfile 200 $mime_type $file
        return ""
        #ad_script_abort
    } else {
        return [cog_rest::error -http_status 500 -message "[_ intranet-filestorage.lt_Did_not_find_the_spec]"]
    }
}

ad_proc -public cog_rest::get::download_fs_file {
    -object_id:required
    -file_path:required
    -rest_user_id:required
} {

    @author malte.sussdorff@cognovis.de
    @creation_date 2020-07-28

    @param object_id integer ID of the object in which path the file exists
    @param file_path string relative path (to the filestorage path of the object_id) for the file

    @return The file if found
} {
    
    # Get the list of all relevant roles and profiles for permissions
    set user_perms [cog_filestorage_file_permissions -user_id $rest_user_id -object_id $object_id -file_path $file_path]

    set read_p [lindex $user_perms 1]
    if {!$read_p} {
        return [cog_rest::error -http_status 403 -message "[_ cognovis-rest.err_perm_file_path_id]"]
    }

    # get the file path
    set path [cog_fs_base_path -object_id $object_id]

    if {$path eq ""} {
        return [cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_filestorage_unsupp]"]
    }

    set file "${path}/$file_path"
    if {[file readable $file]} {
        rp_serve_concrete_file $file
        ad_script_abort
    } else {
        return [cog_rest::error -http_status 500 -message "[_ intranet-filestorage.lt_Did_not_find_the_spec]"]
    }
}

ad_proc -public cog_rest::get::download_fs_folder {
    -object_id:required
    -folder_path:required
    -rest_user_id:required
} {

    @author malte.sussdorff@cognovis.de
    @creation_date 2020-07-28

    @param object_id integer ID of the object in which path the file exists
    @param folder_path string relative path (to the filestorage path of the object_id) for the folder

    @return Zip of the folder if found
} {
    
    # Get the list of all relevant roles and profiles for permissions
    set user_perms [cog_filestorage_file_permissions -user_id $rest_user_id -object_id $object_id -file_path $folder_path]

    set read_p [lindex $user_perms 1]
    if {!$read_p} {
        return [cog_rest::error -http_status 403 -message "[_ cognovis-rest.err_perm_folder_path_id]"]
    }

    # get the file path
    set path [cog_fs_base_path -object_id $object_id]

    if {$path eq ""} {
        return [cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_filestorage_unsupp]"]
    }

    set folder "${path}/$folder_path"
    if {[file isdirectory $folder]} {
        # Determine a random .tgz zip_file
        set root_name [file rootname [file tail $folder_path]]
        if {$root_name eq ""} {
            set root_name [im_name_from_id $object_id]
        }
        set zipfile "[im_filestorage_tmp_path]/${root_name}.zip"
    
        intranet_chilkat::create_zip -directories $folder -zipfile $zipfile
        
        # Return the file
        set outputheaders [ns_conn outputheaders]
        ns_set cput $outputheaders "Content-Disposition" "attachment; filename=\"${root_name}.zip\""
        ns_returnfile 200 application/zip $zipfile

        exec rm -rf $zipfile
        return
    } else {
        return [cog_rest::error -http_status 500 -message "[_ cognovis-rest.err_not_valid_folder]"]
    }
}

ad_proc -public cog_rest::get::cr_folders {
    {-folder_id ""}
    {-project_id ""}
    -rest_user_id:required
} {
    Returns a list of files within a folder or a project

    @param folder_id object fs_folder::read ID of the folder we looking at 
    @param project_id object im_project::* Project for which to return the tasks.

    @return folders json_array cr_folder Array of folders within the folder.

} {
    set folders [list]

    if {$folder_id eq "" && $project_id ne ""} {
        set folder_id [intranet_fs::get_project_folder_id -project_id $project_id]
    }

    set folder_ids [db_list folders "select object_id from fs_objects 
            where parent_id = :folder_id
            and type = 'folder'
    "]
    
    foreach folder_id $folder_ids {
        db_1row folder_info "select f1.object_id as folder_id, f1.parent_id, extract(epoch from f1.last_modified) as date,
            f1.content_size as size, f1.name as value, (select count(*) from fs_objects f2 where f2.parent_id = f1.object_id and f2.type = 'folder') as num_children
        from fs_objects f1
        where object_id = :folder_id"

        if {[permission::permission_p -object_id $folder_id -party_id $rest_user_id -no_cache -no_login -privilege "read"]} {
            set folder_name $value
            set parent_name [im_name_from_id $parent_id]
            set date [lindex [split $date "."] 0]

            lappend folders [cog_rest::json_object]
        }
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::upload_fs_file {
    -rest_user_id:required
} {

    @author michaldrn@wp.pl
    @creation_date 2020-08-29

    Procedure allows upoloading a file to /tmp folder.

    @return The file if found
} {

    set tmp_filename [ns_queryget upload.tmpfile]
    set filename [ns_queryget upload]

    # Copy file
    file copy $tmp_filename "${tmp_filename}.copy"
    # Set path
    set path "${tmp_filename}.copy"


    return "{ \"path\": \"$path\", \"filename\":\"$filename\"}"
}

ad_proc -public cog_rest::post::upload_cr_file {
    { -context_id "" }
    { -file_item_id ""}
    { -folder "" }
    { -uploader_name "upload" }
    { -description "" }
    -rest_user_id:required 
} {
    Uploads a file into the content repository. Assumes the uploader to be called "upload"

    @param context_id integer Object_id in which we upload the file (context) - might be a file_item_id, then we upload a new revision
    @param uploader_name string Name used for the upload. Defaults to uploader
    @param file_item_id object cr_items If we just want to upload a new revision to an existing file, provide the file_item_id. Will also try detect an existing file_item_id from filename and context_id.
    @param folder string Folder in which we want to store the file. Relative to the root folder of the context_id. If empty, attach to the context_id without folders.
    @param description string Description / comment for the file
    
    @return file json_object cr_file File definition

} {

    set tmp_filename [ns_queryget ${uploader_name}.tmpfile]
    set filename [ns_queryget $uploader_name]

    if {$file_item_id ne ""} {
        set file_revision_id [cog::file::add_version  -name $filename  -tmp_filename $tmp_filename -item_id $file_item_id  -creation_user $rest_user_id -description $description]
    } else {
        if {$context_id eq ""} {
            cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_miss_file_or_context]"
        } else {
            # Check if the context is an object or a cr_item or nothing.
            if {![acs_object::object_p -id $context_id]} {
                cog_rest::error -http_status 400 -message "[_ cognovis-rest.err_invalid_context]"
            }
            set file_revision_id [cog::file::import_fs_file -context_id $context_id -folder $folder -user_id $rest_user_id -file_path $tmp_filename -description $description -filename $filename]
            set file_item_id [content::revision::item_id -revision_id $file_revision_id]
        }
    }

    callback cog_rest::cr_file_after_upload -file_item_id $file_item_id -file_revision_id $file_revision_id -context_id $context_id -user_id $rest_user_id -context_type [acs_object_type $context_id]
    
    set file [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::cr_file -file_id $file_item_id -rest_user_id $rest_user_id]]
    
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::upload_to_folder_completed {
    -folder_id:required
    { -file_item_ids ""}
    { -description "" }
    -rest_user_id:required 
} {
    Endpoint to call when an upload to a folder has been completed

    @param folder_id object fs_folder::read ID of the folder we finished uploading to
    @param file_item_ids object_array cr_items FileIds which we succesfully uploaded
    @param description string Description / comment for the file
    
    @return files json_array cr_file Array of files and folders within the folder.

} {

    if {$file_item_ids eq ""} {
        set file_item_ids [db_list items_in_folder "select item_id from cr_items where parent_id = :folder_id"]
    }

    callback cog_rest::cr_folder_after_upload -file_item_ids $file_item_ids -folder_id $folder_id -description $description -user_id $rest_user_id

    set files [ cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::cr_file -parent_id $folder_id -rest_user_id $rest_user_id]]
    
    return [cog_rest::json_response]
}
