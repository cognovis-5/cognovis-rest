ad_library {
  OpenAPI functions
	@author malte.sussdorff@cognovis.de

}

ad_proc -public cog_rest::get::openapi {
    { -package_key ""}
    -rest_user_id:required
} {
    Display  OpenAPI Information of all documented API endpoints

    @param package_key string Limit the OpenAPI to only those endpoints as defined in this package
    @param include_default_p 1

    @return openapi OpenAPI YML string
} {

  if {$package_key eq ""} {
    set package_key "cognovis-rest"
    set endpoints [cog_rest::endpoints]
  } else {
      set endpoints [cog_rest::endpoints -package_key $package_key]  
  }

  #---------------------------------------------------------------
  # Handle the paths
  #---------------------------------------------------------------
  set paths ""

  # List of tags used
  set global_tags [list]

  # List with all component names for parameters
  set param_comps [list]
  set responses [list]

  foreach endpoint $endpoints {

    if {[info exists doc_elements]} {
      array unset doc_elements
    }


    #---------------------------------------------------------------
    # Check if the procedure exists
    #---------------------------------------------------------------
    set proc_found_p 0
    foreach operation [list get post delete put] {
      set proc_names($operation) [cog_rest::endpoint_proc -endpoint $endpoint -operation $operation]
      if {$proc_names($operation) ne ""} {
        set proc_found_p 1
      }
    }
    
    # Check if get and post are empty
    if {$proc_found_p} {
      append paths "  /${endpoint}:\n"
    } else {
      continue
    }

    foreach operation [list get post delete put] {

      #---------------------------------------------------------------
      # Get the description of the documentation
      #---------------------------------------------------------------

      set proc_name [cog_rest::endpoint_proc -endpoint $endpoint -operation $operation]

      if {$proc_name ne ""} {
        array set doc_elements [nsv_get api_proc_doc $proc_name]

        set object_name [cog_rest::openapi::response::object_name -proc_name $proc_name]

        if {$object_name eq ""} {
          set response_schema_ref ""
          set response_path [cog_rest::openapi::response::generic_path]
        } else {
            set response_name [cog_rest::openapi::response::name -proc_name $proc_name -operation $operation -endpoint $endpoint]
            set response_schema($response_name) [cog_rest::openapi::response::schema -proc_name $proc_name -object_name $object_name]
            set response_schema_ref "#/components/schemas/$response_name"
            set response_path [cog_rest::openapi::response::path -proc_name $proc_name -schema_ref $response_schema_ref]
        }

        #---------------------------------------------------------------
        # Now build the path for the operation
        #---------------------------------------------------------------

        append paths "    ${operation}:\n"

        set operationId [cog_rest::openapi::operationId -endpoint "$endpoint" -operation $operation]
        append paths "      operationId: '$operationId'\n"

        set tags [cog_rest::openapi::tags -endpoint "$endpoint" -operation $operation]
        append paths "      tags:\n      - $tags\n"

        # Find the wiki info
        set wiki_url [cog_rest::wiki_url -object_type $endpoint] 
        if {$wiki_url ne ""} {
          append paths "      externalDocs:
          description: project-open Wiki Page
          url: $wiki_url \n"
        } else {
          append paths "      externalDocs:
          description: '$proc_name definition'
          url: [im_system_url][api_proc_url $proc_name] \n"
        }

        # Append summary and description
        set summary [db_string object_type_pretty "select pretty_name from acs_object_types where object_type = :endpoint" -default ""]

        # Overwrite summary from doc_elements
        if {[info exists doc_elements(summary)]} {
          set summary [cog_rest::convert_to_yaml_string -string_from "[join $doc_elements(summary) "<br />"]"]
        }
        set description [cog_rest::convert_to_yaml_string -string_from "[lindex $doc_elements(main) 0]"]

        if {$summary ne ""} {
          append paths "      summary: $summary\n"
        }
        append paths "      description: '$description'\n"

        # Append the tags so we can add it to the global tags.
        if {[lsearch $global_tags $tags]<0} {
          lappend global_tags $tags
        }

        #---------------------------------------------------------------
        # Paramaters to attach
        #---------------------------------------------------------------
        set parameter_path ""
        set request_body_path ""        
        if {[info exists doc_elements(param)]} {
          set parameter_path "[cog_rest::openapi::parameter::path -proc_name $proc_name -schema_arr "parameters_schema"]"
          
          # Check if the procedure even has a request_body
          set request_body_param_name ""
          foreach param $doc_elements(param) {
            set type [lindex $param 1]
            if {$type eq "request_body"} {
                set request_body_param_name [lindex $param 0]
            }
          }

          if {$request_body_param_name ne ""} {
            set request_body_name "I[cog_rest::helper::to_CamelCase -string "$request_body_param_name"]"
            set request_body_comp($request_body_name) [cog_rest::openapi::request_body::schema -proc_name $proc_name -object_name $request_body_param_name -operation $operation -schema_arr "parameters_schema"]
            set request_body_path "      requestBody:
        \$ref: '#/components/requestBodies/${request_body_name}'\n"
          }
        }
        
        # parameters_schema now contains the individual component schemas for parameters

        #---------------------------------------------------------------
        # Append the return values and examples
        #---------------------------------------------------------------

        append paths $parameter_path
        append paths $request_body_path
        append paths $response_path
      }
    }
  }

  #---------------------------------------------------------------
  # Generate the components
  #---------------------------------------------------------------
  set components "components:\n \n"

  append components "  schemas:\n"

  # Initialize the main response references
  cog_rest::openapi::response::default_components -response_comp_arr "response_schema"

  set responses [lsort -unique [array names response_schema]]
  
  foreach response $responses {
    # Now for the responses
    append components "    $response:\n"
    append components "$response_schema($response)"
  }

  set parameter_refs [lsort -unique [array names parameters_schema]]
  foreach parameter_ref $parameter_refs {
    # Now for the responses
    append components "$parameters_schema($parameter_ref)"
  }

  set request_body_refs [lsort -unique [array names request_body_comp]]
  if {[llength $request_body_refs]>0} {
    append components "  requestBodies:\n"
  }
  foreach request_body_ref $request_body_refs {
    # Now for the responses
    append components "    $request_body_ref:\n"
    append components "$request_body_comp($request_body_ref)"
  }
  

  # Finally build and return the OpenAPI code
  set openapi [cog_rest::openapi::build_openapi -global_tags $global_tags -paths $paths -components $components]

  ns_return 200 "text/plain" $openapi
  return ""
}

namespace eval cog_rest::openapi {

  ad_proc -public build_openapi {
    -global_tags:required
    -paths:required
    -components:required
    {-package_key "cognovis-rest"}
  } {
    Return the OpenAPI 3 specification

    @param global_tags List of tags used
    @param paths paths_yml describing the endpoints
    @param components components_yml which describes the schemas.
    @return openapi specification
  } {
    # Set the global tags

    set global_tags [lsort $global_tags]
    if {[llength $global_tags]>0} {
      set tags_yml "tags:\n"
      foreach tag $global_tags {
        append tags_yml "  -\n    name: $tag\n"
      }
    } else {
      set tags_yml ""
    }

    #---------------------------------------------------------------
    # API generic information
    #---------------------------------------------------------------

    # Get this form the package definiton
    apm_version_get -package_key $package_key -array package
    set api_version $package(version_name)
    set api_description $package(description)
    set pretty_name $package(pretty_plural)

    return "openapi: '3.0.2'
info:
  title: '$pretty_name Endpoints'
  description: '$api_description'
  version: '$api_version'
  contact:
    name: 'Malte Sussdorff'
    email: 'malte.sussdorff@cognovis.de'
  license:
    name: 'GPLv3'
    url: 'https://www.gnu.org/licenses/gpl-3.0.en.html'
$tags_yml
paths:
$paths
$components
[cog_rest::openapi::build_security]
servers:
  - url: '[im_system_url]/cognovis-rest'
    description: '[ad_system_name]'
security:
  - app_auth: \[\]
  - ApiKeyAuth: \[\]
  - bearerAuth: \[\] 
    "
  }


  ad_proc -public operationId {
    -endpoint
    -operation
  } {
    Returns the operation_id based on an endpoint

    @param endpoint Endpoint for which to generate the operationId
    @param operation Any of get, post, delete, put
    @return operationId
  } {

    # Find out which proc was used
    set proc_name [cog_rest::endpoint_proc -endpoint $endpoint -operation $operation]

    if {$proc_name eq "cog_rest::${operation}::object_type"} {
      set endpoint "object_type_$endpoint"
    }

    # Custom implementation
    if {[string match "cog_rest::${operation}::custom*" $proc_name]} {
      set endpoint "custom_$endpoint"
    } 

    # First camel case the endpoint
    set endpoint [cog_rest::helper::to_CamelCase -string $endpoint]
    set operationId "${operation}$endpoint"
    return $operationId
  }

  ad_proc -public tags {
    -endpoint
    -operation
  } {
    Returns the tag for the endpoint

    @param endpoint Endpoint for which to generate the operationId
    @param operation Any of get, post, delete, put
    @return tag
  } {

    set proc_name [cog_rest::endpoint_proc -endpoint $endpoint -operation $operation]

    switch $proc_name {
      cog_rest::post::object_type - cog_rest::delete::object_type - cog_rest::get::object_type {
        # List of supertypes
        set supertypes [db_list supertypes "select distinct supertype from acs_object_types"]
        set supertype [db_string supertype "select supertype from acs_object_types where object_type = :endpoint" -default ""]

        switch $supertype {
          acs_object {
            if {[lsearch $supertypes $endpoint]<0} {
              set tags "acs_object"
            } else {
              set tags $endpoint
            }
          }
          default {
            # Check if this is a supertype. then leave it be
            # Ideally this handles im_project which is a supertype, but also a biz_object
            if {[lsearch $supertypes $endpoint]<0} {
              set tags $supertype
            } else {
              set tags $endpoint
            }
          }
        }
      }
      default {

        # Get the package_key and file path 
        array set doc_elements [nsv_get api_proc_doc $proc_name]

        set file_path_list [split $doc_elements(script) "/"]
        set package_key [lindex $file_path_list 1]

        # Try to set a subtag based on the procs-file
        set procs_file [lindex $file_path_list 3]
        set tags $package_key
        
        set result ""

        # Try to match. Use the second one to ignore -rest in the filename, as we don't need that for the tags
        regexp "${package_key}-(.*)-procs.tcl" $procs_file match result
        regexp "${package_key}-(.*)-rest-procs.tcl" $procs_file match result
        if {$result ne ""} {
          # Ignore the standard convention for get post delete
          if {[lsearch [list get post delete put] $result]<0} {
            set tags "${package_key}_$result"
          }
        }
      }
    }
    # Return the CamelCased tags
    return [cog_rest::helper::to_CamelCase -string $tags]
  }

  ad_proc -public convert_to_openapi_type {
    -type:required
  } {
    Converts the type into openapi type + format

    @param type Type from OpenACS/]project-open[
    @return List of OpenAPI Type + Format
  } {
    switch $type {
      integer {
        set type integer
        set format int32
      }
      bigint {
        set type integer
        set format int64
      }
      boolean - json_object {
        set type $type
        set format ""
      }
      numeric {
        set type number
        set format float
      }
      "double precision" {
        set type number
        set format double
      }
      "binary" {
        set type "string"
        set format "binary"
      }
      uuid {
        set type "string"
        set format "uuid"
      }
      number {
        # Make number default to numeric
        set type number
        set format "float"
      }
      date {
        set type "string"
        set format "date"
      }
      date-time {
        set type "string"
        set format "date-time"
      }
      "timestamp with time zone" {
        set type "string"
        set format "date-time"
      }
      category {
        set type "integer"
        set format ""
      }
      material {
        set type "integer"
        set format ""
      }
      object {
        set type "integer"
        set format ""
      }
      default {
        # No strict typing... assuming string..
        set type "string"
        set format ""
        # Find out if we have an enum based on the dynfield widget associated with the acs_attribute
      }
    }
    return [list $type $format]
  }

  ad_proc -public build_security {

  } {
    Returns the security yml for components
  } {

    set sec_components "  securitySchemes:
    app_auth:
      type: http
      scheme: basic
      description: Basic Auth for the server
      
    ApiKeyAuth:
      type: apiKey
      in: query
      name: api_key

    bearerAuth:            # arbitrary name for the security scheme
      type: http
      scheme: bearer
      bearerFormat: base64    # optional, arbitrary value for documentation purposes"

    if {0} {
      # needs testing
    # Append the unauthorized response
    append sec_components "  responses:
        UnauthorizedError:
          description: API key is missing or invalid
          headers:
            WWW_Authenticate:
              schema:
                type: string"
    }

    return $sec_components
  }


}

namespace eval cog_rest::openapi::schema {

  ad_proc -public array_schema_yml {
    -schema_ref:required
    -proc_name:required
    -name:required
  } {
    Return the schema for an array

    @param type Type of data for which to create the schema
    @param name Name of the column
    @param required_p Is the parameter required
  } {
    array set doc_elements [nsv_get api_proc_doc $proc_name]

    # We also need to check if we have a param doc for the array
    set schema_yml "    $schema_ref:\n      type: "

    if {[info exists doc_elements(param_$name)]} {
      append schema_yml "array\n      items:\n        type: object\n        properties:\n"
      foreach array_object_arg $doc_elements(param_$name) {
        append schema_yml "          [lindex $array_object_arg 0]:\n"
        set arg_type [lindex $array_object_arg 1]

        switch $arg_type {
          "category" {
            append schema_yml [cog_rest::openapi::schema::category -array -category_type "[lindex $array_object_arg 2]" -description "[lrange $array_object_arg 3 end]"]
          }
          "material" {
            append schema_yml [cog_rest::openapi::schema::material -array -material_type "[lindex $array_object_arg 2]" -description "[lrange $array_object_arg 3 end]"]
          }
          "object" {
            append schema_yml "              type: integer\n"
            append schema_yml "              description: '[lrange $array_object_arg 3 end]'\n"            
          }
          default {
            append schema_yml "              type: $arg_type\n"
            append schema_yml "              description: '[lrange $array_object_arg 2 end]'\n"
          }
        }
      }
    } else {
        append schema_yml "array\n      items:\n        type: string\n"
    }
    return $schema_yml
  }

   ad_proc -public schema_yml {
        -type:required
        -schema_ref:required
        { -default "" }
    } {
        Return the schema for a parameter

        @param type Type of data for which to create the schema
        @param default Value used as default
        @param schema_arr Array which holds the schemas to be build lrange_start
        
        @return schema_yml YAML for inclusion in the OpenAPI file
    } {
        set type_list [cog_rest::openapi::convert_to_openapi_type -type $type]
        set openapi_type [lindex $type_list 0]
        set format [lindex $type_list 1]
        # Build the schema

        set schema_yml "    $schema_ref:\n      type: $openapi_type\n"
        if {$format ne ""} {
            append schema_yml "      format: $format\n"
        }
        if {$default ne ""} {
            set default_value [string trim [lindex [split $default "::"] 0] "'"]
            switch $default_value {
                'f' {
                    set default_value "false"
                }
                't' {
                    set default_value "true"
                }
            }
            append schema_yml "      default: [cog_rest::quotejson $default_value]\n"
        }

        # Remove schemas for defaults. Just in case they were created
        if {$schema_ref eq "Boolean"} {
          return ""            
        } else {
          return $schema_yml
        }
    }

  ad_proc -public category {
    -category_type:required
    {-default ""}
    -array:boolean
  } {
    Return the schema for a category including setting the enums

    @param category_type Category Type used to define the possible enums
    @param array Is this to be included in an array, then we need to increase indention
  } {

    if {$array_p} {
      set indention "      "
    } else {
      set indention ""
    }

    append schema_yml "$indention      \$ref: '#/components/schemas/[cog_rest::helper::to_CamelCase -string $category_type]'\n"

    return $schema_yml
  }

  ad_proc -public category_enum {
     -category_type:required
     {-indention ""}
  } {
    Return the category enum schema for a category_type

    @param category_type Type of category for which to generate enums
    @param indention additional indention for the enums in case of array
  } {

    set enum_vals [list]
    set enum_descs [list]
    set enum_vars [list]
    set category_type [string trim $category_type "'"]
    set schema_yml ""

    # Check if we want to include the description
    # no need if all are empty
    set include_description_p 0
    
    db_foreach allowed_categories {select category_id, category, category_description from im_categories where category_type = :category_type and enabled_p = 't' order by category_id} {
      lappend enum_vals $category_id
      lappend enum_vars $category

      lappend enum_descs $category_description
      if {$category_description ne ""} {set include_description_p 1}
    }

    if {[llength $enum_vals]>0} {
      # append enum
      append schema_yml "$indention      type: integer\n"
      append schema_yml "$indention      enum:\n"
      foreach enum_val $enum_vals {
        append schema_yml "$indention        - $enum_val\n"
      }

      # append varname
      append schema_yml "$indention      x-enum-varnames:\n"
      foreach enum_var $enum_vars {
        append schema_yml "$indention        - [cog_rest::helper::to_ts_enum_name -string $enum_var]\n"
      }

      if {$include_description_p} {
        # append desc
        append schema_yml "$indention      x-enum-descriptions:\n"
        foreach enum_desc $enum_descs {
          append schema_yml "$indention         - [cog_rest::quotejson $enum_desc]\n"
        }
      }
    }

    return $schema_yml
  }

  ad_proc -public category_array {
    -category_type:required
    {-default ""}
  } {
    Return the schema for a category including setting the enums

    @param category_type Category Type used to define the possible enums
    @param array Is this to be included in an array, then we need to increase indention
  } {
    
    set schema_yml [cog_rest::openapi::schema::array_parameter -default $default] 
    append schema_yml "        \$ref: '#/components/schemas/[cog_rest::helper::to_CamelCase -string $category_type]'\n"
 
    return $schema_yml
  }


  ad_proc -public material {
    -material_type:required
    {-default ""}
    -array:boolean
  } {
    Return the schema for a material including setting the enums

    @param material_type material Type used to define the possible enums
    @param array Is this to be included in an array, then we need to increase indention
  } {

    if {$array_p} {
      set indention "      "
    } else {
      set indention ""
    }

    append schema_yml "$indention      \$ref: '#/components/schemas/[cog_rest::helper::to_CamelCase -string $material_type]'\n"

    return $schema_yml
  }

  ad_proc -public material_enum {
     -material_type:required
     {-indention ""}
  } {
    Return the material enum schema for a material_type

    @param material_type Type of material for which to generate enums
    @param indention additional indention for the enums in case of array
  } {

    set enum_vals [list]
    set enum_descs [list]
    set enum_vars [list]
    set material_type [string trim $material_type "'"]
    set schema_yml ""

    # Check if we want to include the description
    # no need if all are empty
    set include_description_p 0
    
    db_foreach allowed_categories {select material_id, material_name, description from im_materials where material_type_id = (select category_id from im_categories where category = :material_type) and material_status_id = 9100 order by material_id} {
      lappend enum_vals $material_id
      lappend enum_vars $material_name

      lappend enum_descs $description
      if {$description ne ""} {set include_description_p 1}
    }

    if {[llength $enum_vals]>0} {
      # append enum
      append schema_yml "$indention      type: integer\n"
      append schema_yml "$indention      enum:\n"
      foreach enum_val $enum_vals {
        append schema_yml "$indention        - $enum_val\n"
      }

      # append varname
      append schema_yml "$indention      x-enum-varnames:\n"
      foreach enum_var $enum_vars {
        append schema_yml "$indention        - [cog_rest::helper::to_ts_enum_name -string $enum_var]\n"
      }

      if {$include_description_p} {
        # append desc
        append schema_yml "$indention      x-enum-descriptions:\n"
        foreach enum_desc $enum_descs {
          append schema_yml "$indention         - [cog_rest::quotejson $enum_desc]\n"
        }
      }
    }

    return $schema_yml
  }

  ad_proc -public material_array {
    -material_type:required
    {-default ""}
  } {
    Return the schema for a material including setting the enums

    @param material_type material Type used to define the possible enums
    @param array Is this to be included in an array, then we need to increase indention
  } {
    
    set schema_yml [cog_rest::openapi::schema::array_parameter -default $default] 
    append schema_yml "        \$ref: '#/components/schemas/[cog_rest::helper::to_CamelCase -string $material_type]'\n"
 
    return $schema_yml
  }


  ad_proc -public array_parameter {
    { -default "" }
    { -items_type "integer" }
  } {
    Return the schema_yml stub for an array before looping through the individual elements

    @param description Description of the parameter
    @param default default value
  } {
    set schema_yml "      type: array\n"

    if {$default ne ""} {
      append schema_yml "      default: [cog_rest::quotejson $default]\n"
    }

    #---------------------------------------------------------------
    # The array consists of objects with id and subcategories_p
    #---------------------------------------------------------------
    append schema_yml "      items:\n"

    return $schema_yml
  }

  ad_proc -public object {
    {-default ""}
  } {
    Return the schema for an object array

  } {
    
    if {$default ne ""} {
      append schema_yml "      default: [cog_rest::quotejson $default]\n"
    }
    append schema_yml "      type: integer\n"

    return $schema_yml
  }

  ad_proc -public object_array {
    {-default ""}
  } {
    Return the schema for an object array

  } {
    
    set schema_yml [cog_rest::openapi::schema::array_parameter -default $default]
    append schema_yml "        type: integer\n"

    return $schema_yml
  }

  ad_proc -public pagination_object {
    
  } {
    Return a pagination object
  } {
    set schema_yml "      type: object
      properties:
        start:
          type: integer
          description: 'Start for the limit of the pagination'
        limit:
            type: integer
            description: 'Appends pagination information to a SQL statement depending on URL parameters'
        property:
            type: string
            description: 'Should be the column we sort by'
        direction:
            type: string
            description: 'Either ASC or DESC'
            enum:
              - ASC
              - DESC\n"
    
    return $schema_yml

  }

  ad_proc -public json_object {
    { -description "" }
    -proc_name:required
    -name:required
  } {
    Return the schema_yml for a json_object which has additional params described in the format param_\$name

    @param description Description for the json_object
    @param proc_name Name of the procedure which has the additional params making up the object
    @param name Name of the json_object
  } {
    array set doc_elements [nsv_get api_proc_doc $proc_name]
    
    if {[info exists doc_elements(param_$name)]} {
      set schema_yml "      type: object\n"
      if {$description ne ""} {
        append schema_yml "      description: $description\n"
      }
      append schema_yml "      properties:\n"

      foreach array_object_arg $doc_elements(param_$name) {
        append schema_yml "        [lindex $array_object_arg 0]:\n"
        set arg_type [lindex $array_object_arg 1]

        switch $arg_type {
          "category" {
            append schema_yml [cog_rest::openapi::schema::category -array -category_type "[lindex $array_object_arg 2]" -description "[lrange $array_object_arg 3 end]"]
          }
          "material" {
            append schema_yml [cog_rest::openapi::schema::material -array -material_type "[lindex $array_object_arg 2]" -description "[lrange $array_object_arg 3 end]"]
          }
          "object" {
            append schema_yml "            type: integer\n"
            append schema_yml "            description: '[lrange $array_object_arg 3 end]'\n"            
          }
          default {
            append schema_yml "            type: $arg_type\n"
            append schema_yml "            description: '[lrange $array_object_arg 2 end]'\n"
          }
        }
      }      
    } else {
      set schema_yml "      type: string\n"
      if {$description ne ""} {
        append schema_yml "      description: $description"
      }
    }
    return $schema_yml
  }

  ad_proc -public json_array {
    { -default ""}
    { -type ""}
  } {
    Return the schema_yml for a json_object which has additional params described in the format param_\$name

    @param description Description for the json_object
    @param proc_name Name of the procedure which has the additional params making up the object
    @param name Name of the json_object
  } {
      set schema_yml "      type: array\n"
      append schema_yml "      items:\n"
      append schema_yml "        \$ref: '#/components/schemas/I[cog_rest::helper::to_CamelCase -string $type]'\n"
    return $schema_yml
  }
}
