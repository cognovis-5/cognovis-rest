- [Overview](#overview)
- [Procedure Structure](#procedure-structure)
- [Endpoint resolution logic](#endpoint-resolution-logic)
  - [Authentication methods supported](#authentication-methods-supported)
  - [Procedure determination](#procedure-determination)
- [Parameter transformation and procedure call](#parameter-transformation-and-procedure-call)
    - [json_array](#json_array)
    - [Category Type](#category-type)
- [Endpoint return values](#endpoint-return-values)
  - [Overview](#overview-1)
  - [@return section](#return-section)
  - [Special return types](#special-return-types)
  - [Returning arrays](#returning-arrays)
  - [Handling complex return jsons](#handling-complex-return-jsons)
- [Object Type based default procedures](#object-type-based-default-procedures)
  - [Database mapping](#database-mapping)
- [OpenAPI specifics](#openapi-specifics)
  - [Type mapping](#type-mapping)


# Overview
The cognovis-rest package provides a generic REST backend enhancing the functionality available to intranet-rest to make it easy for frontend and backend developers to collaborate together. 

It follows a documentation first approach, allowing backend developers to quickly hash out code that provides the endpoint and generates an OpenAPI documentation, so the frontend can have a "Mock" server to develop against. 

Furthermore it adds stricter type support for parameters which goes beyond the "normal" type support in JSON. For some types it also does type conversion, e.g. type category, which assumes an array of category_ids and turns that into a TCL list, checking for validity of values

# Procedure Structure

As cognovis-rest emphasises a documentation first approach, the documentation is driving a lot of the automated JSON building within cognovis-rest. A typical ad_proc generated procedure is made up of:

1. **proc_name**
The proc_name needs to be in the namespace of cognovis-rest to be registered correctly as an endpoint. This means it should start with cog_rest::${http_method}:: (e.g. cog_rest::get::trans_project_list). See Endpoint resolution logic below.

2. **parameters (switches)**
Parameters are what is made available to the outside of the endpoint. These correspond to the @params section in the documentation block and can be ":required" or have a default. cog_rest::call will parse the available parameters both in the procedures parameters block as well as the @param section. Based on the information provided in *both* it will call the procedure with the values passed in the JSON. In case a required parameter is missing, an error is thrown without even calling the procedure.  Examples:
    * Optional parameter without default: -project_id
    * Required parameter without default: -project_id:required
    * Optional parameter with default if not provided: { -project_status_ids "78" }


3. **documentation block**
The documentation block contains the "meat" of the logic for cognovis-rest. It consists of multiple blocks (explained below), starting with the main block, which is used as the OpenAPI description of the endpoint. You can provide optional @author to indicate who wrote the procedure and @creation_date to indicate the date it was created/last updated. Following it comes a section for the parameters, starting with @param. If you define a param with @param you need to add it as a switch in the parameters section otherwise it won't be available. This is followed by @return, describing what the endpoint returns when called.

4. **execution block**
This is the actual code which is being executed to return the JSON based on the provided parameters in the REST call. Typical scenarios include:
    * Return information about an object - e.g. information about an invoice
    * Return an array of JSON objects - e.g. array of invoices
    * Return an array of JSON Objects with arrays - e.g. array of invoices and the invoice_items (aka invoice lines)
    * Create/Update an object - This highly encourages you to use POST http_method and will take a single valid JSON object and create/update the data in the backend
    * Execute an action - E.g. change a status, move a workflow along. These are typically also get values.

# Endpoint resolution logic
Accessing /cognovis-rest/<endpoint> will result in cog_rest::call doing the following:

1. Check that the user is authenticated 
2. Resolve the endpoint into a procedure to call
3. Parse the documentation and switches section to determine if the values provided in the REST call are sufficient (and correct) to execute the procedure. This is where type checking takes place as well as transformation of arrays etc.
4. Execute/call the procedure
5. Auto Build valid JSON objects and arrays based on the documention (@return section)
6. Handle CORS and return a valid JSON (or an appropriate HTTP Error code along with the error message)

## Authentication methods supported

You can use the following methods of authentication:
1. Basic HTTP - This passes through username and password in the header
2. Token Based - This passes through the user_id and a auto generated token in the URL
3. API Key - This passes an API Key which is base64 encoded that contains a user_id and password to authenticate against.
4. Cookie Based - This looks for a cookie in the browser from the server to authenticate. Not really encouraged.

## Procedure determination
To determine the procedure for the endpoint called, cog_rest::endpoint_proc is executed which returns the procedures based on the first one found in the following order:
1. **cog_rest::\${http_method}::custom::${endpoint}**
    > This is used if you want to overwrite an existing endpoint. E.g. if you want to have your own implementation for a customer for cog_rest::get::i18n you would write it in the intranet-cust-{customer} package and call the procedure cog_rest::get::custom::i18n 

2. **cog_rest::\${http_method}::${endpoint}**
    > This is the most common way to define your endpoints. E.g. cog_rest::get::trans_projects
    
    > It does have support for subtypes, so you call /cognovis-rest/trans_projects/tasks it will try to call cog_rest::get::trans_projects::tasks

3. **im\_rest\_\${http_method}\_custom\_${endpoint}**
    > This one is provided for backwards compatability with 4.x procedures written for intranet-rest.

    > **Deprecated!**

4. **cog_rest::\${http_method}::object_type**
   > This is the catchall endpoint to directly execute get and post commands on ACS Object types. It does support extension tables supporting any parameter defined as an acs attribute for the object_type. It is also the typical way for ]project-open[ to handle calls to the backend.

   > This is heavily based on im_rest_get_object_type, adds strict type support though.

# Parameter transformation and procedure call

The param section is build using the following schema

> @param {switch_name} {param_type} Description

The switch name must correspond to a switch the procedure supports. Switch rest_user_id (should be used always) as well as rest_oid are special switches which don't need a param documentation, as they refer to the rest_user_id (which comes out of authentication) or the object_id, which comes out of the Endpoint call.

Before calling the procedure with the switches, each switch is tested against the param_type and validated that the rest call provides a valid valid of the param_type. 

Currently supported param_types are:
* json_array
* category
* string - This can be anything, no special check done
* integer - TCL check that this is an integer
* boolean - Check that the value provided is a boolean. If true, then call the switch with a value of "1", otherwise "0" (so we do a transformation from false/true to 0/1 as an example)
* number - TCL check that this is a number


### json_array

### Category Type



# Endpoint return values

## Overview

## @return section

## Special return types

* named_id
* category
* boolean
* integer - number
* string

## Returning arrays
* return_$array logic


## Handling complex return jsons

# Object Type based default procedures
In case no special procedure exists for handling the endpoint, a generic approach based on the object_type is supported.

## Database mapping

When returning values from the database, cog_rest::json_objects will look at the database type of each invididual column which we are returning and maps them to JSON object types as well as OpenAPI definitions

* int4 - int2 - float - number - float8 - numeric - Returns a non quoted string.
* bpchar - boolean - bool: Returns a boolean true/false
* default: Returns a json encoded and quoted string

# OpenAPI specifics

OpenAPI gets the version number of the endpoints based on the package_key where the procedure is defined. So if you defined an endpoint in intranet-hr package and intranet-hr has the version number 5.0.3.0.0, so will all of your endpoints defined for this package.

## Type mapping

For the types we support type mappings including the format to provide more details in the documentation than is available in JSON. Below it is provided in the form where format is actually optional.

| column_format | type | format 
| :-----------  | :-------: | -------------: 
| integer| integer | int32 
| bigint | integer | int64 
| boolean | boolean | 
| json_object | json_object | 
| numeric | number | float 
| "double precision" | number | double
| number | number | 
| date | string | date 
| "timestamp with time zone" | string | date-time 

